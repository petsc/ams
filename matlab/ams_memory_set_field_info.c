/* $Revision: 1.1 $ */
/*
 *
 * ams_memory_set_field_info(int memory,char *name,void **data)
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_set_field_info(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char                 name[256];
  int                  ierr,memory,nlist,len,i,dim,*start,*end;
  AMS_Data_type        dtype;
  AMS_Memory_type      mtype;
  AMS_Shared_type      stype;
  AMS_Reduction_type   rtype;
  double               *values;
  void                 *addr,*vaddr;

  memory = *mxGetPr(prhs[0]);
  mxGetString(prhs[1],name,256);

  if (nlhs > 0) {
    plhs[0] = mxCreateDoubleMatrix(1,1,0);
    *mxGetPr(plhs[0]) = 0;
  }
    
  /*
        Get information about data type and size of remote data field
  */
  ierr = AMS_Memory_get_field_info(memory,name,&addr,&len,&dtype,&mtype,&stype,&rtype);
  if (ierr) {
    if (nlhs > 0) {
      *mxGetPr(plhs[0]) = ierr;
    } else {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
    return;
  }
  ierr = AMS_Memory_get_field_block(memory,name,&dim,&start,&end);
  if (ierr) {
    if (nlhs > 1) {
      *mxGetPr(plhs[0]) = ierr;
    } else {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
    return;
  }
  /* 
         Access the underling Matlab data that will be passed to AMS 
  */
  if (dtype == AMS_DOUBLE) {
    vaddr = (void *) mxGetPr(prhs[2]);
  } else if (dtype == AMS_FLOAT) {
    float  *values;
    double *invalues = (double *) mxGetPr(prhs[2]);

    values = (float *) malloc(len*sizeof(float));
    for ( i=0; i<len; i++ ) {
      values[i] = (float) invalues[i];
    }
    vaddr = (void *) values;
  } else if (dtype == AMS_INT) {
    int    *values;
    double *invalues = (double *) mxGetPr(prhs[2]);

    values = (int *) malloc(len*sizeof(int));
    for ( i=0; i<len; i++ ) {
      values[i] = (int) invalues[i];
    }
    vaddr = (void *) values;
  } else {
    if (nlhs > 0) {
      *mxGetPr(plhs[0]) = 2;
    } else {
      mexErrMsgTxt("ams_memory_set_field_info() cannot handle datetype.");
    }
  }

  /*  -------------------------------------------------------------------------------------- */

  ierr = AMS_Memory_set_field_info(memory,name,vaddr,len);
  if (ierr) {
    if (nlhs > 0) {
      *mxGetPr(plhs[0]) = ierr;
    } else {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
    return;
  }


  return;
}


