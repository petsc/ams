function [changed,step,ierr] = ams_memory_update_recv_end(memory)
%
%   Updates memory variables from the server
%
%   [changed,step,ierr] = ams_memory_update_recv_end(memory)
%
%   Input Parameter:
%     memory - ams memory object, obtained with ams_memory_attach()
%
% Notes:    
%    This should usually be paired as
%      ams_memory_update_recv_begin(memory) 
%      [changed,step] = ams_memory_update_recv_end(memory)
%      data = ams_memory_get_field_info(memory,name)
%
if (nargin ~= 1) 
  ['ams_memory_update_recv_end: Requires one input argument']
  return 
end

if (nargout > 3) 
  ['ams_memory_update_recv_end: Allows at most 3 output arguments']
  return 
end

%==============================================================================

if (nargout == 0) 
  ams(11,memory);
elseif (nargout == 1) 
  changed = ams(11,memory);
elseif (nargout == 2) 
  [changed,step] = ams(11,memory);
else
  [changed,step,ierr] = ams(11,memory);
end