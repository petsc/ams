/* $Revision: 1.1 $ */
/*
 *
 * char *commname = ams_connect(char *machine,int port) 
 *
 */

#include "mex.h"
#include "ams.h"

void mxams_connect(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char host[256],**comms,**tcomms;
  int  port,ierr,ncomm;
  
  port = *mxGetPr(prhs[1]);
  mxGetString(prhs[0],host,256);

  ierr = AMS_Connect(host,port,&comms);
  if (ierr) {
    plhs[0] = mxCreateCharMatrixFromStrings(0,(const char **)0);
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[1]) = ierr;
    } else {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
  } else {

    /* count number of strings */
    tcomms = comms;
    ncomm  = 0;
    while (*tcomms++) ncomm++;
    plhs[0] = mxCreateCharMatrixFromStrings(ncomm,(const char **)comms);
    
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[1]) = 0;
    }
  }

  return;
}


