function ierr = ams_memory_lock(memory, timeout)
%
%   Stops application when it next tries to access the memory
%
%   [ierr] = ams_memory_lock(memory, timeout)
%
%  Input Parameter:
%    memory - ams memory object obtained with ams_memory_attach()
%                 of ams_memory_class object
%    timeout - maximum time in MILLISECONDS to stop for before proceeding. Use 0, to stop
%                 until next call to ams_memory_unlock()
%
%seealso: ams_memory_attach(), ams_memory_unlock()
%
if (nargin < 1 ) 
  ['ams_memory_lock: Requires 1 (memory), or 2 (timeout) arguments']
end

if (nargin > 2 ) 
  ['ams_memory_lock: Requires at most 2 input arguments, a memory and timeout ']
end

if (nargout > 1)
  ['ams_memory_lock: Requires at most 1 output argument']
end

%==========================================================
if (isobject(memory))
  memory = struct(memory);
  memory = memory.memory;
end

if (nargin == 1)
    if (nargout == 0)
        ams(14,memory,0);
    else
        ierr = ams(14,memory,0);
    end
else
    if (nargout == 0)
        ams(14,memory,timeout);
    else
        ierr = ams(14,memory,timeout);
    end
end
