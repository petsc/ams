function [list,ierr] = ams_memory_get_field_list(memory)
%
%   Gets a list of memories from a given memory
%
%  [list,ierr] = ams_memory_get_field_list(memory)
%
%  Input Parameter:
%    memory - ams memory object obtained with ams_memory_attach()
%
%  Output Parameter:
%    list - string containing all fields assocated with memory
%
if (nargin ~= 1) 
  ['ams_memory_get_field_list: Requires one input argument']
  return 
end

if (nargout > 2 | nargout < 1) 
  ['ams_memory_get_field_list: Requires one or two output arguments']
  return
end

%=================================================================

if (nargout == 1) 
  list = ams(7,memory);
else
  [list,ierr] = ams(7,memory);
end