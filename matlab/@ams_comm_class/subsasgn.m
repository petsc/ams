function r = subsasgn(comm,s,b)

[n,m] = size(s);
if (m == 1)
  [' Cannot assign to a communictor class object']
  return;
else
  memoryname = s(1,1).subs;
  fieldname  = s(1,2).subs;
  l = ams_memory_class(comm,memoryname);
  eval(['l.' fieldname ' = b;' ]);
end
r = comm;