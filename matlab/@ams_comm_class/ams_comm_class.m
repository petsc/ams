function [c,ierr] = ams_comm_class(comm)
%
%  [c,ierr] = ams_memory_class(comm)
%
%   Input parameters: 
%     comm is a character string or
%     null to use the default
%
%   Creates a comm which is a mirror of the memory 
%  on the server side of the AMS
%
if (nargin == 0)
  comm = ams_connect;
end

c.comm = ams_comm_attach(comm(1,:));
c = class(c,'ams_comm_class');
 