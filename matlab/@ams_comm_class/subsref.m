function r = subsref(comm,s)

[n,m] = size(s);
if (m == 1)
  r = ams_memory_class(comm.comm,s.subs);
else
memoryname = s(1,1).subs;
fieldname  = s(1,2).subs;
 r = ams_memory_class(comm,memoryname);
 r = eval(['r.' fieldname]);
end