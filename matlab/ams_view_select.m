function [result,step,ierr] = ams_view_select(host,port)
%
%  [result,step,ierr] = ams_view_select(host,port)
%
%   Allows one to select a field value from a publisher
%
%  Input Parameters:
%   host - name of publisher machine
%   port - port number on publisher machine (optional)
%
%  Output Parameter:
%   result - Matlab variable containing field value select from GUI
%   step - the number of times the variable has been locked in the remote system'
%
%seealso: ams_view(), ams_get_variable(), ams_connect()
%
%  Notes: this Matlab routine does not have a corresponding routine 
%         in the underlying AMS API.
%
if (nargout == 3) 
  ierr = 0;
end

if (nargin == 0) 
  [host,err] = ams_get_servername;
  if (err ~= 0)
    if (nargout < 3) 
      result = [];
      if (nargout == 2) 
        step = [];
      end
      ['ams_view_select: Unable to determine appropriate default server name']
    else
      ierr   = err;
      result = [];
      step   = [];
    end
    return
  end
end
if (nargin < 2) 
  port = -1;
end

[result,err]  = ams_view(host,port);
if (err ~= 0)
  if (nargout < 3) 
    result = [];
    if (nargout == 2) 
      step = [];
    end
    ['ams_view_select: Unable to determine appropriate default server name']
  else
    result = [];
    step   = [];
    ierr   = err;
  end
  return
end

[n,m]         = size(result);

cellresult = cellstr(result);
[s,v]      = listdlg('ListString',cellresult,'ListSize',[8*m 12*n + 20],'SelectionMode','single');
if (v == 1)
  result = result(s,:);
  if (nargout == 3) 
    [result,step,ierr]  = ams_get_variable(result);
  elseif (nargout == 2) 
    [result,step]       = ams_get_variable(result);
  else
    result              = ams_get_variable(result);
  end
else
  result = [];
end