/* $Revision: 1.1 $ */
/*
 *
 * int *sizes = ams_memory_get_field_block(int memory,char *name)
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_get_field_block(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char                 name[256];
  int                  ierr,memory,nlist,len,i;
  AMS_Data_type        dtype;
  AMS_Memory_type      mtype;
  AMS_Shared_type      stype;
  AMS_Reduction_type   rtype;
  int                  *start,*end,dim;
  double               *values;

  memory = *mxGetPr(prhs[0]);
  mxGetString(prhs[1],name,256);

  ierr = AMS_Memory_get_field_block(memory,name,&dim,&start,&end);
  if (nlhs > 1) {
    plhs[1]  = mxCreateDoubleMatrix(1,1,0);
    *mxGetPr(plhs[1]) = ierr;
  }

  if (ierr) {
    if (nlhs == 1) {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
    plhs[0]  = mxCreateDoubleMatrix(0,0,0);
    return;
  }

  for ( i=0; i<dim; i++ ) {
    if (start[i] != 0) {
      plhs[0]  = mxCreateDoubleMatrix(0,0,0);
      if (nlhs == 1) {
        mexErrMsgTxt("ams_memory_get_field_block: start[] info incorrect, not 0");
      } else {
        *mxGetPr(plhs[1]) = 5;
      }
      return;
    }
  }

  plhs[0]  = mxCreateDoubleMatrix(dim,1,0);
  values   = (double *) mxGetPr(plhs[0]);
  for ( i=0; i<dim; i++ ) {
    values[i] = end[i] + 1;
  }

  return;
}


