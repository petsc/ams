/* $Revision: 1.1 $ */
/*
 *
 * int comm = AMS_comm_attach(char *name) 
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_comm_attach(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char       name[256],*lname;
  int        ierr,ncomm;
  AMS_Comm comm;


  mxGetString(prhs[0],name,256);

  /* name may be of the form "commname|host|port", strips off host and port */
  lname = strtok(name,"|");

  ierr = AMS_Comm_attach(lname,&comm);
  if (nlhs > 1) {
    plhs[1] = mxCreateDoubleMatrix(1,1,0);
    *(mxGetPr(plhs[1])) = (double) ierr;
  } else if (ierr) {
    char *err;
    AMS_Explain_error(ierr,&err);
    mexErrMsgTxt(err);
  }
  plhs[0] = mxCreateDoubleMatrix(1,1,0);
  *(mxGetPr(plhs[0])) = (double) comm;

  return;
}


