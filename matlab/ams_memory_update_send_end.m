function [ierr] = ams_memory_update_send_end(memory)
%
%   Updates memory variables to the server
%
%   [ierr] = ams_memory_update_send_end(memory)
%
%   Input Parameter:
%     memory - ams memory object, obtained with ams_memory_attach()
%
% Notes:    
%    This should usually be paired as
%      ams_memory_set_field_info(memory,name,data)
%      ams_memory_update_send_begin(memory)
%      ams_memory_update_send_end(memory) 
%
if (nargin ~= 1) 
  ['ams_memory_update_send_end: Requires one input argument']
  return 
end

if (nargout > 1) 
  ['ams_memory_update_send_end: Allows at most 1 output argument']
  return 
end

%==============================================================================
%
%  Currently the AMS implements everything in the send begin portion
%
ierr = 0;
return 

if (nargout == 0) 
  ams(18,memory);
else
  ierr = ams(18,memory);
end