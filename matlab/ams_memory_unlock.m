function ierr = ams_memory_unlock(memory)
%
%   Restarts application that has been locked at memory
%
%   [ierr] = ams_memory_unlock(memory)
%
%  Input Parameter:
%    memory - ams memory object obtained with ams_memory_attach()
%              of ams_memory_class object
%
%seealso: ams_memory_attach(), ams_memory_lock()
%
if (nargin ~= 1) 
  ['ams_memory_unlock: Requires 1 memory argument']
end

if (nargout > 1)
  ['ams_memory_unlock: Requires at most 1 output argument']
end

%==========================================================
if (isobject(memory))
  memory = struct(memory);
  memory = memory.memory;
end

if (nargout == 0)
  ams(15,memory);
else
  ierr = ams(15,memory);
end
