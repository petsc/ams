/* $Revision: 1.1 $ */
/*
 *
 * int memory = ams_memory_attach(int comm, char *name) 
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_attach(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char         name[2048],*lname;
  int          ierr,ncomm,one = 1;
  unsigned int *step = NULL;
  AMS_Comm     comm;
  AMS_Memory   *memory = NULL;
  int          nbmem, i = 0;

  comm = *(mxGetPr(prhs[0]));
  mxGetString(prhs[1],name,2048);

  nbmem = 1;
  while(name[i])
    if (name[i++] == '|')
      nbmem++;

  /* Allocate Memory */
  memory = (AMS_Memory *) malloc (nbmem * sizeof (AMS_Memory));
  if (memory == NULL) {
    if (nlhs < 3) {
      mexErrMsgTxt("ams_memory_attach() could not allocate memory .");
    } else {
      plhs[2] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[2]) = 13;
    }
    plhs[0] = mxCreateDoubleMatrix(0,0,0);
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(0,0,0);
    }
    return;
  }

  step = (unsigned int *) malloc (nbmem * sizeof(unsigned int));
  if (step == NULL){
    if (nlhs < 3) {
      mexErrMsgTxt("ams_memory_attach() could not allocate memory .");
    } else {
      plhs[2] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[2]) = 14;
    }
    plhs[0] = mxCreateDoubleMatrix(0,0,0);
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(0,0,0);
    }
    return;
  }

  ierr = AMS_Memory_attach(comm,name,memory,step);

  if (ierr) {
    if (nlhs < 3) {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    } else {
      plhs[2] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[2]) = ierr;
    }
    plhs[0] = mxCreateDoubleMatrix(0,0,0);
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(0,0,0);
    }
  } else {
    plhs[0] = mxCreateDoubleMatrix(nbmem,1,0);
    for (i=0; i<nbmem; i++)
      (mxGetPr(plhs[0]))[i] = (double) memory[i];

    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(nbmem,1,0);
      for (i=0; i<nbmem; i++)
        (mxGetPr(plhs[1]))[i] = (double) step[i];
    }
    if (nlhs > 2) {
      plhs[2] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[2]) = ierr;
    }
  }

  if (memory)
    free(memory);
  if (step)
    free(step);

  return;
}


