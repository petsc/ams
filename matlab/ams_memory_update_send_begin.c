/* $Revision: 1.1 $ */
/*
 *
 * ams_memory_update_send_begin(int memory)
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_update_send_begin(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  int          ierr,memory,flag;
  unsigned int	step;

  memory = *(mxGetPr(prhs[0]));

  ierr = AMS_Memory_update_send_begin(memory);
  if (nlhs > 0) {
    plhs[0] = mxCreateDoubleMatrix(1,1,0);
    *(mxGetPr(plhs[0])) = (double) ierr;
  } else if (ierr) {
    char *err;
    AMS_Explain_error(ierr,&err);
    mexErrMsgTxt(err);
  }

  return;
}


