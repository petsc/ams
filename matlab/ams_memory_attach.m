function [memory,step,ierr] = ams_memory_attach(comm,name)
%
%   Gets a memory object from a given communicator
%
%   [memory,step,ierr] = ams_memory_attach(comm,name)
%
%   Input Parameters:
%    comm - ams communicator obtained with ams_comm_attach()
%    name - name of a memory obtained with ams_comm_get_memory_list()
%
%   Output Parameter:
%    memory - ams memory object
%
%
%
if (nargin ~= 2) 
  ['ams_memory_attach: Requires 2 input arguments']
  return;
end

if ~ischar(name)
  ['ams_memory_attach: Second input argument must be character']
  return;
end

if (nargout == 0 | nargout > 3) 
  ['ams_memory_attach: Requires 1-3 output arguments']
  return;
end

%==================================================================

if (nargout == 1) 
  memory = ams(5,comm,name);
elseif (nargout == 2)
  [memory,step] =  ams(5,comm,name);
else 
  [memory,step,ierr] =  ams(5,comm,name);
end
