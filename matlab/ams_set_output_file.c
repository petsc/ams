/* $Revision: 1.1 $ */
/*
 *
 * int ierr = ams_set_output_file(char *filename)
 *
 */

#include "mex.h"
#include "ams.h"

void mxams_set_output_file(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char filename[256];
  int  ierr;
  
  mxGetString(prhs[0],filename,256);

  ierr = AMS_Set_output_file(filename);
  if (nlhs > 0) {
    plhs[0] = mxCreateDoubleMatrix(1,1,0);
    *mxGetPr(plhs[0]) = ierr;
  } else if (ierr) {
    char *err;
    AMS_Explain_error(ierr,&err);
    mexErrMsgTxt(err);
  }

  return;
}


