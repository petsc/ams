function [commnames,ierr] = ams_connect(host,port)
%
%   Gets a list of communicators from a given process
%
%    commnames = ams_connect(host,port)
%
%    Input Parameters:
%      host - name of host where server is located (optional)
%             AMS_SERVER or HOSTNAME environmental variable used
%      port - port number to use (optional)
%             AMS_PORT environmental variable used
%
%    Output Parameter:
%      commnames - list of names of communicators associated with host
%
%seealso: ams_comm_attach()
%
if (nargout == 0 | nargout > 2) 
  ['ams_connect: Requires one or two output arguments']
  return
end

% ======================================================

% determine default servername
if (nargin == 0) 
  [host,err] = ams_get_servername;

  % if could not determine server name then generate error
  if (err ~= 0)
    commnames = [];
    if (nargout == 1) 
      ['ams_connect: Unable to determine appropriate default server name']
    else
      ierr = err;
    end
    return
  end
elseif ~ischar(host)
  ['ams_connect: First input argument must be charactor string hostname']
  return
end

%determine default port
if (nargin < 2) 
  port = -1; 
end

if (nargout == 1) 
  commnames = ams(1,host,port);
else
  [commnames,ierr] = ams(1,host,port);
end
