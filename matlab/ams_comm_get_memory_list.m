function [list,ierr] = ams_comm_get_memory_list(comm)
%
%   Gets a list of memories from a given communicator
%
%   [list,ierr] = ams_comm_get_memory_list(comm)
%
%    Input Parameter:
%     comm - ams communicator obtained with ams_comm_attach()
%
%    Output Parameter:
%     list - list of memories associated with the communicator
%
%seealso: ams_comm_connect(), ams_comm_detach(), ams_memory_get_field_list()
%
if (nargout < 1 | nargout > 2)
  ['ams_comm_get_memory_list: Requires 1 or 2 output variables']
  return 
end

if (nargin ~= 1) 
  ['ams_comm_get_memory_list: Requires 1 input variables']
  return 
end
   
if (nargout == 1)
  list = ams(4,comm);
else
  [list,ierr] = ams(4,comm);
end
