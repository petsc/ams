/* $Revision: 1.1 $ */
/*
 *
 *  ams_memory_unlock(int memory) 
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_unlock(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  int          ierr;
  AMS_Memory memory;

  memory = *(mxGetPr(prhs[0]));

  ierr = AMS_Memory_unlock(memory);
  if (nlhs > 0) {
      plhs[0] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[0]) = ierr;
  } else if (ierr) {
    char *err;
    AMS_Explain_error(ierr,&err);
    mexErrMsgTxt(err);
  }

  return;
}


