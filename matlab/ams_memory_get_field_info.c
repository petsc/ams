/* $Revision: 1.1 $ */
/*
 *
 * char **list = ams_memory_get_field_info(int memory,char *name)
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_get_field_info(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char                 name[256];
  int                  ierr,memory,nlist,len,i,dim,*start,*end;
  AMS_Data_type        dtype;
  AMS_Memory_type      mtype;
  AMS_Shared_type      stype;
  AMS_Reduction_type   rtype;
  double               *values;
  void                 *addr;

  memory = *mxGetPr(prhs[0]);
  mxGetString(prhs[1],name,256);

  if (nlhs > 1) {
    plhs[1] = mxCreateDoubleMatrix(1,1,0);
    *mxGetPr(plhs[1]) = 0;
  }
  
  ierr = AMS_Memory_get_field_info(memory,name,&addr,&len,&dtype,&mtype,&stype,&rtype);
  if (ierr) {
    if (nlhs > 1) {
      *mxGetPr(plhs[1]) = ierr;
    } else {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
    return;
  }
  ierr = AMS_Memory_get_field_block(memory,name,&dim,&start,&end);
  if (ierr) {
    if (nlhs > 1) {
      *mxGetPr(plhs[1]) = ierr;
    } else {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
    return;
  }
  if (dtype == AMS_DOUBLE || dtype == AMS_FLOAT || dtype == AMS_INT) {
    int dims[16];
    for ( i=0; i<dim; i++ ) {
      if (start[0] != 0) {
        if (nlhs > 1) {
          *mxGetPr(plhs[1]) = 1;
        } else {
          mexErrMsgTxt("ams_memory_get_field_block() block info start not 0.");
        }
        return;
      }
      dims[i] = end[i]+1;
    }
    if (dim == 1) {
      plhs[0] = mxCreateDoubleMatrix(len,1,0);
    } else if (dim == 2) {
      plhs[0] = mxCreateDoubleMatrix(dims[0],dims[1],0);
    } else {
      plhs[0] = mxCreateNumericArray(dim,dims,mxDOUBLE_CLASS,mxREAL);
    }
  }

  if (dtype == AMS_DOUBLE) {
    memcpy(mxGetPr(plhs[0]),addr,len*sizeof(double));
  } else if (dtype == AMS_FLOAT) {
    float *invalues;
    values   = (double *) mxGetPr(plhs[0]);
    invalues = (float *) addr;
    for ( i=0; i<len; i++ ) {
      values[i] = (double) invalues[i];
    }
  } else if (dtype == AMS_INT) {
    int *invalues;
/*    plhs[0]  = mxCreateDoubleMatrix(len,1,0); */
    values   = (double *) mxGetPr(plhs[0]);
    invalues = (int *) addr;
    for ( i=0; i<len; i++ ) {
      values[i] = (double) invalues[i];
    }
  } else if (dtype == AMS_STRING) {
    char **jaddr = (char **) addr;
    int  jlen = 0,i;
    for ( i=0; i<len; i++ ) {
      if (!jaddr[i]) break;
      jlen++;
    }
    plhs[0] = mxCreateCharMatrixFromStrings(jlen,(const char **) addr);
  } else {
    if (nlhs > 1) {
      *mxGetPr(plhs[1]) = 2;
    } else {
      mexErrMsgTxt("ams_memory_get_field_block() cannot handle datetype.");
    }
  }


  return;
}


