/* $Revision: 1.1 $ */
/*
 *
 * ams_comm_detach(int comm) 
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_comm_detach(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  int        ierr;
  AMS_Comm comm;

  comm = *(mxGetPr(prhs[0])); 

  ierr = AMS_Comm_detach(comm);
  if (nlhs > 0) {
    plhs[0] = mxCreateDoubleMatrix(1,1,0);
    *(mxGetPr(plhs[0])) = (double) ierr;
  } else if (ierr) {
    char *err;
    AMS_Explain_error(ierr,&err);
    mexErrMsgTxt(err);
  }

  return;
}


