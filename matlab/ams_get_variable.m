function [result,changed,step,ierr] = ams_get_variable(comm,memory,field,seekchange)
%
%      Gets a specific field variable
%
%   [result,step,ierr]         = ams_get_variable(comm_memory_field) or
%                                                 char 
%   [result,step,ierr]         = ams_get_variable(comm_memory,field)
%                                                 char        char
%   [result,step,ierr]         = ams_get_variable(comm,memory,field)
%                                                 char char   char
%   [result,step,ierr]         = ams_get_variable(comm,memory,field)
%                                                 int  char   char
%
%   [result,ierr]              = ams_get_variable(comm,memory,field)
%                                                 int  int    char
%   [result,changed,step,ierr] = ams_get_variable(comm,memory,field,'changed')
%                                                 int  int    char
%
%   Input Parameters: 
%     comm - string name of communicator - if listed as char
%            AMS integer ..              - if listed as int
%     memory - string name of memory     - if listed as char
%              AMS integer ..            - if listed as int
%     field - string name of field        
%     comm_memory_field - string of the form "comm memory field"
%     comm_memory - string of the form "comm memory"
%
%   Output Parameter:
%     result - Matlab variable containing the result
%     changed - flag indicating if value changed from previous call
%     step - number of times variable has been locked remotely
%
%seealso: ams_memory_get_field_info(), ams_view_select()
%
%  Notes: 
%     If comm and memory are AMS integers obtained via
%     ams_comm_attach() and ams_memory_attach() then this
%     call is much fast than if they are the string names
%
%     When using string arguments it always gets new values from server.
%     With AMS integer arguments it only gets new values if you request 'changed'
%     otherwise it uses the values already loaded into the client.
%
%     This Matlab routine does not have a corresponding routine 
%         in the underlying AMS API.
%
if (nargin > 4 | nargin == 0)
  ['ams_get_variable: Requires 1-4 arguments']
  return
end

%===========================================================
ierr = 0;

if (nargin == 1) % (line 2)
%   split into three parts because sscanf does not work as expected
  [m,n] = size(comm);
  for i=1:n,
    if (comm(i) == ' ') 
      for j=i:n,
        if (comm(j) ~= ' ')
          break;
        end
      end
      memory = comm(j:n);
      comm   = comm(1:i-1);
      break;
    end
  end
  [m,n] = size(memory);
  for i=1:n,
    if (memory(i) == ' ') 
      for j=i:n,
        if (memory(j) ~= ' ')
          break;
        end
      end
      field  = memory(j:n);
      memory = memory(1:i-1);
      break;
    end
  end
  [m,n] = size(field);
  for i=1:n,
    if (field(i) == ' ')
      field = field(1:i-1); 
      break;
    end
  end
end
if (nargin == 2) % (line 3)
%   split into two parts because sscanf does not work as expected
  field = memory;
  [m,n] = size(comm);
  for i=1:n,
    if (comm(i) == ' ') 
      for j=i:n,
        if (comm(j) ~= ' ')
          break;
        end
      end
      memory = comm(j:n);
      comm   = comm(1:i-1);
      break;
    end
  end
  [m,n] = size(memory);
  for i=1:n,
    if (memory(i) == ' ')
      memory = memory(1:i-1); 
      break;
    end
  end
end

if (ischar(comm))
  [comm,err]   = ams_comm_attach(comm);
  if (err) 
    if (nargout == 3) 
      step = err;
    else 
      ['ams_get_variable: cannot attach to communicator']
    end
    changed = [];
    return
  end
end

if (ischar(memory)) % case 1 to 4 ----------------------------------------------
  [memory,step,err] = ams_memory_attach(comm,memory);
  if (err) 
    if (nargout == 3) 
      step = err;
    else 
      ['ams_get_variable: cannot attach to memory']
    end
    changed = [];
    return
  end
  [result,err]      = ams_memory_get_field_info(memory,field);
  if (err) 
    if (nargout == 3) 
      step = err;
    else 
      ['ams_get_variable: cannot get field info']
    end
    changed = [];
    return
  end
  err               = ams_memory_detach(memory);
  if (err) 
    if (nargout == 3) 
      step = err;
    else 
      ['ams_get_variable: cannot detach memory']
    end
    changed = [];
    return
  end

  changed = step; %    Note: changed actually needs to contain step information
  step    = ierr; %          step actually needs to contain ierr

else % case 5 and 6

  if (nargin ~= 4) % case 5 ---------------------------------------------------------
      [result,err]      = ams_memory_get_field_info(memory,field);
      if (err) 
        if (nargout == 2) 
          changed = err;
        else 
          ['ams_get_variable: cannot get field info']
        end
        return
      end
      
      changed = ierr; % Note: changed actually needs to contain ierr information

  else % case 6 ---------------------------------------------------------------------
      err                = ams_memory_update_recv_begin(memory);
      if (err)
        changed = [];
        step    = []; 
        if (nargout == 4) 
          ierr = err;
        else 
          ['ams_get_variable: cannot update receive begin']
        end
        return
      end
      [changed,step,err] = ams_memory_update_recv_end(memory);
      if (err)
        changed = [];
        step    = []; 
        if (nargout == 4) 
          ierr = err;
        else 
          ['ams_get_variable: cannot update receive end']
        end
        return
      end
      [result,err]       = ams_memory_get_field_info(memory,field);
      if (err)
        changed = [];
        step    = []; 
        if (nargout == 4) 
          ierr = err;
        else 
          ['ams_get_variable: cannot get field info']
        end
        return
      end
  end 

end










