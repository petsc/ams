function [comm,ierr] = ams_comm_attach(name)
%
%   Attaches to an ams communicator
%
%   [comm,ierr] = ams_comm_attach(name)
%
%   Input Parameter:
%    name - name of communicator obtained via ams_connect()
%
%   Output Parameter:
%    comm - communicator, used with, for example, ams_comm_get_memory_list()
%
%   See Also: ams_connect(), ams_comm_get_memory_list(),
%             ams_comm_detach()
%
if (nargin ~= 1) 
  ['ams_comm_attach: requires name of communicator as only argument']
  return
end

if (nargout < 1 | nargout > 2)
  ['ams_comm_attach: One or two output arguments required']
  return
end

if ~ischar(name)
  ['ams_comm_attach: Argument must be charactor string']
  return
end

%===========================================================================

if (nargout == 1) 
  comm = ams(2,name);
else
  [comm,ierr] = ams(2,name);
end

