function [data,ierr] = ams_memory_get_field_block(memory,name)
%
%   Gets a memory field 
%
%  [data,ierr] = ams_memory_get_field_block(memory,name)
%
%  Input Parameters:
%    memory - ams memory object
%    name - name of a field
%
%  Output Parameter:
%    data - Matlab variable containing dimensions of field
%           if it is multi-dimensional
%
%seealso: ams_memory_get_field_list(), ams_memory_attach(),
%         ams_memory_recv_update_begin()
%
%
if (nargin ~= 2) 
  ['ams_memory_get_field_block: requires two arguments']
  return
end

if (~ischar(name))
  ['ams_memory_get_field_block: field name must be a string']
  return
end

if (nargout > 2)
  ['ams_memory_get_field_block: requires at most two output arguments']
  return
end
  
%============================================================================

if (nargout == 2)
  [data,ierr] = ams(10,memory,name);
else
  data = ams(10,memory,name);
end
