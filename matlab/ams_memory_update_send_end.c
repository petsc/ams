/* $Revision: 1.1 $ */
/*
 *
 *  ams_memory_update_send_end(int memory)
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_update_send_end(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  int          ierr,memory;


  memory = *(mxGetPr(prhs[0]));

  ierr = AMS_Memory_update_send_end(memory);
  if (ierr) {
    if (nlhs < 1) {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    } else {
      plhs[0] = mxCreateDoubleMatrix(1,1,0);
      *(mxGetPr(plhs[0])) = (double) ierr;    
    }
  }
  return;
}


