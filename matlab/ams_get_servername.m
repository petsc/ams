function [servername,ierr] = ams_get_servername
%
%   Gets the name of the AMS server when it is not provided by user
%
%    servername = ams_get_servername
%
%    Output Parameter:
%      servername - first tries AMS_SERVER environmental variable, then
%                    HOSTNAME, finally the result from the UNIX shell command
%                    hostname
%
%    Note: this is a Matlab only routine, not part of the AMS C API
%
%seealso: ams_connect()
%
if (nargout < 1 | nargout > 2) 
  ['ams_get_servername: Must have one or two output arguments']
  return
end

if (nargout == 2) 
  ierr = 0;
end

%======================================================================

servername = getenv('AMS_SERVER');
if (isempty(servername))
  servername = getenv('HOSTNAME');
  if (isempty(servername))
    [err,servername] = unix('hostname');
    if (err ~= 0) then
      if (nargin < 2) 
        ['Must setenv AMS_SERVER or HOSTNAME to use without host argument']
      else
        ierr = err;
      end
      return
    end
    [m,n] = size(servername);
    servername = servername(1,1:n-1);
  end
end

