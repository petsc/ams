function [ierr] = ams_comm_detach(comm)
%
%   Detaches from a ams communicator
%
%   [ierr] = ams_comm_detach(comm)
%
%   Input Parameters:
%    comm - ams communicator obtained with ams_comm_attach()
%
% See Also: ams_connect(), ams_comm_get_memory_list(),
%             ams_comm_Attach()
%
if (nargin ~= 1) 
  ['ams_comm_detach: Need one input arguement']
  return
end

if (nargout > 1)
  ['ams_comm_detach: At most one output argument allowed']
  return
end

%===========================================================
if (nargout == 0) 
  ams(3,comm);
else 
  ierr = ams(3,comm);
end

