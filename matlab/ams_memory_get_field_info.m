function [data,ierr] = ams_memory_get_field_info(memory,name)
%
%   Gets a field variable from a given memory and field name
%
%  [data,ierr = ams_memory_get_field_info(memory,name)
%
%  Input Parameters:
%    memory - ams memory object, obtained via ams_memory_attach()
%    name - name of a field, ams_memory_get_field_list()
%
%  Output Parameter:
%    data - Matlab variable containing field
%
%  Notes: 
%    This should usually be paired as
%      ams_memory_update_recv_begin(memory) 
%      ams_memory_update_recv_end(memory) 
%      data = ams_memory_get_field_info(memory,name)
%
%.seealso: ams_get_variable(), ams_comm_attach(), ams_memory_attach(),
%          ams_memory_get_field_list()
%
if (nargin ~= 2) 
  ['ams_memory_get_field_info: Requires two arguments']
  return
end

if (nargout > 2)
  ['ams_memory_get_field_info: At most two output arguments']
  return
end

if (~ischar(name))
name
  ['ams_memory_get_field_info: Field name must be character']
  return
end

%=========================================================

if (nargout == 2)
  [data,ierr] = ams(8,memory,name);
else 
  data = ams(8,memory,name);
end
