function ierr = ams_set_output_file(filename)
%
%   Causes all error messages to be printed to a particular file
%
%    [ierr] = ams_set_output_file(char *filename)
%
%    Input Parameters:
%      filename - name of file to dump to
%
%    Output Parameter:
%      ierr - error code, nonzero on error
%
%seealso: ams_connect()
%
if (nargout > 1) 
  ['ams_set_output_file: At most one output argument allowed']
  return
end

if (nargin ~= 1)
  ['ams_set_output_file: Exactly one input argument required']
  return
end

if ~ischar(filename)
  ['ams_set_output_file: Filename must be character string']
  return
end

% ======================================================

if (nargout == 1)
  ierr = ams(12,filename);
else
  ams(12,filename);
end


