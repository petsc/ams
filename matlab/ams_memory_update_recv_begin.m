function [ierr] = ams_memory_update_recv_begin(memory)
%
%   Updates memory variables from the server
%
%   [ierr] = ams_memory_update_recv_begin(memory)
%
%   Input Parameter:
%     memory - ams memory object obtained with ams_memory_attach()
%
% Notes:    
%    This should usually be paired as
%      ams_memory_update_recv_begin(memory)
%      [changed,step] = ams_memory_update_recv_end(memory) 
%      data = ams_memory_get_field_info(memory,name)
%
if (nargin ~= 1) 
  ['ams_memory_update_recv_begin: requires one input argument']
  return
end

if (nargout > 1)
  ['ams_memory_update_recv_begin: allows at most one output argument']
  return
end
  
%=======================================================================================

if (nargout == 1) 
  ierr = ams(9,memory);
else
  ams(9,memory);
end