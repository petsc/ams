function [ierr] = ams_memory_update_send_begin(memory)
%
%   Updates memory variables from the server
%
%   [ierr] = ams_memory_update_send_begin(memory)
%
%   Input Parameter:
%     memory - ams memory object obtained with ams_memory_attach()
%
% Notes:    
%    This should usually be paired as
%      ams_memory_set_field_info(memory,name,data)
%      ams_memory_update_send_begin(memory)
%      ams_memory_update_send_end(memory) 
%
if (nargin ~= 1) 
  ['ams_memory_update_send_begin: requires one input argument']
  return
end

if (nargout > 1)
  ['ams_memory_update_send_begin: allows at most one output argument']
  return
end
  
%=======================================================================================

if (nargout == 1) 
  ierr = ams(17,memory);
else
  ams(17,memory);
end