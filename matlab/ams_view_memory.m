function [result,comm,ierr] = ams_view_memory(host,port,commname)
%
%   Returns all the memorys for an ams Publisher
%
%   [result,comm,ierr] = ams_view_memory(host,port,commname)
%
%  Input Parameters:
%    host - name of publisher machine
%    port - port number (optional)
%    commname - name of communicator
%
%  Output Parameters
%   result - string containing all memories
%   comm - AMS communicator
%
%seealso: ams_view_select(), ams_connect(), ams_get_variable()
%
%  Notes: this Matlab routine does not have a corresponding routine 
%         in the underlying AMS API.
%
result = [];
if (nargout >= 2) 
  comm = [];
end
if (nargout == 3) 
  ierr = 0;
end

if (nargin == 0) 
  [host,err] = ams_get_servername;
  if (err ~= 0)
    if (nargout < 3) 
      ['ams_view_memory: Unable to determine appropriate default server name']
    else
      ierr   = err;
    end
    return
  end
end
if (nargin < 2) 
  port = -1;
end
if (nargin < 3) 
  commname = 'PETSc';
end

result = [];
[commnames,err] = ams_connect(host,port);
if (err ~= 0)
  if (nargout < 3) 
    ['ams_view_memory: Unable to connect']
  else
    ierr   = err;
  end
  return
end

%  --------------- Note commnames are not used at all --------
%
%  Shorten commnames
[m,n] = size(commnames);
b     = 1;
for i=1:m,
  for j=1:n,
    if (commnames(i,j) == '|') 
      b = max(b,j);
      for k=j+1:n,
        commnames(i,k) = ' ';
      end;
      break;
    end;
  end;
end;
commnames = commnames(:,1:b);
%  --------------- Note commnames are not used at all --------

[comm,err] = ams_comm_attach(commname);
if (err ~= 0)
  if (nargout < 3) 
    ['ams_view_memory: Unable to attach to comm']
  else
    ierr   = err;
  end
  return
end

[list,err] = ams_comm_get_memory_list(comm);
if (err ~= 0)
  if (nargout < 3) 
    ['ams_view_memory: Unable to get memory list']
  else
    ierr   = err;
  end
  return
end

[p,q] = size(list);
for j=1:p,
  tmp    = [commname '| ' list(j,:) ' '  blanks(128)];
  result = [result ; tmp(1:80)];
end



