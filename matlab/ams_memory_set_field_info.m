function [ierr] = ams_memory_get_field_info(memory,name,data)
%
%   Sets a field variable from a given memory and field name
%
%  [ierr = ams_memory_get_field_info(memory,name,data)
%
%  Input Parameters:
%    memory - ams memory object, obtained via ams_memory_attach()
%
%  Output Parameter:
%    name - name of a field, ams_memory_get_field_list()
%
%  Notes: 
%    This should usually be paired as
%      ams_memory_get_field_info(memory,name,data)
%      ams_memory_send_update_begin(memory) 
%      ams_memory_send_update_end(memory) 
%
%.seealso: ams_get_variable(), ams_comm_attach(), ams_memory_attach(),
%          ams_memory_get_field_list(), ams_memory_get_field_info()
%
if (nargin ~= 3) 
  ['ams_memory_set_field_info: Requires three arguments']
  return
end

if (nargout > 1)
  ['ams_memory_set_field_info: At most one output argument']
  return
end

if (~ischar(name))
  ['ams_memory_set_field_info: Field name must be character']
  return
end

%=========================================================

if (nargout == 1)
  ierr = ams(16,memory,name,data);
else 
  ams(16,memory,name,data);
end
