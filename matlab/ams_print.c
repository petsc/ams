/* $Revision: 1.1 $ */
/*
 *
 *   
 *
 */

#include "mex.h"
#include "ams.h"

void mxams_print(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char message[1024];
  int  ierr;
  
  mxGetString(prhs[0],message,1024);

  ierr = AMS_Print("%s\n",message);
  if (nlhs > 0) {
    plhs[0] = mxCreateDoubleMatrix(1,1,0);
    *mxGetPr(plhs[0]) = ierr;
  } else if (ierr) {
    char *err;
    AMS_Explain_error(ierr,&err);
    mexErrMsgTxt(err);
  }

  return;
}


