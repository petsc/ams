/* $Revision: 1.1 $ */
/*
 *
 * [changed,step] = ams_memory_update_recv_end(int memory)
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_update_recv_end(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  int          ierr,memory,flag;
  unsigned int	step;


  memory = *(mxGetPr(prhs[0]));

  ierr = AMS_Memory_update_recv_end(memory, &flag, &step);
  if (ierr) {
    if (nlhs < 3) {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    } else {
      plhs[2] = mxCreateDoubleMatrix(1,1,0);
      *(mxGetPr(plhs[2])) = (double) ierr;    
    }

    if (nlhs > 0) {
      plhs[0] = mxCreateDoubleMatrix(0,0,0);
    } 
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(0,0,0);
    }
    return;
  }
  if (nlhs > 0) {
    plhs[0] = mxCreateDoubleMatrix(1,1,0);
    *(mxGetPr(plhs[0])) = (double) flag;
  } 
  if (nlhs > 1) {
    plhs[1] = mxCreateDoubleMatrix(1,1,0);
    *(mxGetPr(plhs[1])) = (double) step;
  }
  if (nlhs > 2) {
    plhs[2] = mxCreateDoubleMatrix(1,1,0);
    *(mxGetPr(plhs[2])) = 0.0;
  }
  return;
}


