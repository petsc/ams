function result = subsasgn(memory,s,b)

ams_memory_set_field_info(memory.memory,s.subs,b);
ams_memory_update_send_begin(memory.memory);
ams_memory_update_send_end(memory.memory);
result = memory;
