function [c,ierr] = ams_memory_class(comm,memory)
%
%  [c,ierr] = ams_memory_class(comm,memory)
%
%   Input parameters: 
%     comm is an ams_comm_class
%     memory is a string
%
%   Creates an object which is a mirror of the memory 
%  on the server side of the AMS
%
c.memory = ams_memory_attach(comm,memory);
c = class(c,'ams_memory_class');
 