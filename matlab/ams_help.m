function ams_help
%
%  Prints helpful information about using the 
%  AMS from Matlab
%
       ['           Getting a variable                          '
        '-------------------------------------------------------'
        'commlist = ams_connect(host,port)                      '
        'comm = ams_comm_attach(commlist(i,:))                  '
        'memlist = ams_comm_get_memory_list(comm)               '
        '[memory,step] = ams_memory_attach(comm,memlist(i,:))   '
        'fieldlist = ams_memory_get_field_list(memory)          '
        'data = ams_memory_get_field_info(memory,fieldlist(i,:))'
        '                                                       '
        '           Getting a variable repeatedly               '
        '-------------------------------------------------------'
        'ams_memory_update_rec_begin(memory)                    '
        '[changed,step] = ams_memory_update_recv_end(memory)    '
        'data = ams_memory_get_field_info(memory,fieldlist(i,:))'
        '                                                       '
        '           Selecting a variable from menu              '
        '-------------------------------------------------------'
        'data = ams_view_select(host,port)                      '
        '                                                       '
        '  All functions return an optional final error code    '
        ' argument. If that is requested then no error message  '
        ' is printed.                                           ']

