function ierr = ams_memory_detach(memory)
%
%   Detaches from a  given memory
%
%   [ierr] = ams_memory_detach(memory)
%
%  Input Parameter:
%    memory - ams memory object obtained with ams_memory_attach()
%
%seealso: ams_memory_attach()
%
if (nargin ~= 1) 
  ['ams_memory_detach: Requires 1 memory argument']
end

if (nargout > 1)
  ['ams_memory_detach: Requires at most 1 output argument']
end

%==========================================================
if (nargout == 0)
  ams(6,memory);
else
  ierr = ams(6,memory);
end
