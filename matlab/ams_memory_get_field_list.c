/* $Revision: 1.1 $ */
/*
 *
 * char **list = ams_memory_get_field_list(int memory)
 *
 */

#include "mex.h"
#include "ams.h"
#include <string.h>

void mxams_memory_get_field_list(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  char       **list,**tlist;
  int        ierr,memory,nlist;

  memory = *mxGetPr(prhs[0]);

  ierr = AMS_Memory_get_field_list(memory,&list);
  if (ierr) {
    plhs[0] = mxCreateCharMatrixFromStrings(0,(const char **)0);
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[1]) = ierr;
    } else {
      char *err;
      AMS_Explain_error(ierr,&err);
      mexErrMsgTxt(err);
    }
  } else {
    /* count number of strings */
    tlist = list;
    nlist  = 0;
    while (*tlist++) nlist++;

    plhs[0] = mxCreateCharMatrixFromStrings(nlist,(const char **)list);
    if (nlhs > 1) {
      plhs[1] = mxCreateDoubleMatrix(1,1,0);
      *mxGetPr(plhs[1]) = 0;
    }
  }

  return;
}


