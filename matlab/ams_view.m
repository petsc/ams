function [result,ierr] = ams_view(host,port)
%
%   Returns all the memory and fields for an ams Publisher
%
% result = ams_View(host,port)
%
%  Input Parameters:
%    host - name of publisher machine
%    port - port number (optional)
%
%  Output Parameters
%   result - string containing all memories and fields
%
%seealso: ams_View_select(), ams_connect(), ams_get_variable()
%
%  Notes: this Matlab routine does not have a corresponding routine 
%         in the underlying AMS C API.
%
if (nargout == 2) 
  ierr = 0;
end

if (nargin == 0) 
  [host,err] = ams_get_servername;
  if (err ~= 0)
    if (nargout < 2) 
      ['ams_view: Unable to determine appropriate default server name']
    else
      ierr = err;
    end
    return
  end
end
if (nargin < 2) 
  port = -1;
end

result = [];
[commnames,err] = ams_connect(host,port);
if (err ~= 0) 
  if (nargout < 2) 
    ['ams_view: Unable to connect']
  else
    ierr = err;
  end
  return
end
%
%  Shorten commnames
[m,n] = size(commnames);
b     = 1;
for i=1:m,
  for j=1:n,
    if (commnames(i,j) == '|') 
      b = max(b,j);
      for k=j+1:n,
        commnames(i,k) = '0';
      end;
      break;
    end;
  end;
end;
commnames = commnames(:,1:b);

[m,n] = size(commnames);
for i=1:m,
  [comm,err] = ams_comm_attach(commnames(i,:));
  if (err ~= 0) 
    if (nargout < 2) 
      ['ams_view: Unable to attach to communicator']
    else
      ierr = err;
    end
    return
  end

  [list,err] = ams_comm_get_memory_list(comm);
  if (err ~= 0) 
    if (nargout < 2) 
      ['ams_view: Unable to get memory list']
    else
      ierr = err;
    end
    return
  end

  [p,q] = size(list);
  for j=1:p,
    [memory,step,err] = ams_memory_attach(comm,list(j,:));
    if (err ~= 0) 
      if (nargout < 2) 
        ['ams_view: Unable to attach to memory:' list(j,:)]
      else
        ierr = err;
      end
      return
    end

    [fields,err] = ams_memory_get_field_list(memory);
    if (err ~= 0) 
      if (nargout < 2) 
        ['ams_view: Unable to get field list']
      else
        ierr = err;
      end
      return
    end

    [r,s] = size(fields);
    for k=1:r,
      tmp    = [commnames(i,:) ' ' list(j,:) ' ' fields(k,:) zeros(1,128)];
      result = [result ; tmp(1:80)];
    end

    [err] = ams_memory_detach(memory);
    if (err ~= 0) 
      if (nargout < 2) 
        ['ams_view: Unable to detach from memory:' list(j,:)]
      else
        ierr = err;
      end
      return
    end

  end
end
%
%  Replace 0 entries in result with ' '
%
[n,m] = size(result);
for i=1:n,
  for j=1:m,
    if (result(i,j) == 0) 
      result(i,j) = ' ';
    end
  end
end


