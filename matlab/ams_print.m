function [ierr] = ams_print(message)
%
%   Prints an AMS error message
%
%    [ierr] = ams_print(message)
%
%    Input Parameters:
%      message - character string containing message
%
%    Output Parameter:
%
%seealso: ams_set_output_file()
%
if (nargin == 0 | nargin > 1) 
  ['ams_print: Must have one input argument']
  return
end

if (nargout > 1) 
  ['ams_print: Must have at most one output argument']
end

if ~ischar(message)
  ['ams_print: Argument must be charactor string']
  return
end

% ======================================================

if (nargout == 0) 
  ams(13,message);
else
  ierr = ams(13,message);
end
