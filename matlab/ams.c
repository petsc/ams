/* $Revision: 1.1 $ */
/*

    ams()
 
      Master function for accessing alice Memory Snooper (AMS) accessor
  from Matlab. All of the AMS functions have to be access through a single
  Matlab mex file because the AMS maintains state in some global variables 
  that all the functions need access to.

      This uses the Matlab 5.0 MEX external interface to C programs.

      This function simply calls the appropriate MEX function interface 
  for the underlying AMS function.
      
 */

#include "mex.h"
#include "ams.h"

void mxams_connect(int, mxArray *[],int, const mxArray *[]);
void mxams_comm_attach(int, mxArray *[],int, const mxArray *[]);
void mxams_comm_get_memory_list(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_attach(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_get_field_list(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_get_field_info(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_set_field_info(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_get_field_block(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_detach(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_lock(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_unlock(int, mxArray *[],int, const mxArray *[]);
void mxams_comm_detach(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_update_recv_begin(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_update_recv_end(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_update_send_begin(int, mxArray *[],int, const mxArray *[]);
void mxams_memory_update_send_end(int, mxArray *[],int, const mxArray *[]);
void mxams_set_output_file(int, mxArray *[],int, const mxArray *[]);
void mxams_print(int, mxArray *[],int, const mxArray *[]);

void mexFunction(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
		 )
{
  int op;
  
  if (nrhs < 2) {
    mexErrMsgTxt("ams() requires at least two input arguments.");
  } 

  /* the first input argument indicates which ams_ function to call */
  op = (int ) *mxGetPr(prhs[0]);

  switch (op) {
    case 1: mxams_connect(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 2: mxams_comm_attach(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 3: mxams_comm_detach(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 4: mxams_comm_get_memory_list(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 5: mxams_memory_attach(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 6: mxams_memory_detach(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 7: mxams_memory_get_field_list(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 8: mxams_memory_get_field_info(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 9: mxams_memory_update_recv_begin(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 10: mxams_memory_get_field_block(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 11: mxams_memory_update_recv_end(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 12: mxams_set_output_file(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 13: mxams_print(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 14: mxams_memory_lock(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 15: mxams_memory_unlock(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 16: mxams_memory_set_field_info(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 17: mxams_memory_update_send_begin(nlhs,plhs,nrhs-1,prhs+1);
            break;
    case 18: mxams_memory_update_send_end(nlhs,plhs,nrhs-1,prhs+1);
            break;
    default: mexErrMsgTxt("ams() unknown operation.");
  }

  return;
}


