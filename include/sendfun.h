#if !defined(H_SENDF)
#define H_SENDF

extern char ll_msg[];
extern void AMSP_extract(const char *, char *, int, int *);
extern int AMSP_send_rq(int, int, ...);
extern int AMSP_send_rslt(int, int, int, ...);
extern int AMSP_send_error(int, char *, int);
extern int AMSP_send_shutdown();
extern int AMSP_fsm_loop(int, int, ...);
extern int AMSP_Check_Net_Version(int, int);

#endif
