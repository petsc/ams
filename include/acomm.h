#ifndef H_AMS_COMM
#define H_AMS_COMM

#include "amem.h"

#ifdef AMS_LDAP
#include "amsldap.h"
#endif

#ifdef AMS_PUBLISHER
#include "mpi.h"
#endif

#ifndef _WIN_ALICE
#define TRUE 1
#define FALSE 0
#endif

#ifndef AMS_ACCESSOR
#include "thrapi.h"
#endif

/* AMS Communicator definition and member functions */

/* Network node definition */
#define MAXHOSTLEN 255
typedef struct host_node {
    char hostname[MAXHOSTLEN+1]; /* hostname */
    int port;   /* port number */
    struct host_node *prev;
    struct host_node *next;
} HOST_NODE;

#ifdef AMS_PUBLISHER
/* Publisher Startup function arguments */
typedef struct proc_arg {
    void *arg;
    int sfd;
	int last;
} Proc_arg;

typedef struct pub_arg {
    int port;
    int care_about_port;
    int alive;
    int nb_accessor;
    Mutex pub_mutex;
    Mutex nbthr_mutex;
	Cond cond; /* To signal to the publisher-thread that the last thread is done */
    Cond cond_port; /* To signal to the publisher-thread that the port number has been set */

#ifdef AMS_LDAP
    char *dn; /* Directory name for this object */
    LDAP *ld; /* LDAP Publisher */
#endif

} Pub_arg;

typedef struct ams_pub {

    char host[255];
#ifdef AMS_LDAP
    char *dn;
    LDAP *ld;
#endif
    int port;
    int alive;
    int nb_com;

} AMS_Pub; 

extern AMS_Pub PUBLISHER;

#endif


/* COMMUNICATOR STRUCTURE */
typedef struct {
    char * name;        /* unique */
    int nb_proc;
    AMS_Comm_type ctype;
    HOST_NODE * host_node;
    HOST_NODE * TailHostNode;
    MEM_NODE * mem_node;
    MEM_NODE * TailMemNode;
    int nb_mem;
    /* Publisher Thread */
#ifdef AMS_PUBLISHER
    Pub_arg *pub_arg;
    Thread thrid;

    Appointment *waiting_room;
    Mutex waiting_room_lock;
    MPI_Comm mpi_comm;
    Mutex comm_mutex;

#endif

} COMM_STRUCT;


/* COMMUNICATOR AND PUBLISHERS TABLE */

/* Communicators' Table */
extern COMM_STRUCT *COMM_TBL[MAX_PUB][MAX_COMM];

/* Published Communicators */
extern int PUBCOMM_TBL[MAX_PUB][MAX_COMM];

/* Global variables used in the accessor (accessor) */
extern char * COMM_LIST[MAX_PUB][MAX_COMM];

/* Current Publisher's id */
extern int pub_id;

/* Publiser's id */
extern char * PUBLISHER_ID[];

/* Communicator's mask */
#define COMM_MASK 0x00ff


#ifdef AMS_PUBLISHER
/* To synchronize the communicator table */
extern Mutex comm_pub_mutex;

/* For future use to synchronize low level error messages */
extern Mutex msg_mutex;
#endif

/* low level error messages */
extern char ll_msg[];

/* 
 * Member functions definition
 * What we really need to do is create a general library for linked lists.
 * At this time, I am just using my bad habits of a C programmer: duplication
 */

/* Host Nodes manipulation functions */
extern HOST_NODE *AMSP_New_Host_Node(int *);
extern int AMSP_Del_Host_Node(HOST_NODE *);

Declare(AMSP_Valid_Host_Node, HOST_NODE *)

extern HOST_NODE *AMSP_Create_Host_Node(const char *, int, int *);

#ifdef AMS_PUBLISHER
/* Waiting Room functions */


/* Lock the waiting room before proceeding with other waiting room functions! */
extern int AMSP_Lock_Waiting_Room(AMS_Comm);
extern int AMSP_UnLock_Waiting_Room(AMS_Comm);

extern int AMSP_Create_Appointment(COMM_STRUCT *, RequestID, AMS_Memory, Appointment **);
/* creates an appointment */

extern int AMSP_Register_Guest(AMS_Comm, RequestID, AMS_Memory, Appointment **, int);
/* identifies request's appointment, creating new one if necessary*/
extern int AMSP_Assign_Appt(AMS_Comm, Appointment *, unsigned char[STEP_BYTES]);
/* records appt time in Waiting Room and calls Announce_Next_Guest */
extern int AMSP_Finish_Appt(AMS_Comm, Appointment *);
/* deletes an appointment and calls Announce_Next_Guest */
extern int AMSP_Announce_Next_Guest(AMS_Comm, AMS_Memory);
/* tells memory about the next guest it must stop for -- lock memory first! */
extern int AMSP_Wait_For_Appt_Step(AMS_Comm, Appointment *);
/* Wait until another thread fills in the appointment */
#endif



/* Communication Structure Member functions */
extern int AMSP_New_Comm_Struct(const char *, AMS_Comm_type, AMS_Comm *);
extern int AMSP_Del_Comm_Struct(COMM_STRUCT *);

Declare(AMSP_Valid_Comm_Struct,COMM_STRUCT *)

#ifdef AMS_PUBLISHER
extern int AMSP_New_Pub_Arguments(int, Pub_arg **);
extern int AMSP_Del_Pub_Arguments(Pub_arg *);
extern int AMSP_Set_Pub_Arguments_Comm_Struct(COMM_STRUCT *, Pub_arg *);

extern int AMSP_Set_MPI_Comm_Comm_Struct(COMM_STRUCT *, MPI_Comm);
extern int AMSP_Get_MPI_Comm_Comm_Struct(COMM_STRUCT *, MPI_Comm *);

extern int AMSP_Set_Thread_ID_Comm_Struct(COMM_STRUCT *, Thread );
extern int AMSP_Get_Thread_ID_Comm_Struct(COMM_STRUCT *, Thread *);
extern int AMSP_EndTread_Comm_Struct(COMM_STRUCT *);
extern void AMSP_Clean_up(void);
extern void AMSP_Start_up(void);

extern int AMSP_Am_I_First_Host_Comm_Struct(COMM_STRUCT *, int *);
extern int AMSP_Who_Is_Next_Host_Comm_Struct(COMM_STRUCT *, HOST_NODE **);

extern int AMSP_Get_Nb_Proc_Comm_Struct(COMM_STRUCT *, int *);

#endif

extern int AMSP_Lock_TBL_Write_Comm_Struct();
extern int AMSP_Lock_TBL_Read_Comm_Struct();
extern int AMSP_UnLock_TBL_Write_Comm_Struct();
extern int AMSP_UnLock_TBL_Read_Comm_Struct();

extern int AMSP_Find_Host_Comm_Struct(COMM_STRUCT *, const char *, HOST_NODE **);
extern int AMSP_Find_Mem_node_Comm_Struct(COMM_STRUCT *, MEM_NODE *, MEM_NODE **);
extern int AMSP_Find_Mem_name_Comm_Struct(COMM_STRUCT *, const char *, AMS_Memory *, MEM_NODE **);
extern int AMSP_Find_Mem_id_Comm_Struct(COMM_STRUCT *, AMS_Memory, MEM_NODE **);
extern int AMSP_Init_Comm_Tbl();
extern int AMSP_Set_Comm_Tbl(int, COMM_STRUCT *);
extern int AMSP_Get_Comm_Tbl(int, COMM_STRUCT **);

extern int AMSP_Publish_Comm_Struct(AMS_Comm);
extern int AMSP_UnPublish_Comm_Struct(AMS_Comm);
extern int AMSP_IsPublished_AMS_Comm(AMS_Comm, int, int *);

extern int AMSP_Add_Host_Node_Comm_Struct(COMM_STRUCT *, HOST_NODE *);
extern int AMSP_Del_Host_Node_Comm_Struct(COMM_STRUCT *, HOST_NODE *);
extern int AMSP_Add_Mem_Node_Comm_Struct(COMM_STRUCT *, MEM_NODE *);
extern int AMSP_Del_Mem_Node_Comm_Struct(COMM_STRUCT *, MEM_NODE *);

extern int AMSP_DelAll_Mem_Comm_Struct(COMM_STRUCT *);
extern int AMSP_DelAll_Host_Comm_Struct(COMM_STRUCT *);

extern int AMSP_Get_Comm_type_Comm_Struct(COMM_STRUCT *, AMS_Comm_type *);

/* Interfaces to AMS_Comm functions */

extern int AMSP_New_AMS_Comm(const char *, AMS_Comm_type, AMS_Comm *);

#ifdef AMS_PUBLISHER
extern int AMSP_Set_Pub_Arguments_AMS_Comm(AMS_Comm, Pub_arg *);
extern int AMSP_Set_Thread_ID_AMS_Comm(AMS_Comm, Thread );
extern int AMSP_Get_Thread_ID_AMS_Comm(AMS_Comm, Thread *);

extern int AMSP_Set_MPI_Comm_AMS_Comm(AMS_Comm, MPI_Comm);
extern int AMSP_Get_MPI_Comm_AMS_Comm(AMS_Comm, MPI_Comm *);

extern int AMSP_EndThread_AMS_Comm(AMS_Comm);
#endif

extern int AMSP_Publish_AMS_Comm(AMS_Comm);
extern int AMSP_UnPublish_AMS_Comm(AMS_Comm);

extern int AMSP_Add_Host_Node_AMS_Comm(AMS_Comm, HOST_NODE *);
extern int AMSP_Del_Host_Node_AMS_Comm(AMS_Comm, HOST_NODE *);
extern int AMSP_Add_Mem_Node_AMS_Comm(AMS_Comm, MEM_NODE *);
extern int AMSP_Find_Mem_name_AMS_Comm(AMS_Comm, const char *, AMS_Memory *, MEM_NODE **);
extern int AMSP_Find_Mem_node_AMS_Comm(AMS_Comm, MEM_NODE *, MEM_NODE **);
extern int AMSP_Find_Mem_id_AMS_Comm(AMS_Comm, AMS_Memory, MEM_NODE **);

extern int AMSP_Del_Mem_Node_AMS_Comm(AMS_Comm, MEM_NODE *);
extern int AMSP_DelAll_Mem_AMS_Comm(AMS_Comm alice);
extern int AMSP_DelAll_Host_AMS_Comm(AMS_Comm alice);
extern int AMSP_Del_AMS_Comm(AMS_Comm);

extern int AMSP_Get_AMS_Comm(const char *, AMS_Comm *);
extern int AMSP_Get_Comm_type_AMS_Comm(AMS_Comm, AMS_Comm_type *);
extern int AMSP_Get_Memory_list_AMS_Comm(AMS_Comm, char **,  int*);
extern int AMSP_Get_Host_list_AMS_Comm(AMS_Comm, char **,  int*);

extern int AMSP_Set_publisher_id(const char *, int);
extern int AMSP_Get_publisher_id(const char *, int);

#ifdef AMS_PUBLISHER

extern int AMSP_Construct_Port_list_AMS_Comm(AMS_Comm, char **, int *, const char *);
extern int AMSP_Am_I_First_Host_AMS_Comm(AMS_Comm, int *);
extern int AMSP_Who_Is_Next_Host_AMS_Comm(AMS_Comm, HOST_NODE **);
extern int AMSP_Get_Nb_Proc_AMS_Comm(AMS_Comm, int *);

extern int AMSP_Lock_Read_AMS_Comm(AMS_Comm);
extern int AMSP_UnLock_Read_AMS_Comm(AMS_Comm);
extern int AMSP_Lock_Write_AMS_Comm(AMS_Comm);
extern int AMSP_UnLock_Write_AMS_Comm(AMS_Comm);

#endif

#endif
