#ifndef H_AMS_MEM
#define H_AMS_MEM

#include <limits.h>

#include "amemfld.h"
#include "requestid.h"

#ifndef _WIN_ALICE
#define TRUE 1
#define FALSE 0

#endif

#ifndef AMS_ACCESSOR
#include "thrapi.h"
#endif

/* 
 * number of bytes to make version counter 
 */
#define STEP_BYTES 16
#define AMS_FORCE_UPDATE 1		/* All fields are updated whether they changed or not */
#define AMS_UPDATE 2			/* Only those fields that changed should be updated */
#define AMS_RELEASE_LOCK 3		/* Release the lock if memory not published */
#define AMS_KEEP_LOCK 4			/* Keep the lock on return */
#define AMS_DONOT_LOCK 5        /* Does not perform the lock */
/* 
 * An ALICE MEMORY OBJECT would be a collection of memory
 * addresses that will accessed (updated) together. Instead of
 * locking each memory address separately, we lock and unlock a
 * a group of them together. This will imporve performance and 
 * code readeability.
 */

typedef struct mem_node {
    AMS_Comm comm;      /* The communicator that owns this memory */
    char * name;        /* Memory name */
#ifndef AMS_ACCESSOR
    Mutex mem_lock;     /* Memory Synchronization variable */
	Cond cond;			/* Memory Condition Variable For Locking */
    Cond w_cond;	    /* Memory Condition Variable For Write Access Wainting */
    Cond r_cond;	    /* Memory Condition Variable For Read Access Wainting */
    unsigned char stop_at[STEP_BYTES];  /* Step to stop at */

#endif
    unsigned char current_step[STEP_BYTES]; /* 
                                             * Current state of the running computations
                                             * This step is also used to synchronize processors
                                             * within the same commnicator.
                                             */
    int c_stop;         /* Flag indicating that the main thread should block */
    int c_stop_wait;    /* How long should the main thread block for */
    Field * fields;     /* First of memory object fileds */
    Field * TailField;  /* Last of memory object fileds */
    Field * CurField;   /* Current memory object filed */
    int nb_fields;      /* Number of data objects (fields) in this memory */
} AMS_MEM;


#ifdef AMS_PUBLISHER
typedef struct appointment {
    RequestID guest;
    AMS_Memory mem_wanted;
    /* Appointment Condition Variable */
	Cond cond;
    unsigned char step[STEP_BYTES];
    struct appointment  *next;
} Appointment;
#endif



/* Low level AMS_MEM functions */

extern AMS_MEM *AMSP_New_AMS_MEM(AMS_Comm , const char *, int *); /* Initialize an ALICE memory structure */
Declare(AMSP_Valid_AMS_MEM, AMS_MEM *)
extern int AMSP_Del_AMS_MEM(AMS_MEM *);

extern int AMSP_IncrStep_AMS_MEM(AMS_MEM *); /* Increment the step */
extern int AMSP_IsNewerStep_AMS_MEM(AMS_MEM *, unsigned char *, int *);
extern int AMSP_GetStep_AMS_MEM(AMS_MEM *, unsigned char *);
/* IsNewerStep tests whether amem's step is greater than the given count */
#ifdef AMS_ACCESSOR
extern int AMSP_SetStep_AMS_MEM(AMS_MEM *, unsigned char *);
#endif
#ifdef AMS_PUBLISHER
extern int AMSP_Reserve_access_AMS_MEM(AMS_MEM *, unsigned char *);
#endif

extern int AMSP_Lock_Write_AMS_MEM(AMS_MEM *); /* Lock a mutex - for worker threads */
extern int AMSP_Lock_Read_AMS_MEM(AMS_MEM *); /* Lock a mutex - for worker threads */

extern int AMSP_Main_thread_read_Lock_AMS_MEM(AMS_MEM *); /* Lock a mutex if allowed */
extern int AMSP_Main_thread_write_Lock_AMS_MEM(AMS_MEM *); /* Lock a mutex if allowed */

extern int AMSP_Main_thread_read_wait_AMS_MEM(AMS_MEM *, int); /* Wait for a memory to be read */
extern int AMSP_Main_thread_write_wait_AMS_MEM(AMS_MEM *, int); /* Wait for a memory to be written */

extern int AMSP_UnLock_Read_AMS_MEM(AMS_MEM *); /* UnLock a mutex */
extern int AMSP_UnLock_Write_AMS_MEM(AMS_MEM *); /* UnLock a mutex */

extern int AMSP_CondBroadcast_AMS_MEM(AMS_MEM *);  /* Send a signal to threads waiting on a cond */

extern Field *AMSP_Find_name_AMS_MEM(AMS_MEM *, const char *, int *);

extern int AMSP_AddField_AMS_MEM(AMS_MEM *, Field *);

extern int AMSP_DelField_AMS_MEM(AMS_MEM *, Field *);
extern int AMSP_UpdField_AMS_MEM(AMS_MEM *, Field *);

extern int AMSP_Activate_AMS_MEM(AMS_MEM *); /* know sizes, allocate fields */

extern int AMSP_Field_AMS_MEM(AMS_MEM *, Field *);
extern int AMSP_Nb_Fields_AMS_MEM(AMS_MEM *, int *nb);
extern int AMSP_Get_field_list_AMS_MEM(AMS_MEM *, char ***);
extern int AMSP_Comm_AMS_MEM(AMS_MEM *, AMS_Comm *); 

/* Memory id nodes definition */
typedef struct memid_node {
    AMS_MEM *amem;      /* Memory structure */
    AMS_Memory memid;   /* Memory id */

    struct memid_node *prev;
    struct memid_node *next;
} MEM_NODE;


/* Memory id nodes manipulation functions */
extern MEM_NODE *AMSP_New_Mem_Node(int *);
extern int AMSP_Del_Mem_Node(MEM_NODE *);
Declare(AMSP_Valid_Mem_Node, MEM_NODE *)
extern int AMSP_Set_Mem_Node(MEM_NODE *, AMS_MEM *, AMS_Memory);
extern MEM_NODE *AMSP_Create_Mem_Node(AMS_MEM *, AMS_Memory *, int *);
extern int AMSP_Compare_Mem_Node(MEM_NODE *, MEM_NODE *);


/* 
 * Table of current allocated memory objects. This should be a linked list,
 * We will see in the future how many memory objects a typical application
 * manages, and if it is worth to create a linked list instead of the table.
 */

/* This number should NOT exceed MAXMEM defined below. This is the maximum number of memories */
#define MAX_MEMID 1020

/* We store both the memory id and the recycle number in a 32 bit integer */

/* This number is power of 2 */
#define MAXMEM 1024

/* Maximum number of recycled memories in one run */
#define MAXREC (INT_MAX/(2*MAXMEM*MAXCOM) - 1)

/* y = x * MAXMEM + memid */
/* The Memory id is given by */
#define getMemid(y) (y%MAXMEM)

/* The recycle number is given by */
#define getRecyc(y) (y/MAXMEM)

/* The id is given by */
#define getMemId(memid, x) (MAXMEM * x + memid)

/* z = y * MAXCOM + pubid */
/* Mask a Memory id with a Publisher Id */
#define maskId(y, pubid) (y * MAXCOM + pubid)

/* UnMaks (Get) a Memory id using a Publisher Id */
#define unmaskId(z) (z/MAXCOM)

/* Get a Publisher Id */
#define getPubId(z) (z%MAXCOM)

extern AMS_MEM *MEM_TBL[][MAX_COMM];
extern int MEM_RECYCLE_NB[][MAX_COMM];
extern int MEM_PUB_ID[][MAX_COMM];
extern int MEM_LAST_REC_NB;
extern int PUBLISHED_MEM_TBL[][MAX_COMM];

/* Global variables used in the accessor (accessor) */
extern char * MEM_LIST[];

#ifndef AMS_ACCESSOR
extern Mutex pub_mem_tbl;
extern Mutex MEM_PUB_LOCK[][MAX_COMM];
#endif


/* Interface to AMS_Memory functions */

/* Macro definition for checking a validity of a memory */
#define CheckMemory(id) if ((getMemid(id)) < 0 || (getMemid(id)) > MAX_MEMID) {printf(" id = %d rec=%d mem=%d\n", getMemid(id),getRecyc(id),id); return AMS_ERROR_INVALID_MEMORY;}
#define CheckMemory1(id, err) if ((getMemid(id)) < 0 || (getMemid(id)) > MAX_MEMID) {printf(" id = %d \n", getMemid(id)); err = AMS_ERROR_INVALID_MEMORY; return NULL;}

extern int AMSP_Detach_AMS_Memory(AMS_Memory);
extern int AMSP_Del_AMS_Memory(AMS_Memory);
extern int AMSP_Activate_AMS_Memory(AMS_Memory *, int); /* know sizes, allocate fields */
extern int AMSP_IncrStep_AMS_Memory(AMS_Memory *, int);
extern int AMSP_IsNewerStep_AMS_Memory(AMS_Memory *, int, unsigned char *, int *);
extern int AMSP_GetStep_AMS_Memory(AMS_Memory *, int, unsigned char *);
extern int AMSP_StepGreaterThan(unsigned char *, unsigned char *);
extern int AMSP_StepUndef(unsigned char *);
extern int AMSP_TranslateStep(unsigned char *, unsigned int *);

#ifdef AMS_ACCESSOR
extern int AMSP_SetStep_AMS_Memory(AMS_Memory *, int, unsigned char *);
#endif

#ifdef AMS_PUBLISHER
extern int AMSP_Worker_take_read_access(AMS_Memory);
extern int AMSP_Worker_take_write_access(AMS_Memory);
extern int AMSP_Worker_grant_write_access(AMS_Memory);
extern int AMSP_Worker_grant_read_access(AMS_Memory);

extern int AMSP_Worker_take_read_access_future(AMS_Memory, unsigned char *); /* watches argument */
extern int AMSP_Worker_take_write_access_future(AMS_Memory, unsigned char *); /* watches argument */

extern int AMSP_Reserve_future_access(AMS_Memory, unsigned char *); /* copies argument */
/* lock memory before calling Reserve_future_access */

extern int AMSP_Take_synchronized_write_access(AMS_Memory *, int, RequestID, Appointment ***);
extern int AMSP_Take_synchronized_read_access(AMS_Memory *, int, RequestID, Appointment ***);
extern int AMSP_Finish_synchronized_write_access(AMS_Memory *, int , Appointment **);
extern int AMSP_Finish_synchronized_read_access(AMS_Memory *, int, Appointment **);
#endif

extern int AMSP_Lock_Read_AMS_Memory(AMS_Memory); /* Read Lock a mutex */
extern int AMSP_Lock_Write_AMS_Memory(AMS_Memory); /* Write Lock a mutex */

extern int AMSP_Main_thread_read_Lock_AMS_Memory(AMS_Memory);
extern int AMSP_Main_thread_write_Lock_AMS_Memory(AMS_Memory);

extern int AMSP_Main_thread_read_wait_AMS_Memory(AMS_Memory, int);
extern int AMSP_Main_thread_write_wait_AMS_Memory(AMS_Memory, int);

extern int AMSP_Main_thread_block_AMS_Memory(AMS_Memory, int); /* Request for blocking the memory */
extern int AMSP_Main_thread_unblock_AMS_Memory(AMS_Memory); /* Request for unblocking the memory */


extern int AMSP_UnLock_Write_AMS_Memory(AMS_Memory); /* Unlock a mutex */
extern int AMSP_UnLock_Read_AMS_Memory(AMS_Memory); /* Unlock a mutex */
extern int AMSP_CondBroadcast_AMS_Memory(AMS_Memory);	/* Send a signal to threads waiting for mem */

extern Field *AMSP_Find_name_AMS_Memory(AMS_Memory , const char *, int *);

extern int AMSP_AddField_AMS_Memory(AMS_Memory, Field *);
extern int AMSP_Add_Field_AMS_Memory(AMS_Memory, const char *, void *, int, AMS_Data_type,
                                AMS_Memory_type , AMS_Shared_type , AMS_Reduction_type);
extern int AMSP_Set_Field_Start_Index_AMS_Memory(AMS_Memory, const char *, int);
extern int AMSP_Set_Field_Permutation_AMS_Memory(AMS_Memory, const char *, int *);
extern int AMSP_Set_Field_Block_AMS_Memory(AMS_Memory, const char *, int, int *, int *);

#ifdef AMS_PUBLISHER
extern int AMSP_Set_Field_Func_C_AMS_Memory(AMS_Memory, const char *, AMS_C_Setter *);
extern int AMSP_Set_Field_Func_CPP_AMS_Memory(AMS_Memory, const char *,
                                         AMS_CPP_Setter *, void *);
extern int AMSP_Set_Field_Func_JAVA_AMS_Memory(AMS_Memory, const char *,
                                          AMS_JAVA_Setter *, void *, void *);
#endif
extern int AMSP_DelField_AMS_Memory(AMS_Memory, Field *);
extern int AMSP_UpdField_AMS_Memory(AMS_Memory, Field *);

extern int AMSP_Field_AMS_Memory(AMS_Memory, Field *);
extern int AMSP_Nb_Fields_AMS_Memory(AMS_Memory, int *);
extern int AMSP_Get_field_list_AMS_Memory(AMS_Memory , char ***);
extern int AMSP_Get_Comm_type_AMS_Memory(AMS_Memory, AMS_Comm_type *);
extern int AMSP_Comm_AMS_Memory(AMS_Memory, AMS_Comm *);

/* Publishes a memory */
extern int AMSP_Publish_AMS_Memory(AMS_Memory);
extern int AMSP_UnPublish_AMS_Memory(AMS_Memory);

/* Tranlates a memory name to an id */
extern int AMSP_Get_AMS_Memory(const char *, int, AMS_Memory *);
extern int AMSP_IsPublished_AMS_Memory(AMS_Memory *, int, int , int *); 
#endif
