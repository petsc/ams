/*
 * Make modifications for site specifics before compilation
 */

#ifdef AMS_LDAP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifndef SGI
#include "lber.h"
#endif

#include "ldap.h"

#include "acomm.h"

/*
 * Host name of LDAP publisher
 */
#define MY_HOST		"safari.mcs.anl.gov"

/*
 * Port number where LDAP publisher is running
 */
#define	MY_PORT		LDAP_PORT

/*
 * DN of directory manager entry.  This entry should have write access to
 * the entire directory.
 */
#define MGR_DN		"cn=Directory Manager"

/*
 * Password for manager DN.
 */
#define MGR_PW		"aliceams"

/*
 * Subtree to search
 */
#define	MY_SEARCHBASE	"o=mcs.anl.gov"

/*
 * Place where AMS objects entries are stored
 */
#define AMS_BASE	"o=mcs.anl.gov"

#define BASE_SEARCH "cn=ALICE Memory Snooper, o=mcs.anl.gov"

/*
 * AMS LDAP API
 */

extern int AMSP_Ldap_init(LDAP **);
extern int AMSP_Ldap_disconnect(LDAP *);
extern int AMSP_Ldap_add_publisher(LDAP *);
extern int AMSP_Ldap_add_communicator(LDAP *, char *, int, AMS_Comm);
extern int AMSP_Ldap_del_object(LDAP *, char *);
extern int AMSP_Ldap_search_object(LDAP *, char *, char *, char ***, int *);
extern int AMSP_Ldap_load_publishers(LDAP *, char *, char *, char ***, int *);

#endif
