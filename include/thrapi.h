#ifndef H_THRAPI
#define H_THRAPI

/* Threads Header files */
#ifdef _WIN_ALICE
#include <windows.h>
#include <process.h>

#include <stddef.h>
#include <stdlib.h>
#include <conio.h>

#else

#ifdef SOLARIS
#include <thread.h>
#include <synch.h>
#else
#include <pthread.h>
#endif

#endif

#ifdef _WIN_ALICE

#define RealMutex CRITICAL_SECTION 
#define Thread unsigned long
#define Ret_Thr void

/* Used in the Condition variable */
#define SIGNAL 0
#define BROADCAST 1
#define MAX_EVENTS 2

typedef struct
{
	u_int waiters_count_;  
	// Count of the number of waiters.  
	
	CRITICAL_SECTION waiters_count_lock_;
	// Serialize access to <waiters_count_>.

	HANDLE events_[MAX_EVENTS];  // Signal and broadcast event HANDLEs.
} win32_cond_t;

#define Cond win32_cond_t

#else 

#ifdef SOLARIS
#define RealMutex mutex_t
#define Thread thread_t
#define Cond cond_t
#else
#define RealMutex pthread_mutex_t
#define Thread pthread_t
#define Cond pthread_cond_t
#endif
#define Ret_Thr void *

#endif

/* 
 * Our Mutex is more complicated so that we can 
 * implement read-write lock using condition variables 
 */
typedef struct {
    RealMutex mutex; /* lock to write or to update reader count */
    int readers_reading;
    int writer_writing;
    Cond lock_free;
} Mutex;


/*
 * Thread Functions
 */
extern void AMSP_CreatThread(Ret_Thr (void *), void *, Thread *);
extern void AMSP_EndThread(void *);
extern void AMSP_CancelThread(Thread );
extern void AMSP_ThreadSleep(int);
extern Thread AMSP_GetThreadId();

/*
 * Mutex Functions. There are two groups: RelMutex and Mutex.
 * Mutex is structure that support the reader/writer locks
 * using condition variables while RelMutex are the native one.
 * I am still not convinced of the performance of the reader/writer
 * locks in real application, but they are implemented for just
 * in case the user ask for them.
 */
extern void AMSP_NewMutex(Mutex *);
extern void AMSP_NewRealMutex(RealMutex *);

extern void AMSP_DelMutex(Mutex *);
extern void AMSP_DelRealMutex(RealMutex *); /* You need the pointer */

extern void AMSP_LockRealMutex( RealMutex *);
extern void AMSP_LockMutexRead( Mutex *);
extern void AMSP_LockMutexWrite( Mutex *);

extern void AMSP_RelRealMutex( RealMutex *);
extern void AMSP_RelMutexRead( Mutex *);
extern void AMSP_RelMutexWrite( Mutex *);

/*
 * Condition variable functions which operate only on real mutex
 */
extern void AMSP_CondInit(Cond *);
extern void AMSP_CondDestroy(Cond *);
extern void AMSP_CondWait(Cond *, RealMutex *, int, int *);
extern void AMSP_CondSignal(Cond *);
extern void AMSP_CondBroadcast(Cond *);

/*
 * Condition variable function that operate on high level mutex.
 * These functions are used with read/write locks. The goal is that
 * once a thread returns from condition wait it will be holding a
 * read or write lock. There is a timeout parameter and a flag
 * returned which indicates if the wait timed out, or not.
 */

extern void AMSP_CondWaitMutexWrite(Cond *, Mutex *, int, int *);
extern void AMSP_CondWaitMutexRead(Cond *, Mutex *, int, int *);

#endif
