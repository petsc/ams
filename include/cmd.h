/*
 * Header file for user command processing functions.
 */

#include        "ams.h"
extern char     token[];        /* temporary token for anyone to use */
extern char     *prg;
extern char     com_name[];
extern AMS_Comm alice;

extern int              Connected;
extern int              Attached;
extern int              Verbose;

typedef struct Cmds {
    const char        *cmd_name;              /* actual command string */
    int (*cmd_func)();          /* pointer to function */
} Cmds;

extern Cmds     commands[];
extern const char *ahelp[];
extern int      ncmds;          /* number of elements in array */
extern char * nexttoken(char []);
extern int nextline(FILE *);
extern int err_cmd(const char *);
extern int docmd(char *);
extern int checkend();

extern int PrintMemory(AMS_Comm, const char *);
extern int PrintField(AMS_Comm, const char *, const char *);
extern int ModifyField(AMS_Comm, const char *, const char *, const char *);
extern int RecvUpdate(AMS_Comm, const char *);

extern int LockMemory(AMS_Comm, const char *, int);
extern int UnlockMemory(AMS_Comm, const char *);

extern char MONT_cur_mem_name[];
extern AMS_Memory MONT_cur_mem;
extern int attach_seldom;
