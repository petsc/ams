#ifndef H_AMS_MEMORY
#define H_AMS_MEMORY

/*
******************************************************************************

	COPYRIGHT NOTIFICATION

******************************************************************************

The following is a notice of limited availability of this software and disclaimer
which must be included in the prologue of the code and in all source listings of
the code.

(c) COPYRIGHT 1998-2010 UNIVERSITY OF CHICAGO

Permission is hereby granted to use, reproduce, prepare derivative works, and to
redistribute to others.  This software was authored by:

Ibrahima Ba
Mathematics and Computer Science Division
Argonne National Laboratory
Argonne, IL 60439 USA

********************************************************************************

ARGONNE NATIONAL LABORATORY (ANL), WITH FACILITIES IN THE STATES OF ILLINOIS
IS OWNED BY THE UNITED STATES GOVERNMENT, AND OPERATED BY THE UNIVERSITY OF
CHICAGO UNDER PROVISION OF A CONTRACT WITH THE DEPARTMENT OF ENERGY.

*******************************************************************************

	GOVERNMENT LICENSE

******************************************************************************

Portions of this material resulted from work developed under a U.S. Goverment
contract and are subject to the following license: the Government is granted for
itself and others acting in its behalf a paid-up, nonexclusive, irrevocable
worldwide license in this computer software to reproduce, prepare derivative works,
and perform publicly and display publicly.

*******************************************************************************

	 DISCLAIMER

********************************************************************************

NEITHER THE UNITED STATES GOVERNMENT NOR ANY AGENCY THEREOF, NOR THE UNIVERSITY
OF CHICAGO, NOR ANY OF THEIR EMPLOYEES, MAKES ANY WARRANTY, EXPRESS OR IMPLIED,
OR ASSUMES ANY LEGAL LIABILITY OR RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS,
OR USEFULNESS OF ANY INFORMATION, APPARATUS, PRODUCT, OR PROCESS DISCLOSED, OR
REPRESENTS THAT ITS USE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS.

*******************************************************************************
*/

#ifdef __cplusplus
extern "C" {
#endif

    /* SUPPORTED COMMUNICATORS TYPE */
    typedef enum { MPI_TYPE, NODE_TYPE} AMS_Comm_type;

    /* AMS COMMUNICATOR TYPE */
    typedef int AMS_Comm;

    /* AMS MEMORY TYPE */
    typedef int AMS_Memory;

    typedef enum {AMS_MEMORY_UNDEF, AMS_READ, AMS_WRITE } AMS_Memory_type;
    typedef enum {AMS_DATA_UNDEF, AMS_CHAR, AMS_BOOLEAN, AMS_INT, AMS_FLOAT, AMS_DOUBLE , AMS_STRING} AMS_Data_type;
    typedef enum {AMS_SHARED_UNDEF, AMS_COMMON, AMS_REDUCED, AMS_DISTRIBUTED } AMS_Shared_type;
    typedef enum {AMS_REDUCT_UNDEF=4, AMS_SUM=1, AMS_MAX=2, AMS_MIN=3} AMS_Reduction_type;
    typedef enum { AMS_C, AMS_CPP, AMS_JAVA } AMS_Language_type;


    /* Prototype for setter functions given to AMS memory fields: */

    typedef int AMS_C_Setter(void *, int, void *, int);
    typedef int AMS_CPP_Setter(void *, void *, int, void *, int);
    typedef int AMS_JAVA_Setter(void *, void *, void *, int, void *, int);

    /**************************************************
     * Routines for the Publisher of shareable memory *
     **************************************************/

    extern int AMS_Comm_publish(const char *, AMS_Comm *, AMS_Comm_type, ...);
    extern int AMS_Memory_create(AMS_Comm, const char *, AMS_Memory *);
    extern int AMS_Memory_add_field(AMS_Memory, const char *, void *, int, AMS_Data_type,
                                    AMS_Memory_type, AMS_Shared_type, AMS_Reduction_type);
    extern int AMS_Memory_set_field_func(AMS_Memory, const char *,
                                         AMS_Language_type, ...);
    extern int AMS_Memory_set_field_start_index(AMS_Memory, const char *, int);
    extern int AMS_Memory_set_field_permutation(AMS_Memory, const char *, int *);
    /* For multidimensional arrays, instead of set_field_start_index: */
    extern int AMS_Memory_set_field_block(AMS_Memory, const char *, int, int *, int *);

    extern int AMS_Memory_publish(AMS_Memory);
    extern int AMS_Memory_grant_read_access(AMS_Memory);
	extern int AMS_Memory_grant_write_access(AMS_Memory);

    extern int AMS_Memory_take_write_access(AMS_Memory);
    extern int AMS_Memory_take_read_access(AMS_Memory);

    extern int AMS_Memory_wait_write_access(AMS_Memory, int);
    extern int AMS_Memory_wait_read_access(AMS_Memory, int);

    extern int AMS_Memory_destroy(AMS_Memory);
    extern int AMS_Comm_destroy(AMS_Comm);

#define AMS_Memory_grant_access AMS_Memory_grant_write_access /* For backward compatibility */
#define AMS_Memory_take_access AMS_Memory_take_write_access /* For backward compatibility */

    /**************************************************
     * Routines for the accessor of shareable memory  *
     **************************************************/

    extern int AMS_Connect(const char *, int, char ***);
    extern int AMS_Disconnect(void);
    extern int AMS_Comm_attach(const char *, AMS_Comm *);
    extern int AMS_Comm_get_memory_list(AMS_Comm, char ***);
    extern int AMS_Memory_attach(AMS_Comm, const char *, AMS_Memory *, unsigned int *step);
    extern int AMS_Memory_get_field_list(AMS_Memory, char ***);
    extern int AMS_Memory_get_field_info(AMS_Memory, const char *, void **, int *, AMS_Data_type *,
                                         AMS_Memory_type *, AMS_Shared_type *, AMS_Reduction_type *);
    /* To get the dimension of a multi-dimensional array */
    extern int AMS_Memory_get_field_block(AMS_Memory, const char *, int *, int **, int **);

    extern int AMS_Memory_set_field_info(AMS_Memory, const char *, void *, int);

    extern int AMS_Memory_update_send_begin(AMS_Memory);
    extern int AMS_Memory_update_send_end(AMS_Memory);

    extern int AMS_Memory_update_recv_end(AMS_Memory, int *, unsigned int *);
    extern int AMS_Memory_update_recv_begin(AMS_Memory);

    extern int AMS_Memory_detach(AMS_Memory);
    extern int AMS_Comm_detach(AMS_Comm);

    /**************************************************************
     * Routines available for both the publisher and the accessor *
     **************************************************************/
    extern int AMS_Explain_error(int , char **);
    extern int AMS_Print(const char *, ...);
    extern int AMS_Set_output_file(const char *);
    extern int AMS_Set_exit_func(void (* )(int));
    extern int AMS_Set_abort_func(void (* )(void));
    extern int AMS_Set_print_func(int (* )(const char *, ...));
	extern int AMS_Memory_lock(AMS_Memory, int);
    extern int AMS_Memory_unlock(AMS_Memory);


    /*****************************
     * Macros for error checking *
     *****************************/
#define AMS_Print_error_msg(err, msg) {AMS_Explain_error((err), (msg)); AMS_Print("%s\n",(*msg));}
#define AMS_Check_error(err, msg) if ((err) != AMS_ERROR_NONE) {AMS_Print_error_msg((err), (msg)); return (err);}

    /***************************************
     * Error Codes from AMS high-level API *
     ***************************************/
#define AMS_ERROR_NONE                                                  0
#define AMS_ERROR_INVALID_COMMUNICATOR                  1
#define AMS_ERROR_INVALID_MEMORY                                        2
#define AMS_ERROR_INVALID_MEMORY_TYPE                           3
#define AMS_ERROR_INVALID_DATA_TYPE                             4
#define AMS_ERROR_INVALID_SHARED_TYPE                           5
#define AMS_ERROR_INVALID_REDUCTION_TYPE                        6
#define AMS_ERROR_PUBLISHER_NO_LONGER_ALIVE             7
#define AMS_ERROR_COMMUNICATOR_NO_LONGER_ALIVE  8
#define AMS_ERROR_INVALID_USER_ARGUMENT                 9
#define AMS_ERROR_MPI_TYPE_NOT_SUPPORTED                        10
#define AMS_ERROR_INVALID_COMM_TYPE                             11
#define AMS_ERROR_NOT_CONNECTED                                 12
#define AMS_ERROR_INVALID_LANGUAGE_TYPE                        13
#define AMS_ERROR_BAD_DIMENSIONS                               14

    /******************************************
     * Error Codes from low level function    *
     ******************************************/

#define AMS_ERROR_BASE1                                 1000
#define AMS_ERROR_BAD_ARGUMENT                          (AMS_ERROR_BASE1 + 1)
#define AMS_ERROR_INSUFFICIENT_MEMORY                   (AMS_ERROR_BASE1 + 2)
#define AMS_ERROR_FIELD_ALREADY_IN_MEMORY               (AMS_ERROR_BASE1 + 3)
#define AMS_ERROR_FIELD_NOT_IN_MEMORY                   (AMS_ERROR_BASE1 + 4)
#define AMS_ERROR_SYSTEM                                (AMS_ERROR_BASE1 + 5)
#define AMS_ERROR_BAD_PORT_NUMBER                       (AMS_ERROR_BASE1 + 6)
#define AMS_ERROR_NODE_ALREADY_IN_COMM                  (AMS_ERROR_BASE1 + 7)
#define AMS_ERROR_FUNCT_NOTIMPLEMENTED                  (AMS_ERROR_BASE1 + 8)
#define AMS_ERROR_COMM_TBL_FULL                         (AMS_ERROR_BASE1 + 9)
#define AMS_ERROR_MEMORY_TBL_FULL                       (AMS_ERROR_BASE1 + 10)
#define AMS_ERROR_BAD_MEMID_NUMBER                      (AMS_ERROR_BASE1 + 11)
#define AMS_PUBLISHED_TBL_FULL                          (AMS_ERROR_BASE1 + 12)
#define AMS_ERROR_NET_OPEN                              (AMS_ERROR_BASE1 + 13)
#define AMS_ERROR_BAD_FIELD_LENGTH                      (AMS_ERROR_BASE1 + 14)
#define AMS_ERROR_BAD_FIELD_STATUS                      (AMS_ERROR_BASE1 + 15)
#define AMS_ERROR_MEMORY_NOT_PUBLISHED                  (AMS_ERROR_BASE1 + 16)
#define AMS_ERROR_MEMORY_NAME_NOT_FOUND                 (AMS_ERROR_BASE1 + 17)
#define AMS_ERROR_COMM_NOT_PUBLISHED                    (AMS_ERROR_BASE1 + 18)
#define AMS_ERROR_COMM_NAME_NOT_FOUND                   (AMS_ERROR_BASE1 + 19)
#define AMS_ERROR_INITIALIZE_NET_LIB                    (AMS_ERROR_BASE1 + 20)
#define AMS_ERROR_TERMINATE_NET_LIB                     (AMS_ERROR_BASE1 + 21)
#define AMS_ERROR_MEMORY_NODE_NOT_IN_COMMUNICATOR       (AMS_ERROR_BASE1 + 22)
#define AMS_ERROR_MEMORY_NO_LONGER_PUBLISHED            (AMS_ERROR_BASE1 + 23)
#define AMS_ERROR_FIELD_READ_ONLY                       (AMS_ERROR_BASE1 + 24)
#define AMS_ERROR_CANNOT_MODIFY_ENV                     (AMS_ERROR_BASE1 + 25)
#define AMS_ERROR_VALIDATION                            (AMS_ERROR_BASE1 + 26)
#define AMS_ERROR_NET_PROTOCOL                          (AMS_ERROR_BASE1 + 27)
#define AMS_ERROR_HOST_NODE_NOT_IN_COMMUNICATOR         (AMS_ERROR_BASE1 + 28)
#define AMS_ERROR_CONNECT_REFUSED                       (AMS_ERROR_BASE1 + 29)
#define AMS_ERROR_CONNECT_TERMINATED                    (AMS_ERROR_BASE1 + 30)
#define AMS_ERROR_COMM_PROTOCOL                         (AMS_ERROR_BASE1 + 31)
#define AMS_ERROR_LDAP_INIT                             (AMS_ERROR_BASE1 + 32)
#define AMS_ERROR_LDAP_BIND                             (AMS_ERROR_BASE1 + 33)
#define AMS_ERROR_LDAP_UNBIND                           (AMS_ERROR_BASE1 + 34)
#define AMS_ERROR_LDAP_DELETE                           (AMS_ERROR_BASE1 + 35)
#define AMS_ERROR_LDAP_SYSTEM                           (AMS_ERROR_BASE1 + 36)
#define AMS_ERROR_PUB_TBL_FULL                          (AMS_ERROR_BASE1 + 37)
#define AMS_ERROR_PUBID_NOT_FOUND                       (AMS_ERROR_BASE1 + 38)

    /************************************
     * Other low-level AMS return codes *
     ************************************/
#define AMS_ERROR_BASE2                 2000
#define AMS_IDENTICAL_MEMORY                            (AMS_ERROR_BASE2 + 1)
#define AMS_IDENTICAL_MEM_ID                            (AMS_ERROR_BASE2 + 2)
#define AMS_IDENTICAL_MEM_NAME                          (AMS_ERROR_BASE2 + 3)
#define AMS_DIFFERENT_MEMORY                            (AMS_ERROR_BASE2 + 4)
#define AMS_EXTRACTED_LAST_FIELD                        (AMS_ERROR_BASE2 + 5)
#define AMS_NO_FIELD_TO_EXTRACT                         (AMS_ERROR_BASE2 + 6)

    /**************************************************
     * Error Message from Java Native Interface calls *
     **************************************************/
#define AMS_ERROR_JAVA_BASE             3000
#define AMS_ERROR_JAVA_JNI_GET_FID                      (AMS_ERROR_JAVA_BASE + 1)
#define AMS_ERROR_JAVA_JNI_GET_MID                      (AMS_ERROR_JAVA_BASE + 2)
#define AMS_ERROR_JAVA_SYSTEM                           (AMS_ERROR_JAVA_BASE + 3)

    /***********************************
     * Low-Level Error Checking macros *
     ***********************************/
#define CheckErr(err)   if ((err) != AMS_ERROR_NONE) return (err)
#define CheckErr1(err)  if ((err) != AMS_ERROR_NONE) return (NULL)

/* This number is power of 2 */
#define MAXCOM 128

#define MAX_COMM        10       /* Maximum nb of Communicators per Job. Should not exceed MAXCOM */
#define MAX_PUB         50       /* Maximum nb of Publishers. Should not exceed MAXCOM */

#define CheckComm(id) if (id < 0 || id >= MAX_COMM)     return AMS_ERROR_INVALID_COMMUNICATOR


    /************************************************************************
	 *AMS DEFAULT PORT NUMBER. DO NOT CHANGE WITHOUT REBUILDING ALL LIBRARY.*
	 ************************************************************************/
#define DEFAULT_PORT    8967


	/*************************************************************
	 * This Marcro is used to disable low-level object validation*
	 * and increase library performance.                         *
	 *************************************************************/


#ifdef _DEBUG
#define Declare(func, parm) extern int func(parm);
#define Validate(func, parm) func(parm)
#else
#define Declare(func, parm)
#define Validate(func, parm) AMS_ERROR_NONE
#endif


#ifdef __cplusplus
}
#endif
#endif
