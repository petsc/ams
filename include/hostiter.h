#ifndef HOSTITER_H
#define HOSTITER_H

#include "acomm.h"

/*
 * ITERATOR FOR LOOPING THROUGH THE HOSTS OF A COMMUNICATOR
 */
/* WARNING: If you do not loop through all the hosts of the iterator,
   then you must close the sfd yourself! */
 
typedef struct host_iter {
    int sfd;
    HOST_NODE *node;
    int is_open;
} HOST_ITER;
 
/* Must pass a pointer to an existing HOST_ITER structure */
extern int AMSP_Comm_Hosts_begin(AMS_Comm, HOST_ITER *);
extern int AMSP_Comm_Hosts_valid(AMS_Comm, HOST_ITER *);
extern int AMSP_Comm_Hosts_advance(AMS_Comm, HOST_ITER *);

extern int AMSP_Comm_Hosts_open_any(AMS_Comm, int *);

extern int AMSP_Comm_Hosts_open_all(AMS_Comm, int *, int **);
extern int AMSP_Comm_Hosts_close_all(int, int *);

#endif
