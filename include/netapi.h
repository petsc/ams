
#ifdef _WIN_ALICE
/* Socket Initialization in Windows */
extern int AMSP_InitSock(void);
extern int AMSP_TermSock(void);
#endif

extern void AMSP_mem_net_init(char *, int, int* , int *);
extern int AMSP_mem_net_open(char *, char *, int, int *, char *);

#ifdef AMS_PUBLISHER
extern int AMSP_mem_net_accept(int, int, void *);
#endif

extern int AMSP_mem_net_close(int);
extern int AMSP_mem_net_recv(char **, int *, int);
extern int AMSP_mem_net_send(char *, int, int);


