#ifndef H_AMS_UPDATESYNC
#define H_AMS_UPDATESYNC

#ifdef AMS_PUBLISHER

#include "amem.h"

extern int AMSP_Start_Step_Negotiation(AMS_Memory, RequestID);
extern int AMSP_Continue_Step_Negotiation(AMS_Comm, RequestID, AMS_Memory,
                                     unsigned char[STEP_BYTES]);
extern int AMSP_Start_Step_Notification(AMS_Comm, RequestID, AMS_Memory,
                                   unsigned char[STEP_BYTES]);
extern int AMSP_Continue_Step_Notification(AMS_Comm, RequestID, AMS_Memory,
                                      unsigned char[STEP_BYTES]);

#endif

#endif
