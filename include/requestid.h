#include "ams.h"

#ifdef _MT
#include "thrapi.h"
#endif

typedef char *RequestID;

extern int AMSP_Generate_RequestID(RequestID *);
extern int AMSP_Equals_RequestID(RequestID, RequestID);
extern int AMSP_Size_RequestID(RequestID, int *); /* TOTAL size */
extern int AMSP_Copy_RequestID(void *, RequestID);
extern int AMSP_Delete_RequestID(RequestID);
