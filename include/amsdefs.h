/*
 * Definitions for AMS memory accessor and publisher.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <setjmp.h>

#include "systype.h"

#define MAXFILENAME       128   /* max filename length */
#define MAXHOSTNAME       128   /* max host name length */

#define AMS_SERVICE     NULL    /* name of the service */

#define AMS_MEMORY_NET_VERSION  3  /* version of the network protocol we transmit */

#ifdef _WIN_ALICE
#define DAEMONLOG       "./ams.log"
#else
#define DAEMONLOG   "/tmp/ams.log"
#endif
/* log file for daemon tracing */

/*
 * Read Only Externals. Since this program uses Thread we have
 * to be careful when using global variable. 
 * We assume that the accessor program is a single-threaded and 
 * there will be no data race in the accessor side.
 */

extern char     AMS_PUB_HOST_NAME[];    /* name of host system */
extern int      AMS_PUB_PORT_NUMBER;    /* name of host system */
extern int      inetdflag;      /* true if we were started by a daemon */


/*
 * Define the AMS opcodes.
 */

#define OP_ATTACH_COMM          1       /* Attaches a communicator */
#define OP_MEM_LIST             2       /* Get the memory list */
#define OP_ATTACH_MEM           3       /* Attache a memory */
#define OP_MEM_FLDS             4       /* Get memory's fields */
#define OP_SEND_UPD_MEM         5       /* Send an update of the memory */
#define OP_RECV_UPD_MEM         6       /* Receives an update of the memory */
#define OP_DETACH_COMM        7       /* Detach a communicator */
#define OP_RSLT                 8       /* Sending Results */
#define OP_ERROR                9       /* Sending an Error message*/
#define OP_SHUTDOWN             10      /* Publisher being shutdown */
#define OP_COMM_LIST            11      /* List of Communicators */
#define OP_COMM_PORT            12      /* Get communicator's local port */
#define OP_NEGOTIATE_STEP       13      /* Negotiate stopping point */
#define OP_NOTIFY_STEP          14      /* Announce stopping point */
#define OP_UNLOCK_MEM           15      /* Unlock the remote memory */
#define OP_LOCK_MEM             16      /* Lock the remote memory */

#define OP_MIN          1       /* minimum opcode value */
#define OP_MAX          16      /* maximum opcode value */

#define INT_SIZE 4              /* my size of integers */
#define FLOAT_SIZE 4            /* my size of floats */
extern const char *str_code[OP_MAX+1];


/*
 * Debug macros, based on the trace flag (-t command line argument,
 * or "trace" command).
 */

#define Debug1(fmt, arg1)       if (traceflag) { \
    fprintf(stderr, fmt, arg1); \
                                    fputc('\n', stderr); \
                                                             fflush(stderr); \
                                                                                 } else ;

#define Debug2(fmt, arg1, arg2) if (traceflag) { \
                                                                                     printf(fmt, arg1, arg2); \
                                                                                                                  printf("\n"); \
                                                                                                                                    } else ;

#ifdef  lint            /* hush up lint */
#undef  AMSP_ldshort
#undef  AMSP_stshort
short   AMSP_ldshort();
#endif  /* lint */

/* 
 * Prototype for functions that process requests
 * The arguments are a pointer to the recv_buff, nbytes
 * received, socket descriptor, op_sent, op_recv, and other argmuent.
 */

typedef int RECVFUN (char *, int, int, int, int, int, va_list);

/*
 * Datatypes of functions that don't return an int.
 */

char * AMSP_sys_err_str(); /* our library routine for system error messages */

/*
 * AMS last error message
 */

extern char ams_msg[];

