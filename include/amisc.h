#ifndef AMISC_H
#define AMISC_H

#include "amem.h"

extern int AMSP_Create_Fields_list(AMS_Memory *, int, int, char **, int *);
extern int AMSP_Create_Upd_Memory_list(AMS_Memory *, int, unsigned char *,
                                  char **, int *, RequestID);
extern int AMSP_Get_Memory_list(AMS_Comm, char **, int *);
extern int AMSP_Get_Host_list(AMS_Comm, char **, int *);
extern int AMSP_Get_Comm_list(char **, int *);

extern int AMSP_Create_Comm_list(const char *, int, char ***);

extern int AMSP_Create_Memory_list(char *, int, char ***);
extern int AMSP_Delete_Memory_list(AMS_Comm);

extern int AMSP_Create_Memory_fields(char *, int);
extern int AMSP_Update_Memory_fields(char *, int, int, int *, unsigned char *,
                                RequestID);

extern int AMSP_Get_Comm_ID(char *, AMS_Comm *);
extern int AMSP_Get_Memory_ID(char *, int, AMS_Memory *);
extern int AMSP_Get_Memory_fields(AMS_Memory *, int, char **, int *);
extern int AMSP_Delete_Comm_list(void);

#ifdef AMS_PUBLISHER
extern int AMSP_Get_Comm_local_port(AMS_Comm, int *);
extern int AMSP_Set_Comm_local_port(AMS_Comm);
extern int AMSP_Get_Comm_port(AMS_Comm, int, int *);
extern int AMSP_Set_Comm_port(AMS_Comm, int, int);

extern int AMSP_Lock_memory(AMS_Memory *, int, int, RequestID);
extern int AMSP_Unlock_memory(AMS_Memory *, int);

#endif

#endif
