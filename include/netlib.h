#ifndef H_NETLIB
#define H_NETLIB

/* Header file for network/error utilities functions */

#include "network.h"

/* Pointer to functions that return void and takes int */
typedef void (* PTF_void_int)( int);

/* Pointer to functions that return void */
typedef void (* PTF_void)();

/* Pointer to functions that has the same prototype as printf */
typedef int (* PTF_print) (const char *, ...);

#ifdef _WIN_ALICE
void AMSP_WinSockErr(int , char *);
#else
extern void daemon_start(int);
#endif

extern void AMSP_err_quit(const char *, ...);
extern void AMSP_err_sys(const char *, ...);
extern void AMSP_err_ret(char *, const char *, ...);
extern void AMSP_err_dump(const char *, ...);
extern void AMSP_perror(char *);
extern void AMSP_err_init(const char *);
extern void AMSP_err_file(const char *);
extern char *AMSP_host_err_str(const char *, int *);
extern int AMSP_readn(int, char *, int);
extern int AMSP_writen(int, char *, int);
extern int AMSP_tcp_open(char *, char *, int, struct sockaddr_in *, struct servent *, int *err, char *errmsg);
extern int AMSP_rresvport(int *, char *errmsg);

extern void AMSP_err_init_exit(PTF_void_int);
extern void AMSP_err_init_abort(PTF_void);
extern void AMSP_err_init_print(PTF_print);
extern int amsp_print_func;
extern PTF_print func_print;


#endif









