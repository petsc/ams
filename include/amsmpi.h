#include "mpi.h"

/* The main program must call MPI_Init before using any AMS functions. */
/* We assume that all the processors in MPI_Comm will have
 * their own publishers (and hence will be running on separate processors) */

extern int AMSP_Construct_Host_list_AMS_MPI(char ***, MPI_Comm);
extern int AMSP_Get_Max_Step_AMS_MPI(unsigned char *, MPI_Comm );
extern int AMSP_Destroy_Host_list_AMS_MPI(char **);
extern int AMSP_Construct_Pub_ports_AMS_MPI(int, int, char **, int **, MPI_Comm);

extern int AMSP_Guess_start_index_AMS_MPI(int, int *, MPI_Comm);

/* wrappers */
extern int AMSP_Get_rank_AMS_MPI(int *, MPI_Comm);
