#if !defined(H_CONVERT)
#define H_CONVERT

extern short int AMSP_ldshort(char *);
extern  void AMSP_stshort(short, char *);
extern long AMSP_ldlong(char *);
extern void AMSP_stlong(long ,char *);
extern float AMSP_ldfloat(char *);
extern void AMSP_stfloat(float ,char *);
extern double AMSP_lddouble(char *);
extern void AMSP_stdouble(double ,char *);

#endif
