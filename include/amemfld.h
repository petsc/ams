#ifndef H_AMS_MEM_FIELD
#define H_AMS_MEM_FIELD

#include "ams.h"

/*
 * This modules defines the ALICE memory fields. Fields are
 * part of an ALICE MEMORY Object. Fields are accessed only
 * through the ALICE MEMORY module.
 */


/* Field structure definition */
#define MAXNAME 255 /* Max lenght for field's name */

/* SUN do not have min macro defined */
#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

typedef struct fieldnode{

    char *name;         /* Field's name */
    void * data;        /* Object data  */
    int len;            /* Object's length */
    int dim;            /* number of dimensions in array, 0 for permutation */
    int *start;  /* our data fills indices start through end-1 in
                    each dimension of the field  */
    int *end;

    AMS_Language_type lang;
    AMS_C_Setter *CSetValue;
    AMS_CPP_Setter *CPPSetValue;
    AMS_JAVA_Setter *JAVASetValue;
    void *Obj; /* object the field is associated with (for C++ setters) */
    void *Env; /* field environment (for Java setters) */

    AMS_Memory_type mtype;    /* READ/WRITE/READ_WRITE */
    AMS_Data_type dtype;      /* AMS_CHAR, AMS_INT, AMS_BOOLEAN, AMS_DOUBLE , AMS_FLOAT, AMS_STRING*/
    AMS_Shared_type stype;    /* AMS_COMMON/REDUCED/DISTRIBUTED */
    AMS_Reduction_type rtype; /* AMS_MIN/MAX/SUM */

    int status;                                 /* Status, 0 = unmodified, 1 = modified */
    struct fieldnode *prev;             /* pointer to previous added field */
    struct fieldnode *next;             /* pointer to next added field */
} Field;

/* Member function */

extern Field * AMSP_New_Field(int *); /* Creates and initializes a new field */
extern int AMSP_Del_Field(Field *);  /* Deletes and frees memory associated with field */
extern int AMSP_Del_All_Field(Field *); /* Free the linked list pointed by Field */
extern int AMSP_Dup_Field(Field *, Field *); /* Create a duplicate of the field */

Declare(AMSP_Valid_Field, Field *)        /* Validate a field */
Declare(AMSP_Valid_Memory_type, AMS_Memory_type)
Declare(AMSP_Valid_Data_type, AMS_Data_type)
Declare(AMSP_Valid_Shared_type, AMS_Shared_type)
Declare(AMSP_Valid_Reduction_type, AMS_Reduction_type)

extern void * AMSP_Get_Buffer_Field(Field *, int *, int *);
extern int AMSP_Get_Reduced_Field(Field *, void *);
extern int AMSP_Reduce_Field(Field *, void *);

extern int AMSP_Size_Elt_Field(Field *, int *); /* Get the size of field's element */

extern int AMSP_Set_Buffer_Field(Field *, void *, int);
extern int AMSP_Upd_Buffer_Field(Field *, void *, int, int);
extern int AMSP_Upd_Buffer_Perm_Field(Field *, void *, int, int *);
extern int AMSP_Upd_Buffer_MultDim_Field(Field *, void *, int, int, int *, int *);

extern int AMSP_Alloc_Memory_Field(Field *, int);
extern int AMSP_Get_Length_Field(Field *, int *);

extern int AMSP_Expand_Field(Field *, int *, int *);
extern int AMSP_Expand_Field_Perm(Field *, int, int *);

extern int AMSP_Get_Starting_Index_Field(Field *, int *);
extern int AMSP_Set_Starting_Index_Field(Field *, int);

extern int AMSP_Get_Block_Field(Field *, int *, int **, int **);
extern int AMSP_Set_Block_Field(Field *, int, int *, int *);

extern int AMSP_Set_Permutation_Field(Field *, int *);

#ifdef AMS_PUBLISHER
extern int AMSP_Set_Setter_Func_C(Field *, AMS_C_Setter *);
extern int AMSP_Set_Setter_Func_CPP(Field *, AMS_CPP_Setter *, void *);
extern int AMSP_Set_Setter_Func_JAVA(Field *, AMS_JAVA_Setter *, void *, void *);
#endif

extern int AMSP_Get_Status_Field(Field *, int *);
extern int AMSP_Set_Status_Field(Field *, int);

extern char * AMSP_Get_Name_Field(Field *, int *);
extern int AMSP_Set_Name_Field(Field *, const char *);

extern int AMSP_Get_Memory_type_Field(Field *, AMS_Memory_type *);
extern int AMSP_Set_Memory_type_Field(Field *, AMS_Memory_type);

extern int AMSP_Get_Data_type_Field(Field *, AMS_Data_type *);
extern int AMSP_Set_Data_type_Field(Field *, AMS_Data_type);

extern int AMSP_Get_Shared_type_Field(Field *, AMS_Shared_type *);
extern int AMSP_Set_Shared_type_Field(Field *, AMS_Shared_type);

extern int AMSP_Get_Reduction_type_Field(Field *, AMS_Reduction_type *);
extern int AMSP_Set_Reduction_type_Field(Field *, AMS_Reduction_type);

#endif
