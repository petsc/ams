/*
 * Miscellaneous functions for user command processing.
 */

#include        <stdio.h>
#include        <string.h>

#include        "cmd.h"

#define MAXLINE 255

/* all of the following functions are in cmd.c */
int     cmd_attach_communicator(), cmd_connect(), cmd_exit(),
    cmd_help(), cmd_modify_field(), cmd_print_memory_list(), cmd_print_memory_fields(),
    cmd_lock_memory(), cmd_unlock_memory(), cmd_print_field(), cmd_exit(), cmd_set_focus_on_memory(), 
    cmd_set_focus_on_communicator(), cmd_set_opt(), cmd_status(), cmd_verbose();

Cmds    commands[] = {  /* keep in alphabetical order for binary search */
  {"?",                cmd_help},
  {"ac",               cmd_attach_communicator},
  {"connect",          cmd_connect},
  {"exit",             cmd_exit},
    {"help",             cmd_help},
    {"lm",               cmd_lock_memory},
    {"mf",               cmd_modify_field},
    {"pf",               cmd_print_field},
    {"pm",               cmd_print_memory_fields},
    {"pml",              cmd_print_memory_list},
    {"quit",             cmd_exit},
    {"set",              cmd_set_opt},
    {"sfc",              cmd_set_focus_on_communicator},
    {"sfm",              cmd_set_focus_on_memory},
    {"status",           cmd_status},
    {"um",               cmd_unlock_memory},
    {"verbose",          cmd_verbose}
};
#define NCMDS   (sizeof(commands) / sizeof(Cmds))

int     ncmds = NCMDS;

const char    *ahelp[] = {
    "?       - to get this help ",
    "ac      - to attach communicator. Syntax: ac <comm_name>",
    "connect - to connect to publisher. Syntax: connect <hostname> [port#] ",
    "exit    - exit the interpreter",
    "help    - to get this help",
    "lm      - to lock the publisher's copy of the memory, thus stopping it. \n            Syntax: lm <mem_name> [timeout]. The timeout is in milliseconds",
    "mf      - to modify a field value. Syntax: mf <fld_name> new_value",
    "pf      - to print the content of a field. Syntax: pf <fld_name>\n            The field must be in current focus memory",
    "pm      - to print memory fields. Syntax: pm [mem_name]\n            If memory name is ommitted, the current focus memory is printed",
    "pml     - to print the list of published memory. Syntax: pml",
    "quit    - to exit the interpreter",
    "set     - to set an option. Syntax: set attach_seldom 1 to avoid re-attaching\n            and set attach_seldom 0 to re-attach every connection.\n            set output_file log.txt will direct AMS output to the file log.txt.\n            If the file name is ommited stderr is used.",
    "sfc     - to set focus on a communicator. Syntax: sfc <comm_name>\n            This will make this communicator the default for memory accesses",
    "sfm     - to set focus on a memory. Syntax: sfm <mem_name>\n            This will make this memory the default for fields accesses",
    "status  - to get current status of the monitor variables",
    "um      - to unlock the publisher's copy of the memory, thus unblocking it ",
    "verbose - not implemented yet"
};

static char     line[MAXLINE] = { 0 };
static char     *lineptr = NULL;

/*
 * Fetch the next command line.
 * For interactive use or batch use, the lines are read from a file.
 *
 * Return 1 if OK, else 0 on error or end-of-file.
 */

int nextline(FILE *fp)
{
    if (fgets(line, MAXLINE, fp) == NULL)
        return(0);              /* error or end-of-file */
    lineptr = line;

    return(1);
}

/*
 * Fetch the next token from the input stream.
 * We use the line that was set up in the most previous call to
 * nextline().
 *
 * Return a pointer to the token (the argument), or NULL if no more exist.
 */

char * nexttoken(char token[])
{
    register int        c;
    register char       *tokenptr;

    while ((c = *lineptr++) == ' ' || c == '\t')
        ;               /* skip leading white space */

    if (c == '\0' || c == '\n')
        return(NULL);   /* nothing there */

    tokenptr = token;
    *tokenptr++ = c;    /* first char of token */

    /*
     * Now collect everything up to the next space, tab, newline, or null.
     */

    while ((c = *lineptr++) != ' ' && c != '\t' && c != '\n' && c != '\0')
        *tokenptr++ = c;

    *tokenptr = 0;              /* null terminate token */
    return(token);
}

/*
 * Verify that there aren't any more tokens left on a command line.
 */

int checkend()
{
    return 0;
}

/*
 * Perform a binary search of the command table
 * to see if a given token is a command.
 */

int binary(char *word, int n)
{
    register int        low, high, mid, cond;

    low  = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if ( (cond = strcmp(word, commands[mid].cmd_name)) < 0)
            high = mid - 1;
        else if (cond > 0)
            low = mid + 1;
        else
            return(mid);        /* found it, return index in array */
    }
    return(-1); /* not found */
}


/*
 * Execute a command.
 * Call the appropriate function.  If all goes well, that function will
 * return, otherwise that function may call an error handler, which will
 * call longjmp() and branch back to the main command processing loop.
 */

int docmd(char *cmdptr)
{
    int i;
    if ( (i = binary(cmdptr, ncmds)) < 0)
        err_cmd(cmdptr);
    else
        (*commands[i].cmd_func)();

    checkend();

    return 0;
}



/*
 * User command error.
 * Print out the command line too, for information.
 */

int err_cmd(const char *str)
{
    if (strlen(str) > 0)
        fprintf(stderr, ": %s", str);
    fprintf(stderr, "\n");
    fflush(stderr);

    return 0;
}
