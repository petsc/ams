/*
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#ifdef _WIN_ALICE 
#include <windows.h>
#endif
#include "ams.h"

int main()
{

    AMS_Comm comm;              /* Communicator */
    AMS_Comm comm1;              /* Communicator */
    AMS_Comm comm2;              /* Communicator */
    AMS_Memory memory, m[20], m1, m2, chg_mem;	/* Memory */
    char *msg;                          /* Pointer to error messages */
    int int_elem = 1;           /* A integer field to be published */
    float flt_elem = 2.5;         /* A Float field to be published */
    float float_arr[20];        /* A field of arrays to be published */
	float flt_elem1 = 3.0;
    int done = 0, i, err, count = 0;
	char buff[50];

	struct myname {
            int int_elem; /* Integer element */
            float flt_arr[200]; /* An array of float elements */
            char *name; /* A character string */
	} s;

	s.name = (char *) malloc(200);
	strcpy(s.name, "ALICE Brown bag string");

    /* 
     * First, create and publish a communicator 
     * "simple": communicator name
     * &comm: pointer to the communicator variable. Upon return, this
     * variable will contain the communicator id
     * NODE_TYPE: to indicate that the following parameters are the
     * list of hostnames and port numbers to be used for this communicator
     * NULL: means use default hostname
     * NULL: lets the system select a port number.
     */
    err = AMS_Comm_publish("simple", &comm, NODE_TYPE, NULL , NULL);

    /* 
     * This function checks the error and prints an error message. Call 
     * this function after each API call to check error codes.
     */
    AMS_Check_error(err, &msg);

    /* 
     * Creates a memory, named "simple_memory"
     * comm: the communicator this memory is attached to
     * "simple_memory": the name of the memory
     * &memory: a pointer to the memory variable. Upon return, this variable
     * will contain the memory id.
     */
    err = AMS_Memory_create(comm, "simple_memory", &memory);
    AMS_Check_error(err, &msg);

    /* 
     * Add a field to the memory 
     * memory: memory id
     * "int_elem": field name
     * &field_int: address of the field
     * 1: number of elements in the field. For an array put the number
     * of element of the array.
     * AMS_BOOLEAN: field type (BOOLEAN)
     * AMS_WRITE: access type (WRITE)
     * AMS_COMMON: Shared type (Here the field is COMMON which means each processor
     * has a complete copy of the field
     * AMS_REDUCT_UNDEF: No reduction is applied to the field. This is always
     * the case if the shared type is COMMON
     */
    err = AMS_Memory_add_field(memory, "int_elem", &int_elem, 1, AMS_BOOLEAN,
                               AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /*
     * Add a float field 
     */
    err = AMS_Memory_add_field(memory, "flt_elem", &flt_elem, 1, AMS_FLOAT,
                               AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /*
     * Add an array of floats that is read only to the memory 
     */
    err = AMS_Memory_add_field(memory, "float_arr", float_arr, sizeof(float_arr)/sizeof(float), AMS_FLOAT,
                               AMS_READ, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

	err = AMS_Memory_add_field(memory, "flt_arr", s.flt_arr, 200, AMS_FLOAT,
                               AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);

	err = AMS_Memory_add_field(memory, "name", &(s.name), 1, AMS_STRING,
                               AMS_READ, AMS_COMMON, AMS_REDUCT_UNDEF);

    /* 
     * Publish the memory. Once the fields are added you need to
     * publish the memory, i.e. make it available for accessors.
     */
    err = AMS_Memory_publish(memory);
    AMS_Check_error(err, &msg);

	
	/*
	 * More memories 
	 */
	for (i=0; i<20; i++) {
		sprintf(buff,"m%d", i);
		err = AMS_Memory_create(comm, buff, m+i);
		AMS_Check_error(err, &msg);

		err = AMS_Memory_add_field(m[i], "flt_elem1", &flt_elem1, 1, AMS_FLOAT,
								   AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
		AMS_Check_error(err, &msg);

		err = AMS_Memory_publish(m[i]);
		AMS_Check_error(err, &msg);
	}

    /*
     * A memory that will be recreated very often
     */

    err = AMS_Memory_create(comm, "chg_memory", &chg_mem);
	AMS_Check_error(err, &msg);

	err = AMS_Memory_add_field(chg_mem, "new_elem", &flt_elem1, 1, AMS_FLOAT,
								   AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
	AMS_Check_error(err, &msg);

	err = AMS_Memory_publish(chg_mem);
	AMS_Check_error(err, &msg);

    /*
     * Publish the communicator
     */
    err = AMS_Comm_publish("simple1", &comm1, NODE_TYPE, NULL , NULL);
    AMS_Check_error(err, &msg);

    err = AMS_Memory_create(comm1, "simple_memory1", &m1);
    AMS_Check_error(err, &msg);
    
    err = AMS_Memory_add_field(m1, "flt_elem1", &flt_elem1, 1, AMS_FLOAT,
								   AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
	AMS_Check_error(err, &msg);

    err = AMS_Memory_publish(m1);
    AMS_Check_error(err, &msg);

    err = AMS_Comm_publish("simple2", &comm2, NODE_TYPE, NULL , NULL);
    AMS_Check_error(err, &msg);

    err = AMS_Memory_create(comm2, "simple_memory2", &m2);
    AMS_Check_error(err, &msg);

    err = AMS_Memory_add_field(m2, "flt_elem1", &flt_elem1, 1, AMS_FLOAT,
								   AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
	AMS_Check_error(err, &msg);

    err = AMS_Memory_publish(m2);
    AMS_Check_error(err, &msg);


    /*
     * Start doing some real stuffs
     */

    count = 0;
    printf("Starting... \n");
    while(!done)
        {

            for(i = 0; i < 20; i++) {
                /* Take access */
                err = AMS_Memory_take_write_access(memory);
                AMS_Check_error(err, &msg);

               if (float_arr[i] || i%2)
                    float_arr[i] = 0;
                else
                    float_arr[i] = (float) int_elem;
                
                /* Grant Access */
                err = AMS_Memory_grant_write_access(memory);
                AMS_Check_error(err, &msg);
            }

            /* Destroy one of the memories */
            err = AMS_Memory_destroy(chg_mem);
            AMS_Check_error(err, &msg);

            /* Recreate the memory ... */
            err = AMS_Memory_create(comm, "chg_memory", &chg_mem);
	        AMS_Check_error(err, &msg);
            
            /* With a new field everytime */
            srand( (unsigned)time( NULL ) );
            sprintf(buff,"new_field(%d)", rand());
	        err = AMS_Memory_add_field(chg_mem, buff, &flt_elem1, 1, AMS_FLOAT,
            AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
	        AMS_Check_error(err, &msg);

	        err = AMS_Memory_publish(chg_mem);
	        AMS_Check_error(err, &msg);

            count = count + 1;
            if ((count%10000) == 0 ) {
                printf(" I am in the loop %d\n", count);
            }

            /* Are we done? */
            if (int_elem == 0) {
                printf(" int_elem is now 0\n");
                done = 1;
            }
        }

    /*
     * Once I am done, destroy all the objects before leaving
     */

    /* This will destroy all the fields within the memory */
    printf("Destroying all the memories \n");
    err = AMS_Memory_destroy(memory);
    AMS_Check_error(err, &msg);

    for (i=0; i<20; i++) {
		err = AMS_Memory_destroy(m[i]);
		AMS_Check_error(err, &msg);
	}

    err = AMS_Memory_destroy(chg_mem);
    AMS_Check_error(err, &msg);

    printf("Destroying all the communicators \n");
    err = AMS_Comm_destroy(comm);
    AMS_Check_error(err, &msg);

    err = AMS_Comm_destroy(comm1);
    AMS_Check_error(err, &msg);

    err = AMS_Comm_destroy(comm2);
    AMS_Check_error(err, &msg);

    return (0);

}


