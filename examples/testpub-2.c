/*
 * ex2.c: This example an example on how to create publish
 * a multi-dimensional array.
 */

#include <stdio.h>
#include <math.h>

#ifdef _WIN_ALICE 
#include <windows.h>
#endif
#include "ams.h"

int main()
{

    AMS_Comm comm;                /* Communicator */
    AMS_Memory memory;            /* Memory */
    char *msg;                    /* Pointer to error messages */
    float flt_elem = 2.5;         /* A Float field to be published */

    float array_1d[100];           /* A 1-d array to be published */
    int dim1 = 1;                  /* Number of dimension of the array */
    int start_ind1[1], end_ind1[1]; /* Array of starting and ending indices */

    double array_2d[200][100];      /* A 2-d array to be published */
    int dim2 = 2;                  /* Number of dimension of the array */
    int start_ind2[2], end_ind2[2]; /* Array of starting and ending indices */

    float array_3d[50][50][50];      /* A 3-d array to be published */
    int dim3 = 3;                  /* Number of dimension of the array */
    int start_ind3[3], end_ind3[3]; /* Array of starting and ending indices */

    int done = 0, i, j, k, err;
    float x, y, z;
    /* 
     * First, create and publish a communicator 
     * "multi_dims": communicator name
     * &comm: pointer to the communicator variable. Upon return, this
     * variable will contain the communicator id
     * NODE_TYPE: to indicate that the following parameters are the
     * list of hostnames and port numbers to be used for this communicator
     * NULL: means use default hostname
     * NULL: lets the system select a port number.
     */
    err = AMS_Comm_publish("multi_dims", &comm, NODE_TYPE, NULL , NULL);

    /* 
     * This function checks the error and prints an error message. Call 
     * this function after each API call to check error codes.
     */
    AMS_Check_error(err, &msg);

    /* 
     * Creates a memory, named "memory"
     * comm: the communicator this memory is attached to
     * "simple_memory": the name of the memory
     * &memory: a pointer to the memory variable. Upon return, this variable
     * will contain the memory id.
     */
    err = AMS_Memory_create(comm, "memory", &memory);
    AMS_Check_error(err, &msg);

    /* 
     * Add a field to the memory 
     * memory: memory id
     * "flt_elem": field name
     * &flt_elem: address of the field
     * 1: number of elements in the field. For an array put the number
     * of element of the array.
     * AMS_FLOAT: field type (FLOAT)
     * AMS_WRITE: access type (WRITE)
     * AMS_COMMON: Shared type (Here the field is COMMON which means each processor
     * has a complete copy of the field
     * AMS_REDUCT_UNDEF: No reduction is applied to the field. This is always
     * the case if the shared type is COMMON
     */

    err = AMS_Memory_add_field(memory, "flt_elem", &flt_elem, 1, AMS_FLOAT,
                               AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /*
     * Add a 1-d array of floats that is read only to the memory 
     */
    err = AMS_Memory_add_field(memory, "array_1d", &array_1d, sizeof(array_1d)/sizeof(float), AMS_FLOAT,
                               AMS_READ, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /*
     * Indicate that that this is a 1-d array and initialize the indices.
     * For a single processor, it is not required to this. By defaults
     * the following values will be used. For multi-processor program,
     * you need to specify the starting indices and ending indices in
     * the global array.
     */

    start_ind1[0] = 0;   /* Starting index in the first dimension */
    end_ind1[0] = 99;    /* Ending index in the first dimension */

    /*
     * This would be an array of 200 (again, not needed for uni-processors)
     */ 
    err = AMS_Memory_set_field_block(memory, "array_1d", dim1, start_ind1, 
                                     end_ind1);
    AMS_Check_error(err, &msg);

    /*
     * Add a 2-d array of floats that is read only to the memory 
     */
    err = AMS_Memory_add_field(memory, "array_2d", &array_2d, sizeof(array_2d)/sizeof(double), AMS_DOUBLE,
                               AMS_READ, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /*
     * Indicate that that this is a 2-d array and initialize the indices
     */

    start_ind2[0] = 0;   /* Starting index in the first dimension */
    end_ind2[0] = 199;    /* Ending index in the first dimension */

    start_ind2[1] = 0;   /* Starting index in the second dimension */
    end_ind2[1] = 99;    /* Ending index in the second dimension */

    /*
     * This would be an array of 200 x 100
     */ 
    err = AMS_Memory_set_field_block(memory, "array_2d", dim2, start_ind2, 
                                     end_ind2);
    AMS_Check_error(err, &msg);

    /*
     * Add a 3-d array of floats that is read only to the memory 
     */
    err = AMS_Memory_add_field(memory, "array_3d", &array_3d, sizeof(array_3d)/sizeof(float), AMS_FLOAT,
                               AMS_READ, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /*
     * Indicate that that this is a 3-d array and initialize the indices
     */

    start_ind3[0] = 0;   /* Starting index in the first dimension */
    end_ind3[0] = 49;    /* Ending index in the first dimension */

    start_ind3[1] = 0;   /* Starting index in the second dimension */
    end_ind3[1] = 49;    /* Ending index in the second dimension */

    start_ind3[2] = 0;   /* Starting index in the second dimension */
    end_ind3[2] = 49;    /* Ending index in the second dimension */

    /*
     * This would be an array of 50 x 50 x 50
     */ 
    err = AMS_Memory_set_field_block(memory, "array_3d", dim3, start_ind3, 
                                     end_ind3);
    AMS_Check_error(err, &msg);

    /* 
     * Publish the memory. Once the fields are added you need to
     * publish the memory, i.e. make it available for accessors.
     */
    err = AMS_Memory_publish(memory);
    AMS_Check_error(err, &msg);

    /*
     * Stat doing some real stuffs
     */

    printf("Starting a multi-dimensions array example ... \n");
    while(!done)
        {
            /* Take write access */
            err = AMS_Memory_take_write_access(memory);
            AMS_Check_error(err, &msg);

            /* Update the 1d array (f(x) = sin(4x*(Pi*l))) */
            for(i = 0; i < 100; i++) {
                array_1d[i] = sin(4.0*(i/100.0)*(2*3.1415));
            }

            /* Update the 2d array f(x, y) = sqrt(...) */
            for(i = 0; i < 200; i++) {
                for (j = 0; j < 100; j++) {
                    array_2d[i][j] = (double) sqrt(flt_elem*i*j*(199.0 -i)*(99.0-j));
                }
            }

            /* Update the 3d array */
            for(i = 0; i < 50; i++) {
                for (j = 0; j < 50; j++) {
                    for (k = 0; k < 50; k++) {
                        x = -1.0 + i/25.0; /* -1, 1 */
                        y = -1.0 + j/25.0; /* -1, 1 */
                        z = -1.0 + k/25.0; /* -1, 1 */
                        array_3d[i][j][k] = x*x + 2*y*y + 3*z*z + y*z;
                    }
                }
            }

            if (flt_elem == 0)
                done = 1;

            /* Grant Write Access */
            err = AMS_Memory_grant_write_access(memory);
            AMS_Check_error(err, &msg);

        }

    /*
     * Once I am done, destroy all the objects before leaving
     */

    /* This will destroy all the fields within the memory */
    printf("Destroying the memory \n");
    err = AMS_Memory_destroy(memory);
    AMS_Check_error(err, &msg);

    printf("Destroying the communicator \n");
    err = AMS_Comm_destroy(comm);

    return (0);

}


