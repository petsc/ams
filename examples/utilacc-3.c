/*
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#ifdef _WIN_ALICE 
#include <windows.h>
#endif
#include "ams.h"

int mainacc(void)
{

    AMS_Comm   comm;              /* Communicator */
    AMS_Memory memory;
    char       **comm_list,**mem_list,**fld_list;
    int        err;
    char *msg;                          /* Pointer to error messages */
    int        i = 0;
    AMS_Memory_type mtype;
    AMS_Data_type dtype;
    AMS_Shared_type stype;
    AMS_Reduction_type rtype;
    int len,*int_elem;
    void *addr;

    err = AMS_Connect("localhost", -1, &comm_list);
    AMS_Check_error(err, &msg);

    while (comm_list[i]) {
      printf("Communicator %s\n",comm_list[i]);
      i++;
    }
    err = AMS_Comm_attach(comm_list[0],&comm);
    AMS_Check_error(err, &msg);
    err = AMS_Comm_get_memory_list(comm,&mem_list);
    AMS_Check_error(err, &msg);

    i = 0;
    while (mem_list[i]) {
      printf("Memory %s\n",mem_list[i]);
      i++;
    }
    err = AMS_Memory_attach(comm,mem_list[0],&memory,NULL);
    AMS_Check_error(err, &msg);

    err = AMS_Memory_get_field_list(memory, &fld_list);
    i = 0;
    while (fld_list[i]) {
      printf("Field %s\n",fld_list[i]);
      err = AMS_Memory_get_field_info(memory, fld_list[i], &addr, &len, &dtype, &mtype, &stype, &rtype);
      AMS_Check_error(err, &msg);
      i++;
    }
    /* check the value of int_elm on the publisher */
    err = AMS_Memory_get_field_info(memory, "int_elem", (void**)&int_elem, &len, &dtype, &mtype, &stype, &rtype);
    AMS_Check_error(err, &msg);
    printf("int_elem %d\n",*int_elem);

    /* change the value of int_elm on the publisher */
    *int_elem = 0;
    err = AMS_Memory_update_send_begin(memory);
    err = AMS_Memory_update_send_end(memory);

    err = AMS_Disconnect();
    AMS_Check_error(err, &msg);

    return 0;
}


