/*
 * Command processing functions, one per command.
 * (Only the accessor side processes user commands.)
 * In alphabetical order.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cmd.h"

int Connected = 0;
int Attached = 0;
int Verbose = 0;
int attach_seldom = 0;
char  MONT_cur_mem_name[255];
AMS_Memory MONT_cur_mem = -1;

static char *my_list[200];

/*
 * External Variables definition
 */

char token[255], *prg, MONT_cur_mem_name[255], com_name[255];
AMS_Comm ams;
/*
 * at <comm>
 * Attaches a communicator 
 */

int cmd_attach_communicator()
{
    int err;
    char *msg;
    extern int cmd_print_memory_list();

    if (Connected == 0) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (nexttoken(com_name) == NULL) {
        err_cmd("missing communicator name");
        return 0;
    }

    /* Attach to a Communicator */
    err = AMS_Comm_attach(com_name, &ams);
    AMS_Check_error(err, &msg);

    Attached = 1;
	MONT_cur_mem_name[0] = 0;
	MONT_cur_mem = -1;

    printf("Communicator %s has been attached \n", com_name);
    printf("Published memory(ies): \n");
    cmd_print_memory_list();

    return 0;
}

/*
 * mf <field> new_val
 * Modifies a field 
 */

int cmd_modify_field()
{
    char fld[255], new_val[10000], other_val[255];
    char *msg;
    int err;

    if (Connected == 0) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (Attached == 0) {
        err_cmd("Please attach a communicator first. Syntax: ac your_comm_name");
        return 0;
    }

    if (nexttoken(fld) == NULL) {
        err_cmd("missing field name");
        return 0;
    }

    if (nexttoken(new_val) == NULL) {
        printf("Enter new value: ");
        gets(new_val);
        if (!strlen(new_val)) {
            err_cmd("missing new value");
            return 0;
        }
    }

    /* Read extra values for arrays */
    while (nexttoken(other_val)) {
        strcat(new_val, " ");
        strcat(new_val, other_val);
    }

    if (strlen(MONT_cur_mem_name) == 0) {
        err_cmd("Please set focus on a particular memory: sfm <mem_nam>");
        return 0;
    }

    /* Modify a field */
    err = ModifyField(ams, MONT_cur_mem_name, fld, new_val);
    AMS_Check_error(err, &msg);

    /* Update accessor copy */
    err = RecvUpdate(ams, MONT_cur_mem_name);
    AMS_Check_error(err, &msg);

    return 0;
}

/*
 * pml
 * Print memory list
 */

int cmd_print_memory_list()
{
    int err, i = 0;
    char **mem_list, *msg;
    if (!Connected) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (Attached == 0) {
        err_cmd("Please attach a communicator first. Syntax: ac your_comm_name");
        return 0;
    }

    /* Get the memory list */
    err = AMS_Comm_get_memory_list(ams, &mem_list);
    AMS_Check_error(err, &msg);
        
    while (mem_list[i] && i < 200) {
        my_list[i] = (char *)realloc(my_list[i], sizeof(char)*(strlen(mem_list[i])+1));
        if (my_list[i] == NULL) {
            printf(" I can't beleive it! I am already out of memory \n");
            exit(0);
        }
        strcpy(my_list[i], mem_list[i]);
        printf("%s\n", mem_list[i++]);
    }

    mem_list[i] = NULL; /* Terminate */

    return 0;
}

/*
 * pm [mem_name]
 * Print memory's fields
 */

int cmd_print_memory_fields()
{
    char mem[4096], buf[4096], *msg;
    int err;

    if (!Connected) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (Attached == 0) {
        err_cmd("Please attach a communicator first. Syntax: ac your_comm_name");
        return 0;
    }

    if (nexttoken(mem)) {
		strcpy(buf, mem);
		strcpy(MONT_cur_mem_name, strtok(buf, "|"));
	}

    if (strlen(MONT_cur_mem_name) == 0) {
        err_cmd("Please set focus on a particular memory: sfm <mem_nam>");
        return 0;
    }
    err = PrintMemory(ams, mem);
    AMS_Check_error(err, &msg);

    return 0;
}


/*
 * lm [mem_name] [timeout]
 * Lock the publisher copy of memory
 */

int cmd_lock_memory()
{
    char mem[4096], buf[4096], *msg;
    int err, timeout;

    if (!Connected) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (Attached == 0) {
        err_cmd("Please attach a communicator first. Syntax: ac your_comm_name");
        return 0;
    }

    if (nexttoken(mem)) {
		strcpy(buf, mem);
		strcpy(MONT_cur_mem_name, strtok(buf, "|"));
	}

    if (strlen(MONT_cur_mem_name) == 0) {
        err_cmd("Please set focus on a particular memory: sfm <mem_nam>");
        return 0;
    }

     if (nexttoken(buf)) {
        /* There is a timeout */
		timeout = atoi(buf);
     } else {
         timeout = 0;
     }

    err = LockMemory(ams, mem, timeout);
    AMS_Check_error(err, &msg);

    return 0;
}

/*
 * um [mem_name]
 * Unlock the publisher copy of memory
 */

int cmd_unlock_memory()
{
    char mem[4096], buf[4096], *msg;
    int err;

    if (!Connected) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (Attached == 0) {
        err_cmd("Please attach a communicator first. Syntax: ac your_comm_name");
        return 0;
    }

    if (nexttoken(mem)) {
		strcpy(buf, mem);
		strcpy(MONT_cur_mem_name, strtok(buf, "|"));
	}

    if (strlen(MONT_cur_mem_name) == 0) {
        err_cmd("Please set focus on a particular memory: sfm <mem_nam>");
        return 0;
    }
    err = UnlockMemory(ams, mem);
    AMS_Check_error(err, &msg);

    return 0;
}

/*
 * pf <fld_name>
 * Print field
 */

int cmd_print_field()
{
    int err;
    char fld_name[255], *msg;

    if (!Connected) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (Attached == 0) {
        err_cmd("Please attach a communicator first. Syntax: ac your_comm_name");
        return 0;
    }

    if (nexttoken(fld_name) == NULL) {
        err_cmd("Missiging field name");
        return 0;
    }

    if (strlen(MONT_cur_mem_name) == 0) {
        err_cmd("Please set focus on a particular memory: sfm <mem_nam>");
        return 0;
    }

    /* Update accessor copy */
    err = RecvUpdate(ams, MONT_cur_mem_name);
    AMS_Check_error(err, &msg);

    /* Print Field */
    err = PrintField(ams, MONT_cur_mem_name, fld_name);
    AMS_Check_error(err, &msg);

    return 0;
}

/*
 * sfc <comm>
 */

int cmd_set_focus_on_communicator()
{
    err_cmd("Not implemented yet");
    return 0;
}
 
/*
 * sfm <mem_name>
 */

int cmd_set_focus_on_memory()
{
  char mem[255];
  int i = 0;;

    if (Connected == 0) {
        err_cmd("Not Connected. Use: connect <hostname> [port#]");
        return 0;
    }

    if (Attached == 0) {
        err_cmd("Please attach a communicator first. Syntax: ac your_comm_name");
        return 0;
    }

    if (nexttoken(mem) == NULL) {
        err_cmd("Missing memory name");
        return 0;
    }

    /* Make sure the memory is in the list */
    while (my_list[i]) {
        if (strcmp(my_list[i], mem) == 0)
            break;
        i++;
    }

    if (my_list[i] == NULL) {
        err_cmd("memory not found");
        return 0;
    }

    strcpy(MONT_cur_mem_name, mem);

    return 0;
}

/*
 * set <opt> <val>
 *
 *  Set the given option to the given value.
 */

int cmd_set_opt()
{
    char opt[255], val[255], *msg;
    int int_val, err;

    if(nexttoken(opt)) {

        if((strlen(opt) == strlen("attach_seldom"))
           && (strcmp(opt, "attach_seldom") == 0)) {
            /* Option: attach_seldom */
            if(nexttoken(val)) {
                int_val = atoi(val);
                attach_seldom = int_val;
            }
            else
                attach_seldom = 1;
        } else if(strcmp(opt, "output_file") == 0) {
            /* Option: output_file */

            if(nexttoken(val)) {
                err = AMS_Set_output_file(val);
                AMS_Check_error(err, &msg);
            } else {
                err = AMS_Set_output_file(NULL);
                AMS_Check_error(err, &msg);
            }
        } else {
                printf(" Unknow option %s \n",opt);
        }

    }

    return 0;
}

/*
 * connect <hostname> [ <port> ]
 *
 *      Set the hostname and optional port number for future transfers.
 */

int cmd_connect()
{
    int port, err, i;
    int k, hostnum, commnamelen; /* for indenting */
    char host[255], **comm_list, *buff = NULL, *p, *msg;

    if (nexttoken(host) == NULL) {
        err_cmd("missing hostname");
        return 0;
    }

    if (nexttoken(token)) {
        port = atoi(token);
        if (port < 0) {
            err_cmd("invalid port number");
            return 0;
        }
    } else port = -1; /* Use defaul port */

    /* Get the communicators' list */
    err = AMS_Connect(host, port, &comm_list);
    AMS_Check_error(err, &msg);
    
    if ((strstr(host, "LDAP:") == NULL) && comm_list) {
        /* We are connecting to a publisher */
        printf("Connected. The following communicators are published: \n");
        i = 0;
        while (comm_list[i] && i < MAX_COMM) {
		    buff = (char *) realloc(buff, sizeof(char)* (strlen(comm_list[i])+1));
		    if (buff == NULL)
			    err_cmd(" Not enough memory ");

            strcpy(buff, comm_list[i]);
            p = strtok(buff, "|");
            printf("\t %s ", p);
            commnamelen = strlen(p);
            p = strtok(NULL, "|");
            hostnum = 0;
    	    while(p) {
                if(hostnum > 0) {
                    /* Indent properly */
                    printf("\t  ");
                    for(k = 0; k < commnamelen; ++k)
                        putchar(' ');
                }
                printf("(host = %s) ", p);
                p = strtok(NULL, "|");
                printf("(port = %s) \n", p);
                p = strtok(NULL, "|");
                ++hostnum;
	        }
            i++;
        }

        Connected = 1;
    } else if (comm_list) {
        /* We are connecting to an LDAP publisher */
        
        i = 0;
        while (comm_list[i] && i < MAX_COMM) {
            if (i == 0)
                printf("The following AMS Publishers are alive: \n");
            
            buff = (char *) realloc(buff, sizeof(char)* (strlen(comm_list[i])+1));
		    if (buff == NULL)
			    err_cmd(" Not enough memory ");

            strcpy(buff, comm_list[i]);

            /* Get dn first */
            p = strtok(buff, "|");

            /* Get hostname */
            p = strtok(NULL, "|");
            printf("(host = %s) ", p);

            /* Get port number */
            p = strtok(NULL, "|");
            printf("(port = %s) \n", p);

            i++;
        }

        if (i == 0)
            printf("There are no alive AMS Publishers\n");
    } else {
        printf(" There is no AMS Publisher alive\n");
    }

    if (Verbose)
        printf("Use: ""ac comm_name"" to attach to a communicator \n");

	if (buff)
		free(buff);
    return 0;

}

/*
 * exit
 */

int cmd_exit()
{
    exit(0);
    return 0; /* Should not get here */
}


/*
 * help
 */

int cmd_help()
{
    register int        i;

    for (i = 0; i < ncmds; i++) {
        printf("  %s\n", ahelp[i]);
    }
    return 0;
}


/*
 * Show current status.
 */

int cmd_status()
{
    if (Connected)
        printf("Connected\n");
    else
        printf("Not connected\n");

    printf("Active Communicator: %s\n", com_name);
    printf("Active Memory: %s\n", MONT_cur_mem_name);
        
    return 0;
}

/*
 * Toggle verbose mode.
 */

int cmd_verbose()
{
    if (Verbose == 1)
        Verbose = 0;
    else
        Verbose = 1;
    return 0;
}
