#include        <stdio.h>
#include        <stdlib.h>
#include        <string.h>

#include        "cmd.h"

int main(int argc, char **argv)
{
    FILE        *fp;
    extern int mainloop(FILE *);

    /* Option Initialization */
    attach_seldom = 0;
    strcpy(MONT_cur_mem_name, "");

    prg = argv[0];

    fp = stdin;
    mainloop(fp);

    return 0;
}

int mainloop(FILE *fp)
{

    char cmd[255];
    /*
     * Main loop.  Read a command and execute it.
     * This loop is terminated by a "quit" command, or an
     * end-of-file on the command stream.
     */

    printf("(mont)");
    while (nextline(fp)) {
        if (nexttoken(cmd) != NULL)
            docmd(cmd);
        printf("(mont)");
    }

    return 0;
}

