/*
 * This example attempts to access the publisher in the publisher process. This means we DO NOT link against the accessor, rather the 
   publisher has its own functions that implement the accessor interface.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

#ifdef _WIN_ALICE 
#include <windows.h>
#endif
#include "ams.h"

extern int mainacc(void);
extern int mainpub(void);

void *runacc(void *dummy) 
{
  sleep(5);
  mainacc();
  printf("Mainacc() returned\n");
  return 0;
}

int main(int argc,char **args)
{
  pthread_t      thread;

  pthread_create(&thread, NULL, runacc, NULL);
  return mainpub();
}


