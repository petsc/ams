#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ams.h"
#include "cmd.h"


int PrintField(AMS_Comm ams, const char *mname, const char *fld_name)
{
    AMS_Memory mem;
    AMS_Memory_type mtype;
    AMS_Data_type dtype;
    AMS_Shared_type stype;
    AMS_Reduction_type rtype;
    int err, len, i, flag, dim, dim2, *start, *end;
    void *addr;
    char *msg, **tmpstr, *p;

    char buf[4096];

    /* Get the memory id: Attach the memory */
    if(attach_seldom == 0 || strlen(MONT_cur_mem_name) != strlen(mname)
       || strcmp(MONT_cur_mem_name, mname)) {
        err = AMS_Memory_attach(ams, mname, &mem, NULL);
        AMS_Check_error(err, &msg);

		strcpy(buf, mname);
		p = strtok(buf, "|");
        strcpy(MONT_cur_mem_name, p);
        MONT_cur_mem = mem;
    }
    else {
        err = AMS_Memory_update_recv_end(MONT_cur_mem, &flag, NULL);
        AMS_Check_error(err, &msg);
        mem = MONT_cur_mem;
    }

    /* Get Field infos */
    err = AMS_Memory_get_field_info(mem, fld_name, &addr, &len, &dtype, &mtype, &stype, &rtype);
    AMS_Check_error(err, &msg);
    printf("Field %s descriptions: \n", fld_name);
        
    /* Field's Memory type */
    switch(mtype) {
    case AMS_READ:
        strcpy(buf, "Read");
        break;
    case AMS_WRITE:
        strcpy(buf, "Read/Write");
        break;
    default:
        strcpy(buf, "Undefined");
        break;
    }
    printf("\t Memory type: %s \n", buf);

    /* Shared type and Reduction type */
    switch(stype) {
    case AMS_COMMON:
        strcpy(buf, "COMMON");
        break;
    case AMS_DISTRIBUTED:
        strcpy(buf, "Distributed");
        break;
    case AMS_REDUCED:
        switch(rtype) {
        case AMS_MIN:
            strcpy(buf, "Reduced: MIN");
            break;
        case AMS_MAX:
            strcpy(buf, "Reduced: MAX");
            break;
        case AMS_SUM:
            strcpy(buf, "Reduced: SUM");
            break;
        default:
            strcpy(buf, "Reduced: Undefined");
            break;
        }
    default:
        strcpy(buf, "Undefined");
        break;
    }
    printf("\t Shared type: %s \n", buf);


    /*
     * Get the field's dimension info so that it can be displayed correctly
     */
    err = AMS_Memory_get_field_block(mem, fld_name, &dim, &start, &end);
    AMS_Check_error(err, &msg);
    
    if (dim > 1) {
        dim2 = 1;
        for (i = 1; i<dim; i++)
            dim2 = dim2 * (end[i] - start[i] + 1);
    }
    /* Data type and data format */
    switch(dtype) {
    case AMS_INT:
        printf("\t Data type: Integer \n");
        printf("\t Data value: \n");
        if (dim > 1) {
            printf("\t Dim: ("); 
            for (i=0; i<dim; i++) {
                printf("%d", end[i] - start[i]+1);
                if ( i != dim -1)
                    printf(", ");
                else
                    printf(")\n");
            }   
            for (i=0; i<len; i++) {
                if ((i % dim2 ) == 0)
                    printf("\n\t");
                printf(" %d",*((int *)(addr) + i ));
            }
        } else {
            for (i=0; i<len;i++)
                printf("\n\t   [%d] = %d",i, *((int *)(addr) + i ));
        }
        break;

    case AMS_BOOLEAN:
        printf("\t Data type: Boolean \n");
        printf("\t Data value: \n");
        if (dim > 1) {
            printf("\t Dim: ("); 
            for (i=0; i<dim; i++) {
                printf("%d", end[i] - start[i]+1);
                if ( i != dim -1)
                    printf(", ");
                else
                    printf(")\n");
            }   
            for (i=0; i<len; i++) {
                if ((i % dim2 ) == 0)
                    printf("\n\t");
                ( (*((int *)(addr) + i )) ? printf(" TRUE") : printf("FALSE") );
            }
        } else {
            for (i=0; i<len;i++) {
                printf("\n\t   [%d] = ",i);
                ( (*((int *)(addr) + i )) ? printf(" TRUE") : printf("FALSE") );
            }
        }
        break;

    case AMS_DOUBLE:
        printf("\t Data type: Double \n");
        printf("\t Data value: \n");
        if (dim > 1) {
            printf("\t Dim: ("); 
            for (i=0; i<dim; i++) {
                printf("%d", end[i] - start[i] +1);
                if ( i != dim -1)
                    printf(", ");
                else
                    printf(")\n");
            }   
            for (i=0; i<len;i++) {
                if ((i % dim2 ) == 0)
                    printf("\n\t");
                printf(" %8.2f",*((double *)(addr) + i ));
            }
        } else {
            for (i=0; i<len;i++)
            printf("\n\t   [%d] = %8.2f",i, *((double *)(addr) + i ));
        }
        break;

    case AMS_FLOAT:
        printf("\t Data type: Float \n");
        printf("\t Data value: \n");
        if (dim > 1) {
            printf("\t Dim: ("); 
            for (i=0; i<dim; i++) {
                printf("%d", end[i] - start[i] +1);
                if ( i != dim -1)
                    printf(", ");
                else
                    printf(")\n");
            }   
            for (i=0; i<len;i++) {
                if ((i % dim2 ) == 0)
                    printf("\n\t");
                printf(" %8.2f",*((float *)(addr) + i ));
            }
        } else {
            for (i=0; i<len;i++)
                printf("\n\t   [%d] = %8.2f ",i, *((float *)(addr) + i ));
        }
        break;

    case AMS_STRING:
        printf("\t Data type: String \n");
        printf("\t Data value: \n");
        tmpstr = (char **)addr;
        for (i=0; i<len;i++) {
            if (tmpstr[i])
                printf("\n\t   [%d] = %s ",i, tmpstr[i]);
            else
                printf("\n\t   [%d] = null ",i);
        }
        break;
    default:
        printf("\t Data type: Undefined \n");
        break;
    }
        
    printf("\n");

    return AMS_ERROR_NONE;
}

int ModifyField(AMS_Comm ams, const char *mname, const char *fld_name, const char *new_val)
{
    AMS_Memory mem;
    AMS_Memory_type mtype;
    AMS_Data_type dtype;
    AMS_Shared_type stype;
    AMS_Reduction_type rtype;

    int *AMS_int = NULL;
    float *AMS_flt = NULL;
    double *AMS_dbl = NULL;

    int err, len, i;
    void *addr;
    char *msg, val[1024], *p;

    /* Get the memory id: Attach the memory */
    if(attach_seldom == 0 || strlen(MONT_cur_mem_name) != strlen(mname)
       || strcmp(MONT_cur_mem_name, mname)) {
        err = AMS_Memory_attach(ams, mname, &mem, NULL);
        AMS_Check_error(err, &msg);

		strcpy(val, mname);
		p = strtok(val, "|");

        strcpy(MONT_cur_mem_name, p);
        MONT_cur_mem = mem;
    }
    else
        mem = MONT_cur_mem;

    /* Get Field infos */
    err = AMS_Memory_get_field_info(mem, fld_name, &addr, &len, &dtype, &mtype, &stype, &rtype);
    AMS_Check_error(err, &msg);
        
    /* Field's Memory type */
    switch(mtype) {
    case AMS_READ:
        printf(" Field is read only \n");
        return AMS_ERROR_NONE;;
    case AMS_WRITE:
        break;
    default:
        printf("Undefined memory type \n");
        return AMS_ERROR_NONE;;
    }

    /* Data type and data format */
    strcpy(val, new_val);
    switch(dtype) {
    case AMS_INT:
    case AMS_BOOLEAN:
        AMS_int = (int *) malloc(len*sizeof(int));
        if (AMS_int == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        for (i=0; i < len; i++) {
            if (i == 0)
                p = strtok(val, " ,");
            else
                p = strtok(NULL, " ,");

            if (p == NULL) {
                printf(" Missing %d value(s) \n", len - i);
                printf(" Field has %d value(s) and you provided only %d value(s) \n",len, i);
                return AMS_ERROR_BAD_ARGUMENT;
            }
            AMS_int[i] = atoi(p);
        }
                
        addr = (void *)AMS_int;
        break;

    case AMS_DOUBLE:
        AMS_dbl = (double *) malloc(len*sizeof(double));
        if (AMS_dbl == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        for (i=0; i<len; i++) {
            if (i == 0)
                p = strtok(val, " ,");
            else
                p = strtok(NULL, " ,");

            if (p == NULL) {
                printf(" Missing %d value(s) \n", len - i);
                printf(" Field has %d value(s) and you provided only %d value(s) \n",len, i);
                return AMS_ERROR_BAD_ARGUMENT;
            }
            AMS_dbl[i] = atof(p);
        }
                
        addr = (void *)AMS_dbl;
        break;

    case AMS_FLOAT:
        AMS_flt = (float *) malloc(len*sizeof(float));
        if (AMS_flt == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        for (i=0; i < len; i++) {
            if (i == 0)
                p = strtok(val, " ,");
            else
                p = strtok(NULL, " ,");

            if (p == NULL) {
                printf(" Missing %d value(s) \n", len - i);
                printf(" Field has %d value(s) and you provided only %d value(s) \n",len, i);
                return AMS_ERROR_BAD_ARGUMENT;
            }
            AMS_flt[i] = (float) atof(p);
        }
                
        addr = (void *)AMS_flt;
        break;

    default:
        printf("Unsupported data type \n");
        break;
    }

    err = AMS_Memory_set_field_info(mem, fld_name, addr, len);
    AMS_Check_error(err, &msg);

    err = AMS_Memory_update_send_begin(mem);
    AMS_Check_error(err, &msg);

    /* Get rid of the memory */
    switch(dtype) {
    case AMS_INT:
    case AMS_BOOLEAN:
        if (AMS_int)
            free(AMS_int);
        break;
    case AMS_FLOAT:
        if (AMS_flt)
            free(AMS_flt);
        break;
    case AMS_DOUBLE:
        if (AMS_dbl)
            free(AMS_dbl);
        break;
    default:
        printf("undefined AMS_data_type\n");
        break;
    }

    return 0;
}

int PrintMemory(AMS_Comm ams, const char *mname)
{
    char **fld_list, *msg;
    AMS_Memory mem[200];
    int i = 0, err, flag, nbmem = 0, j;
    unsigned int step[200];
	char buff[1024], *p;

	strcpy(buff, mname);
	p = strtok(buff, "|");

	while(p) {
		nbmem += 1;
		p = strtok(NULL, "|");
	}

    /* Attach the memory structure */
    if(attach_seldom == 0 || strlen(MONT_cur_mem_name) != strlen(mname)
       || strcmp(MONT_cur_mem_name, mname)) {

        err = AMS_Memory_attach(ams, mname, mem, step);
        AMS_Check_error(err, &msg);

		strcpy(buff, mname);
		p = strtok(buff, "|");

        strcpy(MONT_cur_mem_name, p);
        MONT_cur_mem = mem[0];
    }
    else {
		nbmem = 1; /* At this time only one memory is updated */
		mem[0] = MONT_cur_mem;
		for (i=0 ;i<nbmem; i++) {
			err = AMS_Memory_update_recv_end(mem[i], &flag, &(step[i]));
			AMS_Check_error(err, &msg);
		}
    }

	strcpy(buff, mname);
	p = strtok(buff, "|");

	for (i=0; i<nbmem && p; i++) {

		err = AMS_Memory_get_field_list(mem[i], &fld_list);
		AMS_Check_error(err, &msg);
	    printf("Memory %s-[%u] contains the following fields\n", p, step[i]);
		
		j = 0;
		while(fld_list[j])
			printf("\t\t %s\n", fld_list[j++]);
		
		/* Frees the memory created within get_field_list */
		free(fld_list);
		p = strtok(NULL, "|");
	}

    return 0;
}

/*
 * Lock the remote memory, thus blocking the publisher's main thread
 */
int LockMemory(AMS_Comm ams, const char *mname, int timeout)
{
    char *msg;
    AMS_Memory mem[200];
    int err;
    unsigned int step[200];
	char buff[1024], *p;

	strcpy(buff, mname);
	p = strtok(buff, "|");

    /* Attach the memory, so that you can the memory id */
    err = AMS_Memory_attach(ams, mname, mem, step);
    AMS_Check_error(err, &msg);

    /* Lock the Memory */
    err = AMS_Memory_lock(mem[0], timeout);
    AMS_Check_error(err, &msg);
    
    return 0;
}

/*
 * UnLock the remote memory, thus blocking the publisher's main thread
 */
int UnlockMemory(AMS_Comm ams, const char *mname)
{
    char *msg;
    AMS_Memory mem[200];
    int err;
    unsigned int step[200];
	char buff[1024], *p;

	strcpy(buff, mname);
	p = strtok(buff, "|");

    /* Attach the memory, so that you can the memory id */
    err = AMS_Memory_attach(ams, mname, mem, step);
    AMS_Check_error(err, &msg);

    /* Lock the Memory */
    err = AMS_Memory_unlock(mem[0]);
    AMS_Check_error(err, &msg);
    
    return 0;
}       

int RecvUpdate(AMS_Comm ams, const char *mname)
{
    AMS_Memory mem[200];
    int err, flag;
    char *msg, buff[1024], *p;

    if(attach_seldom == 0 || strlen(MONT_cur_mem_name) != strlen(mname)
       || strcmp(MONT_cur_mem_name, mname)) {

        err = AMS_Memory_attach(ams, mname, mem, NULL);
        AMS_Check_error(err, &msg);

		strcpy(buff, mname);
		p = strtok(buff, "|");

        strcpy(MONT_cur_mem_name, p);
        MONT_cur_mem = mem[0];

    }
    else {
        err = AMS_Memory_update_recv_end(MONT_cur_mem, &flag, NULL);
        AMS_Check_error(err, &msg);
        mem[0] = MONT_cur_mem;
    }

    return 0;
}


        
