/*
 * simple.c: This example helps in creating publishers, memories
 * and fields.
 */

#include <stdio.h>
#include <math.h>

#ifdef _WIN_ALICE 
#include <windows.h>
#endif
#include "ams.h"
#include "mpi.h"

int main(int argc, char *argv[])
{

    AMS_Comm comm, comm2;              /* Communicator */
    AMS_Memory memory, m[20];  /* Memory */
    char *msg;                          /* Pointer to error messages */
    int int_elem = 1;           /* A integer field to be published */
    float float_arr[20] = {0.0};  /* A field of arrays to be published */
	float flt_elem = 3.02f;
    int done = 0, i, err;
	char buff[20];

    /* First, initialize MPI */
    MPI_Init(&argc, &argv);

    /* 
     * Second, create and publish a communicator 
     * "simple": communicator name
     * &comm: pointer to the communicator variable. Upon return, this
     * variable will contain the communicator id
     * TYPE_TYPE: To indicate that the list of nodes/ports is determined 
     * by MPI.
     */
    err = AMS_Comm_publish("simple", &comm, MPI_TYPE, MPI_COMM_WORLD);

    /* 
     * This function checks the error and prints an error message. Call 
     * this function after each API call to check error codes.
     */
    AMS_Check_error(err, &msg);

    /* 
     * Creates a memory, named "simple_memory"
     * comm: the communicator this memory is attached to
     * "simple_memory": the name of the memory
     * &memory: a pointer to the memory variable. Upon return, this variable
     * will contain the memory id.
     */
    err = AMS_Memory_create(comm, "simple_memory", &memory);
    AMS_Check_error(err, &msg);

    /* 
     * Add a field to the memory 
     * memory: memory id
     * "int_elem": field name
     * &field_int: address of the field
     * 1: number of elements in the field. For an array put the number
     * of element of the array.
     * AMS_INT: field type (INTEGER)
     * AMS_WRITE: access type (WRITE)
     * AMS_COMMON: shared type.  Each processor
     * has a complete copy of the field
     * AMS_REDUCT_UNDEF: No reduction is applied to the field. This is always
     * the case if the shared type is COMMON
     */
    err = AMS_Memory_add_field(memory, "int_elem", &int_elem, 1, AMS_INT,
                               AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /*
     * Add an array of floats that is read only to the memory 
     */
    err = AMS_Memory_add_field(memory, "float_arr", &float_arr, sizeof(float_arr)/sizeof(float), AMS_FLOAT,
                               AMS_READ, AMS_DISTRIBUTED, AMS_REDUCT_UNDEF);
    AMS_Check_error(err, &msg);

    /* 
     * Publish the memory. Once the fields are added you need to
     * publish the memory, i.e. make it available for accessors.
     */
    err = AMS_Memory_publish(memory);
    AMS_Check_error(err, &msg);

    /*
     * Second communicator 
     */
    err = AMS_Comm_publish("simple_2", &comm2, MPI_TYPE, MPI_COMM_WORLD);
    AMS_Check_error(err, &msg);

	/*
	 * More memories 
	 */
	for (i=0; i<20; i++) {

		sprintf(buff,"m%d", i);
		err = AMS_Memory_create(comm2, buff, m+i);
		AMS_Check_error(err, &msg);

		err = AMS_Memory_add_field(m[i], "flt_elem", &flt_elem, 1, AMS_FLOAT,
								   AMS_WRITE, AMS_COMMON, AMS_REDUCT_UNDEF);
		AMS_Check_error(err, &msg);

		err = AMS_Memory_publish(m[i]);
		AMS_Check_error(err, &msg);

	}

    /*
     * Stat doing some real stuffs
     */

    printf("Starting... \n");
    while(!done)
        {

            /* Take access */
            err = AMS_Memory_take_write_access(memory);
            AMS_Check_error(err, &msg);

            for(i = 0; i < 20; i++) {

                if (float_arr[i] || i%2)
                    float_arr[i] = 0;
                else
                    float_arr[i] = (float) int_elem;
            }

            if (int_elem == 0)
                done = 1;
                
            /* Grant Access */
            err = AMS_Memory_grant_write_access(memory);
            AMS_Check_error(err, &msg);

            for(i = 0; i < 20; i+=2) {
				/* Take access */
		        err = AMS_Memory_take_write_access(m[i]);
			    AMS_Check_error(err, &msg);

				flt_elem = (float) i;

				/* Grant Access */
				err = AMS_Memory_grant_write_access(m[i]);
				AMS_Check_error(err, &msg);

            }

            MPI_Barrier(MPI_COMM_WORLD);

        }

    /*
     * Once I am done, destroy all the objects before leaving
     */

    /* This will destroy all the fields within the memory */
    printf("Destroying the memory \n");
    err = AMS_Memory_destroy(memory);
    AMS_Check_error(err, &msg);

    printf("Destroying the communicator \n");
    err = AMS_Comm_destroy(comm);
    AMS_Check_error(err, &msg);

    MPI_Finalize();

    return (0);

}


