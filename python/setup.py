"""
AMS is a software library that uses threads, sockets and locks to
allow a running program (called the accessor) to read (and change when
desired) variables in another running code (called the publisher
application).
"""

import sys, os
from distutils.core import setup, Extension

AMS_DIR = os.environ.get('AMS_DIR', '..')

setup(
    name = "AMS",
    version = "0.1",
    description = "The ALICE Memory Snooper",
    long_description = __doc__,
    url = "http://www.mcs.anl.gov/petsc/petsc-as/documentation/ams.html",
    author = "Lisandro Dalcin",
    author_email = "dalcinl@gmail.com",
    platforms = ["POSIX"],
    license="Public Domain",
    ext_modules = [
        Extension("ams",
                  sources = ["ams.c"],
                  include_dirs = [os.path.join(AMS_DIR, 'include')],
                  library_dirs = [os.path.join(AMS_DIR, 'lib')],
                  libraries = ['amsacc'],
                  ),
        ],
    )
