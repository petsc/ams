#cython: embedsignature=True
#cython: always_allow_keywords=True
#cython: autotestdict=False

__doc__ = """
The ALICE Memory Snooper
"""

include "ams.pxi"
include "err.pxi"
include "mem.pxi"


cdef bytes ams_host = b'localhost'
cdef int   ams_port = -1

cdef char** _connect(bytes host, int port) except NULL:
    cdef char *_host = NULL
    if host is not None:
        _host = host
    cdef char **ams_lst = NULL
    CHKERR( AMS_Connect(_host, port, &ams_lst) )
    assert ams_lst != NULL
    global ams_host
    ams_host = host
    global ams_port
    ams_port = port
    return ams_lst

cdef int _disconnect() except -1:
    CHKERR( AMS_Disconnect() )
    return 0


def connect(bytes host=b'localhost', int port=-1):
    cdef int i = 0
    cdef list lst = []
    cdef char **ams_lst = NULL
    ams_lst = _connect(host, port)
    while ams_lst[i] != NULL:
        if i >= MAX_COMM: break
        name, h, p = ams_lst[i].split(b'|')
        entry = (name, (h, int(p)))
        lst.append(entry)
        i += 1
    return lst


def disconnect():
    _disconnect()


def get_comm_list():
    cdef int i = 0
    cdef list lst = []
    cdef char **ams_lst = NULL
    ams_lst = _connect(ams_host, ams_port)
    while ams_lst[i] != NULL:
        if i >= MAX_COMM: break
        name = ams_lst[i].split(b'|', 1)[0]
        lst.append(name)
        i += 1
    return lst


def get_comm(bytes name):
    cdef Comm obj = Comm(name)
    obj.attach()
    return obj


cdef class Comm:

    cdef bytes name
    cdef AMS_Comm ams

    def __cinit__(self, bytes name):
        self.name = name
        self.ams  = -1

    cpdef attach(self):
        CHKERR( AMS_Comm_attach(self.name, &self.ams) )
        return 0

    cpdef detach(self):
        if self.ams != -1:
            CHKERR( AMS_Comm_detach(self.ams) )
            self.ams = -1

    def get_memory_list(self):
        cdef char **ams_lst = NULL
        assert self.ams != -1
        CHKERR( AMS_Comm_get_memory_list(self.ams, &ams_lst) )
        assert ams_lst != NULL
        cdef int i = 0
        cdef list lst = []
        while ams_lst[i] != NULL:
            name = ams_lst[i]
            lst.append(name)
            i += 1
        return lst

    def get_memory(self, bytes name):
        cdef Memory obj = Memory(name, self)
        obj.attach()
        return obj


cdef class Memory:

    cdef bytes name
    cdef AMS_Memory ams
    cdef unsigned int step
    cdef AMS_Comm parent

    def __cinit__(self, bytes name, Comm comm):
        self.name = name
        self.ams = -1
        self.step = 0
        self.parent = comm.ams

    cpdef attach(self):
        assert self.parent != -1
        CHKERR( AMS_Memory_attach(self.parent, self.name,
                                  &self.ams, &self.step) )

    cpdef detach(self):
        if self.ams != -1:
            CHKERR( AMS_Memory_detach(self.ams) )
            self.ams = -1

    def send(self):
        assert self.ams != -1
        CHKERR( AMS_Memory_update_send_begin(self.ams) )
        CHKERR( AMS_Memory_update_send_end(self.ams) )

    def recv(self):
        assert self.ams != -1
        cdef unsigned int step = 0
        cdef bint changed = 0
        CHKERR( AMS_Memory_update_recv_begin(self.ams) )
        CHKERR( AMS_Memory_update_recv_end(self.ams, &changed, &self.step) )
        return changed

    def lock(self, int timeout=0):
        assert self.ams != -1
        CHKERR( AMS_Memory_lock(self.ams, timeout) )

    def unlock(self):
        assert self.ams != -1
        CHKERR( AMS_Memory_unlock(self.ams) )

    def get_field_list(self):
        assert self.ams != -1
        cdef char **ams_lst = NULL
        CHKERR( AMS_Memory_get_field_list(self.ams, &ams_lst) )
        assert ams_lst != NULL
        cdef int i = 0
        cdef list lst = []
        while ams_lst[i] != NULL:
            name = ams_lst[i]
            lst.append(name)
            i += 1
        return lst

    def get_field(self, bytes name):
        assert self.ams != -1
        cdef Field obj = Field(name, self)
        return obj


cdef class Field:

    cdef bytes name
    cdef void *addr
    cdef int len, elen
    cdef AMS_Data_type      dtype
    cdef AMS_Memory_type    mtype
    cdef AMS_Shared_type    stype
    cdef AMS_Reduction_type rtype
    cdef int dim, *start, *end
    cdef AMS_Memory ams
    cdef void *buf

    def __cinit__(self, bytes name, Memory memory):
        self.name = name
        self.addr = NULL
        self.len = 0
        self.elen = 0
        self.dtype = AMS_DATA_UNDEF
        self.mtype = AMS_MEMORY_UNDEF
        self.stype = AMS_SHARED_UNDEF
        self.rtype = AMS_REDUCTION_UNDEF
        self.dim = 0
        self.start = NULL
        self.end = NULL
        self.ams = memory.ams
        self.buf = NULL

    def __dealloc__(self):
        rmbuf(&self.buf)

    cdef int _load(self) except -1:
        assert self.ams != -1
        CHKERR( AMS_Memory_get_field_info(
                self.ams, self.name,
                &self.addr,  &self.len,
                &self.dtype, &self.mtype,
                &self.stype, &self.rtype) )
        CHKERR( AMS_Memory_get_field_block(
                self.ams, self.name,
                &self.dim, &self.start, &self.end) )

    cdef int _store(self) except -1:
        CHKERR( AMS_Memory_set_field_info(
                self.ams, self.name,
                self.buf, self.len) )
        return 0

    def get(self):
        self._load()
        assert <bint>(self.mtype & (AMS_READ|AMS_WRITE))
        assert self.dtype != AMS_DATA_UNDEF
        assert self.len >= 0
        assert self.addr != NULL
        #assert self.dim == 1 # XXX ?
        ob = getvalue(self.addr, self.len, self.dtype)
        return ob

    def set(self, value):
        self._load()
        assert <bint>(self.mtype & (AMS_WRITE))
        assert self.dtype != AMS_DATA_UNDEF
        assert self.len >= 0
        assert self.addr != NULL
        #assert self.dim == 1 # XXX ?
        cdef void *p = mkbuf(&self.buf, self.len, self.dtype)
        if p == NULL: raise MemoryError
        ob = setvalue(self.buf, self.len, self.dtype, value)
        self._store()

    property value:
        def __get__(self):
            return self.get()
        def __set__(self, v):
            self.set(v)
    property ndim:
        def __get__(self):
            self._load()
            return self.dim
    property shape:
        def __get__(self):
            self._load()
            return tuple([(self.end[0]-self.start[0]+1)
                          for i in range (self.dim)])
    property bounds:
        def __get__(self):
            self._load()
            return tuple([(self.start[0], self.end[0]+1)
                          for i in range (self.dim)])
    property size:
        def __get__(self):
            self._load()
            return self.len
    property typecode:
        def __get__(self):
            self._load()
            return <bytes>typecode(self.dtype)
    property typesize:
        def __get__(self):
            self._load()
            return typesize(self.dtype)

    property info:
        def __get__(self):
            self._load()
            cdef dict d = { }
            #
            k = "memory"
            if   self.mtype == AMS_MEMORY_UNDEF: d[k] = None # XXX ?
            elif self.mtype == AMS_WRITE:        d[k] = "write"
            elif self.mtype == AMS_READ:         d[k] = "read"
            else:                                d[k] = None
            #
            k = "shared"
            if   self.stype == AMS_SHARED_UNDEF:  d[k] = None # XXX ?
            elif self.stype == AMS_COMMON:        d[k] = "common"
            elif self.stype == AMS_REDUCED:       d[k] = "reduced"
            elif self.stype == AMS_DISTRIBUTED:   d[k] = "distributed"
            else:                                 d[k] = None
            #
            k = "reduction"
            if   self.rtype == AMS_REDUCTION_UNDEF: d[k] = None # XXX ?
            elif self.rtype == AMS_SUM:             d[k] = "sum"
            elif self.rtype == AMS_MAX:             d[k] = "max"
            elif self.rtype == AMS_MIN:             d[k] = "min"
            else:                                   d[k] = None
            #
            return d
