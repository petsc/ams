cdef extern from "ams.h" nogil:

    # AMS COMMUNICATOR TYPE
    ctypedef int AMS_Comm
    # SUPPORTED COMMUNICATOR TYPES
    ctypedef enum AMS_Comm_type:
        NODE_TYPE
        MPI_TYPE

    # AMS MEMORY TYPE
    ctypedef int AMS_Memory
    # SUPPORTED MEMORY TYPES
    ctypedef enum AMS_Memory_type:
        AMS_MEMORY_UNDEF
        AMS_READ
        AMS_WRITE
    ctypedef enum AMS_Data_type:
        AMS_DATA_UNDEF
        AMS_BOOLEAN
        AMS_INT
        AMS_FLOAT
        AMS_DOUBLE
        AMS_STRING
    ctypedef enum AMS_Shared_type:
        AMS_SHARED_UNDEF
        AMS_COMMON
        AMS_REDUCED
        AMS_DISTRIBUTED
    ctypedef enum AMS_Reduction_type:
        AMS_REDUCTION_UNDEF "AMS_REDUCT_UNDEF"
        AMS_SUM
        AMS_MAX
        AMS_MIN
    ctypedef enum AMS_Language_type:
        AMS_C
        AMS_CPP
        AMS_JAVA

    enum: DEFAULT_PORT
    enum: MAX_COMM

    # Routines for the accessor of shareable memory
    int AMS_Connect(char *, int, char ***)
    int AMS_Disconnect()
    int AMS_Comm_attach(char *, AMS_Comm *)
    int AMS_Comm_get_memory_list(AMS_Comm, char ***)
    int AMS_Memory_attach(AMS_Comm, char *, AMS_Memory *, unsigned int *step)
    int AMS_Memory_get_field_list(AMS_Memory, char ***)
    int AMS_Memory_get_field_info(AMS_Memory, char *, void **, int *,
                                  AMS_Data_type *, AMS_Memory_type *,
                                  AMS_Shared_type *, AMS_Reduction_type *)
    int AMS_Memory_get_field_block(AMS_Memory, char *, int *, int **, int **)
    int AMS_Memory_set_field_info(AMS_Memory, char *, void *, int)
    int AMS_Memory_update_send_begin(AMS_Memory)
    int AMS_Memory_update_send_end(AMS_Memory)
    int AMS_Memory_update_recv_end(AMS_Memory, int *, unsigned int *)
    int AMS_Memory_update_recv_begin(AMS_Memory)
    int AMS_Memory_detach(AMS_Memory)
    int AMS_Comm_detach(AMS_Comm)

    # Routines available for both the publisher and the accessor
    int AMS_Explain_error(int , char **)
    int AMS_Print(char *, ...)
    int AMS_Set_output_file(char *)
    int AMS_Set_exit_func(void (* )(int))
    int AMS_Set_abort_func(void (* )())
    int AMS_Set_print_func(int (* )(char *, ...))
    int AMS_Memory_lock(AMS_Memory, int)
    int AMS_Memory_unlock(AMS_Memory)

    # Error Codes from AMS high-level API
    enum: AMS_ERROR_NONE
    enum: AMS_ERROR_INVALID_COMMUNICATOR
    enum: AMS_ERROR_INVALID_MEMORY
    enum: AMS_ERROR_INVALID_MEMORY_TYPE
    enum: AMS_ERROR_INVALID_DATA_TYPE
    enum: AMS_ERROR_INVALID_SHARED_TYPE
    enum: AMS_ERROR_INVALID_REDUCTION_TYPE
    enum: AMS_ERROR_PUBLISHER_NO_LONGER_ALIVE
    enum: AMS_ERROR_COMMUNICATOR_NO_LONGER_ALIVE
    enum: AMS_ERROR_INVALID_USER_ARGUMENT
    enum: AMS_ERROR_MPI_TYPE_NOT_SUPPORTED
    enum: AMS_ERROR_INVALID_COMM_TYPE
    enum: AMS_ERROR_NOT_CONNECTED
    enum: AMS_ERROR_INVALID_LANGUAGE_TYPE
    enum: AMS_ERROR_BAD_DIMENSIONS
