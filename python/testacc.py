import ams
from pprint import pprint

def test():
   ams.connect()
   for c in ams.get_comm_list():
      comm = ams.get_comm(c)
      for m in comm.get_memory_list():
         if m == "chg_memory": continue
         mem = comm.get_memory(m)
         mem.lock()
         try:
            for f in mem.get_field_list():
               fld = mem.get_field(f)
               print ( '-' * 70         )
               print ( 'comm:   %s' % c )
               print ( 'memory: %s' % m )
               print ( 'field:  %s' % f )
               pprint( fld.info       )
               pprint( fld.get()        )
         finally:
            mem.unlock()
         mem.detach()
      comm.detach()
   ams.disconnect()

if __name__ == "__main__":
   test()
