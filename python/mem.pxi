#from libc.stdlib cimport malloc, realloc, free
cdef extern from "stdlib.h" nogil:
    void *malloc (size_t SIZE)
    void *realloc (void *PTR, size_t NEWSIZE)
    void free (void *PTR)

cdef inline char typecode(AMS_Data_type dtype) nogil:
    if dtype == AMS_DATA_UNDEF:
        return 0
    elif dtype == AMS_BOOLEAN:
        return 'i'
    elif dtype == AMS_INT:
        return 'i'
    elif dtype == AMS_FLOAT:
        return 'f'
    elif dtype == AMS_DOUBLE:
        return 'd'
    elif dtype == AMS_STRING:
        return 'c'
    else:
        return 0 # XXX

cdef inline size_t typesize(AMS_Data_type dtype) nogil:
    if dtype == AMS_DATA_UNDEF:
        return 1 # XXX
    elif dtype == AMS_BOOLEAN:
        return sizeof(int)
    elif dtype == AMS_INT:
        return sizeof(int)
    elif dtype == AMS_FLOAT:
        return sizeof(float)
    elif dtype == AMS_DOUBLE:
        return sizeof(double)
    elif dtype == AMS_STRING:
        return sizeof(char*)
    else:
        return 1 # XXX

cdef void *rmbuf(void **addr) nogil:
        if addr != NULL:
            if addr[0] != NULL:
                free(addr[0])
                addr[0] = NULL
        return NULL

cdef void *mkbuf(void **addr, size_t size, AMS_Data_type dtype) nogil:
        cdef void *p = NULL
        if addr != NULL:
            p = addr[0]
        if size == 0: size = 1
        p = realloc(p, size*typesize(dtype))
        if addr != NULL:
            if p != NULL:
                addr[0] = p
        return p

cdef bytes mkstr(char *s):
    if s == NULL: 
        return None
    return s

cdef getvalue(void *addr, int count, AMS_Data_type dtype):
    #
    cdef list value
    if dtype == AMS_DATA_UNDEF:
        raise ValueError("undefined data type")
    elif dtype == AMS_BOOLEAN:
        value = [(<bint*>   addr)[i] for i in range(count)]
    elif dtype == AMS_INT:
        value = [(<int*>    addr)[i] for i in range(count)]
    elif dtype == AMS_FLOAT:
        value = [(<float*>  addr)[i] for i in range(count)]
    elif dtype == AMS_DOUBLE:
        value = [(<double*> addr)[i] for i in range(count)]
    elif dtype == AMS_STRING:
        value = [mkstr((<char**>addr)[i])
                 for i in range(count)]
    else:
        raise ValueError("unknown data type")
    if count == 1: return value[0]
    return value


cdef setvalue(void *addr, int count, AMS_Data_type dtype, value):
    #
    cdef bint   *addr_b
    cdef int    *addr_i
    cdef float  *addr_f
    cdef double *addr_d
    cdef char  **addr_s
    #
    cdef list entries
    if (isinstance(value, bool)  or
        isinstance(value, int)   or
        isinstance(value, long)  or
        isinstance(value, float) or
        isinstance(value, bytes)):
        entries = [value]
    else:
        entries = list(value)
    if count != len(entries):
        raise ValueError("expecting %d items" % count)
    if dtype == AMS_DATA_UNDEF:
        raise ValueError("undefined data type")
    elif dtype == AMS_BOOLEAN:
        addr_b = <bint*> addr
        for i in range(count):
            addr_b[i] = entries[i]
    elif dtype == AMS_INT:
        addr_i = <int*> addr
        for i in range(count):
            addr_i[i] = entries[i]
    elif dtype == AMS_FLOAT:
        addr_f = <float*> addr
        for i in range(count):
            addr_f[i] = entries[i]
    elif dtype == AMS_DOUBLE:
        addr_d = <double*> addr
        for i in range(count):
            addr_d[i] = entries[i]
    elif dtype == AMS_STRING:
        addr_s = <char**> addr
        for i in range(count):
            addr_s[i] = entries[i]
    else:
        raise ValueError("unknown data type")
    return entries
