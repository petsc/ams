# -----------------------------------------------------------------------------

class Error(RuntimeError):

    def __init__(self, int ierr=0):
        self.ierr = ierr
        RuntimeError.__init__(self, self.ierr)

    def __bool__(self):
        cdef int ierr = self.ierr
        return ierr != 0

    def __repr__(self):
        return 'ams.Error(%d)' % self.ierr

    def __str__(self):
        cdef int ierr = self.ierr
        cdef char *msg = NULL
        AMS_Explain_error(ierr, &msg)
        return msg

# -----------------------------------------------------------------------------

# Vile hack for raising a exception and not contaminate the traceback

cdef extern from *:
    void PyErr_SetObject(object,object)
    void* PyExc_RuntimeError

cdef object AMSError = <object>Error

cdef inline int SETERR(int ierr):
    if (<void*>AMSError != NULL):
        PyErr_SetObject(AMSError, <long>ierr)
    else:
        PyErr_SetObject(<object>PyExc_RuntimeError, <long>ierr)
    return ierr

cdef inline int CHKERR(int ierr) except -1:
    if ierr == AMS_ERROR_NONE:
        return 0
    else:
        SETERR(ierr)
        return -1

# -----------------------------------------------------------------------------
