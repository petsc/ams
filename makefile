
include makeinc

all:
	-@cd c; make all
	-@if [ "${JAVAC}foo" != "foo" ]; then cd java/jni; make all; cd ../accessor; make all;  fi
	-@if [ "${CYTHON}foo" != "foo" ]; then cd python; ${APPLE_ARCHFLAGS} make all;   fi
	-@if [ "${MATLAB_MEX}foo" != "foo" ]; then cd matlab; make all;   fi

clean:
	-@${RM} lib/*
	-@cd c; make clean
	-@if [ "${CYTHON}foo" != "foo" ]; then cd python; make clean;   fi

alletags:
	etags c/*.c include/*.h java/jni/*.c examples/*.c makefile makeinc */makefile */*/makefile java/*/*.java docs/*/*.html docs/*/*.html

test:
	-@cd examples; make test
