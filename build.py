'''Configures and compiles AMS'''
import logging
import sys
import cll
import copy
import os

args = cll.ParseArgs()
logging.basicConfig(filename=args.log,filemode = 'w',level=logging.INFO,format ="%(pathname)s line %(lineno)d %(funcName)s() %(message)s")

# if user requests java then force shared library test
if not args.java == '0' and args.shared == '0': args.shared='1'

# Get the C compiler, this is required
c_compiler = cll.findCCompiler(args)
if c_compiler: print('Found C compiler:'+c_compiler.program)
else: raise RuntimeError('Cannot find C compiler, required by AMS')

# Get the C linker, also required
c_linker = cll.findCLinker(args,c_compiler)
if c_linker: print('Found C linker:'+c_linker.program)
else: raise RuntimeError('Cannot find C linker, required by AMS')

# Get the library archiver, required
c_staticlibrarier = cll.findCStaticLibrarier(args,c_compiler,c_linker)
if c_staticlibrarier: print('Found C static librarier:'+c_staticlibrarier.program)
else: raise RuntimeError('Cannot find library archiver, required by AMS')

# Optional builder for shared libraries
c_sharedlibrarier = cll.findCSharedLibrarier(args,c_compiler,c_linker)
if c_sharedlibrarier: print('Found C shared librarier:'+c_sharedlibrarier.program)
else: 
  if not args.shared == '0': raise RuntimeError('Cannot find shared library maker, required by java requested')
  else: print('Not making shared libraries, not requested or needed')

# Needed by Python interface
cython_compiler = cll.findCythonCompiler(args)
if cython_compiler: print('Successfully found Cython compiler using '+cython_compiler.program)
else: 
  if not args.cython == '0': raise RuntimeError('Cannot find Cython, required by python requested')
  else: print('Skipping Python interface since not requested, use --cython=1 or --cython=program to request it')

# Needed for Java interface
java_compiler = cll.findJavaCompiler(args)
if java_compiler: print('Successfully found Java compiler using '+java_compiler.program)
java_linker   = cll.findJavaLinker(args,java_compiler)
if java_linker: print('Successfully found Java runner '+java_linker.runner)
else: 
  if not args.java == '0': raise RuntimeError('Cannot find Javac or Java, required by Java requested')
  else: print('Skipping Java interface since not requested, use --javac=1 --java=1 or --javac=program --java=program to request it')

# Needed by MATLAB interface
mex_compiler = cll.findMexCompiler(args,ccompiler = c_compiler)
if mex_compiler: print('Successfully found Mex compiler using '+mex_compiler.program)
mex_linker   = cll.findMexLinker(args,mex_compiler)
if mex_linker: print('Successfully found Mex runner '+mex_linker.runner)
else: 
  if not args.mex == '0': raise RuntimeError('Cannot find Mex or Matlab, required by Matlab requested')
  else: print('Skipping Matlab interface since not requested, use --mex=1 --matlab=1 or --mex=program --matlab=program to request it')



class FilterPublisher():
  '''Reject any files labeled not-publisher'''
  def __init__(self):
    pass

  def Filter(self,root,filename):
    fd = open(os.path.join(root,filename))
    contents = fd.read()
    if contents.rfind('not-publisher') > -1: return True
    else: return False

c_compiler.clearSource()
c_compiler.addIncludeDirs(os.path.join('..','include'))
c_compiler.setDestinationdir('c')
c_staticlibrarier.clearCompilers()
pubcompiler = copy.deepcopy(c_compiler)
pubcompiler.addFlags(['-DAMS_PUBLISHER','-D_MT'])
pubfilter = FilterPublisher()
pubwalker = cll.Walker(rootdir = 'c',compilers = pubcompiler,filterfile = pubfilter)
pubwalker.Walk()
print(pubcompiler)
publibrarier = copy.deepcopy(c_staticlibrarier)
publibrarier.setLibraryName(os.path.join('c','libamspub'))
publibrarier.addCompilers(pubcompiler)
publibrarier.library()

class FilterAccessor():
  '''Reject any files labeled not-accessor'''
  def __init__(self):
    pass

  def Filter(self,root,filename):
    fd = open(os.path.join(root,filename))
    contents = fd.read()
    if contents.rfind('not-accessor') > -1: return True
    else: return False

acccompiler = copy.deepcopy(c_compiler)
acccompiler.addFlags(['-DAMS_ACCESSOR'])
accfilter = FilterAccessor()
accwalker = cll.Walker(rootdir = 'c',compilers = acccompiler,filterfile = accfilter)
accwalker.Walk()
print(acccompiler)
acclibrarier = copy.deepcopy(c_staticlibrarier)
acclibrarier.setLibraryName(os.path.join('c','libamsacc'))
acclibrarier.addCompilers(acccompiler)
acclibrarier.library()