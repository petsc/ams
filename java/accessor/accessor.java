import javax.swing.JTextArea;
import javax.swing.JDesktopPane;

import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.Color;


/*
 * Accessor Java Native Interface class. This Class makes calls
 * to ALICE Memory Accessor DLL
 */
class accessor {

    // Static initialization for the Accessor Class
    static {
        System.loadLibrary("amsjni");
    }

    // Class variables
    static int AMS_READ, AMS_WRITE, AMS_READ_WRITE, AMS_MEMORY_UNDEF;
    static int AMS_INT, AMS_DOUBLE, AMS_FLOAT, AMS_STRING, AMS_DATA_UNDEF;
    static int AMS_COMMON, AMS_REDUCED, AMS_DISTRIBUTED, AMS_SHARED_UNDEF;
    static int AMS_SUM, AMS_MAX, AMS_MIN, AMS_REDUCT_UNDEF;

    // Member variable
    private publisher m_pub; // Publisher
    private int m_err;   // Last Error Number
    private String m_errmsg; // Last Error Message
    private JTextArea m_textArea; // Text area for error displays //
    private JDesktopPane m_winDeskTop; // Desktop for fields displays //

    // Constructor
    public accessor(publisher pub)
    {
        AMS_Java_init(); // Call non-native method to initialize static vars
        m_pub = pub;
        m_pub.setAccessor(this);

		//{{INIT_CONTROLS
		//}}
	}

    // Publisher
    public void setPublisher(publisher pub)
    {
        m_pub = pub;
    }
    public publisher getPublisher()
    {
        return m_pub;
    }

    // List of published communicators
    public void addCommItem(String Str)
    {
        m_pub.add(new communicator(Str, this));
    }

    public String[] getCommList()
    {
        return m_pub.getStrChildren();
    }

    public int getNbComm()
    {
        return m_pub.getNbChildren();
    }
    // Communicator ID
    public void setAliceComm(String name, int com)
    {
        m_pub.setComid(name, com);
    }

    // Add a Memory to a communicator
    public void addAliceMemItem(int com, String Str)
    {
        communicator comm = m_pub.getCommunicator(com);
        comm.add(Str); // Add a memory name
    }

    // Add a field to a memory
    public void addFieldItem(memory mem, String Str)
    {
        field fld = new field(Str);
        fld.setAccessor(mem.m_acc);
        fld.setMemory(mem);
        mem.add(fld);
        // Load field info
        try {
            checkErrorAMS_Memory_get_field_info(mem, fld);
        } catch ( accessorexception e){}
    }

    // Error Codes
    public void setError(int err)
    {
        m_err = err;
    }
    public int GetError()
    {
        return m_err;
    }

    // Error Messages
    public void setErrmsg(String msg)
    {
        m_errmsg = msg;
    }
    public String getErrmsg()
    {
        return m_errmsg;
    }

    // Set the text area window
    public void setTextArea(JTextArea textArea)
    {
        m_textArea = textArea;
    }

    // Get the text area window
    public JTextArea getTextArea(JTextArea textArea)
    {
        return m_textArea;
    }

    // Set the Desktop area window
    public void setDeskTop(JDesktopPane winDeskTop)
    {
        m_winDeskTop = winDeskTop;
    }

    // Get the Desktop area window
    public JDesktopPane getDeskTop()
    {
        return m_winDeskTop;
    }


    // Display error message
    public void displayErrmsg()
    {
        m_textArea.append(m_errmsg);
        m_textArea.append("\n");
    }

    // Call to Native methods with error handling

    public int checkErrorAMS_Java_init()
                throws accessorexception
    {
        int err = AMS_Java_init();
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }


    /* Connect to the publishor and list published communicators */
    public int checkErrorAMS_Connect(String host, int port)
                throws accessorexception
    {
        int err = AMS_Connect(host, port);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }


    /* Attaches an ALICE Publisher */
    public int checkErrorAMS_Comm_attach(String name)
                throws accessorexception
    {
        int err = AMS_Comm_attach(name);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }


    /* Get Memory list */
    public int checkErrorAMS_Comm_get_memory_list(int alice)
                throws accessorexception
    {
        int err = AMS_Comm_get_memory_list(alice);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Attaches an ALICE Memory */
    public int checkErrorAMS_Memory_attach(int alice, memory mem)
                throws accessorexception
    {
        int err = AMS_Memory_attach(alice, mem);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Get Memory Field's List */
    public int checkErrorAMS_Memory_get_field_list(memory mem)
                throws accessorexception
    {
        int err = AMS_Memory_get_field_list(mem);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Get Memory Field Info */
    public int checkErrorAMS_Memory_get_field_info(memory mem, field fld)
                throws accessorexception
    {
        int err = AMS_Memory_get_field_info(mem, fld);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Set Memory Field Info */
    public int checkErrorAMS_Memory_set_field_info(memory mem, field fld)
                throws accessorexception
    {
        int err = AMS_Memory_set_field_info(mem, fld);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Send update to publisher */
    public int checkErrorAMS_Memory_update_send_begin(int memid)
                throws accessorexception
    {
        int err = AMS_Memory_update_send_begin(memid);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Receive update from publisher */
    public int checkErrorAMS_Memory_update_recv_end(int memid, int flag, int count)
                throws accessorexception
    {
        int err = AMS_Memory_update_recv_end(memid, flag, count);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Deattach a memory */
    public int checkErrorAMS_Memory_deattach(int memid)
                throws accessorexception
    {
        int err = AMS_Memory_deattach(memid);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }

    /* Deattach a communicator -- Shutdown a comm */
    public int checkErrorAMS_Comm_deattach(int alice)
                throws accessorexception
    {
        int err = AMS_Comm_deattach(alice);
        if (err != 0) {
            AMS_Explain_error(err);
            displayErrmsg();
            throw new accessorexception(getErrmsg());
        }
        return err;
    }


    // Native Methods Interface

    /* Initializes static variables from DLL/Shared Lib */
    public native int AMS_Java_init();

    /* Connect to the publisheur and list published communicators */
    public native int AMS_Connect(String host, int port);

    /* Attaches an ALICE Publisher */
    public native int AMS_Comm_attach(String name);

    /* Get Memory list */
    public native int AMS_Comm_get_memory_list(int alice);

    /* Attaches an ALICE Memory */
    public native int AMS_Memory_attach(int alice, memory mem);

    /* Get Memory Field's List */
    public native int AMS_Memory_get_field_list(memory mem);

    /* Get Memory Field Info */
    public native int AMS_Memory_get_field_info(memory mem, field fld);

    /* Set Memory Field Info */
    public native int AMS_Memory_set_field_info(memory mem, field fld);

    /* Send update to publisher */
    public native int AMS_Memory_update_send_begin(int memid);

    /* Receive update from publisher */
    public native int AMS_Memory_update_recv_end(int memid, int flag, int count);

    /* Deattach a memory */
    public native int AMS_Memory_deattach(int memid);

    /* Deattach a communicator -- Shutdown a comm */
    public native int AMS_Comm_deattach(int alice);

    /* Get an explanation for an error code */
    public native int AMS_Explain_error(int err);

}

