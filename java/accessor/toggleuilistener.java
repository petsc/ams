import javax.swing.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.event.*;


/**
 * Switch the between the Windows, Motif, and the Metal Look and Feel
 */
class toggleuilistener implements ItemListener {

    JFrame m_root;

    public toggleuilistener(JFrame frame)
    {
        super();
        m_root = frame;
    }

	public void itemStateChanged(ItemEvent e) {

	    m_root.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	    JRadioButtonMenuItem rb = (JRadioButtonMenuItem) e.getSource();
            try {
	       if(rb.isSelected() && rb.getText().equals("Windows Style Look and Feel")) {
	    	   UIManager.setLookAndFeel("javax.swing.plaf.windows.WindowsLookAndFeel");
	    	   SwingUtilities.updateComponentTreeUI(m_root);
	       } else if(rb.isSelected() && rb.getText().equals("Motif Look and Feel")) {
	    	   UIManager.setLookAndFeel("javax.swing.plaf.motif.MotifLookAndFeel");
	    	   SwingUtilities.updateComponentTreeUI(m_root);
	       } else if(rb.isSelected() && rb.getText().equals("Metal Look and Feel")) {
	    	   UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
	    	   SwingUtilities.updateComponentTreeUI(m_root);
	       }
            } catch (UnsupportedLookAndFeelException exc) {
		// Error - unsupported L&F
		rb.setEnabled(false);
                System.err.println("Unsupported LookAndFeel: " + rb.getText());

		// Set L&F to Metal
		try {
		    //UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		    SwingUtilities.updateComponentTreeUI(m_root);
		} catch (Exception exc2) {
		    System.err.println("Could not load LookAndFeel: " + exc2);
		}
            } catch (Exception exc) {
                rb.setEnabled(false);
                System.err.println("Could not load LookAndFeel: " + rb.getText());
            }

	    m_root.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

}
