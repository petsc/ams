import javax.swing.*;
import javax.swing.border.*;
import java.util.Vector;

import java.awt.Panel;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Container;
import java.awt.Component;

// Fields class.
public class field extends accessorbase implements ActionListener
{
    // members
    private int m_intdata[];
    private float m_floatdata[];
    private double m_doubledata[];
    private String m_stringdata[];
    private int m_length;
    private int m_status;
    private int m_mtype;
    private int m_dtype;
    private int m_rtype;
    private int m_stype;

    private boolean m_isDisplayed = false;
    private JInternalFrame m_w;

    private memory m_mem;

    // Methods
    public field(String name)
    {
        m_name = name;
        m_length = 0;
        m_status = 0;
        m_children = new Vector(5, 1);
        setColor(Color.red);
    }

    public void displayPropreties()
        throws accessorexception    
    {
        m_acc.checkErrorAMS_Memory_get_field_info(getMemory(), this);
        m_w = createFieldWindow();
        m_acc.getDeskTop().add(m_w, JLayeredPane.PALETTE_LAYER);
        m_isDisplayed = true;
    }

    public void update(String str, int index)
        throws accessorexception    
    {
       	// Set the Data value. Should catch exception
    	if (getDataType() == accessor.AMS_INT) {
    	    Integer val = new Integer(str);
    	    setData(val.intValue(), index);
    	}

    	if (getDataType() == accessor.AMS_DOUBLE) {
    	    Double val = new Double(str);
    	    setData(val.doubleValue(), index);
    	}

    	if (getDataType() == accessor.AMS_FLOAT) {
    	    Float val = new Float(str);
    	    setData(val.floatValue(), index);
    	}

    	if (getDataType() == accessor.AMS_STRING) {
    	    setData(str, index);
    	}
        m_acc.checkErrorAMS_Memory_set_field_info(getMemory(), this);
        m_acc.checkErrorAMS_Memory_update_send_begin(getMemory().getMemid());
        attachChildren();
    }

    public void attachChildren()
    {
        // Add Field's values
	    add(new FieldData(this));

    	// Add Memory Type (Access Type)
    	add(new FieldMemory(getMemoryType()));

    	// Add Data Type
    	add(new FieldDataType(getDataType()));

    	// Add Shared Type
    	add(new FieldSharedType(getSharedType(), getReductionType()));
    }
    public void update(String str) 
    {
        try {
            update(str, 0);
        } catch (accessorexception e) {}
    }

    public void setData(int data[]) {
        m_intdata = data;
        // Assuming the length is set first
        if (m_length == 1) {
       	    Integer val = new Integer(data[0]);
      	    setValue(val.toString());
      	} else {
      	    setValue(null);
      	}
    }
    public void setData(int data, int index) {
        if (m_length > index)
            m_intdata[index] = data;

        // Assuming the length is set first
        if (m_length == 1) {
       	    Integer val = new Integer(data);
      	    setValue(val.toString());
      	} else {
      	    setValue(null);
      	}
    }

    public int[] getIntData() { return m_intdata; }

    public void setData(float data[]) {
        m_floatdata = data;
        // Assuming the length is set first
        if (m_length == 1) {
       	    Float val = new Float(data[0]);
      	    setValue(val.toString());
      	} else {
      	    setValue(null);
      	}
    }
    public void setData(float data, int index) {
        if (m_length > index)
            m_floatdata[index] = data;

        // Assuming the length is set first
        if (m_length == 1) {
       	    Float val = new Float(data);
      	    setValue(val.toString());
      	} else {
      	    setValue(null);
      	}
    }

    public float[] getFloatData() { return m_floatdata; }

    public void setData(double data[]) {
        m_doubledata = data;
        // Assuming the length is set first
        if (m_length == 1) {
       	    Double val = new Double(data[0]);
      	    setValue(val.toString());
      	} else {
      	    setValue(null);
      	}
    }
    public void setData(double data, int index) {
        if (m_length > index)
            m_doubledata[index] = data;
        // Assuming the length is set first
        if (m_length == 1) {
       	    Double val = new Double(data);
      	    setValue(val.toString());
      	} else {
      	    setValue(null);
      	}
    }

    public double[] getDoubleData() { return m_doubledata; }
    
    public void setData(String data[]) {
        m_stringdata = data;
        // Assuming the length is set first
        if (m_length == 1) {
      	    setValue(data[0]);
      	} else {
      	    setValue(null);
      	}
    }
    
    public void setData(String data, int index) {
        if (m_length > index)
            m_stringdata[index] = data;
        // Assuming the length is set first
        if (m_length == 1) {
      	    setValue(data);
      	} else {
      	    setValue(null);
      	}        
    }

    public String[] getStringData() { return m_stringdata; }

    public void setLength(int length) { m_length = length; }
    public int getLength() { return m_length; }

    public void setMemoryType(int mtype) { m_mtype = mtype; }
    public int getMemoryType() { return m_mtype; }

    public void setDataType(int dtype) { m_dtype = dtype; }
    public int getDataType() { return m_dtype; }

    public void setReductionType(int rtype) { m_rtype = rtype; }
    public int getReductionType() { return m_rtype; }

    public void setSharedType(int stype) { m_stype = stype; }
    public int getSharedType() { return m_stype; }

    public void setMemory(memory mem) { m_mem = mem; }
    public memory getMemory() { return m_mem; }

    public boolean isEditable() {
        if (getMemoryType() == accessor.AMS_WRITE) {
            if (getLength() == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /** Create Internal Window That will display the field propreties */
    public JInternalFrame createFieldWindow()
    {
        JTextField valueField;
        JButton cancelAllButton;
        JButton applyButton;
        String value = new String("Undefined");

        JInternalFrame w = new JInternalFrame(getName());

        JPanel tp;
	    Container contentPane;

	    contentPane = w.getContentPane();
        contentPane.setLayout(new GridLayout(0, 1));

        tp = new JPanel();
	    tp.setBorder(BorderFactory.createTitledBorder("Value"));
        tp.setLayout(new BorderLayout());
        valueField = new JTextField();
        valueField.setMinimumSize(new Dimension(10, 5));

        if (getMemoryType() == accessor.AMS_READ)
    	    valueField.setEditable(false);
    	else
    	    valueField.setEditable(true);

    	// Determine the value
    	if (getDataType() == accessor.AMS_INT) {
    	    Integer val = new Integer(getIntData()[0]);
    	    value = new String(val.toString());
    	}

    	if (getDataType() == accessor.AMS_DOUBLE) {
    	    Double val = new Double(getDoubleData()[0]);
    	    value = new String(val.toString());
    	}

    	if (getDataType() == accessor.AMS_FLOAT) {
    	    Float val = new Float(getFloatData()[0]);
    	    value = new String(val.toString());
    	}

    	if (getDataType() == accessor.AMS_STRING) {
    	    value = new String(getStringData()[0]);
    	}

        valueField.setText(value);
        tp.add(valueField, "Center");
        contentPane.add(tp);

        tp = new JPanel();
        tp.setLayout(new GridLayout(1, 2));
        cancelAllButton = new JButton("Cancel");
        cancelAllButton.addActionListener(this);
        tp.add(cancelAllButton);

        applyButton = new JButton("Apply");
        applyButton.addActionListener(this);
        tp.add(applyButton);
        contentPane.add(tp);

        w.setBounds(100, 5, 200, 150);
        w.setResizable(true);

        return w;
    }

    public void actionPerformed(ActionEvent e) 
    {

        if (e.getActionCommand() == "Cancel") {
             m_acc.getDeskTop().remove(m_w);
             m_acc.getDeskTop().repaint();
             m_isDisplayed = false;
        }
        if (e.getActionCommand() == "Apply") {
            // Apply chances to publisher
            // Get the value component

            JPanel tp = (JPanel) m_w.getContentPane().getComponent(0);
            JTextField value = (JTextField) tp.getComponent(0);

           	// Set the Data value. Should catch exception
        	if (getDataType() == accessor.AMS_INT) {
        	    Integer val = new Integer(value.getText());
        	    int arr[] = new int[1];
        	    arr[0] = val.intValue();
        	    setData(arr);
        	}

        	if (getDataType() == accessor.AMS_DOUBLE) {
        	    Double val = new Double(value.getText());
        	    double arr[] = new double[1];
        	    arr[0] = val.doubleValue();
        	    setData(arr);
        	}

        	if (getDataType() == accessor.AMS_FLOAT) {
        	    Float val = new Float(value.getText());
        	    float arr[] = new float[1];
        	    arr[0] = val.floatValue();
        	    setData(arr);
        	}
        	
        	if (getDataType() == accessor.AMS_STRING) {
        	    // Not supported
        	}

            try {
                m_acc.checkErrorAMS_Memory_set_field_info(getMemory(), this);
                m_acc.checkErrorAMS_Memory_update_send_begin(getMemory().getMemid());
            } catch (accessorexception e1){}
            
            m_acc.getDeskTop().remove(m_w);
            m_acc.getDeskTop().repaint();
            m_isDisplayed = false;
        }
    }

    // Field Data inner class.
    class FieldData extends accessorbase
    {
        field m_field;

        // Methods
        public FieldData(field fld)
        {
            m_field = fld;
            int len = fld.getLength();
            setName("Value");

        	if (fld.getDataType() == accessor.AMS_INT) {
                int data[] = fld.getIntData();
                if (len == 1) {
                    m_canHaveChildren = false;
          	        Integer val = new Integer(data[0]);
          	        setValue(val.toString());

                } else {
                    m_children = new Vector(len,5);
                    int index;
                    for (index = 0; index < len; index++)
                        add(index, data[index]);
                }

            }

        	if (fld.getDataType() == accessor.AMS_FLOAT) {
                float data[] = fld.getFloatData();
                if (len == 1) {
                    m_canHaveChildren = false;
               	    Float val = new Float(data[0]);
              	    setValue(val.toString());
                } else {
                    m_children = new Vector(len, 5);
                    int index;
                    for (index = 0; index < len; index++)
                        add(index, data[index]);
                }

            }

        	if (fld.getDataType() == accessor.AMS_DOUBLE) {
                double data[] = fld.getDoubleData();
                if (len == 1) {
                    m_canHaveChildren = false;
               	    Double val = new Double(data[0]);
              	    setValue(val.toString());
                } else {
                    m_children = new Vector(len, 5);
                    int index;
                    for (index = 0; index < len; index++)
                        add(index, data[index]);
                }
            }

        	if (fld.getDataType() == accessor.AMS_STRING) {
                String data[] = fld.getStringData();
                if (len == 1) {
                    m_canHaveChildren = false;
          	        setValue(data[0]);
          	    } else {
                    m_children = new Vector(len, 5);
                    int index;
                    for (index = 0; index < len; index++)
                        add(index, data[index]);
                }

            }

        }

        public boolean isEditable() {
            if (m_field.getMemoryType() == accessor.AMS_WRITE) {
                if (m_field.getLength() == 1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }


        public void add(int index, int elm)
        {
            DataElement newElem = new DataElement(index, m_field);
       	    Integer val = new Integer(elm);
            newElem.setName("["+index+"]");
            super.add(newElem);
            newElem.setValue(val.toString());
        }

        public void add(int index, float elm)
        {
            DataElement newElem = new DataElement(index, m_field);
       	    Float val = new Float(elm);
            newElem.setName("["+index+"]");
            super.add(newElem);
            newElem.setValue(val.toString());
        }

        public void add(int index, double elm)
        {
            DataElement newElem = new DataElement(index, m_field);
       	    Double val = new Double(elm);
            newElem.setName("["+index+"]");
            super.add(newElem);
            newElem.setValue(val.toString());
        }

        public void add(int index, String elm)
        {
            if (elm == null)
                return;
            DataElement newElem = new DataElement(index, m_field);
            newElem.setName("["+index+"]");
            super.add(newElem);
            newElem.setValue(elm);
        }

        public void attachChildren() {;}
        public void update(String str) {
            try {
                m_field.update(str, 0);
            } catch (accessorexception e) {}
        }

        // Another inner-inner class
        class DataElement extends accessorbase
        {
            int m_index = 0;
            public DataElement(int index, field fld)
            {
                m_index = index;
                m_canHaveChildren = false;
                m_field = fld;

                if (fld.getMemoryType() == accessor.AMS_WRITE)
                    m_isEditable = true;
            }

            public void setIndex(int index){ m_index = index;}
            public int getIndex() { return m_index; }

            public void update(String str) {
                try {
                    m_field.update(str, m_index);
                } catch (accessorexception e) {}
            }

            public void attachChildren(){;}
        }
    }

    // Field Memory type inner class.
    class FieldMemory extends accessorbase
    {
        // Methods
        public FieldMemory(int mtype)
        {
            m_canHaveChildren = false;
            if (mtype == accessor.AMS_READ)
                setName("Access: ReadOnly");
            else if (mtype == accessor.AMS_WRITE)
                setName("Access: Read-Write");
            else
                setName("Access: Undefined");

        }

        public void attachChildren(){;}
        public void update(String str) {;}
    }

    // Field Data type inner class.
    public class FieldDataType extends accessorbase
    {
        // Methods
        public FieldDataType(int dtype)
        {
            m_canHaveChildren = false;
            if (dtype == accessor.AMS_INT)
                setName("Type: Integer");
            else if (dtype == accessor.AMS_FLOAT)
                setName("Type: Float");
            else if (dtype == accessor.AMS_DOUBLE)
                setName("Type: Double");
            else if (dtype == accessor.AMS_STRING)
                setName("Type: String");
            else
                setName("Type: Undefined");

        }

        public void attachChildren(){;}
        public void update(String str) {;}
    }

    // Field Reduction type inner class.
    public class FieldSharedType extends accessorbase
    {
        // Methods
        public FieldSharedType(int stype, int rtype)
        {
            m_canHaveChildren = false;

            if (stype == accessor.AMS_COMMON)
                setName("Shared type: COMMON");
            else if (stype == accessor.AMS_DISTRIBUTED)
                setName("Shared type: DISTRIBUTED");
            else if (stype == accessor.AMS_REDUCED) {
                if (rtype == accessor.AMS_MIN)
                    setName("Shared type: REDUCED/MIN");
                else if (rtype == accessor.AMS_MAX)
                    setName("Shared type: REDUCED/MAX");
                else if (rtype == accessor.AMS_SUM)
                    setName("Shared type: REDUCED/SUM");
                else
                    setName("Shared type: REDUCED/Undefined");
            } else
                setName("Shared type: Undefined");
        }
        public void attachChildren(){;}
        public void update(String str) {;}
    }

}

