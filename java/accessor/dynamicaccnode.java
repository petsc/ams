import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeExpansionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import javax.swing.JTree;
import javax.swing.tree.*;
import javax.swing.*;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Vector;
import java.util.Enumeration;

/**
  * Dynamic Tree nodes
  */

public class dynamicaccnode extends DefaultMutableTreeNode implements TreeExpansionListener, MouseListener
{
    // Class stuff.

    /** Potential fonts used to draw with. */
    static protected Font[] m_fonts;

    /** Should we relaod on expansion event? */
    protected boolean   m_reload = true;

    /** Have the children of this node been loaded yet? */
    protected boolean   m_hasLoaded = false;

    /**
      * Constructs a new DynamicAccNode instance with o as the user
      * object.
      */
    public dynamicaccnode() {
    }

    public boolean isLeaf() {
	    return ((accessorbase) getUserObject()).isLeaf();
    }

    /**
      * Listener to node expansions
      */
    public void treeExpanded(TreeExpansionEvent e) {
    }

    public void treeCollapsed(TreeExpansionEvent e){;}

    /**
      * If hasLoaded is false, meaning the children have not yet been
      * loaded, loadChildren is messaged and super is messaged for
      * the return value
      */
    public int getChildCount() {
    	if(!m_hasLoaded) {
    	    try {
         	    loadChildren();
         	} catch (accessorexception e) {}
    	}
    	return super.getChildCount();
    }

    /**
      * If hasLoaded is false, meaning the children have not yet been
      * loaded, loadChildren is messaged and super is messaged for
      * the return value
      */
    public Enumeration children() {
    	if(m_reload) {
    	    try {
         	    loadChildren();
         	} catch (accessorexception e) {}
    	}
    	return super.children();
    }

    /**
      * Messaged the first time getChildCount is messaged.  Creates
      * children with random names from names.
      */
    protected void loadChildren() 
                    throws accessorexception
    {
        dynamicaccnode newNode;
    	Font font;
    	((accessorbase) getUserObject()).attachChildren();
        Vector names = ((accessorbase) getUserObject()).getChildren();
        int count = 0, maxcount = super.getChildCount();

        for (Enumeration e = names.elements() ; e.hasMoreElements(); )
        {
            if (!m_hasLoaded) {
                newNode = new dynamicaccnode();
                insert(newNode, count++);
            } else {
                if (count < maxcount) {
                    newNode = (dynamicaccnode)getChildAt(count++);
                    newNode.setUserObject((accessorbase)e.nextElement());
                } else {
                    newNode = new dynamicaccnode();
                    insert(newNode, count++);
                }
            }
        }

    	/* This node has now been loaded, mark it so. */
        m_hasLoaded = true;
    }

    /**
      * If a node is expanded, this method reload its children and recursively refreshes
      * all its children.
      */
    protected void refreshNode(JTree tree) 
                    throws accessorexception
    {

        if (tree == null)
            return;
        /* Do some real stuff */
        TreePath path = new TreePath(getPath());
        if (tree.isCollapsed(path))
            return;

        /* Load the children */
        loadChildren();
        dynamicaccnode newNode;

        Vector names = ((accessorbase) getUserObject()).getChildren();
        int count = 0, maxcount = super.getChildCount();

        for (Enumeration e = names.elements() ; e.hasMoreElements(); )
        {
            if (count < maxcount) {
                newNode = (dynamicaccnode)getChildAt(count++);
                newNode.refreshNode(tree);
            }
            e.nextElement();
        }
        ((DefaultTreeModel)tree.getModel()).nodeChanged(this);
    }

    /**
      * Mouse Listener
      */
    public void mouseClicked(MouseEvent e) {
        JTree tree = (JTree) e.getSource();
        int selRow = tree.getRowForLocation(e.getX(), e.getY());
        TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());

        if(selRow != -1) {
            DefaultMutableTreeNode      aNode = (DefaultMutableTreeNode)selPath.getLastPathComponent();
   	        ((DefaultTreeModel)tree.getModel()).reload(aNode);

            if(e.getClickCount() == 2) {
                myDoubleClick(selRow, selPath);
            }
            if(e.getClickCount() == 1) {
                mySingleClick(selRow, selPath);
            }
        }
    }

    public void myDoubleClick(int selRow, TreePath selPath){;}

    public void mySingleClick(int selRow, TreePath selPath){;}

    public void mouseEntered(MouseEvent e){;}
    public void mouseReleased(MouseEvent e){;}
    public void mouseExited(MouseEvent e){;}
    public void mousePressed(MouseEvent e){;}
}
