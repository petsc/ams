/*
 * @(#)SampleTreeCellRenderer.java	1.7 97/12/11
 *
 * Copyright (c) 1997 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Sun
 * Microsystems, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Sun.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class accessorcellrenderer extends JLabel implements TreeCellRenderer
{
    /** Icon to use when the item is collapsed. */
    static protected ImageIcon        collapsedIcon;
    /** Icon to use when the item is expanded. */
    static protected ImageIcon        expandedIcon;

    static
    {
        try {
            collapsedIcon = new ImageIcon("images/collapsed.gif");
            expandedIcon = new ImageIcon("images/expanded.gif");
        } catch (Exception e) {
            System.out.println("Couldn't load images: " + e);
        }
    }

    /**
      * This is messaged from JTree whenever it needs to get the size
      * of the component or it wants to draw it.
      * This attempts to set the font based on value, which will be
      * a TreeNode.
      */
    public Component getTreeCellRendererComponent(JTree tree, Object value,
					  boolean selected, boolean expanded,
					  boolean leaf, int row,
						  boolean hasFocus) {
    	Font            font;
    	String          stringValue = tree.convertValueToText(value, selected,
    					   expanded, leaf, row, hasFocus);

    	/* Set the text. */
    	setText(stringValue);
    	/* Tooltips used by the tree. */
        //	setToolTipText(stringValue);

    	/* Set the image. */
    	if(expanded)
    	    setIcon(expandedIcon);
    	else if(!leaf)
    	    setIcon(collapsedIcon);
    	else
    	    setIcon(null);

    	/* Set the color and the font based on the SampleData userObject. */
    	accessorbase userObject = (accessorbase)((DefaultMutableTreeNode)value)
    	                                .getUserObject();
    	if(hasFocus)
    	    setForeground(Color.black);
    	else
    	    setForeground(userObject.getColor());

    	return this;
    }

}
