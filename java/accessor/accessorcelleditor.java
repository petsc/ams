/*
 * Accessor Cell Editor
 */

import javax.swing.JTree;
import javax.swing.JTextField;
import javax.swing.tree.TreePath;
import javax.swing.DefaultCellEditor;
import javax.swing.tree.DefaultMutableTreeNode;

import java.awt.Component;
import java.awt.AWTEvent;
import java.util.EventObject;
import java.awt.event.*;

public class accessorcelleditor extends DefaultCellEditor
{

public accessorcelleditor(JTextField x) {
    super(x);
}

public boolean isCellEditable(EventObject anEvent) {
    Object source = anEvent.getSource();

	if (source instanceof JTree && anEvent instanceof MouseEvent) {
	    JTree tree = (JTree)source;
	    MouseEvent e = (MouseEvent)anEvent;
        TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
        DefaultMutableTreeNode      aNode = (DefaultMutableTreeNode)selPath.getLastPathComponent();
        accessorbase node = (accessorbase)aNode.getUserObject();
        if (node == null)
            return false;
            
        if (!node.isEditable())
            return false;
	}

    return super.isCellEditable(anEvent);

}

public Component getTreeCellEditorComponent(JTree tree,
                                                      Object value,
                                                      boolean isSelected,
                                                      boolean expanded,
                                                      boolean leaf,
                                                      int row)
    {
        TreePath selPath = tree.getPathForRow(row);
        dynamicaccnode      aNode = (dynamicaccnode)selPath.getLastPathComponent();

        String myValue = ((accessorbase)aNode.getUserObject()).getValue();

        return super.getTreeCellEditorComponent(tree, myValue, isSelected, expanded, leaf, row);
    }

}
