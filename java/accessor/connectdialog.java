import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.JTextField;

import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.beans.*; //Property change stuff
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.IOException;
import java.util.Properties;

class connectdialog extends JDialog {
    private JTextField port = null;
    private JLabel lport = null;
    private JTextField cb = null;
    private int cancel = 0;

    private String typedText = null;

    private JOptionPane optionPane;

    public String getValidatedText() {
    	return typedText;
    }

    public connectdialog(JFrame aFrame) {

    	super(aFrame,true);
    	Properties defaultProps = new Properties();
    	InetAddress addr = null;
    	try {
        	addr = InetAddress.getLocalHost();
        } catch ( UnknownHostException e) {}

        String default_host = System.getProperty("AMS_PUBLISHER");
        if (default_host == null)
            default_host = System.getProperty("ams_publisher");

        if ((default_host == null || default_host.trim().length()== 0) && addr != null)
            default_host = addr.getHostName();

        String default_port = System.getProperty("AMS_PORT");
        if (default_port == null)
            default_port = System.getProperty("ams_port");

        if (default_port == null || default_port.trim().length() == 0)
            default_port = "8967";

    	setTitle("Connecting to the Publisher");

        // Publisher name
        JPanel tp = new JPanel();
	    tp.setBorder(BorderFactory.createTitledBorder("Publisher Name"));
        tp.setLayout(new BorderLayout());
        cb = new JTextField(default_host);
        cb.setMinimumSize(new Dimension(10, 5));
        tp.add(cb, "Center");

        // Port number
        JPanel tp1 = new JPanel();
	    tp1.setBorder(BorderFactory.createTitledBorder("Communication Port"));
        tp1.setLayout(new GridLayout(1, 3));

        lport = new JLabel("TCP Port#:");
        tp1.add(lport);

        port = new JTextField(default_port);
        port.setMinimumSize(new Dimension(5, 1));
        port.setPreferredSize(new Dimension(5, 1));
        tp1.add(port);

    	Object[] array = {tp, tp1};
    	Object[] options = {"CONNECT", "CANCEL"};

    	optionPane = new JOptionPane(array,
    				    JOptionPane.QUESTION_MESSAGE,
    				    JOptionPane.YES_NO_OPTION,
    				    null,
    				    options,
    				    options[0]);
    	setContentPane(optionPane);
    	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

    	addWindowListener(new WindowAdapter() {
    		public void windowClosing(WindowEvent we) {
    		/*
    		 * Instead of directly closing the window,
    		 * we're going to change the JOptionPane's
    		 * value property.
    		 */
    		    optionPane.setValue(new Integer(
    					JOptionPane.CLOSED_OPTION));
    	    }
    	});

    	cb.addActionListener(new ActionListener() {
    	    public void actionPerformed(ActionEvent e) {
    		optionPane.setValue("CONNECT");
    	    }
    	});

    	port.addActionListener(new ActionListener() {
    	    public void actionPerformed(ActionEvent e) {
    		optionPane.setValue("CONNECT");
    	    }
    	});

    	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
	        public void propertyChange(PropertyChangeEvent e) {
    		    String prop = e.getPropertyName();

        		if (isVisible()
        		 && (e.getSource() == optionPane)
        		 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
        		     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
        		    Object value = optionPane.getValue();

        		    if (value == JOptionPane.UNINITIALIZED_VALUE) {
        			//ignore reset
        			return;
        		    }

        		    if (value.equals("CONNECT")) {
        			    // we're done; dismiss the dialog
        			    setVisible(false);
    		        } else { // user closed dialog or clicked cancel
    		            cancel = 1;
        			    setVisible(false);
    		        }
    		    }
	        }

	    });

    }

    public int getPort()
    {
        Integer val;
        try {
            val =  new Integer(port.getText());
            if (cancel == 0)
                return val.intValue();
        } catch (NumberFormatException e) {}
        return -1;
    }

    public String getHost()
    {
        String host;
        host = (String) cb.getText();
        host.trim();
        if (host.compareTo("") == 0)
            host = "Default";

        if (cancel == 0)
            return host;
        else
            return null;
    }


}

