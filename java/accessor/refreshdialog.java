import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.JTextField;

import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.beans.*; //Property change stuff
import java.awt.*;
import java.awt.event.*;


public class refreshdialog extends JDialog {

    int min , sec;
    boolean enable;

    private JTextField minText;
    private JLabel label;
    private JTextField secText;
    private JCheckBox cb;
    private JButton cancelAllButton;
    private JButton applyButton;

    private JOptionPane optionPane;
    
    /** Create Dialog */
    public refreshdialog (JFrame frame, boolean b, int m, int s)
    {
        super(frame, "Auto-refresh properties", true);

        // Enable or Disable auto-refresh
        JPanel tp = new JPanel();
	    tp.setBorder(BorderFactory.createTitledBorder("Enable/Disable"));
        tp.setLayout(new BorderLayout());
        cb = new JCheckBox("Enable Auto-Refresh");
        
        cb.setSelected(b);
        enable = b;
        
        tp.add(cb, "Center");

        // Minutes and Seconds before next refresh
        JPanel tp1 = new JPanel();
	    tp1.setBorder(BorderFactory.createTitledBorder("Refresh every:"));
        tp1.setLayout(new GridLayout(1, 4));

        label = new JLabel("Min:");
        tp1.add(label);

        min = m;
        Integer x = new Integer(m);
        minText = new JTextField(x.toString());
        tp1.add(minText);

        label = new JLabel("Sec:");
        tp1.add(label);

        sec = s;
        x = new Integer(s);
        secText = new JTextField(x.toString());
        tp1.add(secText);

        minText.setEditable(b);
        secText.setEditable(b);
        if (!b) {
           	minText.setBackground(new Color(221, 221, 221));
           	secText.setBackground(new Color(221, 221, 221));
        } else {
           	minText.setBackground(Color.white);
           	secText.setBackground(Color.white);
        }
        minText.repaint();
        secText.repaint();

    	Object[] array = {tp, tp1};
    	Object[] options = {"APPLY", "CANCEL"};

    	optionPane = new JOptionPane(array, 
    				    JOptionPane.QUESTION_MESSAGE,
    				    JOptionPane.YES_NO_OPTION,
    				    null,
    				    options,
    				    options[0]);
    	setContentPane(optionPane);
    	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
    	addWindowListener(new WindowAdapter() {
    		public void windowClosing(WindowEvent we) {
    		/*
    		 * Instead of directly closing the window,
    		 * we're going to change the JOptionPane's
    		 * value property.
    		 */
    		    optionPane.setValue(new Integer(
    					JOptionPane.CLOSED_OPTION));
    	    }
    	});

    	minText.addActionListener(new ActionListener() {
    	    public void actionPerformed(ActionEvent e) {
    		optionPane.setValue("APPLY");
    	    }
    	});

    	secText.addActionListener(new ActionListener() {
    	    public void actionPerformed(ActionEvent e) {
    		optionPane.setValue("APPLY");
    	    }
    	});

    	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
	        public void propertyChange(PropertyChangeEvent e) {
    		    String prop = e.getPropertyName();

        		if (isVisible() 
        		 && (e.getSource() == optionPane)
        		 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
        		     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
        		    Object value = optionPane.getValue();

        		    if (value == JOptionPane.UNINITIALIZED_VALUE) {
        			//ignore reset
        			return;
        		    }

        		    if (value.equals("APPLY")) {
        			    // we're done; dismiss the dialog
		                Integer val;
                        try {
                            val =  new Integer(minText.getText());
                            min = val.intValue();
                            val =  new Integer(secText.getText());
                            sec = val.intValue();
                        } catch (NumberFormatException enuma) { 
                            min = 0;
                            sec = 0;
                        }
                        enable = cb.isSelected();
        			    setVisible(false);
    		        } else { // user closed dialog or clicked cancel
        			    setVisible(false);    		            
    		        }
    		    }
	        }
	        
	    });
        // Item listener
    	cb.addItemListener(new refreshItemListener());
    
    }

    public void setMinutes(int nb)
    {
        if (!(nb < 0))
            min = nb;
    }
    public int getMinutes() { return min; }

    public void setSeconds(int nb)
    {
        if (!(nb < 0))
            sec = nb;
    }

    public int getSeconds() { return sec;}

    public boolean isChecked() { return enable;}
    public void setChecked(boolean b) { enable = true ;}

    public class refreshItemListener implements ItemListener {
    	public void itemStateChanged(ItemEvent e2) {
    	    JCheckBox cbox = (JCheckBox) e2.getSource();
    	    if(cbox.isSelected()) {
    	        enable = true;
               	minText.setBackground(Color.white);
               	secText.setBackground(Color.white);
    		} else {
                enable = false;
               	minText.setBackground(new Color(221, 221, 221));
               	secText.setBackground(new Color(221, 221, 221));
	        }
            minText.setEditable(enable);
            secText.setEditable(enable);
            
            minText.repaint();
            secText.repaint();
	    }
	}

}
