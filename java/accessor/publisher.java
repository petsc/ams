import javax.swing.JTextArea;
import javax.swing.JDesktopPane;

import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.Color;


// Pubisher Class
class publisher extends accessorbase {
    // Member variables
    protected int m_port;
    protected String m_host;

    // Methods
    public publisher(String name, String host, int port)
    {
        m_name = name;
        m_host = host;
        m_port = port;
        m_children = new Vector(5,2);
        setColor(Color.magenta);
    }

    public void attachChildren() 
                throws accessorexception
    {
        m_acc.checkErrorAMS_Connect(m_host, m_port);
    }
    public void update(String str) {;}

    public void setPort(int port) { m_port = port; }
    public int getPort() { return m_port; }

    public void setHost(String host) { m_host = host; }
    public String getHost() { return m_host; }

    public communicator getCommunicator(int comid)
    {
        communicator com;
        for (Enumeration e = m_children.elements(); e.hasMoreElements();)
        {
            com = (communicator)(e.nextElement());
            if (comid == com.getComid())
                return com;
        }

        return null;
    }

    public void setComid(String name, int com)
    {
        communicator comm = (communicator)getChild(name);
        comm.setComid(com);
    }

}

