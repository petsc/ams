/* Import the AMS Package */
import gov.anl.mcs.ams.*;
import gov.anl.mcs.ams.AMS_Comm;
import gov.anl.mcs.ams.AMS_Memory;

//import gov.anl.mcs.ams.AMS_Memory;


import java.util.Vector;
import java.util.Enumeration;



/**
  * Testing for the AMSBean class (use AMSBean.jar in your class path)
  * Author: Ibrahima Ba, ibrahba@mcs.anl.gov
  */

public class TestAMSBean extends Object
{
    /** AMSBean Object */
    AMSBean amsbean;
    
    public TestAMSBean() {

        amsbean = new AMSBean();
    }
    
    public void test() {
        /**
         * Get the list of Communicators
         */
        String list[];
        list = AMSBean.get_comm_list("tiamat.mcs.anl.gov", -1);

        if (list == null)
           System.exit(1);
           
        System.out.println("Connected successfully");
        System.out.println("The following Communicators are published");
        int i;
        
        for (i=0; i<list.length; i++)
            System.out.println("   "+list[i]);
            
        /**
         * Attach the first Communicator
         */

        AMS_Comm ams = AMSBean.get_comm(list[0]);

        if (ams == null)
           System.exit(1);
       
        /**
         * Get the list of Memories
         */
        list = ams.get_memory_list();

        if (list == null)
           System.exit(1);
           
        System.out.println("The following Memor(y)ies are published");
        
        for (i=0; i<list.length; i++)
            System.out.println("   "+list[i]);

        /**
         * Attach the first Memory
         */

        AMS_Memory mem = ams.get_memory(list[0]);

        if (mem == null)
           System.exit(1);

        /**
         * Get the list of Fields
         */
        list = mem.get_field_list();

        if (list == null)
           System.exit(1);
           
        System.out.println("The following Fields are published");
        
        for (i=0; i<list.length; i++)
            System.out.println("   "+list[i]);

        /**
         * Attach the first Field
         */

        AMS_Field fld = mem.get_field(list[0]);

        if (fld == null)
           System.exit(1);
        
        /** 
         * Display field properties 
         */
         
        // Field name 
        System.out.println("Field name:   "+fld.getName());
        // Field info
        int x[] = fld.get_field_info();

        // Field length
        System.out.println("   Length: "+x[0]);
            
        // Data type
        if (x[1] == AMSBean.INT)
            System.out.println("   Data type: Integer  ");
        else (x[1] == AMSBean.BOOLEAN)
            System.out.println("   Data type: Boolean  ");
        else if (x[1] == AMSBean.FLOAT)
            System.out.println("   Data type: Float  ");
        else if (x[1] == AMSBean.DOUBLE)
            System.out.println("   Data type: Double  ");
        else if (x[1] == AMSBean.STRING)
            System.out.println("   Data type: String  ");
        else if (x[1] == AMSBean.REDUCT_UNDEF)
            System.out.println("   Data type: Udefined  ");
        else 
            System.out.println("   Data type: Unknown  ");

        // Memory type
        if (x[2] == AMSBean.READ)
            System.out.println("   Memory type: Read  ");
        else if (x[2] == AMSBean.WRITE)
            System.out.println("   Memory type: Read/Write  ");
        else if (x[2] == AMSBean.REDUCT_UNDEF)
            System.out.println("   Memory type: Undefined  ");
        else 
            System.out.println("   Memory type: Unknown  ");

        // Shared type
        if (x[3] == AMSBean.COMMON)
            System.out.println("   Shared type: Common  ");
        else if (x[3] == AMSBean.DISTRIBUTED)
            System.out.println("   Shared type: Distributed  ");
        else if (x[3] == AMSBean.REDUCT_UNDEF)
            System.out.println("   Shared type: Undefined  ");
        else 
            System.out.println("   Shared type: Unknown  ");

        // Reduction type
        if (x[4] == AMSBean.SUM)
            System.out.println("   Reduction type: Sum  ");
        else if (x[4] == AMSBean.MAX)
            System.out.println("   Reduction type: Max  ");
        else if (x[4] == AMSBean.MIN)
            System.out.println("   Reduction type: Min  ");
        else if (x[4] == AMSBean.REDUCT_UNDEF)
            System.out.println("   Reduction type: None  ");
        else 
            System.out.println("   Shared type: Unknown  ");

        // Data values
        int k = 0;
        System.out.println("   Data value(s): ");                        
        if (x[1] == AMSBean.INT) {
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getIntData()[k]);
        } else if (x[1] == AMSBean.BOOLEAN) {
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getBooleanData()[k]);
        } else if (x[1] == AMSBean.FLOAT){
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getFloatData()[k]);
        } else if (x[1] == AMSBean.DOUBLE) {
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getDoubleData()[k]);
        } else if (x[1] == AMSBean.STRING){
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getStringData()[k]);
        } else 
            System.out.println("   Unknown  ");

        /**
         * Change field value to 2
         */
         
        System.out.println(" Changing field values  ");
        if (x[1] == AMSBean.INT && x[2] == AMSBean.WRITE) {
            int y[] = fld.getIntData();
            for (k=0; k<x[0]; k++) 
                y[k] = 2;
            fld.setData(y);
        } else if (x[1] == AMSBean.BOOLEAN && x[2] == AMSBean.WRITE) {
            boolean y[] = fld.getBooleanData();
            for (k=0; k<x[0]; k++) 
                y[k] = 2;
            fld.setData(y);
        } else if (x[1] == AMSBean.FLOAT && x[2] == AMSBean.WRITE){
            float y[] = fld.getFloatData();
            for (k=0; k<x[0]; k++) 
                y[k] = 2;
            fld.setData(y);
        } else if (x[1] == AMSBean.DOUBLE && x[2] == AMSBean.WRITE) {
            double y[] = fld.getDoubleData();
            for (k=0; k<x[0]; k++) 
                y[k] = 2;
            fld.setData(y);
        } else if (x[1] == AMSBean.STRING && x[2] == AMSBean.WRITE) {
            String y[] = fld.getStringData();
            for (k=0; k<x[0]; k++) 
                y[k] = "Change to two";
            fld.setData(y);
        } else
            System.out.println("   Can't change data  ");
            
        /**
         * Update the publisher copy
         */
        mem.send_begin();
        mem.send_end(); // Not implemented

        /**
         * Receive latest copy from the publisher
         */
         
        mem.recv_begin(); 
        mem.recv_end(); // Not implemented
        
        /** 
         * Print new values for the data
         */
        // Data values
        System.out.println("   Data value(s): ");                        
        if (x[1] == AMSBean.INT) {
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getIntData()[k]);
        } else if (x[1] == AMSBean.BOOLEAN) {
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getBooleanData()[k]);
        } else if (x[1] == AMSBean.FLOAT){
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getFloatData()[k]);
        } else if (x[1] == AMSBean.DOUBLE) {
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getDoubleData()[k]);
        } else if (x[1] == AMSBean.STRING){
            for (k=0; k<x[0]; k++) 
            System.out.println("    ["+k+"] = "+fld.getStringData()[k]);
        } else 
            System.out.println("   Unknown  ");
            
    }

     
    static public void main(String args[]) {
        (new TestAMSBean()).test();
    }
    
}