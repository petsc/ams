import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.Color;

// Communicator
public class communicator extends accessorbase {
    // Member variables
    private int m_port;
    private String m_host;
    private int m_comid;

    // Methods
    public communicator(String name, String host, int port)
    {
        m_name = name;
        m_host = host;
        m_port = port;
        m_comid = -1;
        m_children = new Vector(10, 5);
        setColor(Color.pink);
    }

    public communicator(String name, accessor acc)
    {   // Parse the string name|host|port
        StringTokenizer st = new StringTokenizer(name, "|");
        if (st.hasMoreTokens())
            m_name = st.nextToken();
        if (st.hasMoreTokens())
            m_host = st.nextToken();
        if (st.hasMoreTokens()) {
            Integer port = new Integer(st.nextToken());
            m_port = port.intValue();
        }
        m_comid = -1;
        m_children = new Vector(10, 5);
        m_acc = acc;
        setColor(Color.pink);
    }

    public void attachChildren()
        throws accessorexception
    {
        m_acc.checkErrorAMS_Comm_attach(getName());
        m_acc.checkErrorAMS_Comm_get_memory_list(getComid());
    }
    public void update(String str) {;}

    public void setPort(int port) { m_port = port; }
    public int getPort() { return m_port; }

    public void setHost(String host) { m_host = host; }
    public String getHost() { return m_host; }

    public void setComid(int comid) { m_comid = comid; }
    public int getComid() { return m_comid; }

    public void add(memory mem)
    {
        super.add(mem);
    }

    public void add(String name)
    {
        memory mem = new memory(name);
        mem.setComid(m_comid);
        mem.setAccessor(m_acc);
        super.add(mem);
    }


    public memory getMemory(int memid)
    {
        memory mem;

        int index = 0;
        for (Enumeration e = m_children.elements(); e.hasMoreElements();index++)
        {
            mem = (memory)(e.nextElement());
            if (memid == mem.getMemid())
                return mem;
        }

        return null;
     }
}
