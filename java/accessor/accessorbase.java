import java.util.Vector;
import java.util.Enumeration;
import java.awt.Color;
import java.awt.Font;

public abstract class accessorbase extends Object {
    // Members
    protected Font          font = null;
    protected Color         color = Color.blue; /* Default is blue */

    protected String m_name;
    protected String m_img;
    protected String m_val = null;
    protected Vector m_children;
    protected boolean m_canHaveChildren = true;
    protected boolean m_isEditable = false;
    protected int m_id;

    protected accessor m_acc; /* Interface Class to Native Code */

    // Methods
    public abstract void attachChildren() throws accessorexception;
    public abstract void update(String str) throws accessorexception;

    public void setFont(Font newFont) {	font = newFont;}
    public Font getFont() { return font;}
    public void setColor(Color newColor) {color = newColor;}
    public Color getColor() { return color;}

    public void setId(int id) { m_id = id; }
    public int getId() { return m_id; }
    
    public void setString(String newString) { setValue(newString);}
    public String string() { return toString();}

    public boolean isLeaf() { return !m_canHaveChildren;}
    public void setAccessor(accessor acc) {m_acc = acc;}

    public boolean isEditable() { return m_isEditable; }
    public String toString(){
        if (m_val != null) {
            return m_name + " = " + m_val;
        } else {
            return m_name;
        }
    }

    public Vector getChildren(){return m_children;}

    public void  setName(String name) {m_name = name;}
    public String getName() { return m_name; }

    public void  setImage(String img) {m_img = img;}
    public String getImage() { return m_img; }

    public void  setValue(String val) {m_val = val;}
    public String getValue() { return m_val; }

    // What equality means for us
    public boolean equals( Object obj) {
        String str1 = getName(), str2;

        if (obj.getClass().getName() == "String"){
            str2 = (String)obj;
        } else {
            str2 = ((accessorbase)obj).getName();
        }

        return (str1.compareTo(str2) == 0);
    }

    public void add(accessorbase child)
    {
        // Update if child exists
        int index = m_children.indexOf(child);
        if (index != -1) {
            m_children.setElementAt(child, index);
        } else {
            m_children.addElement(child);
        }
    }

    public void remove(accessorbase child)
    {
        m_children.removeElement(child);
    }

    public int getNbChildren()
    {
        return m_children.size();
    }

    public String[] getStrChildren()
    {
        String Str[] = new String[m_children.size()];

        int index = 0;
        for (Enumeration e = m_children.elements();e.hasMoreElements();index++)
        {
            Str[index] = ((accessorbase)e.nextElement()).getName();
        }
        return Str;
    }

    public Object getChild(String name)
    {
        Object child;
        for (Enumeration e = m_children.elements() ; e.hasMoreElements(); )
        {
            child = e.nextElement();
            if (((accessorbase)child).getName() == name){
                return child;
            }
        }
        return null;
    }

}

