import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import java.awt.GridLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Container;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.awt.event.KeyEvent;

import javax.swing.tree.*;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.lang.Thread;


/**
  * ALICE Memory Snooper: Accessor application
  * Author: Ibrahima Ba, ibrahba@mcs.anl.gov
  */

import javax.swing.JTree;
import java.awt.*;
public class jaccessor extends JPanel
{
    /** Window for showing Tree. */
    public static JFrame frame;

    /** Panel for tree */
   	JPanel splitPanePanel, splitPanePanel1;
   	JSplitPane splitPane, splitPane1;
   	JDesktopPane winDesktop;
   	JTextArea   textArea;

   	/** Panel for the field */
   	JInternalFrame  fldFrame;

    /** Tree model. */
    protected DefaultTreeModel        treeModel;

    /** Accessor Tree Node. */
    protected dynamicaccnode accTree;

    /** Tree that contains the nodes */
   	MyTree tree;

    /** Timer for refresh delays */
    Timer timer;

    /** Auto Refresh  Parameters */
    int refresh_sec = 0;
    int refresh_min = 0;
    boolean refresh_enabled = false;

    // The width and height of the frame
    public static int WIDTH = 600;
    public static int HEIGHT = 400;

    /** Accessor object */
    accessor acc;

    /** Are we connected or not */
    int connected = 0;

    /**
      * Constructs a new instance of the Accessor.
      */
    public jaccessor() {

        // Create a new border layout
        setLayout(new BorderLayout());

        /* Split pane */
        splitPanePanel = new JPanel();
        splitPanePanel.setDoubleBuffered(true);
	    splitPanePanel.setLayout(new BorderLayout());

        splitPanePanel1 = new JPanel();
        splitPanePanel1.setDoubleBuffered(true);
	    splitPanePanel1.setLayout(new BorderLayout());

	    splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	    splitPane.setContinuousLayout(true);
	    splitPane.setPreferredSize(new Dimension(WIDTH, HEIGHT));

	    splitPane1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	    splitPane1.setContinuousLayout(true);
	    splitPane1.setPreferredSize(new Dimension(WIDTH, HEIGHT - 100));

	    splitPanePanel1.add(splitPane1, BorderLayout.CENTER);
    	splitPanePanel.add(splitPane, BorderLayout.CENTER);

        /* Add the text area for info or error messages */
    	textArea = new JTextArea();
    	textArea.setEditable(false);

    	/* Create the panel that will hold the field window */
    	winDesktop = new JDesktopPane();
	    winDesktop.setOpaque(true);
	    winDesktop.setPreferredSize(new Dimension(300, HEIGHT - 100));

        JMenuBar  menuBar = constructMenuBar();

    	add(menuBar, BorderLayout.NORTH);
        add(splitPanePanel,BorderLayout.CENTER);

        frame.getContentPane().add(this, BorderLayout.CENTER);
	frame.setSize(new Dimension(WIDTH, HEIGHT));
        frame.pack();
        frame.show();
        Connect();
    }

    /** Create and Add Tree to Panel */
    public void createSplitPanel ()
    {
    	/* Create the Accessor Tree Model. */
        accTree = new dynamicaccnode();

        treeModel = new DefaultTreeModel(accTree){
            public void valueForPathChanged(TreePath path, Object newValue) {
            	/* Update the user object. */
            	DefaultMutableTreeNode      aNode = (DefaultMutableTreeNode)path.getLastPathComponent();
            	accessorbase    accessorBase = (accessorbase)aNode.getUserObject();

                String str = (String)newValue;
                try {
                    Double d = new Double(str);
                	accessorBase.setValue(str);
                	accessorBase.update(str);
                   	/* Since we've changed how the data is displayed, message
                       nodeChanged. */
            	    nodeChanged(aNode);

                } catch (NumberFormatException e) {
                } catch (accessorexception e1) {}

            }
        };

    	/* Create the tree. */
    	tree = new MyTree(treeModel);
    	tree.setCellRenderer(new accessorcellrenderer());
        tree.putClientProperty( "JTree.lineStyle", "Angled" );

    	/* Create the tree. */
    	tree.addTreeExpansionListener(accTree);
    	tree.addMouseListener(accTree);
    	tree.setCellEditor( new accessorcelleditor(new JTextField()));
        tree.setEditable(true);

    	/* Put the Tree in a scroller. */
    	JScrollPane sp = new JScrollPane();
    	sp.getViewport().add(tree);
    	sp.setMinimumSize(new Dimension(10, 10));
    	sp.setPreferredSize(new Dimension(WIDTH -200, HEIGHT - 100));

    	/* Put the text area in a scroller. */
    	JScrollPane        sp1 = new JScrollPane();
    	sp1.getViewport().add(textArea);
    	sp1.setMinimumSize(new Dimension(WIDTH, 10));

    	/* Put the Desktop Pane in a scroller */
    	JScrollPane        sp2 = new JScrollPane();
    	sp2.getViewport().add(winDesktop);
    	sp2.setMinimumSize(new Dimension(10, 10));
    	sp2.setPreferredSize(new Dimension(200, HEIGHT - 100));

        // First Split Pane
       	splitPane1.setLeftComponent(sp);
    	splitPane1.setRightComponent(sp2);

    	splitPane.setTopComponent(splitPanePanel1);
    	splitPane.setBottomComponent(sp1);

    	frame.show();
    }

    public void Connect()
    {
    	int err, port = -1;
        String host = null;

        connectdialog dlg = new connectdialog(frame);
        dlg.pack();
        dlg.setLocationRelativeTo(frame);
        dlg.show();

        host = dlg.getHost();
        port = dlg.getPort();

    	if(host != null) {
    	    if (host == "Default")
    	        acc = new accessor(new publisher(host, "", port));
    	    else
 		    acc = new accessor(new publisher(host, host, port));

            createSplitPanel();

            /* Create a timer */
            timer = new Timer(10000, new RefreshAction());

            /* We are connected, but this might not be true */
            connected = 1;
        }
    }

    /**
      * Refresh: Menu item that will the refresh the tree displayed by reloading all
      * the nodes from the publisher.
      */
    public void Refresh()
    {
        if (connected == 0)
            return;

        if (tree.isEditing())
            return; // Do not refresh while editing
        tree.setEditable(false);
        tree.fireTreeRefresh();
        tree.setEditable(true);
    }

    /**
      * Auto Refresh: Menu item to set up the refresh time
      */
    public void AutoRefresh()
    {
        refreshdialog dlg = new refreshdialog(frame, refresh_enabled, refresh_min, refresh_sec);
        dlg.pack();
        dlg.setLocationRelativeTo(frame);
        dlg.show();

        refresh_min = dlg.getMinutes();
        refresh_sec = dlg.getSeconds();
        refresh_enabled = dlg.isChecked();

       if (refresh_enabled && connected == 1) {
            if (!(refresh_sec < 0) && !(refresh_min < 0)) {
                timer.setDelay(refresh_sec*1000 + refresh_min*1000000);
                timer.start();
            }
        } else {
            // suspend the thread
            timer.stop();
        }
    }

    /** Construct a menu. */
    private JMenuBar constructMenuBar()
    {
    	JMenu            menu;
    	JMenuBar         menuBar = new JMenuBar();
    	JMenuItem        menuItem1, menuItem2, menuItem3, menuItem;

    	/* Good all exit. */
    	menu = new JMenu("File");
       	menu.setMnemonic('f');

    	menu.getAccessibleContext().setAccessibleDescription("Connect menu: to connect, disconnect");
    	menuBar.add(menu);

    	menuItem = menu.add(new JMenuItem("Exit"));
    	menuItem.addActionListener(new ActionListener() {
    	    public void actionPerformed(ActionEvent e) {
    		System.exit(0);
        }});

        /* Connect Menu Item */
        menuItem = menu.add(new JMenuItem("Connect"));
    	menuItem.addActionListener(new ConnectAction());

    	/* View */
    	menu = new JMenu("View");
       	menu.setMnemonic('v');

    	menu.getAccessibleContext().setAccessibleDescription("View menu: Lets you refresh the tree display");
    	menuBar.add(menu);

        /* Refresh menu item */
    	menuItem = menu.add(new JMenuItem("Refresh"));
    	menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        menuItem.addActionListener(new RefreshAction());

        /* Auto Refresh menu item */
    	menuItem = menu.add(new JMenuItem("Auto Refresh"));
    	menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, ActionEvent.ALT_MASK));
        menuItem.addActionListener(new AutoRefreshAction());


        /* Options Menu */
    	JMenu options = (JMenu) menuBar.add(new JMenu("Options"));
        options.setMnemonic('p');
    	options.getAccessibleContext().setAccessibleDescription("Look and Feel options: select one of the L&F");

        // Look and Feel Radio control
    	ButtonGroup group = new ButtonGroup();
    	toggleuilistener toggleUIListener = new toggleuilistener(frame);

        menuItem1 = (JRadioButtonMenuItem) options.add(new JRadioButtonMenuItem("Windows Style Look and Feel"));
    	menuItem1.setSelected(UIManager.getLookAndFeel().getName().equals("Windows"));
    	group.add(menuItem1);
    	menuItem1.addItemListener(toggleUIListener);
    	menuItem1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));

        menuItem2 = (JRadioButtonMenuItem) options.add(new JRadioButtonMenuItem("Motif Look and Feel"));
    	menuItem2.setSelected(UIManager.getLookAndFeel().getName().equals("CDE/Motif"));
    	group.add(menuItem2);
    	menuItem2.addItemListener(toggleUIListener);
    	menuItem2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));

        menuItem3 = (JRadioButtonMenuItem) options.add(new JRadioButtonMenuItem("Metal Look and Feel"));
    	menuItem3.setSelected(UIManager.getLookAndFeel().getName().equals("Metal"));
    	menuItem3.setSelected(true);
    	group.add(menuItem3);
    	menuItem3.addItemListener(toggleUIListener);
    	menuItem3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.ALT_MASK));

    	return menuBar;
    }

    static public void main(String args[]) {

    	frame = new JFrame("ALICE Memory Browser");
    	frame.getAccessibleContext().setAccessibleDescription("ALICE Memory Browser JAVA Application");
    	JOptionPane.setRootFrame(frame);

    	frame.setBackground(Color.white);
        frame.setSize(WIDTH, HEIGHT);
      	frame.getContentPane().setLayout(new BorderLayout());

        /* Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    	frame.setLocation(screenSize.width/2 - WIDTH/2,
    			  screenSize.height/2 - HEIGHT/2);
			  */
    	frame.setLocation(WIDTH/3,
    			  HEIGHT/3);

    	frame.addWindowListener( new WindowAdapter() {
    	    public void windowClosing(WindowEvent e) {System.exit(0);}});

    	new jaccessor();
    }

    /**
      * ConnectAction is used to connect an ALICE Memory Publisher.
      */
    class ConnectAction extends Object implements ActionListener
    {
        /* Implement the action to be performed */
	    public void actionPerformed(ActionEvent e) {
            Connect();
	    }

	}

    /**
      * Refresh Action: refreshes the tree.
      */
    class RefreshAction extends Object implements ActionListener
    {
        /* Implement the action to be performed */
	    public void actionPerformed(ActionEvent e) {
            Refresh();
	    }

	}

    /**
      * Auto Refresh Action: sets up the time.
      */
    class AutoRefreshAction extends Object implements ActionListener
    {
        /* Implement the action to be performed */
	    public void actionPerformed(ActionEvent e) {
            AutoRefresh();
	    }
	}

    class ToggleLogging extends AbstractAction {
        public void actionPerformed(ActionEvent e) {
            ;
        }
        public boolean isEnabled() {
            return true;
        }
    }

    class MyTree extends JTree {
        public MyTree(TreeModel model) {
            super(model);
        }

        public void fireTreeExpanded(TreePath path) {
            dynamicaccnode aNode = (dynamicaccnode)path.getLastPathComponent();
            try {
      	        aNode.loadChildren();
      	    } catch (accessorexception e) {}
            super.fireTreeExpanded(path);
        }

        public void fireTreeRefresh() {
            dynamicaccnode rootNode = (dynamicaccnode) getModel().getRoot();
            if (rootNode != null) {
                try {
          	        rootNode.refreshNode(this);
          	    } catch (accessorexception e) {}
          	}
        }

    }

    /** Thread class for the timer */
    public class ThreadTimer extends Thread {
        Timer tmr = new Timer(5000, new RefreshAction());

        public void run() {
            tmr.start() ;
        }

        public void setDelay(int del) {
            if (del > 0)
                tmr.setDelay(del);
        }

        public void restart() {
            tmr.start();
            resume();
        }
        public void stopit() {
            tmr.stop();
            suspend();
        }
    }

}
