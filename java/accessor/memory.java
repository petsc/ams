import java.util.StringTokenizer;
import java.util.Vector;
import java.awt.Color;

    // Memory Class
    public class memory extends accessorbase {
        // Member variables
        private int m_memid;
        private int m_comid;

        // Methods
        public memory(String name)
        {
            m_name = name;
            m_memid = -1;
            m_children = new Vector(10, 5);
            setColor(Color.darkGray);
        }

        public void attachChildren()
                throws accessorexception
        {
	    int flag = 0, count = 0;
            m_acc.checkErrorAMS_Memory_attach(m_comid, this);
            m_acc.checkErrorAMS_Memory_update_recv_end(getMemid(), flag, count);
            m_acc.checkErrorAMS_Memory_get_field_list(this);
        }
        public void update(String str) {;}

        public void setMemid(int memid) { m_memid = memid; }
        public int getMemid() { return m_memid; }

        public void setComid(int comid) { m_comid = comid; }
        public int getComid() { return m_comid; }
    }


