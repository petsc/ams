#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#ifndef _WIN_ALICE
#include <unistd.h>
#endif

#include "gov_anl_mcs_ams_AMS_0005fField.h"
#include "ams.h"

#ifdef __cplusplus
extern "C" {
#endif

extern JNIEnv *ams_env;
extern jobject ams;

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fField
 * Method:    set_field_info
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_gov_anl_mcs_ams_AMS_1Field_set_1field_1info
(JNIEnv *env, jobject obj) 
{
    int err, len, *intarr = NULL, i;
    float *floatarr = NULL;
    double *doublearr = NULL;
    const char *name_str;
    char **strarr = NULL;
    void *addr;
    char *msg;

    AMS_Memory memid;
    AMS_Data_type dtype;

    jstring jstr, jname;
    jintArray jintarr = NULL;
    jobjectArray jstrarr = NULL;
    jfloatArray jfloatarr = NULL;
    jdoubleArray jdoublearr = NULL;
    jint jlen;
    jmethodID mid;
    jobject jmem;

    jclass fldcls = (*env)->GetObjectClass(env, obj);
    jclass memcls = (*env)->FindClass(env, "gov/anl/mcs/ams/AMS_Memory");

    /* Initialize the global variables */
    ams_env = env;

    /* Get field name */
    mid = (*env)->GetMethodID(env, fldcls, "getName", "()Ljava/lang/String;");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return ;
    }

    /* Call the AMS_Field (Java) method */
    jname = (*env)->CallObjectMethod(env, obj, mid);
    name_str = (*env)->GetStringUTFChars(env, jname, 0);

    /* Get Memory Object */
    mid = (*env)->GetMethodID(env, fldcls, "getMemory", "()Lgov/anl/mcs/ams/AMS_Memory;");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return ;
    }

    jmem = (*env)->CallObjectMethod(env, obj, mid);

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return ;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, jmem, mid);

    /* Data type */
    mid = (*env)->GetMethodID(env, fldcls, "getDataType", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return ;
    }

    dtype = (AMS_Data_type) (*env)->CallIntMethod(env, obj, mid);

    /* Data Length */
    mid = (*env)->GetMethodID(env, fldcls, "getLength", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return ;
    }

    jlen = (AMS_Data_type) (*env)->CallIntMethod(env, obj, mid);

    /* 
     * Given the data type and length, store data into an address space
     */

    switch(dtype) {

    case AMS_INT:
    case AMS_BOOLEAN:

        mid = (*env)->GetMethodID(env, fldcls, "getIntData", "()[I");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return ;
        }

        jintarr = (*env)->CallObjectMethod(env, obj, mid);

        if (jintarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return ;
        }

        intarr = (int *)(*env)->GetIntArrayElements(env, jintarr, 0);
        
        len = jlen;
        addr = intarr;
        break;

    case AMS_FLOAT:
        mid = (*env)->GetMethodID(env, fldcls, "getFloatData", "()[F");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return ;
        }

        jfloatarr = (*env)->CallObjectMethod(env, obj, mid);

        if (jfloatarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return ;
        }

        floatarr = (float *)(*env)->GetFloatArrayElements(env, jfloatarr, 0);
        
        len = jlen;
        addr = floatarr;
        break;

    case AMS_DOUBLE:
        mid = (*env)->GetMethodID(env, fldcls, "getDoubleData", "()[D");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return ;
        }

        jdoublearr = (*env)->CallObjectMethod(env, obj, mid);

        if (jdoublearr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return ;
        }

        doublearr = (double *)(*env)->GetDoubleArrayElements(env, jdoublearr, 0);
        
        len = jlen;
        addr = doublearr;
        break;

    case AMS_STRING:
        mid = (*env)->GetMethodID(env, fldcls, "getStringData", "()[Ljava/lang/String;");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return ;
        }

        jstrarr = (*env)->CallObjectMethod(env, obj, mid);

        if (jstrarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return ;
        }

        strarr = (char **)malloc(jlen*sizeof(char *));
        if (strarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_INSUFFICIENT_MEMORY, &msg);
            return ;
        }

        for (i = 0; i < jlen; i++) {
            jstr = (*env)->GetObjectArrayElement(env, jstrarr, i);
            strarr[i] = (char *) (*env)->GetStringUTFChars(env, jstr, 0);
        }

        len = jlen;
        addr = (void *)strarr;
		break;

    default:
        AMS_Print_error_msg(AMS_ERROR_INVALID_DATA_TYPE, &msg);
        return  ;
        break;
    }
        
    /* Call the Native method */
    err = AMS_Memory_set_field_info(memid, (char *)name_str, addr, len);

    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return ;
    }

    (*env)->ReleaseStringUTFChars(env, jname, name_str);

    if (dtype == AMS_INT)
        (*env)->ReleaseIntArrayElements(env, jintarr, (long *)intarr, 0);

    if (dtype == AMS_BOOLEAN)
        (*env)->ReleaseIntArrayElements(env, jintarr, (long *)intarr, 0);

    if (dtype == AMS_FLOAT)
        (*env)->ReleaseFloatArrayElements(env, jfloatarr, floatarr, 0);

    if (dtype == AMS_DOUBLE)
        (*env)->ReleaseDoubleArrayElements(env, jdoublearr, doublearr, 0);

    if (dtype == AMS_STRING) {
        for (i = 0; i < jlen; i++) {
            jstr = (*env)->GetObjectArrayElement(env, jstrarr, i);
            (*env)->ReleaseStringUTFChars(env, jstr, strarr[i]);
        }
        free(strarr);
    }

    /* We are done. Return the error code */
    return ;
}

#ifdef __cplusplus
}
#endif

