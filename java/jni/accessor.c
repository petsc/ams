#include <jni.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#ifndef _WIN_ALICE
#include <unistd.h>
#endif

#include "accessor.h"
#include "ams.h"


#ifdef __cplusplus
extern "C" {
#endif

    /*
     * Class:     accessor
     * Method:    AMS_Java_init
     * Signature: ()I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Java_1init
        (JNIEnv *env, jobject obj)

        {
            jclass cls = (*env)->GetObjectClass(env, obj);  
            jfieldID fid;  

            /* AMS_Memory_type: AMS_READ, AMS_WRITE, AMS_MEMORY_UNDEF */
            fid = (*env)->GetStaticFieldID(env, cls, "AMS_READ", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_READ);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_WRITE", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_WRITE);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_MEMORY_UNDEF", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_MEMORY_UNDEF);

        
            /* 
             * AMS_Data_type: AMS_INT, AMS_FLOAT, AMS_DOULE, AMS_DATA_UNDEF 
             */
            fid = (*env)->GetStaticFieldID(env, cls, "AMS_INT", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_INT);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_DOUBLE", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_DOUBLE);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_FLOAT", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_FLOAT);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_STRING", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_STRING);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_DATA_UNDEF", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_DATA_UNDEF);

            /* 
             * AMS_Shared_type: AMS_COMMON, AMS_REDUCED, AMS_DISTRIBUTED, AMS_SHARED_UNDEF 
             */
            fid = (*env)->GetStaticFieldID(env, cls, "AMS_COMMON", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_COMMON);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_REDUCED", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_REDUCED);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_DISTRIBUTED", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_DISTRIBUTED);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_SHARED_UNDEF", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_SHARED_UNDEF);


            /* 
             * AMS_Reduction_type: AMS_SUM, AMS_MAX, AMS_MIN, AMS_REDUCT_UNDEF 
             */
            fid = (*env)->GetStaticFieldID(env, cls, "AMS_SUM", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_SUM);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_MAX", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_MAX);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_MIN", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_MIN);

            fid = (*env)->GetStaticFieldID(env, cls, "AMS_REDUCT_UNDEF", "I");  
            if (fid == 0)
                return AMS_ERROR_JAVA_JNI_GET_FID;

            (*env)->SetStaticIntField(env, cls, fid, AMS_REDUCT_UNDEF);

            return AMS_ERROR_NONE; /* What else could it be ? */

        }

    /*
     * Class:     accessor
     * Method:    AMS_Connect
     * Signature: (Ljava/lang/String;I)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Connect
        (JNIEnv *env, jobject obj, jstring name, jint port)
        {
            extern void start_debug(void);
            int err, i;
            char **com_list;
            const char *name_str;

            jstring jstr; 
            jmethodID mid;
            jsize len;
            jclass cls = (*env)->GetObjectClass(env, obj);

            name_str = (*env)->GetStringUTFChars(env, name, 0);

            /* Call the native method */
            err = AMS_Connect((char *)name_str, (int)port, &com_list);

            (*env)->ReleaseStringUTFChars(env, name, name_str);

            /* Create the list of communicators */
            if (err == AMS_ERROR_NONE) {

                mid = (*env)->GetMethodID(env, cls, "addCommItem", "(Ljava/lang/String;)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                /* Compute Array length */
                i = 0;
                while (com_list[i])
                    i++;
                len = i;
                
                for (i = 0; i<len; i++) {
                    /* Call the accessor (Java) method */
                    jstr = (*env)->NewStringUTF(env, com_list[i]);
                    (*env)->CallVoidMethod(env, obj, mid, jstr);
                }

            }

            /* We are done. Return the error code */
            return err;
        }


    /*
     * Class:     accessor
     * Method:    AMS_Comm_attach
     * Signature: (Ljava/lang/String;)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Comm_1attach
        (JNIEnv *env, jobject obj, jstring name)
        {
        
            int err;
            AMS_Comm alice;
            const char *name_str;

            jmethodID mid;
            jclass cls = (*env)->GetObjectClass(env, obj);

            name_str = (*env)->GetStringUTFChars(env, name, 0);

            /* Call the Native method */
            err = AMS_Comm_attach((char *)name_str, &alice);

            (*env)->ReleaseStringUTFChars(env, name, name_str);

            /* Set the value for alice member by calling a Java class method */
            if (err == AMS_ERROR_NONE) {
                mid = (*env)->GetMethodID(env, cls, "setAliceComm", "(Ljava/lang/String;I)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;;
  
                /* Call the accessor (Java) mehod */
                (*env)->CallVoidMethod(env, obj, mid, name, alice);
            }


            /* We are done. Return the error code */
            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Comm_get_memory_list
     * Signature: (I)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Comm_1get_1memory_1list
        (JNIEnv *env, jobject obj, jint com)

        {
            int err, i;
            AMS_Comm alice = (AMS_Comm) com;
            char **mem_list;

            jstring jstr; 
            jmethodID mid;
            jsize len;
            jclass cls = (*env)->GetObjectClass(env, obj);

            /* Call the native method */
            err = AMS_Comm_get_memory_list(alice, &mem_list);

            /* Create a list of memory within the communicator */
            if (err == AMS_ERROR_NONE) {

                mid = (*env)->GetMethodID(env, cls, "addAliceMemItem", "(ILjava/lang/String;)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                /* Compute Array length */
                i = 0;
                while (mem_list[i])
                    i++;
                len = i;
                
                for (i = 0; i<len; i++) {
                    /* Call the accessor (Java) method */
                    jstr = (*env)->NewStringUTF(env, mem_list[i]);
                    (*env)->CallVoidMethod(env, obj, mid, alice, jstr);
                }

            }

            /* We are done. Return the error code */
            return err;
        }


    /*
     * Class:     accessor
     * Method:    AMS_Memory_attach
     * Signature: (ILmemory;)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Memory_1attach
        (JNIEnv *env, jobject obj, jint com, jobject memobj)
        {
            int err;
            AMS_Comm alice = (AMS_Comm) com;
            AMS_Memory memid;
            const char *name_str;

            jmethodID mid;
            jstring jstr;
            jclass mem = (*env)->GetObjectClass(env, memobj);

            /* Get the memory name */
            mid = (*env)->GetMethodID(env, mem, "getName", "()Ljava/lang/String;");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;;

            /* Call the accessor (Java) method */
            jstr = (*env)->CallObjectMethod(env, memobj, mid);
            name_str = (*env)->GetStringUTFChars(env, jstr, 0);

            /* Call the Native method */
            err = AMS_Memory_attach(alice, (char *)name_str, &memid, NULL);

            (*env)->ReleaseStringUTFChars(env, jstr, name_str);

            /* set the memory id */
            if (err == AMS_ERROR_NONE) {
                mid = (*env)->GetMethodID(env, mem, "setMemid", "(I)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;;
  
                /* Call the accessor (Java) method */
                (*env)->CallVoidMethod(env, memobj, mid, memid);
            }

            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Memory_get_field_list
     * Signature: (Lmemory;)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Memory_1get_1field_1list
        (JNIEnv *env, jobject obj, jobject memobj)
        {
            int err, i;
            AMS_Memory memid;
            char **fld_list;

            jstring jstr; 
            jmethodID mid;
            jsize len;
            jclass cls = (*env)->GetObjectClass(env, obj);
            jclass mem = (*env)->GetObjectClass(env, memobj);

            /* Get the memory id */
            mid = (*env)->GetMethodID(env, mem, "getMemid", "()I");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;;
  
            /* Call the accessor (Java) method */
            memid = (AMS_Memory) (*env)->CallIntMethod(env, memobj, mid);

            /* Call the native method */
            err = AMS_Memory_get_field_list(memid, &fld_list);

            /* Create and add the list of fields to the memory */
            if (err == AMS_ERROR_NONE) {

                mid = (*env)->GetMethodID(env, cls, "addFieldItem", "(Lmemory;Ljava/lang/String;)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                /* Compute Array length */
                i = 0;
                while (fld_list[i])
                    i++;
                len = i;
                
                for (i = 0; i<len; i++) {
                    /* Call the accessor (Java) method */
                    jstr = (*env)->NewStringUTF(env, fld_list[i]);
                    (*env)->CallVoidMethod(env, obj, mid, memobj, jstr);
                }

            }

            /* We are done. Return the error code */
            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Memory_get_field_info
     * Signature: (Lmemory;Lfield;)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Memory_1get_1field_1info
        (JNIEnv *env, jobject obj, jobject memobj, jobject fldobj)
        {
            int err, len, *intarr, i;
            float *floatarr;
            double *doublearr;
            const char *name_str;
            char **data_str, **tmpstr;
            void *addr;

            AMS_Memory memid;
            AMS_Memory_type mtype;
            AMS_Data_type dtype;
            AMS_Shared_type stype;
            AMS_Reduction_type rtype;

            jstring jstr;
            jobjectArray jstrarr;
            jintArray jintarr;
            jfloatArray jfloatarr;
            jdoubleArray jdoublearr;
            jint jlen;
            jmethodID mid;
            jclass fld = (*env)->GetObjectClass(env, fldobj);
            jclass mem = (*env)->GetObjectClass(env, memobj);

            /* Access field name */
            mid = (*env)->GetMethodID(env, fld, "getName", "()Ljava/lang/String;");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;;
  
            /* Call the accessor (Java) method */
            jstr = (*env)->CallObjectMethod(env, fldobj, mid);
            name_str = (*env)->GetStringUTFChars(env, jstr, 0);

            /* Access Memory Id */
            mid = (*env)->GetMethodID(env, mem, "getMemid", "()I");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;;
  
            /* Call the accessor (Java) method */
            memid = (AMS_Memory) (*env)->CallIntMethod(env, memobj, mid);

            /* Call the Native method */
            err = AMS_Memory_get_field_info(memid, (char *)name_str, &addr, &len, &dtype, &mtype, &stype, &rtype);

            (*env)->ReleaseStringUTFChars(env, jstr, name_str);

            /* Set the value for accessor members by calling a Java class method */
            if (err == AMS_ERROR_NONE) {

                /* Memory type */
                mid = (*env)->GetMethodID(env, fld, "setMemoryType", "(I)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                (*env)->CallVoidMethod(env, fldobj, mid, mtype);
                
                /* Data type */
                mid = (*env)->GetMethodID(env, fld, "setDataType", "(I)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                (*env)->CallVoidMethod(env, fldobj, mid, dtype);
                
                /* Shared type */
                mid = (*env)->GetMethodID(env, fld, "setSharedType", "(I)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                (*env)->CallVoidMethod(env, fldobj, mid, stype);

                /* Reduction type */
                mid = (*env)->GetMethodID(env, fld, "setReductionType", "(I)V");
                if (mid == 0)
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                (*env)->CallVoidMethod(env, fldobj, mid, rtype);

                
                /* 
                 * Given the data type and length, copy data into an appropriate
                 * Java field 
                 */

                switch(dtype) {
                case AMS_INT:
                    jlen = len;
                    jintarr = (*env)->NewIntArray(env, jlen);
                    if (jintarr == NULL)
                        return AMS_ERROR_JAVA_SYSTEM;

                    /* Allocate memory for temporary array */
                    intarr = (int *) malloc(sizeof(int)*jlen);
                    if (intarr == NULL)
                        return AMS_ERROR_INSUFFICIENT_MEMORY;

                    for (i = 0 ;i<jlen; i++)
                        intarr[i] = *( ((int *)addr) + i);

                    (*env)->SetIntArrayRegion(env, jintarr, 0, jlen , (long *)intarr);
                
                    /* Data length */
                    mid = (*env)->GetMethodID(env, fld, "setLength", "(I)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jlen);

                    /* Data Array */
                    mid = (*env)->GetMethodID(env, fld, "setData", "([I)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jintarr);

                    break;
                case AMS_DOUBLE:

                    jlen = len;
                    jdoublearr = (*env)->NewDoubleArray(env, jlen);
                    if (jdoublearr == NULL)
                        return AMS_ERROR_JAVA_SYSTEM;

                    /* Allocate memory for temporary array */
                    doublearr = (double *) malloc(sizeof(double)*jlen);
                    if (doublearr == NULL)
                        return AMS_ERROR_INSUFFICIENT_MEMORY;

                    for (i = 0 ;i<jlen; i++)
                        doublearr[i] = *( ((double *)addr) + i);

                    (*env)->SetDoubleArrayRegion(env, jdoublearr, 0, jlen , doublearr);
                
                    /* Data length */
                    mid = (*env)->GetMethodID(env, fld, "setLength", "(I)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jlen);

                    /* Data Array */
                    mid = (*env)->GetMethodID(env, fld, "setData", "([D)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jdoublearr);

                    break;

                case AMS_FLOAT:

                    jlen = len;
                    jfloatarr = (*env)->NewFloatArray(env, jlen);
                    if (jfloatarr == NULL)
                        return AMS_ERROR_JAVA_SYSTEM;

                    /* Allocate memory for temporary array */
                    floatarr = (float *) malloc(sizeof(float)*jlen);
                    if (floatarr == NULL)
                        return AMS_ERROR_INSUFFICIENT_MEMORY;

                    for (i = 0 ;i<jlen; i++)
                        floatarr[i] = *( ((float *)addr) + i);

                    (*env)->SetFloatArrayRegion(env, jfloatarr, 0, jlen , floatarr);

                    /* Data length */
                    mid = (*env)->GetMethodID(env, fld, "setLength", "(I)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jlen);

                    /* Data Array */
                    mid = (*env)->GetMethodID(env, fld, "setData", "([F)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jfloatarr);

                    break;

                case AMS_STRING:

                    jlen = len;
                    /* Allocate memory for temporary array */
                    data_str = (char **) malloc(sizeof(char *)*(jlen + 1));
                    if (data_str == NULL)
                        return AMS_ERROR_INSUFFICIENT_MEMORY;

                    tmpstr = (char **)addr;
                    for (i = 0; i < jlen ;i++) {
                        if (tmpstr[i]) {
                            data_str[i] = (char *) malloc(strlen(tmpstr[i]) + 1);
                            if (data_str[i] == NULL)
                                return AMS_ERROR_INSUFFICIENT_MEMORY;

                            strcpy(data_str[i], tmpstr[i]);
                        } else 
                            data_str[i] = NULL;
                    }

                    jstrarr = (*env)->NewObjectArray(env, jlen, (*env)->FindClass(env, "java/lang/String"), NULL);
                    if (jstrarr == NULL)
                        return AMS_ERROR_JAVA_SYSTEM;
                        
                    for (i = 0; i < jlen; i++) {
                        if (data_str[i]) {
                            jstr = (*env)->NewStringUTF(env, data_str[i]);
                            (*env)->SetObjectArrayElement(env, jstrarr, i, jstr);
                        }
                    }

                    /* Data length */
                    mid = (*env)->GetMethodID(env, fld, "setLength", "(I)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jlen);

                    /* Data Array (List of strings) */
                    mid = (*env)->GetMethodID(env, fld, "setData", "([Ljava/lang/String;)V");
                    if (mid == 0)
                        return AMS_ERROR_JAVA_JNI_GET_MID;
  
                    (*env)->CallVoidMethod(env, fldobj, mid, jstrarr);
                    break;

                default:
                    return AMS_ERROR_INVALID_DATA_TYPE;
                }
            }

            /* We are done. Return the error code */
            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Memory_set_field_info
     * Signature: (Lmemory;Lfield;)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Memory_1set_1field_1info
        (JNIEnv *env, jobject obj, jobject memobj, jobject fldobj)
        {
            int err, len, *intarr = NULL, i;
            float *floatarr = NULL;
            double *doublearr = NULL;
            const char *name_str;
            char **strarr = NULL;
            void *addr;

            AMS_Memory memid;
            AMS_Data_type dtype;

            jstring jstr, jname;
            jintArray jintarr = NULL;
            jobjectArray jstrarr = NULL;
            jfloatArray jfloatarr = NULL;
            jdoubleArray jdoublearr = NULL;
            jint jlen;
            jmethodID mid;
            jclass fld = (*env)->GetObjectClass(env, fldobj);
            jclass mem = (*env)->GetObjectClass(env, memobj);

            /* Get field name */
            mid = (*env)->GetMethodID(env, fld, "getName", "()Ljava/lang/String;");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;;
  
            /* Call the accessor (Java) method */
            jname = (*env)->CallObjectMethod(env, fldobj, mid);
            name_str = (*env)->GetStringUTFChars(env, jname, 0);

            /* Data type */
            mid = (*env)->GetMethodID(env, fld, "getDataType", "()I");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;
  
            dtype = (AMS_Data_type) (*env)->CallIntMethod(env, fldobj, mid);

            /* Data Length */
            mid = (*env)->GetMethodID(env, fld, "getLength", "()I");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;
  
            jlen = (AMS_Data_type) (*env)->CallIntMethod(env, fldobj, mid);

            /* 
             * Given the data type and length, read data into an address space
             */

            switch(dtype) {
            case AMS_INT:

                mid = (*env)->GetMethodID(env, fld, "getIntData", "()[I");
                if (mid == 0)
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                jintarr = (*env)->CallObjectMethod(env, fldobj, mid);

                if (jintarr == NULL)
                    return AMS_ERROR_JAVA_SYSTEM;

                intarr = (int *)(*env)->GetIntArrayElements(env, jintarr, 0);
                
                len = jlen;
                addr = intarr;
                break;

            case AMS_FLOAT:
                mid = (*env)->GetMethodID(env, fld, "getFloatData", "()[F");
                if (mid == 0)
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                jfloatarr = (*env)->CallObjectMethod(env, fldobj, mid);

                if (jfloatarr == NULL)
                    return AMS_ERROR_JAVA_SYSTEM;

                floatarr = (*env)->GetFloatArrayElements(env, jfloatarr, 0);
                
                len = jlen;
                addr = floatarr;

                break;
            case AMS_DOUBLE:
                mid = (*env)->GetMethodID(env, fld, "getDoubleData", "()[D");
                if (mid == 0)
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                jdoublearr = (*env)->CallObjectMethod(env, fldobj, mid);

                if (jdoublearr == NULL)
                    return AMS_ERROR_JAVA_SYSTEM;

                doublearr = (*env)->GetDoubleArrayElements(env, jdoublearr, 0);
                
                len = jlen;
                addr = doublearr;
                break;

            case AMS_STRING:
                mid = (*env)->GetMethodID(env, fld, "getStringData", "()[Ljava/lang/String;");
                if (mid == 0)
                    return AMS_ERROR_JAVA_JNI_GET_MID;
  
                jstrarr = (*env)->CallObjectMethod(env, fldobj, mid);

                if (jstrarr == NULL)
                    return AMS_ERROR_JAVA_SYSTEM;

                strarr = (char **)malloc(jlen*sizeof(char *));
                if (strarr == NULL)
                    return AMS_ERROR_INSUFFICIENT_MEMORY;

                for (i = 0; i < jlen; i++) {
                    jstr = (*env)->GetObjectArrayElement(env, jstrarr, i);
                    strarr[i] = (char *) (*env)->GetStringUTFChars(env, jstr, 0);
                }

                len = jlen;
                addr = (void *)strarr;
break;

            default:
                return  AMS_ERROR_INVALID_DATA_TYPE;
                break;
            }
                
            /* Access Memory Id */
            mid = (*env)->GetMethodID(env, mem, "getMemid", "()I");
            if (mid == 0)    
                return AMS_ERROR_JAVA_JNI_GET_MID;;
  
            /* Call the accessor (Java) method */
            memid = (AMS_Memory) (*env)->CallIntMethod(env, memobj, mid);

            /* Call the Native method */
            err = AMS_Memory_set_field_info(memid, (char *)name_str, addr, len);

            (*env)->ReleaseStringUTFChars(env, jname, name_str);
        
            if (dtype == AMS_INT)
                (*env)->ReleaseIntArrayElements(env, jintarr, (long *)intarr, 0);

            if (dtype == AMS_FLOAT)
                (*env)->ReleaseFloatArrayElements(env, jfloatarr, floatarr, 0);

            if (dtype == AMS_DOUBLE)
                (*env)->ReleaseDoubleArrayElements(env, jdoublearr, doublearr, 0);

            if (dtype == AMS_STRING) {
                for (i = 0; i < jlen; i++) {
                    jstr = (*env)->GetObjectArrayElement(env, jstrarr, i);
                    (*env)->ReleaseStringUTFChars(env, jstr, strarr[i]);
                }
                free(strarr);
            }

            /* We are done. Return the error code */
            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Memory_update_send_begin
     * Signature: (I)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Memory_1update_1send_1begin
        (JNIEnv *env, jobject obj, jint memid)
        {
            int err;
            AMS_Memory mem = (AMS_Memory) memid;

            /* Call the native method */
            err = AMS_Memory_update_send_begin(mem);

            /* We are done. Return the error code */
            return err;

        }

    /*
     * Class:     accessor
     * Method:    AMS_Memory_update_recv_end
     * Signature: (III)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Memory_1update_1recv_1end
        (JNIEnv *env, jobject obj, jint memid, jint flag, jint count)
        {
            int err, flg;
	        unsigned int step;
            AMS_Memory mem = (AMS_Memory) memid;

            /* Call the native method */
            err = AMS_Memory_update_recv_end(mem, &flg, &step);
            flag = flg;
	        count = (jint) step; /* What is unsigned for JAVA ? */

            /* We are done. Return the error code */
            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Memory_detach
     * Signature: (I)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Memory_1detach
        (JNIEnv *env, jobject obj, jint memid)
        {
            int err;
            AMS_Memory mem = (AMS_Memory) memid;

            /* Call the native method */
            err = AMS_Memory_detach(mem);

            /* We are done. Return the error code */
            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Comm_detach
     * Signature: (I)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Comm_1detach
        (JNIEnv *env, jobject obj, jint comid)
        {
            int err;
            AMS_Comm alice = (AMS_Comm) comid;

            /* Call the native method */
            err = AMS_Comm_detach(alice);

            /* We are done. Return the error code */
            return err;
        }

    /*
     * Class:     accessor
     * Method:    AMS_Explain_error
     * Signature: (I)I
     */
    JNIEXPORT jint JNICALL Java_accessor_AMS_1Explain_1error
        (JNIEnv *env, jobject obj, jint jerr)
        {
            int err;
            char *errmsg;

            jstring jstr;
            jmethodID mid;
            jclass cls = (*env)->GetObjectClass(env, obj);

            /* Call the native method */
            err = AMS_Explain_error(jerr, &errmsg);

            if (err == AMS_ERROR_NONE) {

                /* Create the Java Error Message String */
                jstr = (*env)->NewStringUTF(env, errmsg);
                
                mid = (*env)->GetMethodID(env, cls, "setErrmsg", "(Ljava/lang/String;)V");
                if (mid == 0)    
                    return AMS_ERROR_JAVA_JNI_GET_MID;;

                /* Call the accessor (Java) method */
                (*env)->CallVoidMethod(env, obj, mid, jstr);
            }

            /* We are done. Return the error code */
            return err;
        }


#ifdef __cplusplus
}
#endif

