#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#ifndef _WIN_ALICE
#include <unistd.h>
#endif

#include "gov_anl_mcs_ams_AMS_0005fComm.h"
#include "ams.h"

#ifdef __cplusplus
extern "C" {
#endif

extern JNIEnv *ams_env;
extern jobject ams;

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fComm
 * Method:    get_memory_list
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_gov_anl_mcs_ams_AMS_1Comm_get_1memory_1list
  (JNIEnv *env, jobject obj)
{
    jclass commcls = (*env)->GetObjectClass(env, obj);
    int err, i;
    AMS_Comm ams;
    char **mem_list, *msg;

    jstring jstr;
    jobjectArray jstrarr;
    jmethodID mid;
    jsize len;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Communicator id */
    mid = (*env)->GetMethodID(env, commcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    }

    ams = (AMS_Comm) (*env)->CallIntMethod(env, obj, mid);

    /* Call the native method */
    err = AMS_Comm_get_memory_list(ams, &mem_list);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }

    /* Compute Array length */
    i = 0;
    while (mem_list[i])
        i++;
    len = i;

    /* Create an empty array */
    jstrarr = (*env)->NewObjectArray(env, len, (*env)->FindClass(env, "java/lang/String"), NULL);
    if (jstrarr == NULL) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    }    

    /* Fill in the array */
    for (i = 0; i<len; i++) {
        /* Call the accessor (Java) method */
        jstr = (*env)->NewStringUTF(env, mem_list[i]);
        (*env)->SetObjectArrayElement(env, jstrarr, i, jstr);
    }

    return jstrarr;

}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fComm
 * Method:    get_memory
 * Signature: (Ljava/lang/String;)Lgov/anl/mcs/ams/AMS_Memory;
 */
JNIEXPORT jobject JNICALL Java_gov_anl_mcs_ams_AMS_1Comm_get_1memory
  (JNIEnv *env, jobject obj, jstring jname)
{
    jclass commcls = (*env)->GetObjectClass(env, obj);
    jclass memcls = (*env)->FindClass(env, "gov/anl/mcs/ams/AMS_Memory");

    int err;
    unsigned int step;
    AMS_Comm ams;
    AMS_Memory memid;
    const char *name_str;
    char *msg;

    jobject jmem;
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Communicator id */
    mid = (*env)->GetMethodID(env, commcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    }

    ams = (AMS_Comm) (*env)->CallIntMethod(env, obj, mid);

    /* Get the Memory name */
    name_str = (*env)->GetStringUTFChars(env, jname, 0);

    /* Call the Native method */
    err = AMS_Memory_attach(ams, (char *)name_str, &memid, &step);

    (*env)->ReleaseStringUTFChars(env, jname, name_str);

    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }

    /* Construct an AMS Memory */
    mid = (*env)->GetMethodID(env, memcls, "<init>", "(Ljava/lang/String;)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    /* Call the AMS_Memory Constructor (Java) method */
    jmem = (*env)->NewObject(env, memcls, mid, jname);

    /* Set the AMS_Memory id */
    mid = (*env)->GetMethodID(env, memcls, "setId", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    /* Call the step id (Java) method */
    (*env)->CallVoidMethod(env, jmem, mid, memid);

    /* Set the AMS_Memory id */
    mid = (*env)->GetMethodID(env, memcls, "setStep", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    /* Call the setStep (Java) method */
    (*env)->CallVoidMethod(env, jmem, mid, step);
    
    /* Return the AMS_Memory object */
    return jmem;

}

#ifdef __cplusplus
}
#endif


