#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#ifndef _WIN_ALICE
#include <unistd.h>
#endif

#include "gov_anl_mcs_ams_AMS_0005fMemory.h"
#include "ams.h"

#ifdef __cplusplus
extern "C" {
#endif 

extern JNIEnv *ams_env;
extern jobject ams;

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    get_field_list
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_get_1field_1list
  (JNIEnv *env, jobject obj)
{
    jclass memcls = (*env)->GetObjectClass(env, obj);
    int err, i;
    AMS_Memory memid;
    char **fld_list, *msg;

    jstring jstr;
    jobjectArray jstrarr;
    jmethodID mid;
    jsize len;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    /* Call the native method */
    err = AMS_Memory_get_field_list(memid, &fld_list);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }

    /* Compute Array length */
    i = 0;
    while (fld_list[i])
        i++;
    len = i;

    /* Create an empty array */
    jstrarr = (*env)->NewObjectArray(env, len, (*env)->FindClass(env, "java/lang/String"), NULL);
    if (jstrarr == NULL) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    } 

    /* Fill in the array */
    for (i = 0; i<len; i++) {
        /* Call the accessor (Java) method */
        jstr = (*env)->NewStringUTF(env, fld_list[i]);
        (*env)->SetObjectArrayElement(env, jstrarr, i, jstr);
    }

    return jstrarr;
}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    get_field
 * Signature: (Ljava/lang/String;)Lgov/anl/mcs/ams/AMS_Field;
 */
JNIEXPORT jobject JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_get_1field
  (JNIEnv *env, jobject obj, jstring jname)
{
    int err, len, *intarr, i;
    int dim, *start, *end;
    float *floatarr;
    double *doublearr;
    const char *name_str;
    char **data_str, **tmpstr;
    void *addr;
    char *msg;

    AMS_Memory memid;
    AMS_Memory_type mtype;
    AMS_Data_type dtype;
    AMS_Shared_type stype;
    AMS_Reduction_type rtype;

    jstring jstr;
    jobjectArray jstrarr;
    jintArray jintarr, jstartarr, jendarr;
    jfloatArray jfloatarr;
    jdoubleArray jdoublearr;
    jobject jfld;
    jint jlen;
    jmethodID mid;

    jclass memcls = (*env)->GetObjectClass(env, obj);
    jclass fldcls = (*env)->FindClass(env, "gov/anl/mcs/ams/AMS_Field");

    /* Initialize the global variables */
    ams_env = env;

    /* Get the Field name */
    name_str = (*env)->GetStringUTFChars(env, jname, 0);

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    /* Call the Native method */
    err = AMS_Memory_get_field_info(memid, (char *)name_str, &addr, &len, &dtype, &mtype, &stype, &rtype);

    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }

    /* Construct an AMS Field */
    mid = (*env)->GetMethodID(env, fldcls, "<init>", "(Ljava/lang/String;)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    /* Call the AMS_Field Constructor (Java) method */
    jfld = (*env)->NewObject(env, fldcls, mid, jname);


    /*
     * Set a bunch of data structure values
     */

    /* Memory type */
    mid = (*env)->GetMethodID(env, fldcls, "setMemoryType", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    (*env)->CallVoidMethod(env, jfld, mid, mtype);
    
    /* Data type */
    mid = (*env)->GetMethodID(env, fldcls, "setDataType", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    (*env)->CallVoidMethod(env, jfld, mid, dtype);
    
    /* Shared type */
    mid = (*env)->GetMethodID(env, fldcls, "setSharedType", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    (*env)->CallVoidMethod(env, jfld, mid, stype);

    /* Reduction type */
    mid = (*env)->GetMethodID(env, fldcls, "setReductionType", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    (*env)->CallVoidMethod(env, jfld, mid, rtype);

    
    /* 
     * Given the data type and length, copy the data into an appropriate
     * Java field member
     */

    switch(dtype) {

    case AMS_INT:
    case AMS_BOOLEAN:

        jlen = len;
        jintarr = (*env)->NewIntArray(env, jlen);
        if (jintarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return NULL;
        }

        /* Allocate memory for temporary array */
        intarr = (int *) malloc(sizeof(int)*jlen);
        if (intarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_INSUFFICIENT_MEMORY, &msg);
            return NULL;
        }

        for (i = 0 ;i<jlen; i++)
            intarr[i] = *( ((int *)addr) + i);

        (*env)->SetIntArrayRegion(env, jintarr, 0, jlen , (long *)intarr);
    
        /* Data length */
        mid = (*env)->GetMethodID(env, fldcls, "setLength", "(I)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jlen);

        /* Data Array */
        mid = (*env)->GetMethodID(env, fldcls, "setNativeData", "([I)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jintarr);

        break;

    case AMS_DOUBLE:

        jlen = len;
        jdoublearr = (*env)->NewDoubleArray(env, jlen);
        if (jdoublearr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return NULL;
        }

        /* Allocate memory for temporary array */
        doublearr = (double *) malloc(sizeof(double)*jlen);
        if (doublearr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_INSUFFICIENT_MEMORY, &msg);
            return NULL;
        }

        for (i = 0 ;i<jlen; i++)
            doublearr[i] = *( ((double *)addr) + i);

        (*env)->SetDoubleArrayRegion(env, jdoublearr, 0, jlen , doublearr);
    
        /* Data length */
        mid = (*env)->GetMethodID(env, fldcls, "setLength", "(I)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jlen);

        /* Data Array */
        mid = (*env)->GetMethodID(env, fldcls, "setNativeData", "([D)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jdoublearr);

        break;

    case AMS_FLOAT:

        jlen = len;
        jfloatarr = (*env)->NewFloatArray(env, jlen);
        if (jfloatarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return NULL;
        }

        /* Allocate memory for temporary array */
        floatarr = (float *) malloc(sizeof(float)*jlen);
        if (floatarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_INSUFFICIENT_MEMORY, &msg);
            return NULL;
        }

        for (i = 0 ;i<jlen; i++)
            floatarr[i] = *( ((float *)addr) + i);

        (*env)->SetFloatArrayRegion(env, jfloatarr, 0, jlen , floatarr);
    
        /* Data length */
        mid = (*env)->GetMethodID(env, fldcls, "setLength", "(I)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jlen);

        /* Data Array */
        mid = (*env)->GetMethodID(env, fldcls, "setNativeData", "([F)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jfloatarr);

        break;

    case AMS_STRING:

        jlen = len;
        /* Allocate memory for temporary array */
        data_str = (char **) malloc(sizeof(char *)*(jlen + 1));
        if (data_str == NULL) {
            AMS_Print_error_msg(AMS_ERROR_INSUFFICIENT_MEMORY, &msg);
            return NULL;
        }

        tmpstr = (char **)addr;
        for (i = 0; i < jlen ;i++) {
            if (tmpstr[i]) {
                data_str[i] = (char *) malloc(strlen(tmpstr[i]) + 1);
                if (data_str[i] == NULL) {
                    AMS_Print_error_msg(AMS_ERROR_INSUFFICIENT_MEMORY, &msg);
                    return NULL;
                }

                strcpy(data_str[i], tmpstr[i]);
            } else 
                data_str[i] = NULL;
        }

        jstrarr = (*env)->NewObjectArray(env, jlen, (*env)->FindClass(env, "java/lang/String"), NULL);
        if (jstrarr == NULL) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
            return NULL;
        }
            
        for (i = 0; i < jlen; i++) {
            if (data_str[i]) {
                jstr = (*env)->NewStringUTF(env, data_str[i]);
                (*env)->SetObjectArrayElement(env, jstrarr, i, jstr);
            }
        }

        /* Data length */
        mid = (*env)->GetMethodID(env, fldcls, "setLength", "(I)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jlen);

        /* Data Array (List of strings) */
        mid = (*env)->GetMethodID(env, fldcls, "setNativeData", "([Ljava/lang/String;)V");
        if (mid == 0) {
            AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
            return NULL;
        }

        (*env)->CallVoidMethod(env, jfld, mid, jstrarr);
        break;

    default:
        AMS_Print_error_msg(AMS_ERROR_INVALID_DATA_TYPE, &msg);
        return NULL;
    }

    /*
     * Get the field's dimension info so that it can be displayed correctly
     */
    err = AMS_Memory_get_field_block(memid, (char *)name_str, &dim, &start, &end);
    
    (*env)->ReleaseStringUTFChars(env, jname, name_str);

    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }

    /*
     * Set Dimensions
     */
    mid = (*env)->GetMethodID(env, fldcls, "setDim", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    (*env)->CallVoidMethod(env, jfld, mid, (jint) dim);

    /*
     * Set start indices for each dimension
     */
    mid = (*env)->GetMethodID(env, fldcls, "setStartIndex", "([I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    jstartarr = (*env)->NewIntArray(env, dim);
    if (jstartarr == NULL) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    }

    (*env)->SetIntArrayRegion(env, jstartarr, 0, (jint) dim , (long *)start);
    (*env)->CallVoidMethod(env, jfld, mid, jstartarr);

    /*
     * Set end indices for each dimension
     */
    mid = (*env)->GetMethodID(env, fldcls, "setEndIndex", "([I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    jendarr = (*env)->NewIntArray(env, dim);
    if (jendarr == NULL) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    }

    (*env)->SetIntArrayRegion(env, jendarr, 0, (jint) dim , (long *)end);
    (*env)->CallVoidMethod(env, jfld, mid, jendarr);

    /* Set the AMS_Memory id */
    mid = (*env)->GetMethodID(env, fldcls, "setMemory", "(Lgov/anl/mcs/ams/AMS_Memory;)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    /* Call the (Java) method */
    (*env)->CallVoidMethod(env, jfld, mid, obj);

    
    /* Return the AMS_Memory object */
    return jfld;
}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    lock
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_lock
  (JNIEnv *env, jobject obj, jint jtimeout)
{
    int err, timeout;
    char *msg;
    AMS_Memory memid;

    jclass memcls = (*env)->GetObjectClass(env, obj);
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    timeout = (int) jtimeout;
    /* Call the native method */
    err = AMS_Memory_lock(memid, timeout);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return;
    }

    return;
}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    unlock
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_unlock
  (JNIEnv *env, jobject obj)
{
    int err;
    char *msg;
    AMS_Memory memid;

    jclass memcls = (*env)->GetObjectClass(env, obj);
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    /* Call the native method */
    err = AMS_Memory_unlock(memid);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return;
    }

    return;
}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    send_begin
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_send_1begin
  (JNIEnv *env, jobject obj)
{
    int err;
    char *msg;
    AMS_Memory memid;

    jclass memcls = (*env)->GetObjectClass(env, obj);
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    /* Call the native method */
    err = AMS_Memory_update_send_begin(memid);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return;
    }

    return;
}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    send_end
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_send_1end
  (JNIEnv *env, jobject obj)
{
    int err;
    char *msg;
    AMS_Memory memid;

    jclass memcls = (*env)->GetObjectClass(env, obj);
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    /* Call the native method */
    err = AMS_Memory_update_send_end(memid);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return;
    }

    return;
}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    recv_begin
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_recv_1begin
  (JNIEnv *env, jobject obj)
{
    int err;
    char *msg;
    AMS_Memory memid;

    jclass memcls = (*env)->GetObjectClass(env, obj);
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    /* Call the native method */
    err = AMS_Memory_update_recv_begin(memid);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return;
    }

}

/*
 * Class:     gov_anl_mcs_ams_AMS_0005fMemory
 * Method:    recv_end
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_gov_anl_mcs_ams_AMS_1Memory_recv_1end
  (JNIEnv *env, jobject obj)
{

    int err, flag, res[2];
    unsigned int step;
    char *msg;
    AMS_Memory memid;

    jintArray jintarr;
    jclass memcls = (*env)->GetObjectClass(env, obj);
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get Memory id */
    mid = (*env)->GetMethodID(env, memcls, "getId", "()I");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    memid = (AMS_Memory) (*env)->CallIntMethod(env, obj, mid);

    /* Call the native method */
    err = AMS_Memory_update_recv_end(memid, &flag, &step);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }

    /* Create the int array of 2 for returns */
    jintarr = (*env)->NewIntArray(env, (jint)2);
    if (jintarr == NULL) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    }

    res[0] = flag;
    res[1] = step;

    (*env)->SetIntArrayRegion(env, jintarr, 0, 2 , (long *)res);

    /* Set the AMS_Memory flag and step anyway */
    mid = (*env)->GetMethodID(env, memcls, "setFlag", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    (*env)->CallVoidMethod(env, obj, mid, flag);

    mid = (*env)->GetMethodID(env, memcls, "setStep", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    (*env)->CallVoidMethod(env, obj, mid, flag);

    return jintarr;
}

#ifdef __cplusplus
}
#endif


