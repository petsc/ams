#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <stdarg.h>

#include <sys/types.h>

#ifndef _WIN_ALICE
#include <unistd.h>
#endif

#include "gov_anl_mcs_ams_AMSBean.h"
#include "ams.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Global environment
 */

JNIEnv *ams_env;
jobject ams;

extern int java_print_func(const char *, ...);

/*
 * Class:     gov_anl_mcs_ams_AMSBean
 * Method:    AMS_Java_init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gov_anl_mcs_ams_AMSBean_AMS_1Java_1init
  (JNIEnv *env, jobject obj)
{
    jclass cls = (*env)->GetObjectClass(env, obj);
    jfieldID fid;

    /* Initialize the global variables */
    ams = (*env)->NewGlobalRef(env, obj);
    ams_env = env;

    /* AMS_Memory_type: AMS_READ, AMS_WRITE, AMS_MEMORY_UNDEF */
    fid = (*env)->GetStaticFieldID(env, cls, "READ", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_READ);

    fid = (*env)->GetStaticFieldID(env, cls, "WRITE", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_WRITE);

    fid = (*env)->GetStaticFieldID(env, cls, "MEMORY_UNDEF", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_MEMORY_UNDEF);

    /* 
     * AMS_Data_type: AMS_INT, AMS_BOOLEAN, AMS_FLOAT, AMS_DOULE, AMS_DATA_UNDEF 
     */
    fid = (*env)->GetStaticFieldID(env, cls, "INT", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_INT);

    fid = (*env)->GetStaticFieldID(env, cls, "BOOLEAN", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_BOOLEAN);

    fid = (*env)->GetStaticFieldID(env, cls, "DOUBLE", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_DOUBLE);

    fid = (*env)->GetStaticFieldID(env, cls, "FLOAT", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_FLOAT);

    fid = (*env)->GetStaticFieldID(env, cls, "STRING", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_STRING);

    fid = (*env)->GetStaticFieldID(env, cls, "DATA_UNDEF", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_DATA_UNDEF);

    /* 
     * AMS_Shared_type: AMS_COMMON, AMS_REDUCED, AMS_DISTRIBUTED, AMS_SHARED_UNDEF 
     */
    fid = (*env)->GetStaticFieldID(env, cls, "COMMON", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_COMMON);

    fid = (*env)->GetStaticFieldID(env, cls, "REDUCED", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_REDUCED);

    fid = (*env)->GetStaticFieldID(env, cls, "DISTRIBUTED", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_DISTRIBUTED);

    fid = (*env)->GetStaticFieldID(env, cls, "SHARED_UNDEF", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_SHARED_UNDEF);

    /* 
     * AMS_Reduction_type: AMS_SUM, AMS_MAX, AMS_MIN, AMS_REDUCT_UNDEF 
     */
    fid = (*env)->GetStaticFieldID(env, cls, "SUM", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_SUM);

    fid = (*env)->GetStaticFieldID(env, cls, "MAX", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_MAX);

    fid = (*env)->GetStaticFieldID(env, cls, "MIN", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_MIN);

    fid = (*env)->GetStaticFieldID(env, cls, "REDUCT_UNDEF", "I");  
    if (fid == 0)
        return AMS_ERROR_JAVA_JNI_GET_FID;

    (*env)->SetStaticIntField(env, cls, fid, AMS_REDUCT_UNDEF);

    /* Set the AMS Print function to be the one provided by Java */
    AMS_Set_print_func(java_print_func);

    return AMS_ERROR_NONE; /* What else could it be ? */

}

/*
 * Class:     gov_anl_mcs_ams_AMSBean
 * Method:    get_comm_list
 * Signature: (Ljava/lang/String;I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_gov_anl_mcs_ams_AMSBean_get_1comm_1list
  (JNIEnv *env, jclass amscls, jstring jhost, jint jport)
{

    int err, i, len, port;
    char **com_list, *msg;
    const char *name_str;

    jstring jstr;
    jobjectArray jstrarr;

    /* Initialize the global variables */
    ams_env = env;

    /* Get the host name */
    name_str = (*env)->GetStringUTFChars(env, jhost, 0);

    /* Get the port number */
    port = (int) jport;


    /* Call the native method */
    err = AMS_Connect((char *)name_str, port, &com_list);
    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }

    /* No Communicator found */
    if (com_list == NULL)
        return NULL;

    /* Compute Array length */
    i = 0;
    while (com_list[i])
        i++;
    len = i;

    /* Create an empty array */
    jstrarr = (*env)->NewObjectArray(env, len, (*env)->FindClass(env, "java/lang/String"), NULL);
    if (jstrarr == NULL) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_SYSTEM, &msg);
        return NULL;
    }    

    /* Fill in the array */
    for (i = 0; i<len; i++) {
        /* Call the accessor (Java) method */
        jstr = (*env)->NewStringUTF(env, com_list[i]);
        (*env)->SetObjectArrayElement(env, jstrarr, i, jstr);
    }

    return jstrarr;

}

/*
 * Class:     gov_anl_mcs_ams_AMSBean
 * Method:    get_comm
 * Signature: (Ljava/lang/String;)Lgov/anl/mcs/ams/AMS_Comm;
 */
JNIEXPORT jobject JNICALL Java_gov_anl_mcs_ams_AMSBean_get_1comm
  (JNIEnv *env, jclass amscls, jstring jname)
{
    jclass comcls = (*env)->FindClass(env, "gov/anl/mcs/ams/AMS_Comm");

    int err;
    AMS_Comm ams;
    const char *name_str;
    char *msg;

    jobject jcom;
    jmethodID mid;

    /* Initialize the global variables */
    ams_env = env;

    /* Get the AMS_Comm name */
    name_str = (*env)->GetStringUTFChars(env, jname, 0);

    /* Call the native method */
    err = AMS_Comm_attach((char *)name_str, &ams);

    (*env)->ReleaseStringUTFChars(env, jname, name_str);

    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return NULL;
    }


    /* Get the AMS Communicator */
    mid = (*env)->GetMethodID(env, comcls, "<init>", "(Ljava/lang/String;)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    /* Constuct the object */
    jcom = (*env)->NewObject(env, comcls, mid, jname);

    /* Set the AMS_Comm id */
    mid = (*env)->GetMethodID(env, comcls, "setId", "(I)V");
    if (mid == 0) {
        AMS_Print_error_msg(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        return NULL;
    }

    /* Call the step id (Java) method */
    (*env)->CallVoidMethod(env, jcom, mid, ams);
    
    /* Return the AMS_Memory object */
    return jcom;
}

/*
 * Class:     gov_anl_mcs_ams_AMSBean
 * Method:    explain_error
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_gov_anl_mcs_ams_AMSBean_explain_1error
  (JNIEnv *env, jclass amscls, jint jerr)
{

    int err;
    char *errmsg;

    jstring jstr;

    /* Initialize the global variables */
    ams_env = env;

    /* Call the native method */
    err = AMS_Explain_error(jerr, &errmsg);

    /* Create the Java Error Message String */
    jstr = (*env)->NewStringUTF(env, errmsg);

    return jstr;
}

/*
 * Class:     gov_anl_mcs_ams_AMSBean
 * Method:    set_output_file
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_gov_anl_mcs_ams_AMSBean_set_1output_1file
  (JNIEnv *env, jclass amscls, jstring jfile)
{
    int err;
    const char *file;
    char *msg;

    /* Initialize the global variables */
    ams_env = env;

    /* Get the file name */
    file = (*env)->GetStringUTFChars(env, jfile, 0);

    /* Call the native method */
    err = AMS_Set_output_file((char *)file);

    (*env)->ReleaseStringUTFChars(env, jfile, file);

    if (err != AMS_ERROR_NONE) {
        AMS_Print_error_msg(err, &msg);
        return;
    }

    return;
}


/*
 * Function called from java to do customize the printing
 */

int java_print_func(const char * str, ...)
{
    char buff[512], *msg;
    jstring jstr;
    jmethodID mid;

    va_list args;
    va_start(args, str);

    /* Format the string to print */
    vsprintf(buff, str, args);

    /* 
     * Print the error message using the java function 
     */

    /* Construct the java string message */
    jstr = (*ams_env)->NewStringUTF(ams_env, (const char *) buff);

    /* Get the method id */
    mid = (*ams_env)->GetMethodID(ams_env, (*ams_env)->GetObjectClass(ams_env, ams), "print_error", "(Ljava/lang/String;)V");
    if (mid == 0) {
        AMS_Explain_error(AMS_ERROR_JAVA_JNI_GET_MID, &msg);
        printf("Can't get Java native call to print_error \n");
        return AMS_ERROR_JAVA_JNI_GET_MID;
    }

    /* Call the java method */
    (*ams_env)->CallVoidMethod(ams_env, ams, mid, jstr);

    va_end(args);

    return AMS_ERROR_NONE;
}

#ifdef __cplusplus
}
#endif

