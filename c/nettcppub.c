/* not-accessor */
/*
 *  network handling for TCP/IP connection.
 */

#include        <signal.h>
#include        <stdlib.h>

#ifndef _WIN_ALICE
#include    <unistd.h>
#else
#include        <io.h>
#endif

#include    <stdio.h>
#include    <stdlib.h>
#include    <errno.h>

#include "ams.h"
#include "netlib.h"
#include "amsdefs.h"

#include        "network.h"
#include        "netdefs.h"


struct sockaddr_in              tcp_cli_addr;   /* set by accept() */

/*
 * Initialize the network connection for the publisher,
 * if the port number is -1, get port# from the system
 */

int AMSP_mem_net_init(char *service, int care_about_port, int *port, int *sfd, char *errmsg)
{
    struct servent      *sp;
    struct sockaddr_in  tcp_srv_addr;
    int err;
#ifndef _WIN_ALICE
int optval;
#endif

    socklen_t sinlen = sizeof(struct sockaddr_in);

    /*
     * We weren't started by a master daemon.
     * We have to create a socket ourselves and bind our well-known
     * address to it.
     */

    memset((char *) &tcp_srv_addr, 0, sizeof(tcp_srv_addr));
    tcp_srv_addr.sin_family      = AF_INET;
    tcp_srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (service != NULL) {
        if ( (sp = getservbyname(service, "tcp")) == NULL)
            AMSP_err_quit("AMSP_mem_net_init: unknown service: %s/tcp", service);

        if (*port > 0)
            tcp_srv_addr.sin_port = htons((short)(*port));
        /* caller's value */
        else
            tcp_srv_addr.sin_port = sp->s_port;
        /* service's value */
    } else {
        if (*port == -1)
            *port = 0;

        if (*port < 0) {
            AMSP_err_ret(errmsg, "AMSP_mem_net_init: must specify a valid port");
            return(-1);
        }
        tcp_srv_addr.sin_port = htons((short)(*port));
    }

    /*
     * Create the socket and Bind our local address so that any
     * accessor can send to us.
     */

    if ( (*sfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        AMSP_err_dump("AMSP_mem_net_init: can't create stream socket");

#ifndef _WIN_ALICE
    /* On NT the port will be "re-used" even though there is another */
    /* using the current port. */
    /* Set socket option to reuse an address after prompt restart */
    optval = 1; /* Turn on the option */
    err = setsockopt(*sfd, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval));
    if (err < 0)
        AMSP_err_sys(" SO_REUSEADDR in setsockopt call ");
#endif
    
    while ((err = bind(*sfd, (struct sockaddr *) &tcp_srv_addr,
                  sizeof(tcp_srv_addr))) < 0) {

        if(care_about_port) {
            perror(" Can't bind socket");
            AMSP_err_dump("AMSP_mem_net_init: can't bind local address");
        } else {
            /* Try another port */
            *port = 0;
            tcp_srv_addr.sin_port = htons((short)(*port));
        }
    }

    /* Get back the port number back*/
    if (*port == 0) {
        if ((err = getsockname( *sfd, (struct sockaddr *) &tcp_srv_addr,
                                &sinlen)) < 0)
            AMSP_err_dump("getsockname: can't get assigned port number");
        *port = ntohs(tcp_srv_addr.sin_port);
    }

    /*
     * And set the listen parameter, telling the system that we're
     * ready  to accept incoming connection requests.
     */

    err=listen(*sfd, 5);
    return err;
}

/*
 * The publisher waits for accessor request on a known port
 * inetd is a flag indicating whether we were started by the deamon
 * alive is a flag used to indicate whether the publisher is being shutdown
 * In case of shutdown we should not accept any new connection. Note that
 * the alive lag will work only in a multithreaded environment or shared
 * memory.
 */

int AMSP_mem_net_accept(int inetd, int sfd, void *arg)
{
    int newsockfd, err, done = 0;
    socklen_t clilen;

    struct sockaddr_in          tcp_cli_addr;

    extern int AMSP_ProcessRequest (void *, int );
    extern void AMSP_Term(int);

    signal(SIGTERM, AMSP_Term);

    while (!done) {
        clilen = sizeof(tcp_cli_addr);
        newsockfd = accept(sfd, (struct sockaddr *) &tcp_cli_addr, &clilen);
        done = 1;
        if (newsockfd < 0) {
            if (errno == EINTR) {
                errno = 0;
                done = 0;
            }
            else {
                AMSP_err_sys("accept failed. system error: ");
                AMSP_err_dump("accept error errno=%d newsockfd=%d",
                         errno, newsockfd);
            }
        }
    }

    /*
     * Create a Child Process or a Worker Thread to handle the new
     * request. We are passing a pointer to the new socket descriptor
     * in case it is a Worker Thread.
     */

    err = AMSP_ProcessRequest(arg, newsockfd);

    if (err == 1)
        /* Publisher being shutdown */
        return 0;
    else
        /* The main thread/process returns to listen for a new request */
        return 1;

}

extern int AMSP_mem_net_close(int);

/*
 * Signal Handlers
 */

void AMSP_Term(int sfd)
{
    AMSP_mem_net_close(sfd);
    exit(0);
}
