/*
 * Return a string containing some additional information after a
 * host name or address lookup error - gethostbyname() or gethostbyaddr().
 * This function is ThreadSafe.
 */

/* More work need to be done here to get the additional information */

#include <stdio.h>
#include "network.h"


#ifdef _WIN_ALICE
/* I just can't believe there is no way to get the error message */

void AMSP_WinSockErr(int err, char *errstr)
{
    /* The following errors messages are in winsock.h */
    /* I tried my best to guess the error messages meanings */
    switch (err)
        {
        case WSAEWOULDBLOCK:
            strcpy(errstr," Socket is marked non-blocking and no connection is present ");
            break;
        case WSAEINPROGRESS:
            strcpy(errstr," Operation now in progress...");
            break;
        case WSAEALREADY:
            strcpy(errstr," Operation already in progress");
            break;
        case WSAENOTSOCK:
            strcpy(errstr," Socket operarion on non socket ");
            break;
        case WSAEDESTADDRREQ:
            strcpy(errstr," Destination address is required ");
            break;
        case WSAEMSGSIZE:
            strcpy(errstr," Message size (too long) prohibits automatic send/recv for a socket");
            break;
        case WSAEPROTOTYPE:
            strcpy(errstr," The protocol is the wrong type for the socket ");
            break;
        case WSAENOPROTOOPT:
            strcpy(errstr," Protocol not available ");
            break;
        case WSAEPROTONOSUPPORT:
            strcpy(errstr," Protocol not supported ");
            break;
        case WSAESOCKTNOSUPPORT:
            strcpy(errstr," Socket not supported ");
            break;
        case WSAEOPNOTSUPP:
            strcpy(errstr," The refrenced socket is not of type SOCK_STREAM ");
            break;
        case WSAEPFNOSUPPORT:
            strcpy(errstr," Protcol family not supported ");
            break;
        case WSAEAFNOSUPPORT:
            strcpy(errstr," Address family not supported by protcol ");
            break;
        case WSAEADDRINUSE:
            strcpy(errstr," The address is already in use ");
            break;
        case WSAEADDRNOTAVAIL:
            strcpy(errstr," Can't assign requested address ");
            break;
        case WSAENETDOWN:
            strcpy(errstr," Network is down ");
            break;
        case WSAENETUNREACH:
            strcpy(errstr," Network is unreachable ");
            break;
        case WSAENETRESET:
            strcpy(errstr," Network droped connection on reset ");
            break;
        case WSAECONNABORTED:
            strcpy(errstr," Software caused connection abort ");
            break;
        case WSAECONNRESET:
            strcpy(errstr," Connection reset by peer ");
            break;
        case WSAENOBUFS:
            strcpy(errstr," Insufficient system resources, no buffers available ");
            break;
        case WSAEISCONN:
            strcpy(errstr," The socket is already connected ");
            break;
        case WSAENOTCONN:
            strcpy(errstr," Socket is not connected ");
            break;
        case WSAESHUTDOWN:
            strcpy(errstr," Can't send after socket shutdown ");
            break;
        case WSAETOOMANYREFS:
            strcpy(errstr," Too many refrences, can't splice ");
            break;
        case WSAETIMEDOUT:
            strcpy(errstr," Connection timeout ");
            break;
        case WSAECONNREFUSED:
            strcpy(errstr," Connection refused by the publisher ");
            break;
        case WSAELOOP:
            strcpy(errstr," Too many symbolic links were encountered in translating the pathname");
            break;
        case WSAENAMETOOLONG:
            strcpy(errstr," The length of the path argument is too long ");
            break;
        case WSAEHOSTDOWN:
            strcpy(errstr," Host is down ");
            break;
        case WSAEHOSTUNREACH:
            strcpy(errstr," No route to host ");
            break;
        case WSAENOTEMPTY:
            strcpy(errstr," Directory not empty ");
            break;
        case WSAEPROCLIM:
            strcpy(errstr," Too many processes ");
            break;
        case WSAEUSERS:
            strcpy(errstr," Too many users ");
            break;
        case WSAEDQUOT:
            strcpy(errstr," Disk quota exceeded ");
            break;
        case WSAESTALE:
            strcpy(errstr," Stale file handle ");
            break;
        case WSAEREMOTE:
            strcpy(errstr," Too many levels of remote in path ");
            break;

        case WSAEDISCON:
            strcpy(errstr," Socket disconnected ");
            break;

        case WSASYSNOTREADY:
            strcpy(errstr," Synchronization not ready ");
            break;
        case WSAVERNOTSUPPORTED:
            strcpy(errstr," Requested Windows version is not supported ");
            break;
        case WSANOTINITIALISED:
            strcpy(errstr," Windows Socket not initialized. Please call WSAStartup() ");
            break;

            /* Authoritative Answer: Host not found */
        case WSAHOST_NOT_FOUND:
            strcpy(errstr," Host not found ");
            break;

            /* Non-Authoritative: Host not found, or PUBLISHERFAIL */
        case WSATRY_AGAIN:
            strcpy(errstr," Host not found, or Publisher failed ");
            break;

            /* Non recoverable errors, FORMERR, REFUSED, NOTIMP */
        case WSANO_RECOVERY:
            strcpy(errstr," Non recoverable errors, FORMERR, REFUSED, NOTIMP ");
            break;

            /* Valid name, no data record of requested type */
        case WSANO_DATA:
            strcpy(errstr," Valid name, but no data record of requested type ");
            break;

        default:
            strcpy(errstr," Undocumented Windows Socket Error ");
            break;

        }

}

#endif

char * AMSP_host_err_str( char *Msgstr, int *err)
{
#ifdef _WIN_ALICE
    char loc_buf[255];

    loc_buf[0] = 0;
    AMSP_WinSockErr(h_errno, loc_buf);
    sprintf(Msgstr, "(h_errno = %d): %s", h_errno, loc_buf);
    *err = h_errno;
#else
    /* Some flavors of UNIX do not define h_errlist */
    /* We will display the error number              */
    sprintf(Msgstr, " (h_errno = %d)", h_errno);
    *err = h_errno;
#endif

    return(Msgstr);
}

