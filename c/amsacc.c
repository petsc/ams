/* not-publisher */
#include <stdlib.h>
#include <string.h>


#include "ams.h"
#include "acomm.h"
#include "netlib.h"
#include "netapi.h"
#include "amsdefs.h"
#include "sendfun.h"
#include "amisc.h"
#include "hostiter.h"

#ifdef AMS_LDAP
#include "amsldap.h"
#endif

/*
 * Routines for the Accessor of shareable memory
 */

/*@

  AMS_Connect - Connects to an AMS publisher or an LDAP publisher. If an LDAP
                publisher is specified and there is only one publisher that is
                alive, then we connect to that publisher. Also, this
                function uses the environment variable AMS_USER_NAME, or
                your login name to narrow the search for AMS Publishers. If
                you want to search all the publishers, set the AMS_USER_NAME
                to ams_allusers. If you want this function to always use the
                LDAP publisher, set the environment variable
                AMS_USE_LDAP_PUBLISHER=yes

    Currently you can only be connected to one publisher at at time.

    Call AMS_Disconnect() to stop being connected.

    Input Parameters:
+       host - hostname prefix it with "LDAP:" to connect to a LDAP publisher,
               and in that situation, host is used as the LDAP publisher.
               If host is ommitted, but the "LDAP:" is used, the default
               LDAP publisher is used.

-       port - tcp port number. Use -1 for system default port

    Output Parameters:
+       list - list of Communicators attached to the Publisher, or
        list of Publishers attached to an LDAP publisher. Each element of the list
        consist of either a triplet (communicator name | host name | port number),
        or a duplet (host|port) in a LDAP connection case. The elements are
        separated by the "|" character.

    Note:
        host is also used as an output parameter. When a connection is done
        using the LDAP publisher and a unique publisher is found, then we connect
        to that publisher and return a list of communicators. To signal this to
        the caller we set the variable host to '0'.
        Barry, 6/10/2010; I don't like this because then host is not a const. If
        this makes sense we may need another flag as output indicating this.

@*/

int AMS_Connect(const char *host, int port, char ***list)
{
  int err, err1, sfd;
    char *p;

#ifdef AMS_LDAP
    int nb_entries, use_ldap = 0;
    LDAP *ld;
    char *filter, buff[255], tmphost[255];
#endif

    /* Copy the hostname */
    if (host == NULL) {
        /* Try default host */
        if ((p=getenv("AMS_PUBLISHER")) || (p=getenv("ams_publisher")))
            strcpy(AMS_PUB_HOST_NAME, p);
        else
            return AMS_ERROR_INVALID_USER_ARGUMENT;
    } else if (strlen(host) == 0) {
        /* Try default host */
        if ((p=getenv("AMS_PUBLISHER")) || (p=getenv("ams_publisher")))
            strcpy(AMS_PUB_HOST_NAME, p);
        else
            return AMS_ERROR_INVALID_USER_ARGUMENT;
    } else
        strcpy(AMS_PUB_HOST_NAME, host);

#ifdef AMS_LDAP

    /* Check if this is an LDAP connection */

    if (p = getenv("AMS_USE_LDAP_PUBLISHER")) {
         if (strcmp(p, "YES") == 0)
             use_ldap = 1;
         if (strcmp(p, "yes") == 0)
             use_ldap = 1;
    }

    if (strstr(host, "LDAP:") || use_ldap) {

        /* Setup the ldap port if different from the default one */
        if (port != -1) {
            sprintf(buff,"AMS_LDAP_PORT=%d",port);
            putenv(buff);
        }

        /* Which LDAP publisher to use */
        if (use_ldap == 0) {

            p = strchr(host, ':');

            /* Use whatever is after the prefix "LDAP:" */
            if (p[1]) {
                sprintf(tmphost, "AMS_LDAP_PUBLISHER=%s", p+1);
                putenv(tmphost);
            }
        }

        /* Initialize the LDAP publisher */
        err = AMSP_Ldap_init(&ld);
        CheckErr(err);

        /* Get current user name */
#ifdef _WIN_ALICE
        if ((p = getenv("AMS_USER_NAME")) == NULL) {
            p = getenv("USERNAME");
            if (p == NULL)
                p = "ams_user"; /* Default user name */
        }
#else
        if ((p = getenv("AMS_USER_NAME"))  == NULL) {
            p = getlogin();
            if (p == NULL)
                p = "ams_user"; /* Default user name */
        }
#endif

        if (strcmp(p, "ams_allusers") == 0)
            filter = NULL;
        else {
            filter = (char *) malloc(255);
            if (filter == NULL)
                return AMS_ERROR_INSUFFICIENT_MEMORY;
            sprintf(filter,"(userid=%s)",p);
        }

        /* Search all AMS Publishers that match the filter */
        err = AMSP_Ldap_load_publishers(ld, BASE_SEARCH, filter, list, &nb_entries);
        CheckErr(err);

        /* If unique publisher then connect to that publisher */
        if (nb_entries != 1) {
            return AMS_ERROR_NONE; /* We are done */
        } else {
            /* Else, we should connect to the one publisher that was found */
            strcpy (buff, (*list)[0]);
            p = strtok(buff, "|"); /* Skip the dn */
            if (p == NULL)
                return AMS_ERROR_SYSTEM;

            /* Get the host name */
            p = strtok(NULL, "|");
            if (p == NULL)
                return AMS_ERROR_SYSTEM;

            host[0] = 0; /* Tell the caller that we found a unique */
                         /* and that we are connecting to it */
            strcpy(AMS_PUB_HOST_NAME, p);

            /* Get the port number */
            p = strtok(NULL, "|");
            if (p == NULL)
                return AMS_ERROR_SYSTEM;
            port = atoi(p);

        }
    }
#endif

    /* Which port to use? */
    if (port == -1) {
        port = DEFAULT_PORT; /* Use default port */
        if ((p=getenv("AMS_PORT")) || (p=getenv("ams_port")))
            port = (int) atoi(p);
    }

    if (port < 0 )
        return AMS_ERROR_BAD_PORT_NUMBER;

    AMS_PUB_PORT_NUMBER = port;

#ifdef _WIN_ALICE
    /* Initialize Windows Sockets */
    if (!AMSP_InitSock())
        return AMS_ERROR_INITIALIZE_NET_LIB;
#endif

    /* Before connecting reserve publisher's id */
    err = AMSP_Set_publisher_id(AMS_PUB_HOST_NAME, AMS_PUB_PORT_NUMBER);
    CheckErr(err);

    /* Connect to the publisher */
    if (AMSP_mem_net_open(AMS_PUB_HOST_NAME, AMS_SERVICE, AMS_PUB_PORT_NUMBER, &sfd, ams_msg) == AMS_ERROR_CONNECT_REFUSED)
        return AMS_ERROR_NET_OPEN;

    /* Attaches the publisher and get the list of communicators */
    err = AMSP_send_rq(OP_COMM_LIST, sfd);
    CheckErr(err);

    err1 = AMSP_fsm_loop(OP_COMM_LIST, sfd, &err, list);
    AMSP_mem_net_close(sfd);

    CheckErr(err1);
    CheckErr(err);

    return AMS_ERROR_NONE;
}


/*@

    AMS_Comm_attach - Attaches to an existing AMS Communicator

    Input Parameter:
+   name - the unique name of the communicator. The name should be appended with
           "|host|port number" to assure uniqueness in case of a connection to
           multiple Publishers

    Output Parameter:
+   ams - the computational context or Communicator

    Note: If the accessor is connecting to mulitple Publishers, then the communicator
          name should be in the form of name|host|port. This is the format returned in
          the AMS_Connect() call.

@*/

int AMS_Comm_attach(const char *name, AMS_Comm *ams)
{

    int err, err1, list_len, index = 0, sfd, port_number, local_port = -1, use_local = 0;
    HOST_NODE *host_node; /* For testing we will connect to a known host */
    char *p, *host_list, str[300], comm_name[255], host[255], local_host[255] = {0};

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    /* Pre-process the name if necessary */
    strcpy(comm_name, name);
    if ((p = strtok(comm_name, "|"))) {

        p = strtok(NULL, "|");
        if (p) {
            strcpy(local_host, p);
            use_local = 1;
        } else {
            use_local = 0;
        }

        p = strtok(NULL, "|");
        if (p) {
            local_port = atoi(p);
            use_local = 1;
        } else {
            use_local = 0;
        }

    }

    if (strlen(AMS_PUB_HOST_NAME) == 0)
        return AMS_ERROR_NOT_CONNECTED;

    /* Get the port number */
    if (AMS_PUB_PORT_NUMBER == -1)
        AMS_PUB_PORT_NUMBER = DEFAULT_PORT;

    /*
     * When using complete information in the Communicator's name
     * find and set the current publisher to the corresponding id
     */

    if (use_local == 1) {
        err = AMSP_Get_publisher_id(local_host, local_port);
        CheckErr(err);
        /* From now on, we will connect using this information */
        strcpy(AMS_PUB_HOST_NAME, local_host);
        AMS_PUB_PORT_NUMBER = local_port;
    }

    /* If the communicator is attached detach it */
    err = AMSP_Get_AMS_Comm(comm_name, ams);
    if (err == AMS_ERROR_COMM_NOT_PUBLISHED || err == AMS_ERROR_NONE) {
        /* Unpublish the communicator */
        err = AMSP_UnPublish_AMS_Comm(*ams);
        CheckErr(err);

        /* Deletes the memory list */
        err = AMSP_Delete_Memory_list(*ams);
        CheckErr(err);

        /* Delete memory nodes */
        err = AMSP_DelAll_Mem_AMS_Comm(*ams);
        CheckErr(err);

        /* Delete host nodes */
        err = AMSP_DelAll_Host_AMS_Comm(*ams);
        CheckErr(err);

        /* Delete the Communicator */
        err = AMSP_Del_AMS_Comm(*ams);
        CheckErr(err);
    } else {
        if (err != AMS_ERROR_COMM_NAME_NOT_FOUND)
            return err;
    }

#ifdef _WIN_ALICE
    /* Initialize Windows Sockets */
    if (!AMSP_InitSock())
        return AMS_ERROR_INITIALIZE_NET_LIB;
#endif

    /* Connect to the publisher */
    if (use_local == 1) {
        if (AMSP_mem_net_open(local_host, NULL, local_port, &sfd, ams_msg) == AMS_ERROR_CONNECT_REFUSED)
            return AMS_ERROR_NET_OPEN;
    } else {
        if (AMSP_mem_net_open(AMS_PUB_HOST_NAME, AMS_SERVICE, AMS_PUB_PORT_NUMBER, &sfd, ams_msg) == AMS_ERROR_CONNECT_REFUSED)
            return AMS_ERROR_NET_OPEN;
    }

    /* Attaches the communicator */
    /* Enough memory for 200 processors names */
    host_list = (char *)malloc(10000*sizeof(char));
    if (host_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    err1 = AMSP_send_rq(OP_ATTACH_COMM, sfd, comm_name);
	CheckErr(err1);

    err1 = AMSP_fsm_loop(OP_ATTACH_COMM, sfd, &err, ams, host_list, &list_len);
    AMSP_mem_net_close(sfd);
    CheckErr(err1);
	CheckErr(err);

    /* Creates the communicator structure */
    /* (ctype is ignored on accessor side; just fill in something) */
    err = AMSP_New_Comm_Struct(comm_name, NODE_TYPE, ams);
    CheckErr(err);

    AMSP_extract(host_list, str, list_len, &index);
    p = strtok(str, "|");
    while(p && (index <= list_len)) {
        strcpy(host, p);
        p = strtok(NULL, "|");
        if (p == NULL)
            return AMS_ERROR_SYSTEM;
        port_number = atoi(p);

        host_node = AMSP_Create_Host_Node(host, port_number, &err);
        CheckErr(err);

        /* Add the host node to the communicator structure */
        err = AMSP_Add_Host_Node_AMS_Comm(*ams, host_node);
        CheckErr(err);

        AMSP_extract(host_list, str, list_len, &index);
        p = strtok(str, "|");
    }

    /* No bleeding */
    free(host_list);

    /* Initialize the communicators' table */
    err = AMSP_Publish_AMS_Comm(*ams);
    CheckErr(err);

    /* Add publisher id in the mask before return */
    *ams = *ams + pub_id*MAX_PUB;

    return AMS_ERROR_NONE;

}


/*@

  AMS_Comm_get_memory_list - Gets the list of memories attached to a communicator

        Input Parameters:
+       ams - the computational context id
        Output Parameters:
+       mem_list - Pointer to a list of Memory names

@*/

int AMS_Comm_get_memory_list(AMS_Comm ams, char ***mem_list)
{
    int err, err1, sfd;

    /* Get the publisher id */
    pub_id = ams / MAX_PUB;

    /* Get the real ams id */
    ams = ams % MAX_PUB;

    /* Connect to the publisher */
    err = AMSP_Comm_Hosts_open_any(ams, &sfd);
    CheckErr(err);

    /* Get Memory list */
    err1 = AMSP_send_rq(OP_MEM_LIST, sfd, ams);
	CheckErr(err1);

    err = AMSP_fsm_loop(OP_MEM_LIST, sfd, &err, mem_list);
	CheckErr(err1);

    /* Close connection */
    AMSP_mem_net_close(sfd);
    CheckErr(err);

    return AMS_ERROR_NONE;

}

/*@

  AMS_Memory_attach - Attaches published memory objects. This function
                      creates an array of local memory objects that corresponds to
                      an array of remote memory objects published with AMS_Memory_publish()

        Input Parameters:
+       ams - AMS communicator
-       name - the unique name of the memory. To attach multiple memories
               at the same time (for efficiency), use the "|" separator in
			   the argument name to seperate the different memories. In such
			   a case the output parameter mem should point to enough space
			   to receive the different memory ids attached.

        Output Parameters:
+       mem - Pointer to the memory object, or an array of memory objects
-       step - Pointer to the memory step number, or an array of step numbers

@*/

int AMS_Memory_attach(AMS_Comm ams, const char *name, AMS_Memory *mem, unsigned int *step)
{
    int err, err1, sfd, connected, i, flag, nbmem;
    int *socket_list, num_hosts;
    HOST_ITER cur_socket;
    AMS_MEM *amem;
    MEM_NODE *mem_node;
    unsigned char *step_bytes;
    RequestID req;
	char *buff = NULL, *p;

	if (name == NULL)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

	buff = (char *) malloc (sizeof(char)* (strlen(name)+1));
	if (buff == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Get the publisher id */
    pub_id = ams / MAX_PUB;

    /* Get the real ams id */
    ams = ams % MAX_PUB;

	strcpy(buff, name);
	p = strtok(buff, "|");
	nbmem = 0; /* How many memories are being attached */
	while(p) {
		nbmem++;
		/* Check if the memory is already attached  */
		err = AMSP_Find_Mem_name_AMS_Comm(ams, p, mem, &mem_node);
		CheckErr(err);

		if (mem_node) {
			/* Delete the memory node from the communicator */
			err = AMSP_Del_Mem_Node_AMS_Comm(ams, mem_node);
			CheckErr(err);
		}
		p = strtok(NULL, "|");
	}

    /* Generate a request ID */
    err = AMSP_Generate_RequestID(&req);
    CheckErr(err);

    /* Connect to any of the publishers */
    err = AMSP_Comm_Hosts_open_any(ams, &sfd);
    CheckErr(err);

    /* Attaches the memory */
    err = AMSP_send_rq(OP_ATTACH_MEM, sfd, name, nbmem);
	CheckErr(err);

    err1 = AMSP_fsm_loop(OP_ATTACH_MEM, sfd, &err, mem, nbmem);
    AMSP_mem_net_close(sfd);
    CheckErr(err1);
    CheckErr(err);

    /*
     * Create a "mirror" memory structure of published memory.
     * Note that the mem is not modified in the following call
     */

	strcpy(buff, name);
	p = strtok(buff, "|");
	i = 0;
	while(p) {

		/*
		 * Creates a new memory sturcture
		 */
		amem = AMSP_New_AMS_MEM(ams, p, &err);
		CheckErr(err);

		/*
		 * This creates a memory node
		 */
		mem_node = AMSP_Create_Mem_Node(amem, mem+i, &err);
		CheckErr(err);

		/*
		 * Add the node to the communicator
		 */
		err = AMSP_Add_Mem_Node_AMS_Comm(ams, mem_node);
		CheckErr(err);

		i++;
		p = strtok(NULL, "|");
	}

    /*
	 * Send two rounds of requests to all our processors.
     * First round, get the properties of the memory field and figure
     * out its length, but don't fill the buffers yet.
	 */

    for(connected = AMSP_Comm_Hosts_begin(ams, &cur_socket);
        AMSP_Comm_Hosts_valid(ams, &cur_socket);
        connected = AMSP_Comm_Hosts_advance(ams, &cur_socket)) {

        if(connected == AMS_ERROR_NONE) {
            err = AMSP_send_rq(OP_MEM_FLDS, cur_socket.sfd, mem, nbmem);
			CheckErr(err);

            err1 = AMSP_fsm_loop(OP_MEM_FLDS, cur_socket.sfd, &err);
			CheckErr(err1);

            if(err != AMS_ERROR_NONE) {
                AMSP_mem_net_close(cur_socket.sfd);
                return err;
            }
        }
    }

    /* Now that we know field sizes, allocate memory for them. */
    err = AMSP_Activate_AMS_Memory(mem, nbmem);
    CheckErr(err);

	step_bytes = (unsigned char *) malloc (sizeof(char) * STEP_BYTES * nbmem);
	if (step_bytes == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* send step 0 to force an update */
    for(i = 0; i < STEP_BYTES * nbmem; ++i)
        step_bytes[i] = (unsigned char) 0;

    /*
	 * Now contact all the hosts to fill the buffers.
	 */

    connected = AMSP_Comm_Hosts_open_all(ams, &num_hosts, &socket_list);
    if(connected == AMS_ERROR_NONE && num_hosts > 0) {
        /*
		 * First send all the requests, then wait for all the answers
		 * Host 0 starts the action, so contact it only when everyone else
         * is ready
		 */
        for(i = 1; i < num_hosts; ++i) {
            err = AMSP_send_rq(OP_RECV_UPD_MEM, socket_list[i], mem, nbmem, step_bytes, req);
			CheckErr(err);
		}

        err = AMSP_send_rq(OP_RECV_UPD_MEM, socket_list[0], mem, nbmem, step_bytes, req);
		CheckErr(err);

        for(i = 0; i < num_hosts && err == AMS_ERROR_NONE; ++i) {
            err1 = AMSP_fsm_loop(OP_RECV_UPD_MEM, socket_list[i], &err, i, &flag, NULL);
			CheckErr(err1);
		}
    }

    AMSP_Comm_Hosts_close_all(num_hosts, socket_list);
    if(connected != AMS_ERROR_NONE)
        return AMS_ERROR_NET_OPEN;

    /* Check errors from fsm loops */
    CheckErr(err);


	/* Get the new step id */
	err = AMSP_GetStep_AMS_Memory(mem, nbmem, step_bytes);
	CheckErr(err);

	for (i=0; i<nbmem; i++) {
		/* Translate the step id from bytes to integer */
		if(step != NULL) {
			err = AMSP_TranslateStep(step_bytes + i * STEP_BYTES, step + i);
			CheckErr(err);
		}
	}

    /* Deletes the request ID */
    err = AMSP_Delete_RequestID(req);
    CheckErr(err);

    /* Free memory */
    if (buff)
        free(buff);

    /* Mask the memory id with the publisher id */
    for (i = 0;i<nbmem; i++)
        mem[i] = maskId(mem[i], pub_id);


    return AMS_ERROR_NONE;
}

/*@
AMS_Memory_get_field_list - Gets the list of fields attached to a memory

  Input Parameters:
+   mem - the memory id
  Output Parameters:
+   fld_list - An array of pointers to the field names

@*/

int AMS_Memory_get_field_list(AMS_Memory mem, char *** fld_list)
{
    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    return AMSP_Get_field_list_AMS_Memory(mem, fld_list);
}

/*@

  AMS_Memory_get_field_info - Get fields attributes
  object field.

        Input Parameters:
+       mem - the memory object
-       name - the field name

        Output Parameters:
+         addr - address of memory
.         len - length of the memory
.         dtype - AMS Data type: AMS_CHAR, AMS_INT, AMS_BOOLEAN, AMS_FLOAT, AMS_DOUBLE, or AMS_STRING
.         mtype - AMS Memory type: AMS_READ, AMS_WRITE
.         stype - AMS Shared type: AMS_COMMON, AMS_DISTRIBUTED or AMS_REDUCED
-         rtyep - AMS Reduction type: AMS_SUM, AMS_MAX, AMS_MIN, or AMS_NONE

@*/

int AMS_Memory_get_field_info(AMS_Memory mem, const char *name, void **addr, int *len, AMS_Data_type *dtype,
                              AMS_Memory_type *mtype, AMS_Shared_type *stype, AMS_Reduction_type *rtype)
{
    int err;
    Field *fld;

    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    fld = AMSP_Find_name_AMS_Memory(mem, name, &err);
    CheckErr(err);

    if (fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    /* Get Field's data */
    *addr = AMSP_Get_Buffer_Field(fld, len, &err);
    CheckErr(err);

    /* Get Field's data type */
    err = AMSP_Get_Data_type_Field(fld, dtype);
    CheckErr(err);

    /* Get Field's memory type */
    err = AMSP_Get_Memory_type_Field(fld, mtype);
    CheckErr(err);

    /* Get Field's shared type */
    err = AMSP_Get_Shared_type_Field(fld, stype);
    CheckErr(err);

    /* Get Field's reduction type */
    err = AMSP_Get_Reduction_type_Field(fld, rtype);
    CheckErr(err);

    return AMS_ERROR_NONE;
}


/*@

  AMS_Memory_get_field_block - Gets the dimensions of a particular field in memory
  object.

        Input Parameters:
+       mem  - the memory object
-       name - the field name

        Output Parameters:
+         len   - pointer to the number of dimensions (i.e 2 for 2-D, 3 for 3-D field
.         start - array of starting indices (one per dimension)
-         end   - array of ending indices (one per dimension)

@*/

int AMS_Memory_get_field_block(AMS_Memory mem, const char *name, int *dim, int **start, int **end)
{
    int err;
    Field *fld;

    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    fld = AMSP_Find_name_AMS_Memory(mem, name, &err);
    CheckErr(err);

    if (fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    /* Get Field's data */
    return AMSP_Get_Block_Field(fld, dim, start, end);
}

/*@

  AMS_Memory_set_field_info - Modifies the accessor field's data. To publisher
  copy is modifed only after a call to AMS_Memory_update_send_end.

        Input Parameters:
+       mem - the memory object
.       name - the field name
.       addr - pointer to the new field's data
-       len - new field's length (number of elements)

@*/
int AMS_Memory_set_field_info(AMS_Memory mem, const char *name, void *addr, int len)
{
    Field *fld;
    int err, start;
    AMS_Memory_type mtype;

    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    /* Make sure the field exist */
    fld = AMSP_Find_name_AMS_Memory(mem, name, &err);
    CheckErr(err);

    if (fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    /* Make sure the field is not read-only */
    err = AMSP_Get_Memory_type_Field(fld, &mtype);
    CheckErr(err);

    if (fld->mtype == AMS_READ)
        return AMS_ERROR_FIELD_READ_ONLY;

    /* Change the field's data */
    err = AMSP_Get_Starting_Index_Field(fld, &start);
    CheckErr(err);
    err = AMSP_Upd_Buffer_Field(fld, addr, len, start);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@

  AMS_Memory_update_send_begin - Begin updating the memory of the publisher's memory object
  from the accessor's representation.

        Input Parameters:
+       mem - the AMS memory object

          Note: Only the AMS_WRITE fields are updated

@*/

int AMS_Memory_update_send_begin(AMS_Memory mem)
{
    int err, err1, connected;
    int *socket_list, num_hosts, i, nbmem = 1; /* Can update only one memory at this time */
    char *upd_list;
    unsigned char step[STEP_BYTES];
    RequestID req;

    int list_len;
    AMS_Comm ams;

    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    /* set step to 0 to force update: */
    for(i = 0; i < STEP_BYTES; ++i)
        step[i] = (unsigned char) 0;

    /* Who is our communicator? */
    err = AMSP_Comm_AMS_Memory(mem, &ams);
    CheckErr(err);

    err = AMSP_Generate_RequestID(&req);
    CheckErr(err);

    /* Create the updating list of fields */
    /* Don't care about Request ID (-1) since accessor side */
    err = AMSP_Create_Upd_Memory_list(&mem, nbmem, step, &upd_list, &list_len, NULL);
    CheckErr(err);

    if(list_len > 0) {
        /* (connect only if necessary) */

        /* I just inserted the part up to: for(connected = Comm_Hosts_begin) */

        connected = AMSP_Comm_Hosts_open_all(ams, &num_hosts, &socket_list);
        if(connected == AMS_ERROR_NONE && num_hosts > 0) {
            /* First send all the requests, then wait for all the answers */
            /* Host 0 starts the action, so contact it only when everyone */
            /* else is ready */
            for(i = 1; i < num_hosts; ++i) {
                err = AMSP_send_rq(OP_SEND_UPD_MEM, socket_list[i], req, &mem, nbmem, upd_list, list_len);
				CheckErr(err);
			}

            err = AMSP_send_rq(OP_SEND_UPD_MEM, socket_list[0], req, &mem, nbmem, upd_list, list_len);
			CheckErr(err);

            for(i = 0; i < num_hosts && err == AMS_ERROR_NONE; ++i) {
                err1 = AMSP_fsm_loop(OP_SEND_UPD_MEM, socket_list[i], &err);
				CheckErr(err1);
			}
        }

        AMSP_Comm_Hosts_close_all(num_hosts, socket_list);
        if(connected != AMS_ERROR_NONE)
            return AMS_ERROR_NET_OPEN;

        /* Check errors from fsm loops */
        CheckErr(err);
    }

    if (upd_list)
        free(upd_list);

    err = AMSP_Delete_RequestID(req);
    CheckErr(err);

    return AMS_ERROR_NONE;

}

/*@

  AMS_Memory_update_send_end - Concludes the Memory update. Only the
  AMS_WRITE fields are updated.


    Input Parameters:
+   mem - the AMS memory object

@*/

int AMS_Memory_update_send_end(AMS_Memory mem)
{
    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    /* Not implemented yet */
    return AMS_ERROR_NONE;
}

/*@

  AMS_Memory_update_recv_end - Finish receiving memory update
        from the publisher's reprentation of the memory.

        Input Parameters:
+       mem - the AMS memory object

        Output Parameters:
+        flag - 1 if the memory changed since the last update, 0 otherwise
-        step - Pointer to the Memory step

@*/

int AMS_Memory_update_recv_end(AMS_Memory mem, int *flag, unsigned int *step)
{
    int err, err1, connected, i, nbmem = 1;
    int num_hosts, *socket_list;
    unsigned char step_bytes[STEP_BYTES];
    AMS_Comm ams;
    RequestID req;

    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    /* What is our communicator? */
    err = AMSP_Comm_AMS_Memory(mem, &ams);
    CheckErr(err);

    /* Check the validity of the Memory */
    CheckMemory(mem);

    err = AMSP_GetStep_AMS_Memory(&mem, nbmem, step_bytes);
    CheckErr(err);

    err = AMSP_Generate_RequestID(&req);
    CheckErr(err);

    connected = AMSP_Comm_Hosts_open_all(ams, &num_hosts, &socket_list);
    if(connected == AMS_ERROR_NONE && num_hosts > 0) {
        /* First send all the requests, then wait for all the answers */
        /* Host 0 starts the action, so contact it only when everyone else */
        /* is ready */
        for(i = 1; i < num_hosts; ++i) {
            err = AMSP_send_rq(OP_RECV_UPD_MEM, socket_list[i], &mem, nbmem, step_bytes, req);
			CheckErr(err);
		}

        err = AMSP_send_rq(OP_RECV_UPD_MEM, socket_list[0], &mem, nbmem, step_bytes, req);
		CheckErr(err);

        for(i = 0; i < num_hosts && err == AMS_ERROR_NONE; ++i) {
            err1 = AMSP_fsm_loop(OP_RECV_UPD_MEM, socket_list[i], &err, i, flag, (unsigned char *) step_bytes);
			CheckErr(err1);
		}
    }

    AMSP_Comm_Hosts_close_all(num_hosts, socket_list);
    if(connected != AMS_ERROR_NONE)
        return AMS_ERROR_NET_OPEN;

    /* Check errors from fsm loops */
    CheckErr(err);

    err = AMSP_Delete_RequestID(req);
    CheckErr(err);

    if(step != NULL) {
        err = AMSP_TranslateStep((unsigned char *) step_bytes, step);
        CheckErr(err);
    }

    return AMS_ERROR_NONE;
}

/*@

  AMS_Memory_update_recv_begin - Begin receiving the memory update
                                 from the publisher. To complete the
                                 update, the user need to call
                                 AMS_Memory_update_recv_end.

          Input Parameters:
+         mem - the AMS memory object


@*/

int AMS_Memory_update_recv_begin(AMS_Memory mem)
{
    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    return AMS_ERROR_NONE;
}


/*@

  AMS_Memory_detach - Disconnect from the publisher's memory object.

        Input Parameters:
+       mem - the AMS memory object

@*/

int AMS_Memory_detach(AMS_Memory mem)
{
    int err;
    AMS_Comm ams;
    MEM_NODE *node;

    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);

    CheckMemory(mem);

    /* Gets the communicator */
    err = AMSP_Comm_AMS_Memory(mem, &ams);
    CheckErr(err);

    /* Find the memory node to delete */
    err = AMSP_Find_Mem_id_AMS_Comm(ams, mem, &node);
    CheckErr(err);

    if (!node)
        return AMS_ERROR_NONE;

    /* Delete the memory node from the communicator */
    err = AMSP_Del_Mem_Node_AMS_Comm(ams, node);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@

  AMS_Comm_detach - Disconnect from a publisher

        Input Parameters:
        ams - the AMS Communicator

@*/

int AMS_Comm_detach(AMS_Comm ams)
{
    char *msg;
    int err, err1, connected;
    HOST_ITER cur_host;

    /* Get the publisher id */
    pub_id = ams / MAX_PUB;

    /* Get the real ams id */
    ams = ams % MAX_PUB;

    /* Unpublish the communicator */
    err = AMSP_UnPublish_AMS_Comm(ams);
    CheckErr(err);

    /* Deletes the memory list */
    err = AMSP_Delete_Memory_list(ams);
    CheckErr(err);

    /* Delete memory nodes */
    err = AMSP_DelAll_Mem_AMS_Comm(ams);
    CheckErr(err);

    for(connected = AMSP_Comm_Hosts_begin(ams, &cur_host);
        AMSP_Comm_Hosts_valid(ams, &cur_host);
        connected = AMSP_Comm_Hosts_advance(ams, &cur_host)) {

        if(connected == AMS_ERROR_NONE) {
            err = AMSP_send_rq(OP_DETACH_COMM, cur_host.sfd, ams);
            if (err != AMS_ERROR_NONE) { AMS_Print_error_msg(err, &msg); }

            err1 = AMSP_fsm_loop(OP_DETACH_COMM, cur_host.sfd, &err);
            if (err != AMS_ERROR_NONE) { AMS_Print_error_msg(err, &msg); }
            if (err1 != AMS_ERROR_NONE) { AMS_Print_error_msg(err1, &msg); }

            if(err != AMS_ERROR_NONE) {
                err = AMSP_mem_net_close(cur_host.sfd);
                if (err != AMS_ERROR_NONE) { AMS_Print_error_msg(err1, &msg); }
            }
        }
    }

    /* Delete host nodes */
    err = AMSP_DelAll_Host_AMS_Comm(ams);
    CheckErr(err);

    /* Delete the Communicator */
    err = AMSP_Del_AMS_Comm(ams);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@

  AMS_Disconnect - Frees (small) amount of resources allocated by AMS_Connect()

@*/

int AMS_Disconnect(void)
{
  int err;
  err = AMSP_Delete_Comm_list(); CheckErr(err);

    return AMS_ERROR_NONE;
}
