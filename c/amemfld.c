
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ams.h"
#include "amemfld.h"
#include "netlib.h"

/*
 * Member function to access the object fields
 */

/*@
        AMSP_New_Field - Create a new field structure and Initialize it

        Input Parameters:
			fld - Field to be created and initialized

@*/
Field * AMSP_New_Field(int *err)
{
    Field * fld = (Field *) malloc(sizeof(Field));
    if (fld == NULL) {
        *err = AMS_ERROR_INSUFFICIENT_MEMORY;
        return NULL;
    }

    fld->name = NULL;
    fld->data = NULL;
    fld->len = -1;
    fld->dim = 1;
    fld->start = (int *) malloc(sizeof(int));
    fld->end = (int *) malloc(sizeof(int));
    if(fld->start == NULL || fld->end == NULL) {
        *err =  AMS_ERROR_INSUFFICIENT_MEMORY;
        return NULL;
    }
    *(fld->start) = 0;
    *(fld->end) = 0;
    fld->lang = AMS_C;
    fld->CSetValue = NULL;
    fld->CPPSetValue = NULL;
    fld->JAVASetValue = NULL;
    fld->Obj = NULL;
    fld->Env = NULL;
    fld->mtype = AMS_MEMORY_UNDEF;
    fld->dtype = AMS_DATA_UNDEF;
    fld->stype = AMS_SHARED_UNDEF;
    fld->rtype = AMS_REDUCT_UNDEF;
    fld->status = 0;
    fld->prev = NULL;
    fld->next = NULL;

    *err = AMS_ERROR_NONE;
    return fld;
}

/*@
        AMSP_Del_Field - Deletes a field structure and frees the associated memory

        Input Parameters:
			fld - Field to be deleted

@*/
int AMSP_Del_Field(Field *fld)
{
        
    if (fld == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (fld->name)
        free(fld->name);

#ifdef AMS_ACCESSOR
    if (fld->data)
        free(fld->data);
#endif

    if(fld->start)
        free(fld->start);
    if(fld->end)
        free(fld->end);

    free(fld);
    return AMS_ERROR_NONE;
}


/*@
	AMSP_Del_Field - Deletes all the fields in the linked list

        Input Parameters:
			fld - first Field in the linked list to be deleted

@*/
int AMSP_Del_All_Field(Field *fld)
{
    int err;
    Field *nfld = fld;

    if (nfld == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    while (nfld->next) {
        nfld = nfld->next;
        err = AMSP_Del_Field(nfld->prev);
        CheckErr(err);
    }

    /* Last field to delete */
    err = AMSP_Del_Field(nfld);
    CheckErr(err);
        
    return AMS_ERROR_NONE;
}

/*@
        AMSP_Dup_Field - Creates a duplicate of a field

        Input Parameters:
             src_fld - Field to duplicate
        Input/Output
             dest_fld - Destination field. The caller has the responsibility
             to create the dest_fld using New_Field. After completition the
             two fields will be identical

@*/
int AMSP_Dup_Field(Field *src_fld, Field *dest_fld)
{
    char **dest_str, **src_str;
    int k, err = Validate(AMSP_Valid_Field, src_fld), i;
    CheckErr(err);

    err = Validate(AMSP_Valid_Field, dest_fld);
    CheckErr(err);
    /* Probably the user did not create this object first */
        
    /* Copy the name */
    strcpy(dest_fld->name, src_fld->name);
        
    /* Allocate a new one */
    switch (src_fld->dtype) {
    case AMS_CHAR:
        if (dest_fld->data)
          /* We release previous memory */
          free(dest_fld->data);

        dest_fld->data = malloc((src_fld->len)*sizeof(char));
        if (dest_fld->data == NULL)
          return AMS_ERROR_INSUFFICIENT_MEMORY;

        /* Copy the data */
        memcpy(dest_fld->data, src_fld->data, src_fld->len*sizeof(char));
        break;
    case AMS_INT:
    case AMS_BOOLEAN:
        if (dest_fld->data)
            /* We release previous memory */
            free(dest_fld->data);

        dest_fld->data = malloc((src_fld->len)*sizeof(int));
        if (dest_fld->data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        /* Copy the data */
        memcpy(dest_fld->data, src_fld->data, src_fld->len*sizeof(int));
        break;

    case AMS_FLOAT:
        if (dest_fld->data)
            /* We release previous memory */
            free(dest_fld->data);

        dest_fld->data = malloc((src_fld->len)*sizeof(float));
        if (dest_fld->data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        /* Copy the data */
        memcpy(dest_fld->data, src_fld->data, src_fld->len*sizeof(float));
        break;

    case AMS_DOUBLE:
        if (dest_fld->data)
            /* We release previous memory */
            free(dest_fld->data);

        dest_fld->data = malloc((src_fld->len)*sizeof(double));
        if (dest_fld->data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        /* Copy the data */
        memcpy(dest_fld->data, src_fld->data, src_fld->len*sizeof(double));
        break;

    case AMS_STRING:
        /* Free previously allocated data */
        dest_str = (char **)dest_fld->data;
        if (dest_str) {
            for (i = 0; i < dest_fld->len; i++)
                if (dest_str[i])
                    free(dest_str[i]);
            free(dest_str);
        }
                                        
        dest_fld->data = malloc((src_fld->len)*sizeof(char *));
        if (dest_fld->data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        dest_str = (char **)dest_fld->data;
        src_str = (char **)src_fld->data;
        for (i = 0; i < src_fld->len; i++) {
            dest_str[i] = (char *)malloc(strlen(src_str[i]) + 1);
            if (dest_str[i] == NULL)
                return AMS_ERROR_INSUFFICIENT_MEMORY;
            strcpy(dest_str[i], src_str[i]);
        }
        break;
    default:
        /* Can't duplicate the data types if memory type is not known */
        return AMS_ERROR_INVALID_DATA_TYPE;
    }

    dest_fld->len = src_fld->len;
    dest_fld->mtype = src_fld->mtype;;
    dest_fld->dtype = src_fld->dtype;
    dest_fld->stype = src_fld->stype;
    dest_fld->rtype = src_fld->rtype;
    dest_fld->status = src_fld->status;
    dest_fld->lang = src_fld->lang;
    dest_fld->CSetValue = src_fld->CSetValue;
    dest_fld->CPPSetValue = src_fld->CPPSetValue;
    dest_fld->JAVASetValue = src_fld->JAVASetValue;
    dest_fld->dim = src_fld->dim;
    if(src_fld->dim > 0) {
        dest_fld->start = (int *) realloc(dest_fld->start, src_fld->dim * sizeof(int));
        dest_fld->end = (int *) realloc(dest_fld->end, src_fld->dim * sizeof(int));
        if(dest_fld->start == NULL || dest_fld->end == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;
        for(k = 0; k < src_fld->dim; ++k) {
            dest_fld->start[k] = src_fld->start[k];
            dest_fld->end[k] = src_fld->end[k];
        }
    } else
        AMSP_err_quit("Put permutation support in Dup_Field!");

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Get_Buffer_Field - Gets field buffer

        Input Parameters:
			fld - Field object

        Output Parameter:
            buf - address of the buffer
            len - the number of element in the buffer, not its size


@*/
void * AMSP_Get_Buffer_Field(Field *fld, int *len, int *err)
{
    *err = Validate(AMSP_Valid_Field, fld);
    CheckErr1(*err);

    *len = fld->len;

    /* 
     * The caller should copy the data in a different buffer 
     * before doing any modif
     */
        
    if(fld->len <= 0)
        *err = AMS_ERROR_VALIDATION;
    else
        *err = AMS_ERROR_NONE;
    return fld->data;
}

/*@
        AMSP_Get_Reduced_Field - Get reduced field

        Input Parameters:
			fld - Field object

        Output Parameter:
            answer - Pre-allocated int/float/double for answer

        The function returns a pointer to the reduced field, which
        must be freed.

@*/

int AMSP_Get_Reduced_Field(Field *fld, void *answer)
{
    int i, err;
    char tmpchar, *pchar;
    int tmpint, *pint;
    float tmpflt, *pflt;
    double tmpdbl, *pdbl;

    err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    if(fld->len <= 0)
        return AMS_ERROR_BAD_FIELD_LENGTH;
    if(fld->stype != AMS_REDUCED)
        return AMS_ERROR_INVALID_REDUCTION_TYPE;

    pchar = (char *) fld->data;
    pint = (int *) fld->data;
    pflt = (float *) fld->data;
    pdbl = (double *) fld->data;

    switch(fld->dtype) {
    case AMS_CHAR:
        /* Element 0: */
        tmpchar = *pchar;
        /* Yes, start with 1, not 0! */
        for(i = 1; i < fld->len; ++i)
        {
          ++pchar;
          switch(fld->rtype)
          {
            case AMS_SUM:
              tmpchar = tmpchar + *pchar;
              break;
            case AMS_MIN:
              if(*pchar < tmpchar)
                tmpchar = *pchar;
              break;
            case AMS_MAX:
              if(*pchar > tmpchar)
                tmpchar = *pchar;
              break;
            default:
              return AMS_ERROR_INVALID_REDUCTION_TYPE;
          }
        }
        pchar = (char *) answer;
        *pchar = tmpchar;
        break;
    case AMS_INT:
    case AMS_BOOLEAN:
        /* Element 0: */
        tmpint = *pint;
        /* Yes, start with 1, not 0! */
        for(i = 1; i < fld->len; ++i) {
            ++pint;
            switch(fld->rtype) {
            case AMS_SUM:
                tmpint = tmpint + *pint;
                break;
            case AMS_MIN:
                if(*pint < tmpint)
                    tmpint = *pint;
                break;
            case AMS_MAX:
                if(*pint > tmpint)
                    tmpint = *pint;
                break;
            default:
                return AMS_ERROR_INVALID_REDUCTION_TYPE;
            }
        }
        pint = (int *) answer;
        *pint = tmpint;
        break;


    case AMS_FLOAT:
        /* Element 0: */
        tmpflt = *pflt;
        /* Yes, start with 1, not 0! */
        for(i = 1; i < fld->len; ++i) {
            ++pflt;
            switch(fld->rtype) {
            case AMS_SUM:
                tmpflt = tmpflt + *pflt;
                break;
            case AMS_MIN:
                if(*pflt < tmpflt)
                    tmpflt = *pflt;
                break;
            case AMS_MAX:
                if(*pflt > tmpflt)
                    tmpflt = *pflt;
                break;
            default:
                return AMS_ERROR_INVALID_REDUCTION_TYPE;
            }
        }
        pflt = (float *) answer;
        *pflt = tmpflt;
        break;

    case AMS_DOUBLE:
        /* Element 0: */
        tmpdbl = *pdbl;
        /* Yes, start with 1, not 0! */
        for(i = 1; i < fld->len; ++i) {
            ++pdbl;
            switch(fld->rtype) {
            case AMS_SUM:
                tmpdbl = tmpdbl + *pdbl;
                break;
            case AMS_MIN:
                if(*pdbl < tmpdbl)
                    tmpdbl = *pdbl;
                break;
            case AMS_MAX:
                if(*pdbl > tmpdbl)
                    tmpdbl = *pdbl;
                break;
            default:
                return AMS_ERROR_INVALID_REDUCTION_TYPE;
            }
        }
        pdbl = (double *) answer;
        *pdbl = tmpdbl;
        break;

    default:
        return AMS_ERROR_INVALID_DATA_TYPE;
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Reduce_Field - Given a field consisting of one element,
                       reduce the pair of this element and a new element
                       and store it in the field.

        Input Parameters:
            fld - Field object
			second - second element

        Output Parameter:
			err - Error

        The function returns a pointer to the reduced field, which
        must be freed.

@*/

int AMSP_Reduce_Field(Field *fld, void *second)
{
    int err;
    char oldchar, newchar;
    int oldint, newint;
    float oldflt, newflt;
    double olddbl, newdbl;

    err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    if(fld->len != 1)
        return AMS_ERROR_BAD_FIELD_LENGTH;
    if(fld->stype != AMS_REDUCED)
        return AMS_ERROR_INVALID_REDUCTION_TYPE;

    switch(fld->dtype) {
    case AMS_CHAR:
        oldchar = *((char *) fld->data);
        newchar = *((char *) second);

        switch(fld->rtype)
        {
          case AMS_SUM:
            *((char *) fld->data) = oldchar + newchar;
            break;
          case AMS_MIN:
            if(newchar < oldchar)
              *((char *) fld->data) = newchar;
            break;
          case AMS_MAX:
            if(newchar > oldchar)
              *((char *) fld->data) = newchar;
            break;
          default:
            return AMS_ERROR_INVALID_REDUCTION_TYPE;
        }
        break;
    case AMS_INT:
    case AMS_BOOLEAN:
        oldint = *((int *) fld->data);
        newint = *((int *) second);

        switch(fld->rtype) {
        case AMS_SUM:
            *((int *) fld->data) = oldint + newint;
            break;
        case AMS_MIN:
            if(newint < oldint)
                *((int *) fld->data) = newint;
            break;
        case AMS_MAX:
            if(newint > oldint)
                *((int *) fld->data) = newint;
            break;
        default:
            return AMS_ERROR_INVALID_REDUCTION_TYPE;
        }
        break;

    case AMS_FLOAT:
        oldflt = *((float *) fld->data);
        newflt = *((float *) second);

        switch(fld->rtype) {
        case AMS_SUM:
            *((float *) fld->data) = oldflt + newflt;
            break;
        case AMS_MIN:
            if(newflt < oldflt)
                *((float *) fld->data) = newflt;
            break;
        case AMS_MAX:
            if(newflt > oldflt)
                *((float *) fld->data) = newflt;
            break;
        default:
            return AMS_ERROR_INVALID_REDUCTION_TYPE;
        }
        break;

    case AMS_DOUBLE:
        olddbl = *((double *) fld->data);
        newdbl = *((double *) second);

        switch(fld->rtype) {
        case AMS_SUM:
            *((double *) fld->data) = olddbl + newdbl;
            break;
        case AMS_MIN:
            if(newdbl < olddbl)
                *((double *) fld->data) = newdbl;
            break;
        case AMS_MAX:
            if(newdbl > olddbl)
                *((double *) fld->data) = newdbl;
            break;
        default:
            return AMS_ERROR_INVALID_REDUCTION_TYPE;
        }
        break;

    default:
        return AMS_ERROR_INVALID_DATA_TYPE;
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Get_Starting_Index_Field - get index where this processor's entries
                                   start

        Input Parameters:
			fld - Field object

        Output Parameters:
			start - starting index
@*/

int AMSP_Get_Starting_Index_Field(Field *fld, int *start)
{
    *start = fld->start[0];
    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_Buffer_Field - Sets the field buffer

        Input Parameters:
+			fld - Field object
.			buf - address of the buffer
+			len - number of elements in the buffer

@*/
int AMSP_Set_Buffer_Field(Field *fld, void *buf, int len)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    if (buf == NULL)
        return AMS_ERROR_BAD_ARGUMENT;
        
    if (len <= 0)
        return AMS_ERROR_BAD_ARGUMENT;

    /* The publisher need not allocate memory nor copy the data */

    fld->data = buf;
    fld->len = len;
    if(fld->dim == 1)
        *(fld->end) = *(fld->start) + len -1;
    fld->status = 1; /* Field has changed */

    return AMS_ERROR_NONE;
}

int AMSP_Size_Elt_Field(Field *fld, int *nbytes)
{
    switch(fld->dtype) {
        case AMS_CHAR:
            *nbytes = sizeof(char);
            break;
        case AMS_INT:
        case AMS_BOOLEAN:
            *nbytes = sizeof(int);
            break;
        case AMS_FLOAT:
            *nbytes = sizeof(float);
            break;
        case AMS_DOUBLE:
            *nbytes = sizeof(double);
            break;
        case AMS_STRING:
            *nbytes = sizeof(char *);
            break;
        default:
            return AMS_ERROR_INVALID_DATA_TYPE;
    }

    return AMS_ERROR_NONE;
}

/*@     AMSP_Upd_Buffer_Perm_Field - Updates the field buffer using permutation

        Input Parameters:
+			fld - Field object
.			buf - address of the buffer
.			len - number of items in the buffer being sent
+			perm - array indicating permutation from received indices
                   to global indices

@*/

int AMSP_Upd_Buffer_Perm_Field(Field *fld, void *buf, int len, int *perm)
{
    int k, err, eltsize;
    void *get_from;

    err = AMSP_Size_Elt_Field(fld, &eltsize);
    CheckErr(err);

#ifdef AMS_ACCESSOR
    for(k = 0; k < len; ++k) {
        if(perm[k] >= fld->len)
            return AMS_ERROR_BAD_DIMENSIONS;

        get_from = (void *) (((char *) buf) + (k * eltsize));

        err = AMSP_Upd_Buffer_Field(fld, get_from, 1, perm[k]);
        CheckErr(err);
    }
#else
    /* AMS_PUBLISHER TAKES INVERSE OPERATION */

    for(k = 0; k < fld->len; ++k) {
        if(perm[k] >= len)
            return AMS_ERROR_BAD_DIMENSIONS;

        get_from = (void *) (((char *) buf) + (perm[k] * eltsize));

        err = AMSP_Upd_Buffer_Field(fld, get_from, 1, k);
        CheckErr(err);
    }
#endif

    return AMS_ERROR_NONE;
}

/*@     AMSP_Upd_Buffer_MultDim_Field - Updates the field buffer of n-dim array

        Input Parameters:
+			fld - Field object
.			buf - address of the buffer
.			len - number of items in the buffer being sent
.			dim - number of dimensions being updated
.			start - indices in each dimension where the buffer being sent
                    starts
+			end - indices in each dimension where sent buffer ends

@*/
int AMSP_Upd_Buffer_MultDim_Field(Field *fld, void *buf, int len,
                             int dim, int *start, int *end)
{

    int y, uplim, lowlim, err, sendmax, sendmin, sendlen, chunklen, sheetlen, oldend, eltsize, myoldstart, myoldend, subchunklen;
    void *oldbuf;

    /* Use AMSP_Upd_Buffer_Perm_Field to update a field with a permutation */
    if(dim <= 0 || dim > fld->dim)
        return AMS_ERROR_BAD_ARGUMENT;

    if(dim == 1)
        return AMSP_Upd_Buffer_Field(fld, buf, len, *start);


    /* STEP 1: Figure out how much our data and our field overlap */
    /* (a) How many rows overlap? */
    lowlim = start[dim-1];
    uplim = end[dim-1];
    if(fld->start[dim-1] > lowlim)
        lowlim = fld->start[dim-1];
    if(fld->end[dim-1] < uplim)
        uplim = fld->end[dim-1];
    /* (b) How much of a given row overlaps? */
    sendmin = start[dim-2];
    sendmax = end[dim-2];
    if(fld->start[dim-2] > sendmin)
        sendmin = fld->start[dim-2];
    if(fld->end[dim-2] < sendmax)
        sendmax = fld->end[dim-2];
    sendlen = (sendmax - sendmin +1)*len / 
                   ((end[dim-1]-start[dim-1]+1)*(end[dim-2]-start[dim-2]+1));
    /* sendlen = number of tiny elements we'll be sending in a row */

    /* STEP 2: How many bytes are there in each row of incoming data? */
    err = AMSP_Size_Elt_Field(fld, &eltsize); /* how many bytes is each element */
    CheckErr(err);
    chunklen = len * eltsize / (end[dim-1] - start[dim-1]+1);
    /* make sure it's an integer */
    if(chunklen * (end[dim-1] - start[dim-1]+1) != len * eltsize)
        return AMS_ERROR_BAD_DIMENSIONS;
    
    /* STEP 3: Move our pointer to the first chunk to be copied in */
    oldbuf = buf;
    /* (a) Move it to the proper row */
    buf = (void *) (((char *) buf) + (chunklen * (lowlim - start[dim-1])));
    /* (b) Move it to the proper entry in that row */
    subchunklen = chunklen / (end[dim-2] - start[dim-2]+1);
    if(subchunklen * (end[dim-2] - start[dim-2]+1) != chunklen)
        return AMS_ERROR_BAD_DIMENSIONS;
    buf = (void *) (((char *) buf) + (subchunklen * (sendmin - start[dim-2])));
    
    /* STEP 4: Collapse field to (n-1)-d */
    sheetlen = fld->end[dim-2] - fld->start[dim-2]+1;
    oldend = fld->end[dim-2];
    fld->end[dim-2] = fld->start[dim-2] + (fld->end[dim-1] - fld->start[dim-1]+1)
                                          * sheetlen -1;

    /* STEP 5: Express starting and ending points in collapsed coordinates */
    myoldstart = start[dim-2];
    myoldend = end[dim-2];
    start[dim-2] = sendmin + (lowlim - fld->start[dim-1])*sheetlen;
    end[dim-2] = sendmax + (lowlim - fld->start[dim-1])*sheetlen;

    for(y = lowlim; y <= uplim; ++y) {
        /* Update a row */
        err = AMSP_Upd_Buffer_MultDim_Field(fld, buf, sendlen,
                                       dim-1, start, end);
        CheckErr(err);

        /* Move the pointer to the next row */
        buf = (void *) (((char *) buf) + chunklen);

        /* Advance the starting indices, in collapsed coordinates */
        start[dim-2] += sheetlen;
        end[dim-2] += sheetlen;
    }

    /* Restore original values of arguments we modified */
    start[dim-2] = myoldstart;
    end[dim-2] = myoldend;
    fld->end[dim-2] = oldend;
    buf = oldbuf;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Upd_Buffer_Field - Updates the field buffer

        Input Parameters:
+			fld - Field object
.			buf - address of the buffer
.			len - Number of elements in the buffer being sent
+			start - index where the buffer being sent starts

@*/
int AMSP_Upd_Buffer_Field(Field *fld, void *buf, int len, int start)
{
    char **tmpstr, **bufstr;
    int eltsize;

    void *get_from; /* pointer to where to start memcpy from */
    void *put_into; /* where to start copying into */
    int start_copy; /* index where we start copying */
    int end_copy; /* stop copying right before you get to this index */
    int amt_copy; /* how many elements (not bytes) to copy */

#ifdef AMS_PUBLISHER
    typedef void * (* PTSetter)(); 
    PTSetter SetValue; /* pointer to setter function */
#endif

    int err = Validate(AMSP_Valid_Field, fld), i;
    CheckErr(err);

    if (buf == NULL)
        return AMS_ERROR_BAD_ARGUMENT;
        
    if (len <= 0)
        return AMS_ERROR_BAD_ARGUMENT;

    /* Figure out where to start */
    start_copy = start;
    end_copy = start + len;
    if(fld->start[0] > start)
        start_copy = fld->start[0];
    if(fld->start[0] + fld->len < end_copy)
        end_copy = fld->start[0] + fld->len;
    amt_copy = end_copy - start_copy;

    if(amt_copy > 0) {
#ifdef AMS_PUBLISHER
        switch(fld->lang) {
        case AMS_C:
            SetValue = (PTSetter) fld->CSetValue;
            break;

        case AMS_CPP:
            SetValue = (PTSetter) fld->CPPSetValue;
            break;

        case AMS_JAVA:
            SetValue = (PTSetter) fld->JAVASetValue;
            break;

        default:
            AMSP_err_quit("Unknown language\n");
        }

        if(SetValue == NULL) {
#endif
            /* Allocate a new one */
            switch (fld->dtype) {
            case AMS_CHAR:
            case AMS_INT:
            case AMS_BOOLEAN:
            case AMS_FLOAT:
            case AMS_DOUBLE:
                err = AMSP_Size_Elt_Field(fld, &eltsize);
                CheckErr(err);

                /* Figure out where to start */
                get_from = (void *)
                    ((char *) buf + (start_copy - start)*eltsize);
                put_into = (void *) ((char *) fld->data
                                     + (start_copy - fld->start[0])*eltsize);

                /* Copy the data */
                memcpy(put_into, get_from, amt_copy*eltsize);
                 
                break;
             
            case AMS_STRING:
                /* accessor allocates memory if it's the first time: */
                tmpstr = (char **)fld->data;
                bufstr = (char **)buf;
                  
                if (fld->data == NULL)
                    return AMS_ERROR_INSUFFICIENT_MEMORY;

                for (i = start_copy; i < end_copy; i++) {
                    if (bufstr[i - start]) {
                        tmpstr[i - fld->start[0]]
                            = (char *)malloc(strlen(bufstr[i - start]) + 1);
                        if (tmpstr[i - fld->start[0]] == NULL)
                            return AMS_ERROR_INSUFFICIENT_MEMORY;
                        strcpy(tmpstr[i - fld->start[0]],
                               bufstr[i - start]);
                    } else {
                        tmpstr[i - fld->start[0]] = NULL;
                    }
                }
                break;

            default:
                return AMS_ERROR_INVALID_DATA_TYPE;
            }
#ifdef AMS_PUBLISHER
        }
        else {
            /* Don't change buffer ourselves; call the setter method! */
               
            /* Handling of starting and ending points isn't as sophisticated */
            if((amt_copy != fld->len) || (start_copy != start))
                return AMS_ERROR_VALIDATION;

            switch(fld->lang) {
            case AMS_C:
                if(fld->CSetValue(fld->data, fld->len, buf, amt_copy)
                   != AMS_ERROR_NONE)
                    return AMS_ERROR_VALIDATION;
                break;
                       
            case AMS_CPP:
                if(fld->CPPSetValue(fld->Obj, fld->data, fld->len,
                                    buf, amt_copy)
                   != AMS_ERROR_NONE)
                    return AMS_ERROR_VALIDATION;
                break;

            case AMS_JAVA:
                if(fld->JAVASetValue(fld->Env, fld->Obj,
                                     fld->data, fld->len, buf, amt_copy)
                   != AMS_ERROR_NONE)
                    return AMS_ERROR_VALIDATION;
                break;

            default:
                return AMS_ERROR_INVALID_DATA_TYPE;
            }
        }
#endif

        if(fld->len == -1)
            fld->len = len;
        fld->status = 1; /* Field has changed */

    }

    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Set_Starting_Index_Field - define the index where this processor's
                               fields start
@*/

int AMSP_Set_Starting_Index_Field(Field *fld, int index)
{
    fld->dim = 1;
    fld->start = (int *) realloc(fld->start, sizeof(int));
    fld->end = (int *) realloc(fld->end, sizeof(int));
    if(fld->start == NULL || fld->end == NULL)
       return AMS_ERROR_INSUFFICIENT_MEMORY;

    *(fld->start) = index;
    *(fld->end) = index + fld->len -1;
    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Set_Permutation_Field - set a permutation that takes this processors
                            elements to particular indices in a global array
@*/

int AMSP_Set_Permutation_Field(Field *fld, int *perm)
{
    int k;

    fld->dim = 0;  /* dim 0 indicates permutation */
    fld->start = (int *) realloc(fld->start, sizeof(int));
    fld->end = (int *) realloc(fld->end, fld->len * sizeof(int));
    if(fld->start == NULL || fld->end == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    fld->start[0] = 0;
    /* store the permutation in fld->end */
    for(k = 0; k < fld->len; ++k)
        fld->end[k] = perm[k];

    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Set_Block_Field - for a multidimensional array, define the indices in
                      each dimension where this processor's fields start
                      and end
@*/

int AMSP_Set_Block_Field(Field *fld, int dim, int *start, int *end)
{
    int k, len = 1;

    for(k = 0; k < dim; ++k)
        len = len * (end[k] - start[k] + 1); /* Add +1 so that the user specifies 0, n-1 as indices */
    if(len != abs(fld->len))
        return AMS_ERROR_BAD_DIMENSIONS;

    fld->dim = dim;
    fld->start = (int *) realloc(fld->start, dim * sizeof(int));
    fld->end = (int *) realloc(fld->end, dim * sizeof(int));
    if(fld->start == NULL || fld->end == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    for(k = 0; k < dim; ++k) {
        fld->start[k] = start[k];
        fld->end[k] = end[k];
    }

    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Get_Block_Field - for a multidimensional array, gets the indices in
                      each dimension for the whole fields start
                      and end
@*/

int AMSP_Get_Block_Field(Field *fld, int *dim, int **start, int **end)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    *dim = fld->dim;
    *start = fld->start;
    *end = fld->end;

    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Expand_Field - Expand the starting index and/or length of a field
                   so that it includes the range [start, end - 1]
@*/

int AMSP_Expand_Field(Field *fld, int *start, int *end)
{
    int err, newlen = 1, k;

    for(k = 0; k < fld->dim; ++k) {
        /* Move back starting position if necessary */
        if(start[k] < fld->start[k])
            (fld->start)[k] = start[k];
        /* Move forward ending position if necessary */
        if(end[k] > fld->end[k])
            (fld->end)[k] = end[k];

        newlen *= (fld->end[k] - fld->start[k] + 1);
    }

    if(newlen != abs(fld->len)) {
        err = AMSP_Alloc_Memory_Field(fld, -1 * newlen);
        CheckErr(err);
    }

    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Expand_Field_Perm - Expand the length of a field
                        to include all indices referenced by a permutation
@*/

int AMSP_Expand_Field_Perm(Field *fld, int perm_len, int *perm)
{
    int err, k, newlen;

    newlen = abs(fld->len);

    fld->start[0] = 0;
    for(k = 0; k < perm_len; ++k) {
        if(perm[k] < 0)
            return AMS_ERROR_BAD_DIMENSIONS;
        if(perm[k] >= newlen)
            newlen = perm[k] + 1;
    }

    if(newlen != abs(fld->len)) {
        err = AMSP_Alloc_Memory_Field(fld, -1 * newlen);
        CheckErr(err);
    }

    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Alloc_Memory_Field(Field *, int) - change the length of the field,
                                       and make buf be the appropriate size
@*/

int AMSP_Alloc_Memory_Field(Field *fld, int len)
{
    fld->len = len;

    if(len > 0) {
        switch (fld->dtype) {
        case AMS_CHAR:
            fld->data = realloc(fld->data, len*sizeof(char));
            break;
        case AMS_INT:
        case AMS_BOOLEAN:
            fld->data = realloc(fld->data, len*sizeof(int));
            break;
        case AMS_FLOAT:
            fld->data = realloc(fld->data, len*sizeof(float));
            break;
        case AMS_DOUBLE:
            fld->data = realloc(fld->data, len*sizeof(double));
            break;
        case AMS_STRING:
            fld->data = realloc(fld->data, len*sizeof(char *));
            break;
        default:
            return AMS_ERROR_INVALID_DATA_TYPE;
        }
    }

    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Get_Length_Field - get the length of the field
@*/

int AMSP_Get_Length_Field(Field *fld, int *answer)
{
    *answer = fld->len;
    return AMS_ERROR_NONE;
}


/*@
        AMSP_Get_Status_Field - Gets field's modification status

        Input Parameters:
			fld - Field object

        Output Parameter:
			status - modification status (1 = changed, 0 = unchanged)


@*/
int AMSP_Get_Status_Field(Field *fld, int *status)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    *status = fld->status;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_Status_Field - Sets the field status

        Input Parameters:
+			fld - Field object
+			status - modification status (1 = changed, 0 = unchanged)

@*/
int AMSP_Set_Status_Field(Field *fld, int status)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    if (status == 0 || status == 1)
        fld->status = status;
    else
        return AMS_ERROR_BAD_ARGUMENT;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Get_Name_Field - Gets field's name

        Input Parameters:
			fld - Field object

        Output Parameter:
			name - address of the field

@*/
char * AMSP_Get_Name_Field(Field *fld, int *err)
{
    *err = Validate(AMSP_Valid_Field, fld);
    CheckErr1(*err);

    *err = AMS_ERROR_NONE;
    /* Return a pointer to the name */
    return fld->name;
}

/*@
        AMSP_Set_Name_Field - Sets the field's name

        Input Parameters:
+			fld - Field object
+			name - name of the field


@*/
int AMSP_Set_Name_Field(Field *fld, const char *name)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    if (name == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    /* Can't allocate more than MAXNAME characters for a name */
    fld->name = (char *) malloc(min(strlen(name)+1, MAXNAME+1)*sizeof(char));
    if (fld->name == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    strcpy(fld->name, name);

    return AMS_ERROR_NONE;
}


/*@
	AMSP_Get_Memory_type_Field - Gets field's memory type

        Input Parameters:
+			fld - Field object

        Output Parameter:
+			mtype - Field's memory type


@*/
int AMSP_Get_Memory_type_Field(Field *fld, AMS_Memory_type *mtype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    *mtype = fld->mtype;

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Set_Memory_type_Field - Sets the field's memory type

        Input Parameters:
+			fld - field
+			mtype - Field's memory type


@*/
int AMSP_Set_Memory_type_Field(Field *fld, AMS_Memory_type mtype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    err = Validate(AMSP_Valid_Memory_type, mtype);
    CheckErr(err);

    fld->mtype = mtype;
        
    return AMS_ERROR_NONE;
}

/*@
        AMSP_Get_Data_type_Field - Gets field's data type

        Input Parameters:
+			fld - Field object

        Output Parameter:
+			dtype - Field's data type


@*/
int AMSP_Get_Data_type_Field(Field *fld, AMS_Data_type *dtype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    *dtype = fld->dtype;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_Data_type_Field - Sets the field's data type

        Input Parameters:
+			fld - field
+			dtype - Field's data type

@*/
int AMSP_Set_Data_type_Field(Field *fld, AMS_Data_type dtype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    err = Validate(AMSP_Valid_Data_type, dtype);
    CheckErr(err);

    fld->dtype = dtype;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Get_Shared_type_Field - Gets field's shared type

        Input Parameters:
+			fld - Field object

        Output Parameter:
+			stype - Field's shared type

@*/
int AMSP_Get_Shared_type_Field(Field *fld, AMS_Shared_type *stype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    *stype = fld->stype;
        
    return AMS_ERROR_NONE;
}

/*@
        Set_Shared_type_Field - Sets the field shared type

        Input Parameters:
+			fld - field
+			stype - Field's shared type

@*/
int AMSP_Set_Shared_type_Field(Field *fld, AMS_Shared_type stype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    err = Validate(AMSP_Valid_Shared_type, stype);
    CheckErr(err);

    fld->stype = stype;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Get_Reduction_type_Field - Gets field's reduction type

        Input Parameters:
+			fld - Field object

        Output Parameter:
+			stype - Field's shared type
@*/
int AMSP_Get_Reduction_type_Field(Field *fld, AMS_Reduction_type *rtype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    *rtype = fld->rtype;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_Reduction_type_Field - Sets the field reduction type

        Input Parameters:
+			fld - field
+			rtype - Field's reduction type


@*/
int AMSP_Set_Reduction_type_Field(Field *fld, AMS_Reduction_type rtype)
{
    int err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    err = Validate(AMSP_Valid_Reduction_type, rtype);
    CheckErr(err);

    fld->rtype = rtype;

    return AMS_ERROR_NONE;
}


#ifdef _DEBUG

/*@
        AMSP_Valid_Language_type - Checks the validity of a language type

        Input Parameters:
+			lang - Language to be validated

@*/

int AMSP_Valid_Language_type(AMS_Language_type lang)
{
    switch (lang) {
    case AMS_C:
    case AMS_CPP:
    case AMS_JAVA:
        break;
    default:
        return AMS_ERROR_INVALID_LANGUAGE_TYPE;
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Valid_Memory_type - Checks the validity of a memory type

        Input Parameters:
+			mtype - Memory Type to be validated

@*/
int AMSP_Valid_Memory_type(AMS_Memory_type mtype)
{

    switch (mtype) {
    case AMS_MEMORY_UNDEF:
    case AMS_READ:
    case AMS_WRITE:
        break;
    default:
        return  AMS_ERROR_INVALID_MEMORY_TYPE;
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Valid_Shared_type - Checks the validity of a shared type

        Input Parameters:
+			stype - Shared Type to be validated

@*/
int AMSP_Valid_Shared_type(AMS_Shared_type stype)
{

    switch (stype) {
    case AMS_SHARED_UNDEF:
    case AMS_COMMON:
    case AMS_REDUCED:
    case AMS_DISTRIBUTED:
        break;
    default:
        return  AMS_ERROR_INVALID_SHARED_TYPE;
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Valid_Reduction_type - Checks the validity of a reduction type

        Input Parameters:
+			rtype - Reduction type to be validated

@*/
int AMSP_Valid_Reduction_type(AMS_Reduction_type rtype)
{
    switch (rtype) {
    case AMS_REDUCT_UNDEF:
    case AMS_SUM:
    case AMS_MAX:
    case AMS_MIN:
        break;
    default:
        return  AMS_ERROR_INVALID_REDUCTION_TYPE;
    }

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Valid_Data_type - Checks the validity of a data type

        Input Parameters:
+			dtype - Data type to be validated

@*/
int AMSP_Valid_Data_type(AMS_Data_type dtype)
{
    switch (dtype) {
    case AMS_DATA_UNDEF:
    case AMS_CHAR:
    case AMS_INT:
    case AMS_BOOLEAN:
    case AMS_FLOAT:
    case AMS_DOUBLE:
    case AMS_STRING:
        break;
    default:
        return  AMS_ERROR_INVALID_DATA_TYPE;
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Valid_Field - Checks the validity of a field

        Input Parameters:
+			fld - Field to be validated

@*/
int AMSP_Valid_Field(Field *fld)
{
    int err;

    if (fld == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (fld->status != 0 && fld->status != 1)
        return AMS_ERROR_BAD_FIELD_STATUS;

    if(fld->stype == AMS_REDUCED && fld->dtype == AMS_STRING)
        return AMS_ERROR_INVALID_SHARED_TYPE;

    if(fld->stype == AMS_REDUCED && fld->mtype != AMS_READ)
        return AMS_ERROR_INVALID_MEMORY_TYPE;

    if ((err = AMSP_Valid_Language_type(fld->lang)) != AMS_ERROR_NONE)
        return err;

    if ((err = AMSP_Valid_Memory_type(fld->mtype)) != AMS_ERROR_NONE)
        return err;

    if ((err = AMSP_Valid_Shared_type(fld->stype)) != AMS_ERROR_NONE)
        return err;

    if ((err = AMSP_Valid_Reduction_type(fld->rtype)) != AMS_ERROR_NONE)
        return err;

    if ((err = AMSP_Valid_Data_type(fld->dtype)) != AMS_ERROR_NONE)
        return err;

    return AMS_ERROR_NONE;
}

#endif
