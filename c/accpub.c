#include <stdarg.h>
#include <stdio.h>

#include "ams.h"
#include "netlib.h"
#include "acomm.h"
#include "amsdefs.h"
#include "sendfun.h"
#include "hostiter.h"

/*
 * This file contains functions high-level function that are used
 * by both the Accessor and the Publisher. Most of this functions
 * are used to change the default behavior the AMS API.
 */

char ams_out_file[255] = {0}; /* AMS output file */
char ams_msg[1024] = {0};     /* AMS low-level error message */

/*@
    AMS_Set_output_file - Sets the AMS output file. By default 
    information and error messages are sent to the stderr

    Input Parameters:
+   outfile - Filename
@*/
int AMS_Set_output_file(const char *outfile)
{
    if (outfile == NULL)
        strcpy(ams_out_file, "");
    else
        strcpy(ams_out_file, outfile);

    return AMS_ERROR_NONE;
}

/*@
    AMS_Set_abort_func - Sets the AMS abort function. By default 
    the C function abort is called. The user can alter this by 
    providing a pointer to a function that has the same prototype 
    as abort: void func(void).

    Input Parameters:
+   abort_func - A pointer to the user abort function
@*/
int AMS_Set_abort_func(void (* abort_func)())
{
    AMSP_err_init_abort(abort_func);
    return AMS_ERROR_NONE;
}

/*@
    AMS_Set_exit_func - Sets the AMS exit function. By default 
    the C function exit is called. The user can alter this by 
    providing a pointer to a function that has the same prototype 
    as exit: void func(int).

    Input Parameters:
+   exit_func - A pointer to the user exit function
@*/
int AMS_Set_exit_func(void (* exit_func)(int))
{
    AMSP_err_init_exit(exit_func);
    return AMS_ERROR_NONE;
}

/*@
    AMS_Set_print_func - Sets the AMS print function. By default 
    the C function printf is called. The user can alter this by 
    providing a pointer to a function that has the same prototype 
    as printf: int func(const char *str, ...)..

    Input Parameters:
+   print_func - A pointer to the user print function
@*/
int AMS_Set_print_func(int (* print_func)(const char *str, ...))
{
    AMSP_err_init_print(print_func);
    return AMS_ERROR_NONE;
}

/*@
        AMS_Explain_error - Returns an explanation of an AMS error code. Note 
        this function is not thread safe.

  Input Parameters:
+       err - Received AMS error code

  Output
+       msg - Error message
@*/
int AMS_Explain_error(int err, char **msg)
{

    static char buf[1024];
        
    switch(err) {
    case AMS_ERROR_NONE:
        strcpy(buf, "No error encoutered");
        break;
    case AMS_ERROR_INVALID_COMMUNICATOR:
        strcpy(buf, "Invalid AMS Communicator");
        break;
    case AMS_ERROR_INVALID_MEMORY:
        strcpy(buf, "Invalid Memory");
        break;
    case AMS_ERROR_INVALID_MEMORY_TYPE:
        strcpy(buf, "Invalid Memory Type");
        break;
    case AMS_ERROR_INVALID_DATA_TYPE:
        strcpy(buf, "Invalid Data Type");
        break;
    case AMS_ERROR_INVALID_SHARED_TYPE:
        strcpy(buf, "Invalid Shared Type");
        break;
    case AMS_ERROR_INVALID_REDUCTION_TYPE:
        strcpy(buf, "Invalid Reduction Type");
        break;
    case AMS_ERROR_INVALID_LANGUAGE_TYPE:
        strcpy(buf, "Invalid Language Type");
        break;
    case AMS_ERROR_PUBLISHER_NO_LONGER_ALIVE:
        strcpy(buf, "Publisher No Longer Alive");
        break;
    case AMS_ERROR_COMMUNICATOR_NO_LONGER_ALIVE:
        strcpy(buf, "AMS Communicator No Longer Alive");
        break;
    case AMS_ERROR_BAD_ARGUMENT:
        strcpy(buf, "Function Called With An Invalid Argument");
        break;
    case AMS_ERROR_INSUFFICIENT_MEMORY:
        strcpy(buf, "Insufficient Memory");
        break;
    case AMS_ERROR_FIELD_ALREADY_IN_MEMORY:
        strcpy(buf, "Field Already In Memory");
        break;
    case AMS_ERROR_FIELD_NOT_IN_MEMORY:
        strcpy(buf, "Field Not Found In Memory");
        break;
    case AMS_ERROR_VALIDATION:
        strcpy(buf, "Value Failed Validation Function");
        break;
    case AMS_ERROR_NET_PROTOCOL:
        strcpy(buf, "Incompatible network packet formats spoken.  Upgrade AMS version.");
        break;
    case AMS_ERROR_HOST_NODE_NOT_IN_COMMUNICATOR:
        strcpy(buf, "Host Node Not In AMS Communicator");
        break;
    case AMS_ERROR_SYSTEM:
        strcpy(buf, "Inconsistant calling sequence or systems error");
        break;
    case AMS_ERROR_BAD_PORT_NUMBER:
        strcpy(buf, "Bad Port Number for Communication");
        break;
    case AMS_ERROR_NODE_ALREADY_IN_COMM:
        strcpy(buf, "Node Already in AMS Communicator");
        break;
    case AMS_ERROR_FUNCT_NOTIMPLEMENTED:
        strcpy(buf, "Function not implemented yet. Please report this problem");
        break;
    case AMS_ERROR_COMM_TBL_FULL:
        strcpy(buf, "Exceeded The Maximum Number of AMS Communicators.");
        break;
    case AMS_ERROR_MEMORY_TBL_FULL:
        strcpy(buf, "Exceeded The Maximum Number of Memory Structure. Please Report");
        break;
    case AMS_ERROR_PUB_TBL_FULL:
        strcpy(buf, "Exceeded The Maximum Nubmer Publishers.");
        break;
    case AMS_ERROR_BAD_MEMID_NUMBER:
        strcpy(buf, "Invalid Memory ID");
        break;
    case AMS_PUBLISHED_TBL_FULL:
        strcpy(buf, "Exceeded The Maximum Number of Publishers");
        break;
    case AMS_ERROR_NET_OPEN:
        strcpy(buf, "Unable To Connect To The Publisher");
        break;
    case AMS_ERROR_BAD_FIELD_LENGTH:
        strcpy(buf, "Invalid Field's Length");
        break;
    case AMS_ERROR_BAD_FIELD_STATUS :
        strcpy(buf, "Invalid Field's Status");
        break;
    case AMS_ERROR_BAD_DIMENSIONS:
        strcpy(buf, "Product of field dimensions disagrees with field length");
        break;
    case AMS_ERROR_MEMORY_NOT_PUBLISHED:
        strcpy(buf, "Memory Not Published");
        break;
    case AMS_ERROR_MEMORY_NAME_NOT_FOUND:
        strcpy(buf, "Memory Name Not Found");
        break;
    case AMS_ERROR_COMM_NOT_PUBLISHED:
        strcpy(buf, "AMS Communicator Not Published");
        break;
    case AMS_ERROR_COMM_NAME_NOT_FOUND:
        strcpy(buf, "AMS Communicator Name Not Found");
        break;
    case AMS_ERROR_PUBID_NOT_FOUND:
        strcpy(buf, "Can't find a Publisher id for the given AMS Communicator");
        break;
    case AMS_ERROR_INITIALIZE_NET_LIB:
        strcpy(buf, "Unable to Initialize The AMS Communication Libray");
        break;
    case AMS_ERROR_TERMINATE_NET_LIB:
        strcpy(buf, "Unable to Unload The AMS Communication Library");
        break;
    case AMS_ERROR_MEMORY_NODE_NOT_IN_COMMUNICATOR:
        strcpy(buf, "Memory Node not in publisher");
        break;
    case AMS_ERROR_INVALID_USER_ARGUMENT:
        strcpy(buf, "Invalid User Parameter");
        break;
    case AMS_ERROR_MEMORY_NO_LONGER_PUBLISHED:
        strcpy(buf, "Memory no longer published");
        break;
    case AMS_ERROR_FIELD_READ_ONLY:
        strcpy(buf, "Can't modify field; Field is Read Only");
        break;
    case AMS_ERROR_CANNOT_MODIFY_ENV:
        strcpy(buf, "Can't Modify Environment Variable");
        break;
    case AMS_ERROR_MPI_TYPE_NOT_SUPPORTED:
        strcpy(buf, "The MPI Communicator Type Is Not Yet Supportrd");
        break;
    case AMS_ERROR_INVALID_COMM_TYPE:
        strcpy(buf, "Invalid AMS Communicator Type");
        break;
    case AMS_ERROR_NOT_CONNECTED:
        strcpy(buf, "Not Connected! Call AMS_Connect or \n Define Env Variables AMS_PUBLISHER and AMS_PORT");
		break;
	case AMS_ERROR_CONNECT_REFUSED:
		strcpy(buf, "Connection refused by the publisher. Make sure the publisher is running");				
		break;
	case AMS_ERROR_CONNECT_TERMINATED:
		strcpy(buf, "Connections has been abnormally reset/terminated by the other side");				
		break;
    case AMS_ERROR_JAVA_JNI_GET_MID:
        strcpy(buf, "JAVA JNI Error: Can't get java method id");
        break;
    case AMS_ERROR_JAVA_SYSTEM:
        strcpy(buf, "JAVA JNI Error: Can't create or call java object from C");
        break;
    case AMS_ERROR_LDAP_INIT:
        strcpy(buf, "LDAP initialization failed");
		break;
    case AMS_ERROR_LDAP_BIND:
        strcpy(buf, "LDAP binding failed");
		break;
    case AMS_ERROR_LDAP_UNBIND:
        strcpy(buf, "LDAP unbinding failed");
		break;
    case AMS_ERROR_LDAP_DELETE:
        strcpy(buf, "LDAP delete failed");
        break;
    case AMS_ERROR_LDAP_SYSTEM:
        strcpy(buf, "LDAP systems error");
        break;
    case AMS_ERROR_COMM_PROTOCOL:
        strcpy(buf, "AMS Wrong protocol used. You need to recompile both the accessor and publisher with the same version");
        break;
    default:
        sprintf(buf, "Undocumented Error: %d", err);
        break;
    }

    /* 007: Need to synchronize this if called from publisher */
    if (err != AMS_ERROR_NONE) {
        strcat(buf, "\n");
        strcat(buf, ams_msg);
		ams_msg[0] = 0; /* reset ams messages */
    }

    /* Send back the results */
    *msg = buf;

    return AMS_ERROR_NONE;
}

/*@
    AMS_Print - Directs output to the AMS output file. By default the 
    output is directed to the standard error. If a print function is defined
    using AMS_Set_print_func, that function will be used instead.
    This function has the same interface as the C function printf.

    Input Parameters:
+   str - output format
-   ... - additional arguments 

    See Also: AMS_Set_output_file(), AMS_Set_print_func()

@*/
int AMS_Print(const char *str, ...)
{
    char buff[512];
    FILE *fd = stderr;
    va_list args;
    va_start(args, str);

    vsprintf(buff, str, args);

    if (amsp_print_func == 0) {
        /* No external function, just print using printf */
    
        if (strlen(ams_out_file))
            fd = fopen(ams_out_file, "a+");

        if (fd == NULL) {
            perror("System error:");
            fprintf(stderr, "\n Can't open output file: %s\n", ams_out_file);
        }

        /* Print the error message */
        fprintf(fd, "%s", buff);
        if (strlen(ams_out_file))
            fclose(fd);

    } else {
        /* Use user provided function */
        func_print("%s", buff);
    }

    va_end(args);

    return AMS_ERROR_NONE;
}

/*@

        AMS_Memory_unlock - Unlock the the publisher's memory so that the application main 
		thread can proceed.

	Input Parameters:
+       mem - the AMS memory object


	See Also: 
		AMS_Memory_lock()

@*/

int AMS_Memory_unlock(AMS_Memory mem)
{
    int err, err1, connected;
    int *socket_list, num_hosts, i, nbmem = 1; /* Can lock only one memory at this time */

    AMS_Comm ams;

#ifdef AMS_ACCESSOR
    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);
#endif
    CheckMemory(mem);

    /* What is our communicator? */
    err = AMSP_Comm_AMS_Memory(mem, &ams);
    CheckErr(err);

    connected = AMSP_Comm_Hosts_open_all(ams, &num_hosts, &socket_list);
    if(connected == AMS_ERROR_NONE && num_hosts > 0) {
        /* First send all the requests, then wait for all the answers */
        /* Host 0 starts the action, so contact it only when everyone */
        /* else is ready */
        for(i = 1; i < num_hosts; ++i) {
            err = AMSP_send_rq(OP_UNLOCK_MEM, socket_list[i], &mem, nbmem);
			CheckErr(err);
		}

        err = AMSP_send_rq(OP_UNLOCK_MEM, socket_list[0], &mem, nbmem);
		CheckErr(err);

        for(i = 0; i < num_hosts && err == AMS_ERROR_NONE; ++i) {
            err1 = AMSP_fsm_loop(OP_UNLOCK_MEM, socket_list[i], &err);
			CheckErr(err1);
		}
    }

    AMSP_Comm_Hosts_close_all(num_hosts, socket_list);
    if(connected != AMS_ERROR_NONE)
        return AMS_ERROR_NET_OPEN;

    /* Check errors from fsm loops */
    CheckErr(err);

    return AMS_ERROR_NONE;
        
}

/*@

        AMS_Memory_lock - Lock the publisher's memory so that the application main
		thread would block the next time it tries to access the memory. The block
        will end after timeout or is the AMS_Memory_unlock is called.

	Input Parameters:
+       mem - the AMS memory object
+       timeout - maximum time in milliseconds to block for

  	See Also: 
		AMS_Memory_unlock()

@*/

int AMS_Memory_lock(AMS_Memory mem, int timeout)
{
    int err, err1, connected;
    int *socket_list, num_hosts, i, nbmem = 1; /* Can lock only one memory at this time */

    RequestID req;
        
    AMS_Comm ams;

#ifdef AMS_ACCESSOR
    /* Get the current publisher and unmask the memory id. The order is important! */
    pub_id = getPubId(mem);
    mem = unmaskId(mem);
#endif
    CheckMemory(mem);

    /* What is our communicator? */
    err = AMSP_Comm_AMS_Memory(mem, &ams);
    CheckErr(err);

    err = AMSP_Generate_RequestID(&req);
    CheckErr(err);

    connected = AMSP_Comm_Hosts_open_all(ams, &num_hosts, &socket_list);
    if(connected == AMS_ERROR_NONE && num_hosts > 0) {
        /* First send all the requests, then wait for all the answers */
        /* Host 0 starts the action, so contact it only when everyone */
        /* else is ready */
        for(i = 1; i < num_hosts; ++i) {
            err = AMSP_send_rq(OP_LOCK_MEM, socket_list[i],&mem, nbmem, timeout, req);
			CheckErr(err);
		}

        err = AMSP_send_rq(OP_LOCK_MEM, socket_list[0], &mem, nbmem, timeout, req);
		CheckErr(err);

        for(i = 0; i < num_hosts && err == AMS_ERROR_NONE; ++i) {
            err1 = AMSP_fsm_loop(OP_LOCK_MEM, socket_list[i], &err);
			CheckErr(err1);
		}
    }

    AMSP_Comm_Hosts_close_all(num_hosts, socket_list);
    if(connected != AMS_ERROR_NONE)
        return AMS_ERROR_NET_OPEN;

    /* Check errors from fsm loops */
    CheckErr(err);

    err = AMSP_Delete_RequestID(req);
    CheckErr(err);
        
    return AMS_ERROR_NONE;
}

