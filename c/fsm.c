/*
 * Finite state machine routines.
 */


#include        <signal.h>
#include        <stdarg.h>

#include "amsdefs.h"
#include "netlib.h"
#include "convert.h"
#include "netapi.h"
#include "ams.h"

RECVFUN AMSP_recv_rqerr, AMSP_recv_rslt, AMSP_recv_rq, AMSP_recv_shutdown;
RECVFUN AMSP_fsm_error , AMSP_fsm_invalid, *AMSP_fsm_ptr [ OP_MAX + 1 ] [ OP_MAX + 1 ];

/*
 * Finite state machine table.
 * This is just a 2-d array indexed by the last opcode sent and
 * the opcode just received.  The result is the address of a
 * function to call to process the received opcode.
 */

void AMSP_init_fsm()
{
    /*
     * Protocol operations.
     * This is just a 2-d array indexed by the last opcode sent and
     * the opcode just received.  The result is the address of a
     * function to call to process the received opcode.
     * Add entries to this table whenever new operations are added to 
     * the protocol. The best way to figure out this is to look for a 
     * similar opearation and duplicate the initialization code.
     */

    int i, j;
    static int firstime = 1;

    if (firstime) {
        firstime = 0;
        for (i = 0; i <= OP_MAX; i++)
            for (j = 0; j <= OP_MAX; j++)
                AMSP_fsm_ptr[i][j] = AMSP_fsm_invalid;

        /* Init OP_CODES Descriptons */
        str_code[OP_ATTACH_COMM]        = " Attach a communicator ";
        str_code[OP_MEM_LIST]           = " Get the memory list ";
        str_code[OP_ATTACH_MEM]         = " Attache a memory ";
        str_code[OP_MEM_FLDS]           = " Get memory's fields ";
        str_code[OP_SEND_UPD_MEM]       = " Send an update of the memory ";
        str_code[OP_RECV_UPD_MEM]       = " Receive an update of the memory ";
        str_code[OP_DETACH_COMM]        = " Detach a commnunicator ";
        str_code[OP_RSLT]               = " Publisher sending results to accessor ";
        str_code[OP_ERROR]              = " Publisher sending an error message to accessor ";
        str_code[OP_SHUTDOWN]           = " Publisher is being shutdown ";
        str_code[OP_COMM_LIST]          = " Get communicators' list";
        str_code[OP_COMM_PORT]          = " Get communicator's port";
        str_code[OP_NEGOTIATE_STEP]     = " Decide stopping point";
        str_code[OP_NOTIFY_STEP]        = " Announce stopping point";
        str_code[OP_LOCK_MEM]           = " Lock a remote memory ";
        str_code[OP_UNLOCK_MEM]         = " Unlock a remote memory ";

        /*
         * Accessor Side of the Work 
         */

        /* sent = OP_ATTACH_COMM , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_ATTACH_COMM][ OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_ATTACH_COMM , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_ATTACH_COMM][ OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_ATTACH_COMM , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_ATTACH_COMM][ OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_MEM_LIST , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_MEM_LIST][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_MEM_LIST , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_MEM_LIST][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_MEM_LIST , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_MEM_LIST][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_COMM_LIST , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_COMM_LIST][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_COMM_LIST , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_COMM_LIST][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_COMM_LIST , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_COMM_LIST][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_ATTACH_MEM , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_ATTACH_MEM][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_ATTACH_MEM , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_ATTACH_MEM][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_ATTACH_MEM , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_ATTACH_MEM][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_MEM_FLDS , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_MEM_FLDS][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_MEM_FLDS , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_MEM_FLDS][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_MEM_FLDS , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_MEM_FLDS][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_SEND_UPD_MEM , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_SEND_UPD_MEM][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_SEND_UPD_MEM , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_SEND_UPD_MEM][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_SEND_UPD_MEM , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_SEND_UPD_MEM][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_RECV_UPD_MEM , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_RECV_UPD_MEM][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_RECV_UPD_MEM , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_RECV_UPD_MEM][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_RECV_UPD_MEM , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_RECV_UPD_MEM][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_DETACH_COMM , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_DETACH_COMM][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_DETACH_COMM , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_DETACH_COMM][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_DETACH_COMM , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_DETACH_COMM][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_COMM_PORT , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_COMM_PORT][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_COMM_PORT , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_COMM_PORT][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_COMM_PORT , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_COMM_PORT][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_NEGOTIATE_STEP , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_NEGOTIATE_STEP][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_NEGOTIATE_STEP , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_NEGOTIATE_STEP][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_NEGOTIATE_STEP , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_NEGOTIATE_STEP][OP_SHUTDOWN] = AMSP_recv_shutdown;
                
        /* sent = OP_NOTIFY_STEP , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_NOTIFY_STEP][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_NOTIFY_STEP , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_NOTIFY_STEP][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_NOTIFY_STEP , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_NOTIFY_STEP][OP_SHUTDOWN] = AMSP_recv_shutdown;

        /* sent = OP_LOCK_MEM , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_LOCK_MEM][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_LOCK_MEM , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_LOCK_MEM][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_LOCK_MEM , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_LOCK_MEM][OP_SHUTDOWN] = AMSP_recv_shutdown;
        
        /* sent = OP_UNLOCK_MEM , recv = OP_RSLT */
        AMSP_fsm_ptr[OP_UNLOCK_MEM][OP_RSLT] = AMSP_recv_rslt;

        /* sent = OP_UNLOCK_MEM , recv = OP_ERROR */
        AMSP_fsm_ptr[OP_UNLOCK_MEM][OP_ERROR] = AMSP_recv_rqerr;

        /* sent = OP_UNLOCK_MEM , recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_UNLOCK_MEM][OP_SHUTDOWN] = AMSP_recv_shutdown;  

        /* 
         * Publisher side of the work
         */

        /* sent = 0, recv = OP_ATTACH_COMM */
        AMSP_fsm_ptr[0][ OP_ATTACH_COMM] = AMSP_recv_rq;

        /* sent = 0, recv = OP_MEM_LIST */
        AMSP_fsm_ptr[0][ OP_MEM_LIST] = AMSP_recv_rq;

        /* sent = 0, recv = OP_COMM_LIST */
        AMSP_fsm_ptr[0][ OP_COMM_LIST] = AMSP_recv_rq;

        /* sent = 0, recv = OP_COMM_PORT */
        AMSP_fsm_ptr[0][ OP_COMM_PORT] = AMSP_recv_rq;

        /* sent = 0, recv = OP_ATTACH_MEM */
        AMSP_fsm_ptr[0][ OP_ATTACH_MEM] = AMSP_recv_rq;

        /* sent = 0, recv = OP_MEM_FLDS */
        AMSP_fsm_ptr[0][ OP_MEM_FLDS] = AMSP_recv_rq;

        /* sent = 0, recv = OP_SEND_UPD_MEM */
        AMSP_fsm_ptr[0][ OP_SEND_UPD_MEM] = AMSP_recv_rq;

        /* sent = 0, recv = OP_RECV_UPD_MEM */
        AMSP_fsm_ptr[0][ OP_RECV_UPD_MEM] = AMSP_recv_rq;

        /* sent = 0, recv = OP_DETACH_COMM */
        AMSP_fsm_ptr[0][ OP_DETACH_COMM] = AMSP_recv_rq;

        /* sent = 0, recv = OP_NEGOTIATE_STEP */
        AMSP_fsm_ptr[0][ OP_NEGOTIATE_STEP] = AMSP_recv_rq;

        /* sent = 0, recv = OP_NOTIFY_STEP */
        AMSP_fsm_ptr[0][ OP_NOTIFY_STEP ] = AMSP_recv_rq;

        /* sent = 0, recv = OP_LOCK_MEM */
        AMSP_fsm_ptr[0][ OP_LOCK_MEM ] = AMSP_recv_rq;

        /* sent = 0, recv = OP_UNLOCK_MEM */
        AMSP_fsm_ptr[0][ OP_UNLOCK_MEM ] = AMSP_recv_rq;

        /* sent = 0, recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[0][ OP_SHUTDOWN] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_ATTACH_COMM */
        AMSP_fsm_ptr[OP_RSLT][ OP_ATTACH_COMM] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_MEM_LIST */
        AMSP_fsm_ptr[OP_RSLT][ OP_MEM_LIST] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_COMM_LIST */
        AMSP_fsm_ptr[OP_RSLT][ OP_COMM_LIST] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_ATTACH_MEM */
        AMSP_fsm_ptr[OP_RSLT][ OP_ATTACH_MEM] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_MEM_FLDS */
        AMSP_fsm_ptr[OP_RSLT][ OP_MEM_FLDS] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_SEND_UPD_MEM */
        AMSP_fsm_ptr[OP_RSLT][ OP_SEND_UPD_MEM] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_RECV_UPD_MEM */
        AMSP_fsm_ptr[OP_RSLT][ OP_RECV_UPD_MEM] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_DETACH_COMM */
        AMSP_fsm_ptr[OP_RSLT][ OP_DETACH_COMM] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_RSLT][ OP_SHUTDOWN] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_COMM_PORT */
        AMSP_fsm_ptr[OP_RSLT][ OP_COMM_PORT] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_NEGOTIATE_STEP */
        AMSP_fsm_ptr[OP_RSLT][ OP_NEGOTIATE_STEP] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_NOTIFY_STEP */
        AMSP_fsm_ptr[OP_RSLT][ OP_NOTIFY_STEP] = AMSP_recv_rq;

       /* sent = OP_RSLT, recv = OP_LOCK_MEM */
        AMSP_fsm_ptr[OP_RSLT][ OP_LOCK_MEM] = AMSP_recv_rq;

        /* sent = OP_RSLT, recv = OP_UNLOCK_MEM */
        AMSP_fsm_ptr[OP_RSLT][ OP_UNLOCK_MEM] = AMSP_recv_rq;

        /* sent = OP_ERROR, recv = OP_ERROR */
        AMSP_fsm_ptr[OP_ERROR][ OP_ERROR] = AMSP_fsm_error;

        /* sent = OP_SHUTDOWN, recv = OP_SHUTDOWN */
        AMSP_fsm_ptr[OP_SHUTDOWN][ OP_SHUTDOWN] = AMSP_recv_shutdown;

    }

}


/*
 * Main loop of finite state machine.
 */

/* return 0 on normal termination, -1 on timeout */
int AMSP_fsm_loop(int op_sent, int sfd, ...)
{
    va_list args;
    static int firstime = 1;
    int  err;
        
    char *recvbuff = NULL;
    int nbytes, op_recv, ver_sent;
        
    for ( ; ; ) {
        err = AMSP_mem_net_recv(&recvbuff, &nbytes, sfd);
		if (err == AMS_ERROR_CONNECT_TERMINATED)
			return 0; /* Connection terminated by the other side */

		if (err != 0)
            AMSP_err_quit("AMSP_mem_net_recv error");

        if (nbytes < 2)
            AMSP_err_dump("receive length = %d bytes", nbytes);

        ver_sent = AMSP_ldlong(recvbuff);
        nbytes = nbytes - INT_SIZE; /* nbytes included ver_sent until this statement */

        op_recv = AMSP_ldlong(recvbuff + INT_SIZE);

        if (op_recv < OP_MIN || op_recv > OP_MAX)
            AMSP_err_dump("invalid opcode received: %d", op_recv);

        /*
         * We call the appropriate function, passing the address
         * of the receive buffer and its length.
         *
         * We assume the called function will send a response to the
         * other side.  It is the called function's responsibility to
         * set op_sent to the op-code that it sends to the other side.
         */

        if (firstime) {
            firstime = 0;
            AMSP_init_fsm();
        }

        va_start(args, sfd);

        if (op_sent == -1) {
            op_sent = 0;
            op_recv = OP_SHUTDOWN; /* Publisher being shutdown */
        }

		err = (*AMSP_fsm_ptr[op_sent][op_recv])(recvbuff + 2*INT_SIZE, (nbytes -sizeof(op_recv)), sfd, ver_sent, op_sent, op_recv, args);
        if (err >= 0) {
            if (recvbuff)
                free(recvbuff);
            va_end(args);
            return err ;
        }

        va_end(args);
        if (recvbuff)
            free(recvbuff);
        recvbuff = NULL;

		/* Continue receiving more data */
    }
}

/*
 * Error packet received and we weren't expecting it.
 */

int AMSP_fsm_error(char *ptr, int nbytes, int sfd, int ver_sent, int op_sent, int op_recv,
              va_list argptr)
{
    AMSP_err_dump("error received: op_sent = %d, op_recv = %d",
             op_sent, op_recv);
    return 0; /* Should not get here */
}

/*
 * Invalid state transition.  Something is wrong.
 */

int AMSP_fsm_invalid(char *ptr, int nbytes, int sfd, int ver_sent, int op_sent, int op_recv,
                va_list argptr)
{
    AMSP_err_dump("protocol botch: op_sent = %d, op_recv = %d",
             op_sent, op_recv);
    return 0; /* Should not be reached */
}

