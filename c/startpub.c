/* This detached thread wait on a socket for a resquest over the  */
/* network to process. The typical request type is to read/modify */
/* a variable.                                                    */
/* not-accessor */

#include <stdio.h>
#ifndef _WIN_ALICE
#include <unistd.h>
#endif

#include "amsdefs.h"
#include "netlib.h"
#include "netapi.h"
#include "thrapi.h"
#include "acomm.h"
#include "amsldap.h"

Ret_Thr AMSP_start_publisher_func(void *arg)
{

    int childpid, sfd;
    Pub_arg *argp = (Pub_arg *) arg;

    /* We're alive for accessor requests */
    AMSP_LockMutexWrite(&(argp->pub_mutex));
    AMSP_mem_net_init(AMS_SERVICE, argp->care_about_port,
                     &(argp->port), &sfd);
    /* (care_about_port now indicates that port decision has been finalized) */
    argp->care_about_port = 2;

    /* Broadcast a signal to other threads waiting for this port to be set */
    AMSP_CondBroadcast(&(argp->cond_port));

    /* Release Mutex */
    AMSP_RelMutexWrite(&(argp->pub_mutex));

    inetdflag = 0;
        
    for ( ; ; ) {

        if ( (childpid = AMSP_mem_net_accept(inetdflag, sfd, (void *)arg)) == 0) {
            /* If main program has terminated, then exist */
            AMSP_mem_net_close(sfd);

#ifdef AMS_LDAP
            /* Delete LDAP Object */
            if (argp->ld && argp->dn)
                AMSP_Ldap_del_object(argp->ld, argp->dn);
#endif
            /* I am done */
            AMSP_EndThread((void *)&sfd);
        }

        /* parent waits for another accessor's request */
    }
    /* NOTREACHED */
}











