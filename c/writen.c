/*
 * Write "n" bytes to a descriptor.
 * Use in place of write() when fd is a stream socket.
 * This function is ThreadSafe
 * If return is < 0, then caller should handle the error
 * If return is = 0, then we assume that the other party
 * interepted the connection. The caller once again should
 * handle this situation
 */
#include <stdio.h>
#include <string.h>
#include "network.h"
#include "netlib.h"
 
#ifdef _WIN_ALICE
#include <io.h>  
#else
#include <errno.h>
#endif

int AMSP_writen(fd, ptr, nbytes)
     register int  fd;
     register char *ptr;
     register int  nbytes;
{
  int nleft, nwritten;
#ifdef _WIN_ALICE
  int err;
    char mesg[255];
#endif

    nleft = nbytes;
    while (nleft > 0) {
        nwritten = send(fd, ptr, nleft, 0);
        if (nwritten <= 0) {
#ifdef _WIN_ALICE
	    switch(GetLastError()) {
	    case WSAECONNRESET:
	    case WSAECONNABORTED :
                return 0; /* EOF */
            case WSAEINTR:
                return nwritten; /* Call has been interepted */
	    default:
		AMSP_err_quit("writen error: %s", AMSP_host_err_str(mesg, &err));
	    }
#else
            switch(errno) {
	    case ECONNRESET:
	    case ECONNABORTED :
	        return 0; /* End of File */
	    case EINTR:
	        return nwritten; /* Call has been interepted */
	    default:
	        AMSP_err_quit("writen error: %s", strerror(errno));
	    }
#endif
       }
        nleft -= nwritten;
        ptr   += nwritten;
    }

    return(nbytes - nleft);
}


