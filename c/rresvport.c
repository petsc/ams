/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * From the file: "@(#)rcmd.c   5.20 (Berkeley) 1/24/89";
 */

/*
 * Obtain a reserved TCP port.
 *
 * Note that we only create the socket and bind a local reserved port
 * to it.  It is the caller's responsibility to then connect the socket.
 */

/*
 * This function is ThreadSafe
 */

#include        <unistd.h>
#include        <string.h>
#include        <sys/types.h>
#include        <errno.h>

#include        "network.h"
#ifdef _WIN_ALICE
#include        <memory.h>
#include        <io.h>
#else

#endif

#include		"netlib.h"
extern int      errno;

/* return socket descriptor, or -1 on error */
int AMSP_rresvport(int *alport, char *errmsg)
       /* value-result; on entry = first port# to try */
       /* on return = actual port# we were able to bind */
{
        struct sockaddr_in my_addr;
        int sockfd, err = -1; /* Undefined error */
        char locmsg[255];

        memset((char *) &my_addr, 0, sizeof(my_addr));
        my_addr.sin_family      = AF_INET;
        my_addr.sin_addr.s_addr = INADDR_ANY;

        if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			AMSP_err_ret(errmsg, "Can't get reserved port: %s", AMSP_host_err_str(locmsg, &err));
            return(-1);
		}

        for ( ; ; ) {
                my_addr.sin_port = htons((u_short) *alport);
                if (bind(sockfd, (struct sockaddr *) &my_addr, sizeof(my_addr)) >= 0)
                        break;          /* OK, all done */
#ifdef _WIN_ALICE
                if (errno != WSAEADDRINUSE) {   /* some other error */
					AMSP_err_ret(errmsg, " Bind: %s ", AMSP_host_err_str(locmsg, &err));
                    close(sockfd);
                    return(-1);
                }
#else
                if (errno != EADDRINUSE) {      /* some other error */
					AMSP_err_ret(errmsg, " Bind: %s ", AMSP_host_err_str(locmsg, &err));
                    close(sockfd);
                    return(-1);
                }
#endif

                (*alport)--;    /* port already in use, try the next one */
                if (*alport == IPPORT_RESERVED / 2) {
                    close(sockfd);
                    errno = EAGAIN;
                    return(-1);
                }
        }
        return(sockfd);
}
