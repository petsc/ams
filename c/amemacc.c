/* not-publisher */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "amem.h"
#include "acomm.h"
#include "netlib.h"

int AMSP_SetStep_AMS_MEM(AMS_MEM *amem, unsigned char *count)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    memcpy((void *) amem->current_step, (const void *) count, STEP_BYTES);

    return AMS_ERROR_NONE;
}

int AMSP_SetStep_AMS_Memory(AMS_Memory *memid, int nbmem, unsigned char *step)
{
	int i, err;

	for (i=0; i<nbmem; i++) {
		CheckMemory(memid[i]);
		err = AMSP_SetStep_AMS_MEM(MEM_TBL[getMemid(memid[i])][pub_id], step + i*STEP_BYTES);
		CheckErr(err);
	}

	return AMS_ERROR_NONE;
}

