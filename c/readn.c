/*
 * Read "n" bytes from a descriptor.
 * Use in place of read() when fd is a stream socket.
 * This function is ThreadSafe.
 * If return is < 0, then caller should handle the error
 * If return is = 0, then we assume that the other party
 * interepted the connection. The caller once again should
 * handle this situation
 */
 
#include <stdio.h>
#include <string.h>
#include  <unistd.h>
#include "network.h"
#include "netlib.h"
 
#ifdef _WIN_ALICE
#include <io.h> 
#else
#include <errno.h>
#endif

int AMSP_readn(fd, ptr, nbytes)
     int  fd;
     char *ptr;
     int  nbytes;
{
  int nleft, nread;
#ifdef _WIN_ALICE
  int  err;
  char mesg[255];
#endif

    nleft = nbytes;
    while (nleft > 0) {
#ifdef _WIN_ALICE
        nread = recv(fd, ptr, nleft, 0);
#else
        nread = read(fd, ptr, nleft);
#endif

        if (nread < 0) {
#ifdef _WIN_ALICE
            switch(GetLastError()) {
            case WSAECONNRESET:
            case WSAECONNABORTED :
                return 0; /* End of File */
            case WSAEINTR:
                return nread; /* Call has been interepted */
            default:
                AMSP_err_quit("writen error: %s", AMSP_host_err_str(mesg, &err));
	    }
#else
            switch(errno) {
            case ECONNRESET:
            case ECONNABORTED :
                return 0; /* End of File */
            case EINTR:
                return nread; /* Call has been interepted */
            default:
                AMSP_err_quit("writen error: %s", strerror(errno));
            }
#endif
        }
        else if (nread == 0)
            break;      /* EOF */

        nleft -= nread;
        ptr   += nread;
    }
    return (nbytes - nleft);   /* return >= 0 */
}


