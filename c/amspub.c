/* not-accessor */
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>

#include "ams.h"        /*I "ams.h" I*/
#include "acomm.h"
#include "amsdefs.h"
#include "netlib.h"
#include "netapi.h"
#include "thrapi.h"
#include "amisc.h"
#include "sendfun.h"
#include "amsmpi.h"

#if AMS_LDAP
#include "amsldap.h"
#endif

/*
 * Routines for the Publisher of shareable memory
 */

/*@

        AMS_Comm_publish - Publishes a computational context or
        Communicator. The first time the function is called it
        creates a Publisher (a unique Communicator) that would serve
        as a first point of contact between the accessor (Accessor),
        and the Publisher.

  Input Parameters:
+       name - the unique name of the computational context
.       ctype - Communicator Type (NODE_TYPE or MPI_TYPE)
.       host_list - list of hosts terminated by the NULL pointer (NODE_TYPE).
        You could use NULL to let the system decide on the default hostname
-       port_list - list of ports for each node to communicate on (NODE_TYPE).
        You could use NULL to let the system choose a port number for the
        Communicator.

  Output Parameter:
+       ams - the computational context

  Note: This function is not thread-safe.
  See Also:
    AMS_Comm_destroy()
@*/

int AMS_Comm_publish(const char *name, AMS_Comm *ams, AMS_Comm_type ctype, ...)
{

    HOST_NODE *host_node;
    char host[MAXHOSTLEN+1], hosterr[255], *p;
    int err, AMS_port, my_port = -1, tmp_port = -1, size = 0, rank, t_flag;
    static int firstcall = 1;

#ifdef AMS_LDAP
    LDAP *ld;
#endif

    static Pub_arg *pub_arg; /* Main publisher arguments: port, mutex */

    char **host_list;
    int *port_list, i, default_port;
	int mysetport = -1;
    va_list args;

    Pub_arg *comm_arg;/* Communicator arguments: port, mutex */
    Thread Publisher_Thread;
    MPI_Comm mpi_comm;

    /* This is the entry function for the publisher-thread */
    extern Ret_Thr AMSP_start_publisher_func(void *arg);


#ifdef _WIN_ALICE
	/* Initialize Windows Sockets if necessary */
    if (!AMSP_InitSock())
        AMSP_err_quit("Can't Initialize Windows Socket");
#endif

    /* Check parameters */
    va_start(args, ctype);
    switch(ctype) {
    case MPI_TYPE:
        mpi_comm = (MPI_Comm) va_arg(args, MPI_Comm);
	MPI_Comm_size(mpi_comm, &size);
        MPI_Comm_rank(mpi_comm, &rank);
        /*mysetport = *(int *) va_arg(args, int *);*/
	if (size > 1) {
	    err = AMSP_Construct_Host_list_AMS_MPI(&host_list, mpi_comm);
	    CheckErr(err);
	    port_list = NULL;
	    break;
	}
	ctype = NODE_TYPE;
	host_list = NULL;
        port_list = NULL;
	break;
    case NODE_TYPE:
        host_list = (char **) va_arg(args, char **);
        port_list = (int *) va_arg(args, int *);
        break;
    default:
        return AMS_ERROR_INVALID_COMM_TYPE;
    }

    if (port_list == NULL) {
        port_list = &tmp_port;
        default_port = 1; /* Use default port numbers */
    } else {
        default_port = 0; /* Use user provided port numbers  */
    }

    /* Get local host name 007 How to get the full host + domain_name */
    if ((err = gethostname(host, MAXHOSTLEN)))
        AMSP_err_quit("gethostname: %s", AMSP_host_err_str(hosterr, &err));

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    /* Get the port number */
    if ((p=getenv("AMS_PORT")))
        AMS_port = (int) atoi(p);
    else if ((p=getenv("AMS_port")))
        AMS_port = (int) atoi(p);
    else
        AMS_port = DEFAULT_PORT; /* Default port */

    /*
	if (mysetport != -1) {
		AMS_port = mysetport;
	}
    */

    if (firstcall == 1 || PUBLISHER.alive == 0) { /* Start the publisher **NOT THE COMMUNICATOR** */
        /* Should be called once */
        firstcall = 0;

        /* Perform start up initialization */
        AMSP_Start_up();

        PUBLISHER.nb_com = 0;
        PUBLISHER.alive = 1;

        strcpy(PUBLISHER.host, host);
        PUBLISHER.port = AMS_port;

        /* Create publisher arguments */
        err = AMSP_New_Pub_Arguments(AMS_port, &pub_arg);
        CheckErr(err);

        /* Publisher (but not communicator) is immediately alive */
        pub_arg->alive = 1;

        if(ctype == MPI_TYPE)
        {
          if(rank == 0)
          {
            pub_arg->care_about_port = 1;
          }
          else
          {
            pub_arg->care_about_port = 0;
            pub_arg->port = -1;
          }
        }
        else
            pub_arg->care_about_port = 1;

        /* Create a thread that will listen to general requests from accessors */
        AMSP_CreatThread(AMSP_start_publisher_func, (void *)pub_arg, &Publisher_Thread);

#ifdef AMS_LDAP
        /* Initialize the ldap sever */
        err = AMSP_Ldap_init(&ld);
        CheckErr(err);

        /* Add publisher to the ldap sever */
        err =AMSP_Ldap_add_publisher(ld);
        CheckErr(err);

#endif
    }

    /* Now we create a communicator structure */
    err = AMSP_New_AMS_Comm(name, ctype, ams);
    CheckErr(err);

    /* Get all the publisher ports if necessary */
    if(ctype == MPI_TYPE) {
        err = AMSP_Set_MPI_Comm_AMS_Comm(*ams, mpi_comm);
        CheckErr(err);

        AMSP_LockMutexWrite(&(pub_arg->pub_mutex));
        /* (wait for another thread to fill it in): */
        while(pub_arg->care_about_port != 2) {
            AMSP_CondWaitMutexWrite(&(pub_arg->cond_port), &(pub_arg->pub_mutex), 0, &t_flag);
        }

        err = AMSP_Construct_Pub_ports_AMS_MPI(pub_arg->port, firstcall,
                                          host_list, &port_list, mpi_comm);
        AMSP_RelMutexWrite(&(pub_arg->pub_mutex));
        CheckErr(err);
    }

    /* Create and add the list of host nodes */
    i = 0;
    if (host_list == NULL) { /* Add default host */
        /* host_list == NULL HERE implies single processor */
        /* Create a host_node and initialize it */
        host_node = AMSP_Create_Host_Node(host, port_list[i], &err);
        CheckErr(err);

        /* Add a host_node to the communication structure */
        err = AMSP_Add_Host_Node_AMS_Comm(*ams, host_node);
        CheckErr(err);

        /* This node's port is */
        my_port = port_list[i];
    } else {
        while(host_list[i]) {
            /* Create a host_node and initialize it */
            if (default_port)
                host_node = AMSP_Create_Host_Node(host_list[i], my_port = -1, &err);
            else
                host_node = AMSP_Create_Host_Node(host_list[i], my_port = port_list[i], &err);
            CheckErr(err);

            /* Add a host_node to the communication structure */
            err = AMSP_Add_Host_Node_AMS_Comm(*ams, host_node);
            CheckErr(err);

            /* Should NOT check for duplicates */
            i++;
        }
    }

    /* Create communicator arguments */
    err = AMSP_New_Pub_Arguments(my_port, &comm_arg);
    CheckErr(err);

    /* Store communicator startup function's arguments */
    err = AMSP_Set_Pub_Arguments_AMS_Comm(*ams, comm_arg);
    CheckErr(err);

    /* Create a thread that will listen to further requests from accessors */
    AMSP_CreatThread(AMSP_start_publisher_func, (void *)comm_arg, &Publisher_Thread);

    /* Store the Publisher-Thread ID for further references, such as termination */
    err = AMSP_Set_Thread_ID_AMS_Comm(*ams, Publisher_Thread);
    CheckErr(err);

    /* Update the communication structure table and register with the daemon */
    err = AMSP_Publish_AMS_Comm(*ams);
    CheckErr(err);

    err = AMSP_Construct_Port_list_AMS_Comm(*ams, host_list, port_list, name);
    /* (we're passing the publisher port list, not the communicator
        port list (which is to be determined)) */
    CheckErr(err);

    /* Record the fact that we're alive */
    err = AMSP_Set_Pub_Arguments_AMS_Comm(*ams, comm_arg);
    CheckErr(err);

    /* Increment the number of communicators */
    PUBLISHER.nb_com++;

    /* Free memory if necessary */
    if(ctype == MPI_TYPE) {
        err = AMSP_Destroy_Host_list_AMS_MPI(host_list);
        CheckErr(err);
        free(port_list);
    }

#ifdef AMS_LDAP
    /* Add AMS Communicator to the ldap sever */
    err =AMSP_Ldap_add_communicator(ld, host, AMS_port, *ams);
    CheckErr(err);
#endif

    return AMS_ERROR_NONE;
}

/*@

        AMS_Memory_create - Creates an AMS memory object.

    Input Parameters:
+       ams - the Communicator that shares this memory object
-       name - the unique name within the AMS context

    Output Parameter:
+       memid - A pointer to the memory object id

    See Also
        AMS_Memory_destroy()

@*/

int AMS_Memory_create(AMS_Comm ams, const char *name, AMS_Memory *memid)
{
    /*
     * Creates an AMS memory structure, add the structure to the memory
     * table and returns the structure id.
     */

    AMS_MEM * amem;
    MEM_NODE *mem_node;
    int err;

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    /* Create a memory structure */
    amem = AMSP_New_AMS_MEM(ams, name, &err);
    CheckErr(err);

    /* Create a memory node */
    mem_node = AMSP_Create_Mem_Node(amem, memid, &err);
    CheckErr(err);

    /*
     * Add the node to the communicator. Note that the memory
     * has not yet been published
     */
    err = AMSP_Add_Mem_Node_AMS_Comm(ams, mem_node);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
    AMS_Memory_AMS_set_field_func - Establish a setter function to be
        used with an AMS Memory field. The function is called when a
        modify memory request is received from the accessor (Accessor)

  Input parameters:
+       mem - the AMS memory object id
.       name - the unique name of the field
.       lang - the language to which the setter function's prototype conforms
-       SetterFunc - pointer to the setter function to be used

  Note: The following arguments are need for C++, and JAVA
+       obj - pointer to setter method's object (the this object in C++)
.       env - environment of object (JAVA)
-       obj - pointer to setter method's object (JAVA)

@*/

int AMS_Memory_set_field_func(AMS_Memory mem, const char *name,
                              AMS_Language_type lang, ...)
{
    int err;
    void *x, *y;
    AMS_C_Setter *CSetValue;
    AMS_CPP_Setter *CPPSetValue;
    AMS_JAVA_Setter *JAVASetValue;
    va_list args;
    va_start(args, lang);

    switch(lang) {
    case AMS_C:
        CSetValue = (AMS_C_Setter *) va_arg(args, AMS_C_Setter *);
        err = AMSP_Set_Field_Func_C_AMS_Memory(mem, name, CSetValue);
        break;

    case AMS_CPP:
        CPPSetValue = (AMS_CPP_Setter *)
            va_arg(args, AMS_CPP_Setter *);
        x = (void *) va_arg(args, void *);
        err = AMSP_Set_Field_Func_CPP_AMS_Memory(mem, name,
                                            CPPSetValue, x);
        break;

    case AMS_JAVA:
        JAVASetValue = (AMS_JAVA_Setter *)
            va_arg(args, AMS_JAVA_Setter *);
        x = (void *) va_arg(args, void *);
        y = (void *) va_arg(args, void *);
        err = AMSP_Set_Field_Func_JAVA_AMS_Memory(mem, name,
                                             JAVASetValue, x, y);
        break;

    default:
        va_end(args);
        return AMS_ERROR_INVALID_DATA_TYPE;
    }
    va_end(args);

    CheckErr(err);
    return AMS_ERROR_NONE;
}

/*@
        AMS_Memory_set_field_start_index - sets the starting index of a
        field's data in the global array. This function is called for
        one dimensional distributed arrays. See AMS_Memory_set_field_block
        for multi-dimensional arrays.

  Input Parameters:
+       mem - the AMS memory object
.       name - the name of the field
-       start - the starting index

    See Also:
        AMS_Memory_set_field_block()
@*/

int AMS_Memory_set_field_start_index(AMS_Memory mem, const char *name, int start)
{
    int err;

    err = AMSP_Set_Field_Start_Index_AMS_Memory(mem, name, start);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
        AMS_Memory_set_field_permutation - sets the permutation indices on how
        local elements are positioned in the global array.

  Input Parameters:
 +      mem - the AMS memory object
 .      name - the name of the field
 -      perm - list of indices where our elements should go in global array

@*/

int AMS_Memory_set_field_permutation(AMS_Memory mem, const char *name, int *perm)
{
    int err;

    err = AMSP_Set_Field_Permutation_AMS_Memory(mem, name, perm);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
        AMS_Memory_set_field_block - Sets the starting and ending indices
                                     in each direction of a multidimensional
                                     array stored as a field in an
                                     AMS Memory object

  Input Parameters:
+       mem - the AMS memory object
.       name - the name of the field
.       dim - number of dimensions
.       start - array of starting indices (one per dimension)
-       end - array of ending indices (one per dimension)

    See Also:
    AMS_Memory_set_field_start_index()
@*/

int AMS_Memory_set_field_block(AMS_Memory mem, const char *name,
                               int dim, int *start, int *end)
{
    int err;

    err = AMSP_Set_Field_Block_AMS_Memory(mem, name, dim, start, end);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@

        AMS_Memory_add_field - Add a field to AMS memory object.

  Input Parameters:
+       mem - the AMS memory object
.       name - the unique name of field
.       addr - address of memory
.       len - number of elements in the field (size of the array)
.       dtype - AMS Data type: AMS_CHAR, AMS_INT, AMS_BOOLEAN, AMS_FLOAT, AMS_DOUBLE, or AMS_STRING
.       mtype - AMS Memory type: AMS_READ, AMS_WRITE, or AMS_READ_WRITE
.       stype - AMS Shared type: AMS_COMMON, AMS_DISTRIBUTED or AMS_REDUCED
-       rtyep - AMS Reduction type: AMS_SUM, AMS_MAX, AMS_MIN, or AMS_NONE

    See Also:
        AMS_Memory_publish()
@*/

int AMS_Memory_add_field(AMS_Memory memid, const char *name, void *addr, int len, AMS_Data_type dtype,
                         AMS_Memory_type mtype, AMS_Shared_type stype, AMS_Reduction_type rtype)
{
    int err = AMSP_Add_Field_AMS_Memory(memid, name, addr, len, dtype, mtype, stype, rtype);
    CheckErr(err);
    return AMS_ERROR_NONE;
}

/*@

        AMS_Memory_publish - Publishes an AMS memory object. This function
        is called after AMS_Memory_create() and AMS_Memory_add_field().
        The function makes the memory available to accessors (Accessors).

  Input Parameters:
+       mem - the AMS memory object

  See Also:
    AMS_Memory_destroy()

@*/

int AMS_Memory_publish(AMS_Memory mem)
{

    int err, nbmem = 1;

    /*
	 * Sets a flag in the table of published memories
	 */
    err = AMSP_Publish_AMS_Memory(mem);
    CheckErr(err);

	/*
	 * Increment the Memory step, so that after it is published the
	 * the accessor (whose step is 0) can get a new copy
	 */
	err = AMSP_IncrStep_AMS_Memory(&mem, nbmem);
	CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@

        AMS_Memory_grant_write_access - Grant write access to an AMS memory object.
        It unlocks a memory object, and allows the accessor and other threads
        to read/write the memory fields. This function is the same as
		AMS_Memory_grant_access which is being deprecated.

	Input Parameters:
+       mem - the AMS memory object

	See Also:
		AMS_Memory_grant_read_access(), AMS_Memory_take_write_access(),
		AMS_Memory_take_read_access()

@*/

int AMS_Memory_grant_write_access(AMS_Memory mem)
{
    return AMSP_UnLock_Write_AMS_Memory(mem);
}

/*@

        AMS_Memory_grant_read_access - Grant read access to an AMS memory object.
        It unlocks a memory object, and allows the accessor and other threads
        to read the memory fields.

	Input Parameters:
+       mem - the AMS memory object

  	See Also:
		AMS_Memory_grant_write_access(), AMS_Memory_take_write_access(),
		AMS_Memory_take_read_access()

@*/

int AMS_Memory_grant_read_access(AMS_Memory mem)
{
    return AMSP_UnLock_Read_AMS_Memory(mem);
}


/*@
    AMS_Memory_take_write_access - The calling thread takes write access of
    an AMS memory object from other threads. The memory step (time-stamp)
    is then incremented.

	Input Parameters:
+       mem - the AMS memory object

  	See Also:
		AMS_Memory_grant_write_access(), AMS_Memory_grant_read_access(),
		AMS_Memory_take_read_access()
@*/
int AMS_Memory_take_write_access(AMS_Memory mem)
{
    int err = AMSP_Main_thread_write_Lock_AMS_Memory(mem), nbmem = 1;
    CheckErr(err);

    err = AMSP_IncrStep_AMS_Memory(&mem, nbmem);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
    AMS_Memory_take_read_access - Take a read access of an AMS
    memory object from other threads. While access is taken, the Memory
    cannot be modified, but read access is still allowed. The Memory step
    (time-stamp) is NOT incremented.

	Input Parameters:
+       mem - the AMS memory object

	See Also:
		AMS_Memory_grant_write_access(), AMS_Memory_take_write_access(),
		AMS_Memory_grant_read_access()

  Note: Use this function if the main application if accessing a memory but
        not modifying it.
@*/

int AMS_Memory_take_read_access(AMS_Memory mem)
{
    return AMSP_Main_thread_read_Lock_AMS_Memory(mem);
}


/*@
    AMS_Memory_wait_read_access - Wait for the memory to be read by another thread.
    The function suspends the calling thread util the memory is accessed for reading
    by another thread, or until the time has expired.

	Input Parameters:
+       mem - the AMS memory object
+       time - number of milliseconds to wait before return. Use the 0 if you do not want an expiration


@*/

int AMS_Memory_wait_read_access(AMS_Memory mem, int time)
{
    return AMSP_Main_thread_read_wait_AMS_Memory(mem, time);
}


/*@
    AMS_Memory_wait_write_access - Wait for the memory to be written by another thread.
    The function suspends the calling thread util the memory is accessed for writing
    by another thread, or until the time has expired.

  Note: the function returns even if the other thread does not modify it.

	Input Parameters:
+       mem - the AMS memory object
+       time - number of milliseconds to wait before return. Use the 0 if you do not want an expiration


@*/

int AMS_Memory_wait_write_access(AMS_Memory mem, int time)
{
    return AMSP_Main_thread_write_wait_AMS_Memory(mem, time);
}

/*@

    AMS_Memory_destroy - Destroys an AMS memory object

    Input Parameters:
+       mem - the AMS memory object
    See Also:
        AMS_Memory_create()
@*/

int AMS_Memory_destroy(AMS_Memory mem)
{
    AMS_Comm ams;
    MEM_NODE *node;

    int err = AMS_Memory_take_write_access(mem);
    CheckErr(err);

    /* Unpublish the memory: delete memory from communicator */
    err = AMSP_UnPublish_AMS_Memory(mem);
    CheckErr(err);

    /* Grant access to notify waiting accessors that the memory has been unpublished */
    err = AMS_Memory_grant_write_access(mem);
    CheckErr(err);

    /* Gets the communicator */
    err = AMSP_Comm_AMS_Memory(mem, &ams);
    CheckErr(err);

    /* Lock the publisher */
    err = AMSP_Lock_Write_AMS_Comm(ams);
    CheckErr(err);

    /* Find the memory node to delete */
    err = AMSP_Find_Mem_id_AMS_Comm(ams, mem, &node);
    CheckErr(err);

    /* Destroy the memory: free the allocated resources */

    err = AMSP_Del_Mem_Node_AMS_Comm(ams, node);
    CheckErr(err);

    /* Unlock the publisher */
    err = AMSP_UnLock_Write_AMS_Comm(ams);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@

        AMS_Comm_destroy - Destroys an AMS communicator

  Input Parameters:
+       ams - the AMS communicator

  See Also:
    AMS_Comm_publish()
@*/

int AMS_Comm_destroy(AMS_Comm ams)
{

    /*
     * Unpublish the communicator: What happens if some memory
     * was not destroyed ? We need to look at it for
     * improvements. We also need to contact the daemon to signal
     * the retirement of this communicator.
     */

    /* Shutdown the Publisher-Thread */
    int err = AMSP_EndThread_AMS_Comm(ams);
    CheckErr(err);

    /* Unpublish the communicator */
    err = AMSP_UnPublish_AMS_Comm(ams);
    CheckErr(err);

    /* Deletes all hosts (Comm) and their associated resources */
    err = AMSP_DelAll_Host_AMS_Comm(ams);
    CheckErr(err);

    /* Deletes what left of the memory */
    err = AMSP_DelAll_Mem_AMS_Comm(ams);
    CheckErr(err);

    /* Deletes the Communicator */
    err = AMSP_Del_AMS_Comm(ams);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

extern int comm_pub_mutex_created;

#if 0
/*
   Since this is the publisher the host and port are ignored.
*/
int AMS_Connect(const char *host, int port, char ***list)
{
  int err,list_len;
  char *buff;
  if (comm_pub_mutex_created == 1) {
    err = AMSP_Get_Comm_list(&buff, &list_len);
    CheckErr(err);
    err = AMSP_Create_Comm_list(buff, list_len, list);
    CheckErr(err);
  } else {
    *list = NULL;
  }
  return AMS_ERROR_NONE;
}

int AMS_Comm_attach(const char *name, AMS_Comm *ams)
{
  int err,i;
  char buff[256];
  strcpy(buff,name);
  for (i=0; i<256; i++) {
    if (!buff[i]) break;
    if (buff[i] == '|') buff[i] = 0;
  }

  err = AMSP_Get_AMS_Comm(buff, ams);
  CheckErr(err);
  return AMSP_Lock_TBL_Read_Comm_Struct();
}

int AMS_Disconnect(void)
{
  int err;
  err = AMSP_Delete_Comm_list(); CheckErr(err);
  CheckErr(err);
  if (comm_pub_mutex_created == 1) {
    err = AMSP_UnLock_TBL_Read_Comm_Struct();
    CheckErr(err);
  }
  return AMS_ERROR_NONE;
}

int AMS_Comm_get_memory_list(AMS_Comm ams, char ***list)
{
  int err,list_len;
  char *buff;
  err = AMSP_Get_Memory_list(ams, &buff, &list_len);
  CheckErr(err);
  err = AMSP_Create_Memory_list(buff, list_len, list);
  CheckErr(err);
  return AMS_ERROR_NONE;
}

/*
   How come this doesn't use the AMS_Comm argument?
*/
int AMS_Memory_attach(AMS_Comm ams, const char *name, AMS_Memory *mem, unsigned int *step)
{
  int err;
  err = AMSP_Get_AMS_Memory(name, 0,mem);
  CheckErr(err);
  return AMS_ERROR_NONE;
}

int AMS_Memory_get_field_list(AMS_Memory mem, char *** fld_list)
{
  int err;
  err =  AMSP_Get_field_list_AMS_Memory(mem,fld_list);
  CheckErr(err);
  return AMS_ERROR_NONE;
}


/*
  The addr here is dangerous because we can change it without doing a lock.

  If we want to be able to use the accessor from the publisher we are doing to need to modify the accessor
  interface so that one must "lock" the values before changing. For example

       AMS_Memory_update_[un]lock() ?????
*/
int AMS_Memory_get_field_info(AMS_Memory mem, const char *name, void **addr, int *len, AMS_Data_type *dtype,
                              AMS_Memory_type *mtype, AMS_Shared_type *stype, AMS_Reduction_type *rtype)
{
  int   err;
  Field *fld;

  fld = AMSP_Find_name_AMS_Memory(mem, name, &err);
  CheckErr(err);
  err = AMSP_Get_Data_type_Field(fld,dtype);
  CheckErr(err);
  err = AMSP_Get_Memory_type_Field(fld,mtype);
  CheckErr(err);
  err = AMSP_Get_Shared_type_Field(fld,stype);
  CheckErr(err);
  err = AMSP_Get_Reduction_type_Field(fld,rtype);
  CheckErr(err);
  *addr = AMSP_Get_Buffer_Field(fld,len,&err);
  CheckErr(err);
  return AMS_ERROR_NONE;
}

/*
  Since these are run on the publisher they don't need to do anything.
*/
int AMS_Memory_update_send_begin(AMS_Memory mem)
{
  return AMS_ERROR_NONE;
}

int AMS_Memory_update_send_end(AMS_Memory mem) {
  return AMS_ERROR_NONE;
}
#endif

