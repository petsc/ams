/* not-accessor */
#include <stdlib.h>
/*#include <malloc.h> */
#include <string.h>
#include <stdio.h>

#include "amem.h"
#include "acomm.h"
#include "netlib.h"
#include "updatesync.h"
#include "amsmpi.h"


/* 
 * Implementation of AMS_MEM Low level functions
 * These low-level functions are hidden to the user
 * of the AMS memory module. They are provided to help
 * in the design of high-level functions.
 */

Mutex pub_mem_tbl;
Mutex  MEM_PUB_LOCK[MAX_MEMID][MAX_COMM];

int AMSP_Reserve_access_AMS_MEM(AMS_MEM *amem, unsigned char *step)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    memcpy(amem->stop_at, step, STEP_BYTES);
	if ( memcmp(amem->stop_at, amem->current_step, STEP_BYTES) < 0) {
		unsigned int c_step, stop_at;
		AMSP_TranslateStep(amem->current_step, &c_step);
		AMSP_TranslateStep(amem->stop_at, &stop_at);
		AMSP_err_quit(" Reserve_access_AMS_MEM: Late appointment, current_step = %u, stop_at = %u \n", c_step, stop_at);
	}
    return AMS_ERROR_NONE;
}

/*
 * Take a read access when the memory's current_step is equal to step
 */
int AMSP_Take_read_access_future_AMS_MEM(AMS_MEM *amem, unsigned char *step)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem), t_flag;
    CheckErr(err);

    /* 
	 * Must lock waiting room, because that can change what step points to (???)
	 */
    AMSP_LockMutexRead(&(amem->mem_lock));
    AMSP_Lock_Waiting_Room(amem->comm);
    while(AMSP_StepGreaterThan(step, amem->current_step)) {
        AMSP_UnLock_Waiting_Room(amem->comm);
		AMSP_CondWaitMutexRead(&(amem->cond), &(amem->mem_lock), 0, &t_flag);
        AMSP_Lock_Waiting_Room(amem->comm);
    }
    AMSP_UnLock_Waiting_Room(amem->comm);

    return AMS_ERROR_NONE;
}

int AMSP_Take_write_access_future_AMS_MEM(AMS_MEM *amem, unsigned char *step)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem), t_flag;
    CheckErr(err);

    /* 
	 * Must lock waiting room, because that can change what step points to (???)
	 */
    AMSP_LockMutexWrite(&(amem->mem_lock));
    AMSP_Lock_Waiting_Room(amem->comm);
    while(AMSP_StepGreaterThan(step, amem->current_step)) {
        AMSP_UnLock_Waiting_Room(amem->comm);
		AMSP_CondWaitMutexWrite(&(amem->cond), &(amem->mem_lock), 0, &t_flag); 
        AMSP_Lock_Waiting_Room(amem->comm);
    }
    AMSP_UnLock_Waiting_Room(amem->comm);

    return AMS_ERROR_NONE;
}

/*
 * Send a signal to other threads that are waiting for this memory
 */

int AMSP_CondBroadcast_AMS_MEM(AMS_MEM *amem)
{
	int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

	AMSP_CondBroadcast(&(amem->cond));
	return AMS_ERROR_NONE;
}

int AMSP_Reserve_future_access(AMS_Memory memid, unsigned char *step)
{
    CheckMemory(memid);
    return AMSP_Reserve_access_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], step);
}

int AMSP_Worker_take_read_access_future(AMS_Memory memid, unsigned char *step)
{
    CheckMemory(memid);
    return AMSP_Take_read_access_future_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], step);
}

int AMSP_Worker_take_write_access_future(AMS_Memory memid, unsigned char *step)
{
    CheckMemory(memid);
    return AMSP_Take_write_access_future_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], step);
}

int AMSP_Take_synchronized_read_access(AMS_Memory *memid, int nbmem, RequestID req,
                                  Appointment ***my_appt)
{
    AMS_Comm ams;
	int err, i, nb_proc;

	/* Creates an array of pointers to appointments */
	*my_appt = (Appointment **) malloc (sizeof(Appointment *) * nbmem);
	if (*my_appt == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	for (i=0; i<nbmem; i++) {

		err = AMSP_Comm_AMS_Memory(memid[i], &ams);
		CheckErr(err);

		/* How many processors are invloved in */
		err = AMSP_Get_Nb_Proc_AMS_Comm(ams, &nb_proc);
		CheckErr(err);

		if (nb_proc > 1) {
			err = AMSP_Lock_Waiting_Room(ams);
			CheckErr(err);

			err = AMSP_Register_Guest(ams, req, memid[i], (*my_appt) + i, 0);
			CheckErr(err);

			err = AMSP_UnLock_Waiting_Room(ams);
			CheckErr(err);

			err = AMSP_Start_Step_Negotiation(memid[i], req);
			CheckErr(err);

			/*
			 * Wait until the appointment is set by another thread
			 */
			err = AMSP_Wait_For_Appt_Step(ams, *((*my_appt) + i));
			CheckErr(err);

			err = AMSP_Worker_take_read_access_future(memid[i], (*((*my_appt) + i))->step);
			CheckErr(err);
		} else { /* Uniprocessor. No processors synchronization */
			err = AMSP_Worker_take_read_access(memid[i]);
			CheckErr(err);
		}

	} /* For each memory */

	return AMS_ERROR_NONE;
}

int AMSP_Take_synchronized_write_access(AMS_Memory *memid, int nbmem, RequestID req,
                                   Appointment ***my_appt)
{
    AMS_Comm ams;
	int i, err, nb_proc;

	/* Creates an array of pointers to appointments */
	*my_appt = (Appointment **) malloc (sizeof(Appointment *) * nbmem);
	if (*my_appt == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	for (i=0; i<nbmem; i++) {
    
		err = AMSP_Comm_AMS_Memory(memid[i], &ams);
		CheckErr(err);

		/* How many processors are invloved in */
		err = AMSP_Get_Nb_Proc_AMS_Comm(ams, &nb_proc);
		CheckErr(err);

		if (nb_proc > 1) {

			err = AMSP_Lock_Waiting_Room(ams);
			CheckErr(err);

			err = AMSP_Register_Guest(ams, req, memid[i], (*my_appt)+i, 0);
			CheckErr(err);

			err = AMSP_UnLock_Waiting_Room(ams);
			CheckErr(err);

			err = AMSP_Start_Step_Negotiation(memid[i], req);
			CheckErr(err);

			/*
			 * Wait until the appointment is set by another thread
			 */
			err = AMSP_Wait_For_Appt_Step(ams, *my_appt[i]);
			CheckErr(err);

			err = AMSP_Worker_take_write_access_future(memid[i], (*my_appt)[i]->step);
			CheckErr(err);

			err = AMSP_IncrStep_AMS_Memory(memid+i, 1);
			CheckErr(err);

		} else {/* Uniprocessor. No processors synchronization */
			err = AMSP_Worker_take_write_access(memid[i]);
			CheckErr(err);
		}

	} /* For each memory */

    return AMS_ERROR_NONE;
}

int AMSP_Finish_synchronized_write_access(AMS_Memory *memid, int nbmem, Appointment **my_appt)
{
    AMS_Comm ams;
    int err, err1, i, nb_proc;

	for (i=0; i<nbmem; i++) {
    
		err = AMSP_Comm_AMS_Memory(memid[i], &ams);
		CheckErr(err);

		err = AMSP_Get_Nb_Proc_AMS_Comm(ams, &nb_proc);
		CheckErr(err);

		if (nb_proc > 1) {

			err = AMSP_Lock_Waiting_Room(ams);
			CheckErr(err);

			err1 = AMSP_Finish_Appt(ams, my_appt[i]);

			err = AMSP_UnLock_Waiting_Room(ams);
			CheckErr(err1);
			CheckErr(err);

			/* 
			 * Make sure that main thread is not waiting on a condition 
			 */
			err = AMSP_CondBroadcast_AMS_Memory(memid[i]);
			CheckErr(err);

			err = AMSP_Worker_grant_write_access(memid[i]);
			CheckErr(err);
		} else { /* Uniprocessor */
			err = AMSP_Worker_grant_write_access(memid[i]);
			CheckErr(err);
		}

	} /* For each memory */

	/* Free the memory allocated in Take_... */
	free(my_appt);

    return AMS_ERROR_NONE;
}

int AMSP_Finish_synchronized_read_access(AMS_Memory *memid, int nbmem, Appointment **my_appt)
{
    AMS_Comm ams;
    int err, err1, i, nb_proc;

	for (i=0; i<nbmem; i++) {
    
		err = AMSP_Comm_AMS_Memory(memid[i], &ams);
		CheckErr(err);

		err = AMSP_Get_Nb_Proc_AMS_Comm(ams, &nb_proc);
		CheckErr(err);

		if (nb_proc > 1) {
			err = AMSP_Lock_Waiting_Room(ams);
			CheckErr(err);

			err1 = AMSP_Finish_Appt(ams, my_appt[i]);

			err = AMSP_UnLock_Waiting_Room(ams);
			CheckErr(err1);
			CheckErr(err);

			/* 
			 * Make sure that main thread is not waiting on a condition 
			 */
			err = AMSP_CondBroadcast_AMS_Memory(memid[i]);
			CheckErr(err);

			err = AMSP_Worker_grant_read_access(memid[i]);
			CheckErr(err);
		} else { /* Uniprocessor */
			err = AMSP_Worker_grant_read_access(memid[i]);
			CheckErr(err);
		}

	} /* For each memory */

	/* Free the memory allocated in Take_... */
	free(my_appt);

    return AMS_ERROR_NONE;
}

int AMSP_Worker_take_read_access(AMS_Memory mem)
{
    return AMSP_Lock_Read_AMS_Memory(mem);
}

int AMSP_Worker_take_write_access(AMS_Memory mem)
{
    int err = AMSP_Lock_Write_AMS_Memory(mem);
    CheckErr(err);

    err = AMSP_IncrStep_AMS_Memory(&mem, 1);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

int AMSP_Worker_grant_read_access(AMS_Memory mem)
{
    return AMSP_UnLock_Read_AMS_Memory(mem);
}

int AMSP_Worker_grant_write_access(AMS_Memory mem)
{
    return AMSP_UnLock_Write_AMS_Memory(mem);
}

int AMSP_CondBroadcast_AMS_Memory(AMS_Memory memid)
{
	CheckMemory(memid);
    return AMSP_CondBroadcast_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

int AMSP_Set_Field_Func_C_AMS_Memory(AMS_Memory memid, const char *name,
                                AMS_C_Setter *SetValue)
{
    Field *fld;
    int err;

    CheckMemory(memid);

    fld = AMSP_Find_name_AMS_Memory(memid, name, &err);
    CheckErr(err);

    if(fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    AMSP_Set_Setter_Func_C(fld, SetValue);

    return AMS_ERROR_NONE;
}

int AMSP_Set_Field_Func_CPP_AMS_Memory(AMS_Memory memid, const char *name,
                                  AMS_CPP_Setter *SetValue, void *obj)
{
    Field *fld;
    int err;

    CheckMemory(memid);

    fld = AMSP_Find_name_AMS_Memory(memid, name, &err);
    CheckErr(err);

    if(fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    AMSP_Set_Setter_Func_CPP(fld, SetValue, obj);

    return AMS_ERROR_NONE;
}

int AMSP_Set_Field_Func_JAVA_AMS_Memory(AMS_Memory memid, const char *name,
                                   AMS_JAVA_Setter *SetValue,
                                   void *env, void *obj)
{
    Field *fld;
    int err;

    CheckMemory(memid);

    fld = AMSP_Find_name_AMS_Memory(memid, name, &err);
    CheckErr(err);

    if(fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    AMSP_Set_Setter_Func_JAVA(fld, SetValue, env, obj);

    return AMS_ERROR_NONE;
}

