#include <unistd.h>
#include <stdlib.h>
/* #include <malloc.h>*/
#include <memory.h>
#include <string.h>
#include <stdio.h>

#include "ams.h"
#include "acomm.h"
#include "netapi.h"
#include "netlib.h"
#include "sendfun.h"
#include "amsdefs.h"
#include "amisc.h"


/* 
 * An communicator is a computational context of one or several
 * processors or nodes. Each node is identified by a hostname and a port
 * number 
 */

/* 
 * Global variables that I will get rid off them one of these days
 */

/* Communicators' Table */
COMM_STRUCT *COMM_TBL[MAX_PUB][MAX_COMM];

/* Published Communicators */
int PUBCOMM_TBL[MAX_PUB][MAX_COMM];

/* Global variables used in the accessor (accessor) */
char * COMM_LIST[MAX_PUB][MAX_COMM];

/* Current Publisher's id */
int pub_id = 0;

/* Publisher's id initialized to NULL */
char * PUBLISHER_ID[MAX_PUB] = {NULL};

/* AMS Error message defined in accpub.c */
extern char ams_msg[];

#ifdef AMS_PUBLISHER
/* To synchronize the communicator table */
Mutex comm_pub_mutex;
int   comm_pub_mutex_created = 0;
#endif

/*@
        AMSP_New_Host_Node - Creates a communicator and initialize it with some default values
        
        Output Parameters:
                err_code - Error code return.

@*/
HOST_NODE *AMSP_New_Host_Node(int *err)
{
    HOST_NODE *host_node = (HOST_NODE *) malloc(sizeof(HOST_NODE));
    if (host_node == NULL) {
        *err = AMS_ERROR_INSUFFICIENT_MEMORY;
        return NULL;
    }

    host_node->port = -1;
    host_node->prev = NULL;
    host_node->next = NULL;
    memset(host_node->hostname, 0, 255);

    *err = AMS_ERROR_NONE;
    return host_node;
}

/*@
        Del_Host_Node - Frees the resources associated with the node
        
        Input Parameters:
                host_node - A node within the communicator

@*/
int AMSP_Del_Host_Node(HOST_NODE *host)
{
    int err = Validate(AMSP_Valid_Host_Node,host);
    CheckErr(err);

    free(host);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Create_Host_Node - Creates host_node and sets its the hostname and port number.
        
        Input Parameters:
+			host_node - Node to be created and initialized.
.			host - hostname
+			port - port number for network connection


@*/
HOST_NODE * AMSP_Create_Host_Node(const char *host, int port, int *err)
{
    HOST_NODE *host_node = AMSP_New_Host_Node(err);
    if (*err != AMS_ERROR_NONE)
        return NULL;

    if (port <= 0 && port != -1) {
        *err = AMS_ERROR_BAD_PORT_NUMBER;
        return NULL;
    }

    /* I doubt there will be hostname of 255 length */
    strncpy(host_node->hostname, host, 255);
    host_node->hostname[255] = 0;

    host_node->port = port;

    *err = AMS_ERROR_NONE;
    return host_node;
}


/*@
        AMSP_Find_Host_Comm_Struct - Given a hostname, the function checks if the
        host is in the Commnucation structure.

        Input Parameters:
+			com_str - A Communication structure
+			host - hostname to be searched
        Output Parameters:
+			found - Flag indicating the search status

@*/

int AMSP_Find_Host_Comm_Struct(COMM_STRUCT *com_str, const char *host, HOST_NODE **found)
{       
    HOST_NODE *host_node;
    /* use "first names" until we have fully qualified hostnames */
    char our_firstname[MAXHOSTLEN+1], their_firstname[MAXHOSTLEN+1];
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
                
    *found = NULL;
    host_node = com_str->host_node;

    strcpy(our_firstname, host);
    strtok(our_firstname, ".");

    while (host_node->next) {
        strcpy(their_firstname, host_node->hostname);
        strtok(their_firstname, ".");
        if (strlen(their_firstname) == strlen(our_firstname)
            && !strcmp(their_firstname,  our_firstname)) {
            *found = host_node;
            break;
        }
        host_node = host_node->next;
    } /* Should terminate unless there is a bug */

    return AMS_ERROR_NONE;
}

/*@
        AMSP_New_Comm_Struct - Creates an AMS communicator structure.
        
        Input Parameters:
			com_str - Structure to be initialized.

@*/
int AMSP_New_Comm_Struct(const char *name, AMS_Comm_type ctype, AMS_Comm *ams)
{
    int i;
    COMM_STRUCT *com_str;
    HOST_NODE *tail_host;
    MEM_NODE *tail_mem;

    /* Init communication table if it is the first time */
    int err = AMSP_Init_Comm_Tbl();
    CheckErr(err);

    /* Lock the COMM_TBL table for modif */
    err = AMSP_Lock_TBL_Write_Comm_Struct();
    CheckErr(err);

    /* First, find an entry table */
    for (i=0; i<MAX_COMM; i++)
        if (COMM_TBL[pub_id][i] == NULL)
            break;
        
    if (i == MAX_COMM)
        return AMS_ERROR_COMM_TBL_FULL;

    /* Allocate the communication structure */
    com_str = (COMM_STRUCT *) malloc(sizeof(COMM_STRUCT));
    if (com_str == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Give it a name */
    com_str->name = (char *) malloc((strlen(name)+1)*sizeof(char));
    if (com_str->name == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;
    strcpy(com_str->name, name);

    com_str->ctype = ctype;

    /* Create Tails of the list */
    tail_host = AMSP_New_Host_Node(&err);
    CheckErr(err);

    tail_mem = AMSP_New_Mem_Node(&err);
    CheckErr(err);


    /* Initialize the structure */

    com_str->nb_proc = 0;
    com_str->nb_mem = 0;
    com_str->TailHostNode = tail_host;
    com_str->TailMemNode = tail_mem;
    com_str->mem_node = tail_mem;
    com_str->host_node = tail_host;

#ifdef AMS_PUBLISHER
    com_str->waiting_room = NULL;
    AMSP_NewMutex(&(com_str->waiting_room_lock));

    /* Create a mutex */
    AMSP_NewMutex(&(com_str->comm_mutex));

    /* Initialize the table */
    COMM_TBL[pub_id][i] = com_str;
    *ams = (AMS_Comm) i;
#else
    /* The entry is already know */
    CheckComm(*ams);
    COMM_TBL[pub_id][*ams] = com_str;
#endif

    /* Release the Lock on the Table */
    err = AMSP_UnLock_TBL_Write_Comm_Struct();
    CheckErr(err);

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Del_Comm_Struct - a Deletes a communication structure and frees associated resources.
        
        Input Parameters:
			com_str - Structure to be initialized.

@*/
int AMSP_Del_Comm_Struct(COMM_STRUCT *com_str)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    err = AMSP_Del_Host_Node(com_str->TailHostNode);
    CheckErr(err);

    err = AMSP_Del_Mem_Node(com_str->TailMemNode);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    AMSP_DelMutex(&(com_str->waiting_room_lock));
    AMSP_DelMutex(&(com_str->comm_mutex));
#endif

    free(com_str->name);
    free(com_str);

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Lock_TBL_Write_Comm_Struct - Lock the Communicators' table 
@*/
int AMSP_Lock_TBL_Write_Comm_Struct(void)
{
    /* This function will abort if there is an error */
#ifdef AMS_PUBLISHER
    AMSP_LockMutexWrite(&comm_pub_mutex);
#endif

    return AMS_ERROR_NONE;
}

int AMSP_Lock_TBL_Read_Comm_Struct(void)
{
    /* This function will abort if there is an error */
#ifdef AMS_PUBLISHER
    AMSP_LockMutexRead(&comm_pub_mutex);
#endif

    return AMS_ERROR_NONE;
}

/*@
    AMSP_UnLock_TBL_Write_Comm_Struct - Unlock the Communicators' table 
@*/
int AMSP_UnLock_TBL_Write_Comm_Struct()
{
    /* This function will abort if there is an error */
#ifdef AMS_PUBLISHER
    AMSP_RelMutexWrite(&comm_pub_mutex);
#endif

    return AMS_ERROR_NONE;
}

/*@
	AMSP_UnLock_TBL_Read_Comm_Struct - Unlock the Communicators' table 
@*/
int AMSP_UnLock_TBL_Read_Comm_Struct(void)
{
    /* This function will abort if there is an error */
#ifdef AMS_PUBLISHER
    AMSP_RelMutexRead(&comm_pub_mutex);
#endif

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Find_Mem_Node_Comm_Struct - Given a memory node, the function checks if the
        node is in the Communication structure.

        Input Parameters:
+			com_str - A Communication structure
+			mem - Memory node to be searched
        Output Parameters:
                mem_node - pointer to the node if found, NULL otherwise

@*/

int AMSP_Find_Mem_node_Comm_Struct(COMM_STRUCT *com_str, MEM_NODE *mem, MEM_NODE **mem_node)
{       
    MEM_NODE *node;
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
                
    err = Validate(AMSP_Valid_Mem_Node, mem);
    CheckErr(err);

    *mem_node = NULL;

    node = com_str->mem_node;

    while (node->next) {
        switch ((err = AMSP_Compare_Mem_Node(mem, node))) {
        case AMS_IDENTICAL_MEMORY:
            *mem_node = node;
            break;
        case AMS_DIFFERENT_MEMORY:
            break;
        default:
            return err;
        }

        if (*mem_node)
            break;

        node = node->next;
    } /* Should terminate unless I am a dummy */

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Find_Mem_name_Comm_Struct - Given a memory name, the function checks if the
        memory is in the Commnucation structure (has been published).

        Input Parameters:
+			com_str - A Communication structure
+			name - memory name to search
        Output Parameters:
+			mem - memory id
+			found - Flag indicating the search result

@*/

int AMSP_Find_Mem_name_Comm_Struct(COMM_STRUCT *com_str, const char *name, AMS_Memory *mem, MEM_NODE **node)
{       
    MEM_NODE *mem_node;
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
                
    mem_node = com_str->mem_node;

    while (mem_node->next) {
        if (!strcmp(name, mem_node->amem->name)) {
            *node = mem_node;
            *mem = mem_node->memid;
            break;
        }
        mem_node = mem_node->next;
    } /* Should terminate unless there is a bug */

    if (mem_node->next == NULL)
        *node = NULL;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Find_Mem_id_Comm_Struct - Given a memory id, the function checks if the
        memory id is in the Commnucation structure.

        Input Parameters:
+			com_str - A Communication structure
+			id - memory structure id to search
        Output Parameters:
+			node - Memory node (NULL if the id is not found)

@*/

int AMSP_Find_Mem_id_Comm_Struct(COMM_STRUCT *com_str, AMS_Memory id, MEM_NODE **node)
{       
    MEM_NODE *mem_node;
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
                
    mem_node = com_str->mem_node;

    while (mem_node->next) {
        if (mem_node->memid == id) {
            *node = mem_node;
            break;
        }
        mem_node = mem_node->next;
    } /* Should terminate unless there is a bug */

    if (mem_node->next == NULL)
        *node = NULL;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Init_Comm_Tbl - Initializes the communication table. This 
        function is not thread-safe.
        
@*/
int AMSP_Init_Comm_Tbl(void )
{
    int i;
    static int firstime = 1;

#ifdef AMS_PUBLISHER
    int err;
#endif

    if (firstime) {
        /* 
         * 007: there is a potential conflict if two threads
         * enter here at the same time. We will look at it later.
         */

        firstime = 0;
#ifdef AMS_PUBLISHER
        AMSP_NewMutex(&comm_pub_mutex);
        comm_pub_mutex_created = 1;
        err = AMSP_Lock_TBL_Write_Comm_Struct();
        CheckErr(err);
#endif

        for (i=0; i<MAX_COMM; i++) {
            int j;
            for (j=0; j<MAX_PUB;j++) {

                COMM_TBL[j][i] = NULL;
                PUBCOMM_TBL[j][i] = -1;
            }
        }

#ifdef AMS_PUBLISHER
        err = AMSP_UnLock_TBL_Write_Comm_Struct();
        CheckErr(err);
#endif
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_Comm_Tbl - Assign a value to a table entry
@*/

int AMSP_Set_Comm_Tbl(int id, COMM_STRUCT *com_str)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    CheckComm(id);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_TBL_Write_Comm_Struct();
    CheckErr(err);
#endif

    COMM_TBL[pub_id][id] = com_str;

#ifdef AMS_PUBLISHER
    err = AMSP_UnLock_TBL_Write_Comm_Struct();
    CheckErr(err);
#endif

    return err;
}


/*@
        AMSP_Get_Comm_Tbl - Get a value within the table
@*/

int AMSP_Get_Comm_Tbl(int id, COMM_STRUCT **com_str)
{
    int err = AMS_ERROR_NONE;

    CheckComm(id);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_TBL_Read_Comm_Struct();
    CheckErr(err);
#endif

    *com_str = COMM_TBL[pub_id][id];

#ifdef AMS_PUBLISHER
    err = AMSP_UnLock_TBL_Read_Comm_Struct();
    CheckErr(err);
#endif

    return err;
}


/*@
        AMSP_Publish_Comm_Struct - Connects to the daemon to publish the communicator
        
        Input Parameters:
+			com_str - Communication structure

@*/
int AMSP_Publish_Comm_Struct(AMS_Comm ams)
{
    int err;
    COMM_STRUCT *com_str;

    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_TBL_Write_Comm_Struct();
    CheckErr(err);
#endif

    if (PUBCOMM_TBL[pub_id][ams] != -1)
        return AMS_ERROR_SYSTEM;

    PUBCOMM_TBL[pub_id][ams] = 1;

#ifdef AMS_PUBLISHER
    err = AMSP_UnLock_TBL_Write_Comm_Struct();
    CheckErr(err);
#endif

    return AMS_ERROR_NONE;
}

/*@
        AMSP_UnPublish_Comm_Struct - Deletes a communication structure from the table
        
        Input Parameters:
+			com_str - Coomunication structure

@*/
int AMSP_UnPublish_Comm_Struct(AMS_Comm ams)
{
    int err;
    COMM_STRUCT *com_str;

    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    /* Lock the table */
    err = AMSP_Lock_TBL_Write_Comm_Struct();
    CheckErr(err);
#endif

    /* Modify the table */
    PUBCOMM_TBL[pub_id][ams] = -1;

#ifdef AMS_PUBLISHER
    /* UnLock the table */
    err = AMSP_UnLock_TBL_Write_Comm_Struct();
    CheckErr(err);
#endif

    return AMS_ERROR_NONE;

}

/*@
        AMSP_Add_Host_Node_Comm_Struct - Attaches a host_node to a communication structure
        
        Input Parameters:
+			com_str - Communication structure
+			host_node - HOST_NODE to add

@*/
int AMSP_Add_Host_Node_Comm_Struct(COMM_STRUCT *com_str, HOST_NODE *host_node)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);

    /* Check the validity of the arguments */
    CheckErr(err);
                
    err = Validate(AMSP_Valid_Host_Node, host_node);
    CheckErr(err);
                
        
    /* Appends a new field at the end */
    if (com_str->host_node == com_str->TailHostNode) {
        /* Empty list, add the new field at the beginning */
        host_node->next = com_str->TailHostNode;
        host_node->prev = NULL;
        com_str->host_node = host_node;
        com_str->TailHostNode->prev = host_node;
    } else {
        /* Append at the end */
        host_node->next = com_str->TailHostNode;
        host_node->prev = com_str->TailHostNode->prev;
        host_node->prev->next = host_node;
        com_str->TailHostNode->prev = host_node;
    }

    com_str->nb_proc += 1;

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Del_Host_Node_Comm_Struct - Deletes a host_node from a communication structure.
        
        Input Parameters:
+			com_str - Communication structure
+			host_node - Node to delete

@*/
int AMSP_Del_Host_Node_Comm_Struct(COMM_STRUCT *com_str, HOST_NODE *host_node)
{
    return AMS_ERROR_FUNCT_NOTIMPLEMENTED;
}

/*@
        AMSP_Del_Host_Node_Comm_Struct - Deletes a host_node from a communication structure.
        
        Input Parameters:
+			com_str - Communication structure
+			host_node - Node to delete

@*/
int AMSP_DelAll_Host_Comm_Struct(COMM_STRUCT *com_str)
{
    HOST_NODE *host;
    int err = Validate(AMSP_Valid_Comm_Struct,com_str), nb_host = 0;
    CheckErr(err);

    host = com_str->host_node;
    while (host->next) {
        host = host->next;
        err = AMSP_Del_Host_Node(host->prev);
        CheckErr(err);
        nb_host++;
    }

    /* Reconnect the tail */
    com_str->host_node = host; 

    /* Just Checking */
    if (nb_host != com_str->nb_proc)
        return AMS_ERROR_SYSTEM;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Add_Mem_Node_Comm_Struct - Attaches a mem_node to a communication structure
        
        Input Parameters:
+			com_str - Communication structure
+			mem_node - Memory node to add

@*/
int AMSP_Add_Mem_Node_Comm_Struct(COMM_STRUCT *com_str, MEM_NODE *mem_node)
{
    int err = Validate(AMSP_Valid_Comm_Struct,com_str);
    MEM_NODE *node;

    /* Check the validity of the arguments */
    CheckErr(err);

    err = Validate(AMSP_Valid_Mem_Node, mem_node);
    CheckErr(err);
        
    /* Make sure the mem_node is not already in the communicator */
    err = AMSP_Find_Mem_node_Comm_Struct(com_str, mem_node, &node);
    CheckErr(err);

    if (node)
        return AMS_ERROR_NODE_ALREADY_IN_COMM;

    /* Appends a new node at the end */
    if (com_str->mem_node == com_str->TailMemNode) {
        /* Empty list, add the new node at the beginning */
        mem_node->next = com_str->TailMemNode;
        mem_node->prev = NULL;
        com_str->mem_node = mem_node;
        com_str->TailMemNode->prev = mem_node;
    } else {
        /* Append at the end */
        mem_node->next = com_str->TailMemNode;
        mem_node->prev = com_str->TailMemNode->prev;
        mem_node->prev->next = mem_node;
        com_str->TailMemNode->prev = mem_node;
    }

    com_str->nb_mem += 1;

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Del_Mem_Node_Comm_Struct - Deletes a mem_node from a communication structure.
        
        Input Parameters:
+			com_str - Communication structure
+			mem_node - Node to delete

@*/
int AMSP_Del_Mem_Node_Comm_Struct(COMM_STRUCT *com_str, MEM_NODE *mem_node)
{
    MEM_NODE *node;
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    /* Find the memory node */
    err = AMSP_Find_Mem_node_Comm_Struct(com_str, mem_node, &node);
    CheckErr(err);

    if (!node)
        return AMS_ERROR_MEMORY_NODE_NOT_IN_COMMUNICATOR;

    /* Delete from the beginning */
    if (node->prev == NULL) {
        com_str->mem_node = node->next;
        node->next->prev = NULL;
    } else {
        node->prev->next = node->next;
        node->next->prev = node->prev;
    }
    com_str->nb_mem -= 1;

    err = AMSP_Del_Mem_Node(node);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_DelAll_Mem_Comm_Struct - Deletes all nodes from a communication structure.
        
        Input Parameters:
+			com_str - Communication structure

@*/
int AMSP_DelAll_Mem_Comm_Struct(COMM_STRUCT *com_str)
{

    MEM_NODE *mem;
    int err = Validate(AMSP_Valid_Comm_Struct, com_str), nb_mem = 0;
    CheckErr(err);

    mem = com_str->mem_node;
    while (mem->next) {
        mem = mem->next;
        err = AMSP_Del_Mem_Node(mem->prev);
        CheckErr(err);
        nb_mem++;
    }

    /* Reconnect the tail */
    com_str->mem_node = mem; 

    if (nb_mem != com_str->nb_mem)
        return AMS_ERROR_SYSTEM;

    return AMS_ERROR_NONE;
}

/*@
    AMSP_Get_Comm_type_Comm_Struct - Gets a communication type
        
    Input Parameter:
+   com_str - Communication structure
    Output Parameter:
+   ctype - Pointer to the communicator type (MPI_TYPE, NODE_TYPE)

@*/
int AMSP_Get_Comm_type_Comm_Struct(COMM_STRUCT *com_str, AMS_Comm_type *ctype)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    *ctype = com_str->ctype;

    return AMS_ERROR_NONE;
}


/* Interface to AMS_Comm functions */
int AMSP_New_AMS_Comm(const char *name, AMS_Comm_type ctype, AMS_Comm *ams)
{
    return AMSP_New_Comm_Struct(name, ctype, ams);
}

/*
 * Given an AMS_Comm, this function returns a Comm Structure
 */
int AMSP_Get_Comm_struct_AMS_Comm(AMS_Comm ams, COMM_STRUCT **com_str)
{

    /* Lock the table */
    int err;
    CheckComm(ams);

    err = AMSP_Lock_TBL_Read_Comm_Struct();
    CheckErr(err);

    *com_str = COMM_TBL[pub_id][ams];

    /* UnLock the table */
    err = AMSP_UnLock_TBL_Read_Comm_Struct();
    CheckErr(err);

    return AMS_ERROR_NONE;
}


int AMSP_Publish_AMS_Comm(AMS_Comm ams)
{
    CheckComm(ams);
    return AMSP_Publish_Comm_Struct(ams);
}

int AMSP_UnPublish_AMS_Comm(AMS_Comm ams)
{
    CheckComm(ams);
    return AMSP_UnPublish_Comm_Struct(ams);
}
/*
 * Check if the communicator is currently published 
 */
/* NOTE: This function need not be called by a communicator thread! */
int AMSP_IsPublished_AMS_Comm(AMS_Comm ams, int lock, int *flag)
{

#ifdef AMS_PUBLISHER
    int err;
    if (lock != AMS_DONOT_LOCK) {
        err = AMSP_Lock_TBL_Read_Comm_Struct();
        CheckErr(err);
    }

    if (PUBCOMM_TBL[pub_id][ams] == 1)
        *flag = TRUE;
    else
        *flag = FALSE;

    if (lock == AMS_RELEASE_LOCK || *flag == FALSE)
        if (lock != AMS_DONOT_LOCK)
            AMSP_UnLock_TBL_Read_Comm_Struct();
        
#endif
    return AMS_ERROR_NONE;

}


int AMSP_Add_Host_Node_AMS_Comm(AMS_Comm ams, HOST_NODE *host)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_Write_AMS_Comm(ams);
    CheckErr(err);
#endif

    err = AMSP_Add_Host_Node_Comm_Struct(com_str, host);

#ifdef AMS_PUBLISHER
    AMSP_UnLock_Write_AMS_Comm(ams);
#endif

    return err;
}

int AMSP_Del_Host_Node_AMS_Comm(AMS_Comm ams, HOST_NODE *host)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_Write_AMS_Comm(ams);
    CheckErr(err);
#endif

    err = AMSP_Del_Host_Node_Comm_Struct(com_str, host);

#ifdef AMS_PUBLISHER
    AMSP_UnLock_Write_AMS_Comm(ams);
#endif

    return err;
}

int AMSP_Add_Mem_Node_AMS_Comm(AMS_Comm ams, MEM_NODE *mem)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_Write_AMS_Comm(ams);
    CheckErr(err);
#endif

    err = AMSP_Add_Mem_Node_Comm_Struct(com_str, mem);

#ifdef AMS_PUBLISHER
    AMSP_UnLock_Write_AMS_Comm(ams);
#endif

    return err;

}

int AMSP_Find_Mem_name_AMS_Comm(AMS_Comm ams, const char *name, AMS_Memory *mem, MEM_NODE **mem_node)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
    return AMSP_Find_Mem_name_Comm_Struct(com_str, name, mem, mem_node);
}

int AMSP_Find_Mem_node_AMS_Comm(AMS_Comm ams, MEM_NODE *mem_node, MEM_NODE **node)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
    return AMSP_Find_Mem_node_Comm_Struct(com_str, mem_node, node);
}

int AMSP_Find_Mem_id_AMS_Comm(AMS_Comm ams, AMS_Memory mem, MEM_NODE **node)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
    return AMSP_Find_Mem_id_Comm_Struct(com_str, mem, node);
}

int AMSP_Del_Mem_Node_AMS_Comm(AMS_Comm ams, MEM_NODE *mem_node)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
    return AMSP_Del_Mem_Node_Comm_Struct(com_str, mem_node);
}

int AMSP_DelAll_Host_AMS_Comm(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
    return      AMSP_DelAll_Host_Comm_Struct(com_str);
}

int AMSP_DelAll_Mem_AMS_Comm(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_Write_AMS_Comm(ams);
    CheckErr(err);
#endif

    err = AMSP_DelAll_Mem_Comm_Struct(com_str);

#ifdef AMS_PUBLISHER
    AMSP_UnLock_Write_AMS_Comm(ams);
#endif

    return err;
}
        
int AMSP_Del_AMS_Comm(ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    err = AMSP_Del_Comm_Struct(com_str);
    CheckErr(err);

    /* Lock the table */
    err = AMSP_Lock_TBL_Write_Comm_Struct();
    CheckErr(err);

    /* Modify the table */
    COMM_TBL[pub_id][ams] = NULL;

    /* UnLock the table */
    err = AMSP_UnLock_TBL_Write_Comm_Struct();
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*
 * Given a Communicator name, this function returns its id
 */
int AMSP_Get_AMS_Comm(const char *name, AMS_Comm *ams)
{
    int id, published = 0, err;
    COMM_STRUCT *com_str;

    /* Lock the table */
    err = AMSP_Lock_TBL_Read_Comm_Struct();
    CheckErr(err);

    for (id = 0; id < MAX_COMM; id++) {
      if ((com_str = COMM_TBL[pub_id][id]))
            if (!strcmp(com_str->name, name)) {
                *ams = id;
                published = PUBCOMM_TBL[pub_id][id];
                break;
            }
    }

    /* UnLock the table */
    err = AMSP_UnLock_TBL_Read_Comm_Struct();
    CheckErr(err);

    if (id == MAX_COMM)
        return AMS_ERROR_COMM_NAME_NOT_FOUND;
        
    if (published == 0)
        return AMS_ERROR_COMM_NOT_PUBLISHED;

    return AMS_ERROR_NONE;
}

/*
 * Given a communicator id, this function returns its type (MPI or NODE)
 *
 */
int AMSP_Get_Comm_type_AMS_Comm(AMS_Comm ams, AMS_Comm_type *ctype)
{
    COMM_STRUCT *com_str;

    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    return AMSP_Get_Comm_type_Comm_Struct(com_str, ctype);
}

/*
 * Given a communicator id, this function returns the list
 * of memory names attached to the communicator

   Each entry is seperated from the previous by a 0 entry in the char string
 */
int AMSP_Get_Memory_list_AMS_Comm(AMS_Comm ams, char **mem_list, int *list_len)
{

    COMM_STRUCT *com_str;
    MEM_NODE *mem_node;

    int flag, len, err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_Read_AMS_Comm(ams);
    CheckErr(err);
#endif

    /* Length */
    mem_node = com_str->mem_node;
    len = 0;
    while(mem_node->next) {
        if (mem_node->amem == NULL)
            return AMS_ERROR_SYSTEM;

        /* Do not lock. Already locked */
        err = AMSP_IsPublished_AMS_Memory(&(mem_node->memid), 1, AMS_DONOT_LOCK, &flag);
        CheckErr(err);

        if (flag)
            len += strlen(mem_node->amem->name) + 1; /* +1 for zero terminator */
        mem_node = mem_node->next;
    }

    /* Memory allocation */
    *mem_list = (char *) malloc(len * sizeof(char));
    if (*mem_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Data */
    mem_node = com_str->mem_node;
    *list_len = 0;
    while(mem_node->next) {
        if (mem_node->amem == NULL)
            return AMS_ERROR_SYSTEM;

        /* Do not lock. Already locked */
        err = AMSP_IsPublished_AMS_Memory(&(mem_node->memid), 1, AMS_DONOT_LOCK, &flag);
        CheckErr(err);

        if (flag) {
            strcpy(*mem_list + *list_len, mem_node->amem->name);
            *list_len += strlen(mem_node->amem->name) + 1; /* +1 for zero terminator */
        }
        mem_node = mem_node->next;
    }


#ifdef AMS_PUBLISHER
    AMSP_UnLock_Read_AMS_Comm(ams);
#endif

    if (len != *list_len)
        AMSP_err_quit("Get_Memory_list_AMS_Comm: Length calculated differs from length sent");

    return AMS_ERROR_NONE;
}

/*
 * Given a communicator id, this function returns the list
 * of host names and port# used within the communicator
 */
int AMSP_Get_Host_list_AMS_Comm(AMS_Comm ams, char **host_list, int *list_len)
{

    COMM_STRUCT *com_str;
    HOST_NODE *host_node;
    char tmp[20];

    int flag, err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    /* Necessary since we're the publisher thread */
    err = AMSP_IsPublished_AMS_Comm(ams, AMS_RELEASE_LOCK, &flag);
    CheckErr(err);

    if (!flag)
        return AMS_ERROR_COMMUNICATOR_NO_LONGER_ALIVE;

#ifdef AMS_PUBLISHER
    err = AMSP_Lock_Read_AMS_Comm(ams);
    CheckErr(err);
#endif

    /* Length */
    host_node = com_str->host_node;
    *list_len = 0;
    while(host_node->next) {
        sprintf(tmp, "%d", host_node->port);
        *list_len += strlen(host_node->hostname) + strlen(tmp) + 2; /* +2 for zero terminator and "|" */
        host_node = host_node->next;
    }

    /* Allocated Memory: the caller is responsible of freeing it */
    *host_list = (char *)malloc(*list_len * sizeof(char));
    if (*host_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Data */
    host_node = com_str->host_node;
    *list_len = 0;
    while(host_node->next) {
        strcpy(*host_list + *list_len, host_node->hostname);
        strcat(*host_list + *list_len, "|");
        sprintf(tmp, "%d", host_node->port);
        strcat(*host_list + *list_len, tmp);
        *list_len += strlen(host_node->hostname) + strlen(tmp) + 2; /* +2 for zero terminator and "|" */
        host_node = host_node->next;
    }

#ifdef AMS_PUBLISHER
    AMSP_UnLock_Read_AMS_Comm(ams);
#endif

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_publisher_id - Find the hostname and port number in the Publishers'
        table and sets the current publisher id to the the index that was found.

        Input parameters:
+			host - Publisher's host name
+			port - Publisher's port number

@*/

int AMSP_Set_publisher_id(const char *host, int port)
{
    int i;
    char s_port[20];

    /* Convert port number to string */
    sprintf(s_port, "%d", port);

    for (i=0; i<MAX_PUB; i++) {
        if (PUBLISHER_ID[i])
            if (strstr(PUBLISHER_ID[i], host) && strstr(PUBLISHER_ID[i], s_port)) {
                pub_id = i;
                break;
            }
    }

    if (i == MAX_PUB) {
        /* Did not find anything, add a new entry */
        for (i=0; i<MAX_PUB; i++)
            if (PUBLISHER_ID[i] == NULL) {
                int len = strlen(host) + strlen(s_port) + 2;
                PUBLISHER_ID[i] = (char *) malloc (len * sizeof(char));
                if (PUBLISHER_ID[i] == NULL)
                    return AMS_ERROR_INSUFFICIENT_MEMORY;

                strcpy(PUBLISHER_ID[i], host);
                strcat(PUBLISHER_ID[i], "|");
                strcat(PUBLISHER_ID[i], s_port);
                pub_id = i;
                break;
            }
    }

    if (i == MAX_PUB)
        return AMS_ERROR_PUB_TBL_FULL; /* Maximum number of the publishers */

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Get_publisher_id - Find the hostname and port number in the Communicators
        list and sets the current publisher id to the the index that was found.

        Input parameters:
+			host - Publisher's host name
+			port - Publisher's port number

@*/

int AMSP_Get_publisher_id(const char *host, int port)
{
    int i, j, done = 0;
    char s_port[20];

    /* Convert port number to string */
    sprintf(s_port, "%d", port);

    for (i=0; i<MAX_PUB && !done; i++) {
        for (j=0; j<MAX_COMM && !done; j++) {
            if (COMM_LIST[i][j])
                if (strstr(COMM_LIST[i][j], host) && strstr(COMM_LIST[i][j], s_port)) {
                    pub_id = i;
                    done = 1;
                }
        }
    }


    if (i == MAX_PUB) {
        /* Did not find a publisher id for this communicator */
        return AMS_ERROR_PUBID_NOT_FOUND;
    }

    return AMS_ERROR_NONE;
}


#ifdef _DEBUG

/*@
        AMSP_Valid_Host_Node - Checks the validity of a Node

        Input Parameters:
+			host_node - Node to be validated

@*/
int AMSP_Valid_Host_Node(HOST_NODE *host_node)
{

    if (host_node == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (host_node->port != -1 && host_node->port < 0)
        return AMS_ERROR_BAD_ARGUMENT;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Valid_Comm_Struct - Checks the validity of a communication structure

        Input Parameters:
+			com_str - Communication structure to be validated

@*/
int AMSP_Valid_Comm_Struct(COMM_STRUCT *com_str)
{

    int err;
    if (com_str == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (com_str->nb_proc < 0)
        return AMS_ERROR_BAD_ARGUMENT;

    if (com_str->nb_mem < 0)
        return AMS_ERROR_BAD_ARGUMENT;

    err = Validate(AMSP_Valid_Host_Node, com_str->TailHostNode);
    CheckErr(err);

    err = Validate(AMSP_Valid_Host_Node, com_str->host_node);
    CheckErr(err);

    err = Validate(AMSP_Valid_Mem_Node, com_str->TailMemNode);
    CheckErr(err);

    err = Validate(AMSP_Valid_Mem_Node, com_str->mem_node);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

#endif

