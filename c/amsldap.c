/*
 * This module contains AMS Interface to LDAP. It is used to add, query
 * and delete AMS objects from an LDAP publisher
 */

#ifdef AMS_LDAP
#include "acomm.h"
#include "amsldap.h"
#include "netlib.h"

static void free_mods( LDAPMod **, int );


/*@
    AMSP_Ldap_init - Get a handle to an LDAP connection. If the environment
                     variable AMS_LDAP_PUBLISHER is defined, the connection is
                     directed to that value. Otherwise the default publisher 
                     defined in the header file amsldap.h is used. To also
                     change the default port number, use AMS_LDAP_PORT
                     environment variable.
    
    Output Parameters:
    ld - An address of a pointer to an LDAP handle


@*/

int AMSP_Ldap_init(LDAP **ld)
{
    char *lhost = getenv("AMS_LDAP_PUBLISHER"), *ldap_host;
    char *lport = getenv("AMS_LDAP_PORT");
    int ldap_port;

    /* Check environment variable for LDAP host */
    if (lhost)
        ldap_host = lhost;
    else
        ldap_host = MY_HOST;

    /* Check environment variable for LDAP port */
    if (lport) {
        ldap_port = atoi(lport);

    } else {
        ldap_port = MY_PORT;
    }

    /* get a handle to an LDAP connection */
    if ( (*ld = ldap_init( ldap_host, ldap_port )) == NULL ) {
        ldap_perror(*ld, "ldap_init failed");
	    AMSP_err_quit("");
    }

    /* authenticate to the directory as the Directory Manager */
    if ( ldap_simple_bind_s( *ld, MGR_DN, MGR_PW ) != LDAP_SUCCESS ) {
        ldap_perror(*ld, "ldap_simple_bind_s failedd:");
	    AMSP_err_quit("");
	    return AMS_ERROR_LDAP_BIND;
    }

    return AMS_ERROR_NONE;
}

/*@
    AMSP_Ldap_disconnect - Unbind an LDAP publisher
    
    Input Parameters:
+   ld - Pointer to an LDAP publisher

@*/

int AMSP_Ldap_disconnect(LDAP *ld)
{
    if (ldap_unbind( ld ) < 0)
        return AMS_ERROR_LDAP_UNBIND;

    return AMS_ERROR_NONE;
}


/*@
    AMSP_Ldap_del_object - Deletes an LDAP object
    
    Input Parameters:
+   ld - Pointer to an LDAP publisher
+   dn - Direcotry Name for the object to be deleted

@*/

int AMSP_Ldap_del_object(LDAP *ld, char *dn)
{
    int		rc, nb_entries = 0, i, err;
    char  **list, **buff;

    /* Initiate the delete operation */
    if (( rc = ldap_delete_s( ld, dn )) < 0 ) {
	    ldap_perror( ld, "ldap_delete" );
	    return AMS_ERROR_LDAP_DELETE;
    }

    switch (rc) {
    case LDAP_SUCCESS:
        break;
    case LDAP_NOT_ALLOWED_ON_NONLEAF:
        /* This object is not a leaf. We should recursively delete the children */
        err = AMSP_Ldap_search_object(ld, dn, "(cn=*)", &list, &nb_entries);
        CheckErr(err);

        if (nb_entries) {
            buff = (char **) malloc(nb_entries * sizeof(char *));
            if (buff == NULL)
                return AMS_ERROR_INSUFFICIENT_MEMORY;
        }

        /* Transfer the data since we are making a recusive calls */
        for (i=0; i<nb_entries; i++) {
            buff[i] = (char *) malloc(sizeof(char *) * (strlen(list[i]) + 1));
            if (buff[i] == NULL)
                return AMS_ERROR_INSUFFICIENT_MEMORY;

            strcpy(buff[i], list[i]);
        }

        /* Recursively delete all the children */
        for (i=0; i<nb_entries; i++) {
            err = AMSP_Ldap_del_object(ld, buff[i]);
            CheckErr(err);
        }

         /* Free the memory */
        for (i=0; i<nb_entries; i++)
            free(buff[i]);
        free(buff);

        /* Finally delete this object */
        err = AMSP_Ldap_del_object(ld, dn);
        CheckErr(err);

        break;

    default:
        /* some error occurred */
	    AMSP_err_dump("Error while deleting entry: %s\n", ldap_err2string( rc ));
    }
    
    return AMS_ERROR_NONE;

}

/*@
    AMSP_Ldap_search_object - Search ldap objects
    
    Input Parameters:
+   ld - Pointer to an LDAP publisher.
.   base - dn for the base search.
.   filter - filter for the search. Use (cn=*) to display all children.
.   list - dn list of objects that matches the search criteria.
+   nb_entries - Number of the entries retrieved.


@*/
int AMSP_Ldap_search_object(LDAP *ld, char *base, char *filter, char ***list, int *nb_entries)
{

#define MAX_ENTRIES 200

    LDAPMessage *result, *e;
    char    *dn;
    static char	*vals[MAX_ENTRIES] ={NULL}, *matched_msg = NULL, *error_msg = NULL;
    int	i, rc, parse_rc, msgtype, num_refs;
    int	num_entries = 0, max_entries = MAX_ENTRIES; /* Should be enough */
    LDAPControl **publisherctrls;


    *list = NULL;  /* Initialize to no found */
     
    for (i=0; i<max_entries; i++) {
        if (vals[i]) {
            free(vals[i]);
            vals[i] = NULL;
        }
    }  /* Initialize new search */


    /* search for all objects */
    if (( rc = ldap_search_s( ld, base, LDAP_SCOPE_ONELEVEL,
	    filter, NULL, 0, &result)) != LDAP_SUCCESS ) {
        ldap_perror( ld, "ldap_search");
	AMSP_err_quit("ldap_search" );
    }

    /* How many did we find ? */
    *nb_entries = ldap_count_entries(ld, result);
    num_refs = ldap_count_references(ld, result);

    /* Iterate through results */
    for (e = ldap_first_message(ld, result); e != NULL ; e = ldap_next_message(ld, e) ) {
        msgtype = ldap_msgtype(e);
	switch ( msgtype ) {

	case LDAP_RES_SEARCH_ENTRY:
	    if (( dn = ldap_get_dn( ld, e )) != NULL ) {
                vals[num_entries] = (char *) realloc(vals[num_entries], strlen(dn)+1);
                if (vals[num_entries] == NULL)
                    return AMS_ERROR_INSUFFICIENT_MEMORY;

                strcpy(vals[num_entries++], dn);
		ldap_memfree( dn );

            } else {
                return AMS_ERROR_LDAP_SYSTEM;
            }
            break;

        case LDAP_RES_SEARCH_REFERENCE:
            AMS_Print("Found a reference. Should follow it \n");
            break;

        case LDAP_RES_SEARCH_RESULT:

#ifdef _WIN_ALICE
            /* I cannot figure out this does not work for Solaris */
            /* Final result received from the publisher */
            parse_rc = ldap_parse_result(ld, e, &rc, &matched_msg, &error_msg, NULL, &publisherctrls, 0);
            if (parse_rc != LDAP_SUCCESS)
                AMSP_err_quit("ldap_parse_result: %s\n", ldap_err2string(parse_rc));

            if (rc != LDAP_SUCCESS)
                 AMSP_err_quit("ldap_search_s: %s\n", ldap_err2string(rc));
#endif
            /* Search completed */
            if (num_entries )
                *list = vals;
            else
                *list = NULL;

            *nb_entries = num_entries;
            num_entries++; /* To exit the loop */
                                  
            break;
        default:
            break;    
        }
    }

    return AMS_ERROR_NONE;
}

/*@
    AMSP_Ldap_load_publishers - Loads a list of publisher from an LDAP publisher
    
    Input Parameters:
+   ld - Pointer to an LDAP publisher.
.   base - dn for the base search.
.   filter - filter for the search. Use (cn=*) to display all children.
.   list - dn list of objects that matches the search criteria.
+   nb_entries - Number of the entries retrieved.


@*/
int AMSP_Ldap_load_publishers(LDAP *ld, char *base, char *filter, char ***list, int *nb_entries)
{

    LDAPMessage *e;
    char    **v1, **v2, **dn_list;
    static char	*vals[MAX_ENTRIES] ={NULL};
    int	i, rc, err;

    if (filter == NULL)
        filter = "(cn=*)";

    /* Load objects */
    err = AMSP_Ldap_search_object(ld, base, filter, &dn_list, nb_entries);
    CheckErr(err);

    for (i=0; i < *nb_entries; i++) {

        /* Search the specific entry */
        if (( rc = ldap_search_s(ld, (dn_list)[i], LDAP_SCOPE_BASE, "(objectclass=*)", NULL, 0, &e)) == LDAP_SUCCESS ) {
            int len;

            /* Get the values of the attribute */
            v1 = ldap_get_values(ld, e, "host");
            if (v1 == NULL)
                return AMS_ERROR_SYSTEM;

            /* Get the values of the attribute */
            v2 = ldap_get_values(ld, e, "ipserviceport");
            if (v2 == NULL)
                return AMS_ERROR_SYSTEM;

            /* Compute the length: dn + "|" + v1 + "|" + v2 + "|" + 0 */
            len = strlen((dn_list)[i]) + 1 + strlen(v1[0]) + 1 + strlen(v2[0]) + 1 + 1;

		    vals[i] = (char *) realloc(vals[i], len);
            if (vals[i] == NULL)
                return AMS_ERROR_INSUFFICIENT_MEMORY;

            strcpy(vals[i], (dn_list)[i]);
            strcat(vals[i], "|");
            strcat(vals[i], v1[0]);
            strcat(vals[i], "|");
            strcat(vals[i], v2[0]);

            ldap_value_free( v1 );
            ldap_value_free( v2 );

        } else {
            return AMS_ERROR_LDAP_SYSTEM;
        }
    }

    if (*nb_entries) {
        *list = vals;
        ldap_msgfree(e);
    } else {
        *list = NULL;
    }

    return AMS_ERROR_NONE;
}

/*
 * Free a mods array.
 */
static void
free_mods( LDAPMod **mods, int nmods )
{
    int i;

    for ( i = 0; i < nmods; i++ ) {
	    free( mods[ i ] );
    }
    free( mods );
}
	
#endif

