
#include <stdlib.h>
/*#include <malloc.h> */
#include <string.h>
#include <stdio.h>

#include "amem.h"
#include "acomm.h"
#include "netlib.h"
#ifdef AMS_PUBLISHER
#include "updatesync.h"
#include "amsmpi.h"
#endif


/* 
 * Implementation of AMS_MEM Low level functions
 * These low-level functions are hidden to the user
 * of the AMS memory module. They are provided to help
 * in the design of high-level functions.
 */

/* 
 * Define some global variables. We will get rid of this later
 * I hate global variables, we shoudl be able to live without them
 */

AMS_MEM *MEM_TBL[MAX_MEMID][MAX_COMM];
int MEM_RECYCLE_NB[MAX_MEMID][MAX_COMM];
int MEM_PUB_ID[MAX_MEMID][MAX_COMM];
int LAST_RECYCLE_NB = 0;
int PUBLISHED_MEM_TBL[MAX_MEMID+1][MAX_COMM];

/* Global variables used in the accessor (accessor) */
char * MEM_LIST[MAX_MEMID];

#ifdef AMS_PUBLISHER
Mutex pub_mem_tbl;
Mutex  MEM_PUB_LOCK[MAX_MEMID][MAX_COMM];
#endif

/*@
        AMSP_New_AMS_MEM - Creates an AMS memory structure

        Input Parameters:
+			ams - an AMS computational context
+			name - Memory unique name
        Output parameter
			amem - AMS memory structure

@*/

AMS_MEM * AMSP_New_AMS_MEM(AMS_Comm ams, const char *name, int *err)
{
    int k;
    AMS_MEM * amem = (AMS_MEM *) malloc(sizeof(AMS_MEM));
    if (amem == NULL) {
        *err = AMS_ERROR_INSUFFICIENT_MEMORY;
        return NULL;
    }

    amem->comm = ams;
    amem->name = (char *) malloc(sizeof(char) * (strlen(name)+1));
    if (amem->name == NULL) {
        *err = AMS_ERROR_INSUFFICIENT_MEMORY;
        return NULL;
    }

    /* Give it a name */
    strcpy(amem->name, name);

#ifdef AMS_PUBLISHER
    AMSP_NewMutex(&(amem->mem_lock));
	AMSP_CondInit(&(amem->cond));
    AMSP_CondInit(&(amem->w_cond));
    AMSP_CondInit(&(amem->r_cond));
#endif

    for(k = 0; k < STEP_BYTES; ++k) {
        amem->current_step[k] = (unsigned char) 0;

#ifdef AMS_PUBLISHER
        amem->stop_at[k] = (unsigned char) 255;
#endif
    }

    amem->c_stop = 0; /* Do not block by default */
    amem->c_stop_wait = 0; /* No timeout by delfault */

    amem->TailField = AMSP_New_Field(err);
    CheckErr1(*err);

    amem->fields = amem->TailField;
    amem->CurField = amem->TailField;
    amem->nb_fields = 0;

    *err = AMS_ERROR_NONE;
    return amem;

}


/*@
        AMSP_Del_AMS_MEM - Deletes and Frees an AMS memory structure

        Input Parameters:
			amem - AMS memory structure

@*/

int AMSP_Del_AMS_MEM(AMS_MEM *amem)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);

    /* 
     * Check the validity of the memory structure before deleting it
     * Why should we worry ? We're deleting it ?
     * I am just trying to check for inconsistencies in the code. It
     * would be helpful to know that you're deleting an invalid object. This
     * can help diagnosis another problem in the code
     */
    CheckErr(err);
                
    /* Deletes each of the structure's field */
    err = AMSP_Del_All_Field(amem->fields);
    CheckErr(err);

    /* Frees the memory name */
    free(amem->name);

#ifdef AMS_PUBLISHER
    /* Deletes the synchronization variable */
    AMSP_DelMutex(&(amem->mem_lock));
    AMSP_CondDestroy(&(amem->cond));
    AMSP_CondDestroy(&(amem->w_cond));
    AMSP_CondDestroy(&(amem->r_cond));
#endif

    /* Frees the Memory Structure */
    free(amem);
        
    return AMS_ERROR_NONE;
}

/*@
        AMSP_IncrStep_AMS_MEM - Increments the computational context's count

        Input Parameters:
                amem - an AMS memory structure

@*/
int AMSP_IncrStep_AMS_MEM(AMS_MEM *amem)
{
    int k = 0, done = 0, err = Validate(AMSP_Valid_AMS_MEM, amem);
        
    /* Check if argument is valid */
    CheckErr(err);
        
    while(!done) {
        amem->current_step[k] = (unsigned char) ((int) amem->current_step[k] + 1);
        if((int) amem->current_step[k] <= 0) {
            /* Must carry */
            ++k;
            if(k == STEP_BYTES)
                AMSP_err_quit("Step limit exceeded!  Increase STEP_BYTES.\n");
        }
        else
            done = 1;
    }

    return AMS_ERROR_NONE;
}

/*@
        AMSP_IsNewerStep_AMS_MEM - Tests whether an AMS memory structure is ahead
                                                of a given count

        Input Parameters:
+			amem - an AMS memory structure
+			count - Current computational count to be compared with

        Output Parameters:
            answer - true if amem's step is greater than count, false otherwise
@*/
int AMSP_IsNewerStep_AMS_MEM(AMS_MEM *amem, unsigned char *count, int *answer)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
        
    /* Check if argument is valid */
    CheckErr(err);

    *answer = AMSP_StepGreaterThan(amem->current_step, count);
    return AMS_ERROR_NONE;
}

int AMSP_GetStep_AMS_MEM(AMS_MEM *amem, unsigned char step[STEP_BYTES])
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    memcpy((void *) step, (const void *) amem->current_step, STEP_BYTES);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Lock_Write_AMS_MEM - Locks the memory, preventing other threads from accessing it.

        Input Parameters:
			amem - an AMS memory structure
@*/
int AMSP_Lock_Write_AMS_MEM(AMS_MEM *amem)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
        
    /* Check if argument is valid */
    CheckErr(err);

    AMSP_LockMutexWrite(&(amem->mem_lock));
#endif
    return AMS_ERROR_NONE;
}

/*@
        AMSP_Lock_Read_AMS_MEM - Locks the memory, preventing other threads from writing to it.

        Input Parameters:
			amem - an AMS memory structure
@*/
int AMSP_Lock_Read_AMS_MEM(AMS_MEM *amem)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
        
    /* Check if argument is valid */
    CheckErr(err);

    AMSP_LockMutexRead(&(amem->mem_lock));
#endif
    return AMS_ERROR_NONE;
}

/*@
    AMSP_Main_thread_write_Lock_AMS_MEM - As along as the current step 
    is smaller than stop_at, and the main thread is not being blocked by a 
    accessor call get write access. Otherwise, wait untill the
    worker thread is done, or a timeout.

    Input parameters:
    amem - AMS Memory struture

 @*/
int AMSP_Main_thread_write_Lock_AMS_MEM(AMS_MEM *amem)
{
#ifdef AMS_PUBLISHER
    int t_flag;
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    AMSP_LockMutexWrite(&(amem->mem_lock));
    while(!AMSP_StepGreaterThan(amem->stop_at, amem->current_step) ||
          amem->c_stop) {

		/* Tell worker thread to proceed */
		AMSP_CondSignal(&(amem->cond)); 

        /* Wait until end of block call, and/or worker thread is done */
		AMSP_CondWaitMutexWrite(&(amem->cond), &(amem->mem_lock), amem->c_stop_wait, &t_flag);

        if (t_flag == 1) {
            /* wait timed out */
            amem->c_stop = 0;
            amem->c_stop_wait = 0;
        }
    }
    
#endif
    return AMS_ERROR_NONE;
}

/*@
    AMSP_Main_thread_read_Lock_AMS_MEM - As along as the current step 
    is smaller than stop_at, and the main thread is not being blocked by a 
    accessor call get read access. Otherwise, wait untill the
    worker thread is done.

    Input parameters:
    amem - AMS Memory struture

 @*/
int AMSP_Main_thread_read_Lock_AMS_MEM(AMS_MEM *amem)
{
#ifdef AMS_PUBLISHER
    int t_flag;
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    AMSP_LockMutexRead(&(amem->mem_lock));
    while(!AMSP_StepGreaterThan(amem->stop_at, amem->current_step) ||
        amem->c_stop) {
		/* Tell worker thread to proceed */
		AMSP_CondSignal(&(amem->cond)); 

        /* Wait until end of block call, and/or worker thread is done */
        AMSP_CondWaitMutexRead(&(amem->cond), &(amem->mem_lock), amem->c_stop_wait, &t_flag);
         
        if (t_flag == 1) {
            /* wait timed out */
            amem->c_stop = 0;
            amem->c_stop_wait = 0;
        }
    }

#endif
    return AMS_ERROR_NONE;
}

/*@
    AMSP_Main_thread_read_wait_AMS_MEM - Block the calling thread until the memory is 
    read, or until the time expires.

    Input parameters:
    amem - AMS Memory struture
    timeout - Time to wait in milliseconds

 @*/
int AMSP_Main_thread_read_wait_AMS_MEM(AMS_MEM *amem, int timeout)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem), t_flag;
    CheckErr(err);

    AMSP_LockMutexRead(&(amem->mem_lock));
    AMSP_CondWaitMutexRead(&(amem->r_cond), &(amem->mem_lock), timeout, &t_flag);
    AMSP_RelMutexRead(&(amem->mem_lock));

#endif
    return AMS_ERROR_NONE;
}

/*@
    AMSP_Main_thread_write_wait_AMS_MEM - Block the calling thread until the memory is 
    written, or until the time expires.

    Input parameters:
    amem - AMS Memory struture
    time - Time to wait in milliseconds

 @*/
int AMSP_Main_thread_write_wait_AMS_MEM(AMS_MEM *amem, int timeout)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem), t_flag;
    CheckErr(err);

    AMSP_LockMutexWrite(&(amem->mem_lock));
    AMSP_CondWaitMutexWrite(&(amem->w_cond), &(amem->mem_lock), timeout, &t_flag);
    AMSP_RelMutexWrite(&(amem->mem_lock));

#endif
    return AMS_ERROR_NONE;
}


/*@
    AMSP_Main_thread_block_AMS_MEM - Block the main thread from the accessor side. The call is 
    performed by a worker thread. This function assumes that the lock was acquired earlier.

  Input Parameters:
    amem - an AMS Memory structure
    timeout - Time in milliseconds after which the main thread should proceed.

 @*/
int AMSP_Main_thread_block_AMS_MEM(AMS_MEM *amem, int timeout)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    amem->c_stop = 1; /* Tell the main thread to stop the next time */
    amem->c_stop_wait = timeout; /* Time to wait for */

#endif
    return AMS_ERROR_NONE;
}

/*@
	AMSP_UnLock_Write_AMS_MEM - Releases the memory, granting access to other threads

	Input Parameters:
		amem - an AMS memory structure
@*/
int AMSP_UnLock_Write_AMS_MEM(AMS_MEM *amem)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
        
    /* Check if argument is valid */
    CheckErr(err);

    /* Release the lock */
    AMSP_RelMutexWrite(&(amem->mem_lock));

    /* Other threads might be waiting for this memory to be written */
    AMSP_CondBroadcast(&amem->w_cond);
#endif
    return AMS_ERROR_NONE;
}

/*@
	AMSP_UnLock_Read_AMS_MEM - Releases the memory, granting other threads access to it.

	Input Parameters:
		amem - an AMS memory structure
@*/
int AMSP_UnLock_Read_AMS_MEM(AMS_MEM *amem)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
        
    /* Check if argument is valid */
    CheckErr(err);

    /* Release the lock */
    AMSP_RelMutexRead(&(amem->mem_lock));

    /* Other threads might be waiting for this memory to be read */
    AMSP_CondBroadcast(&amem->r_cond);
#endif
    return AMS_ERROR_NONE;
}

/*@
    AMSP_Main_thread_unblock_AMS_MEM - Unblock the main thread. The call is 
    performed by a worker thread. This function assumes that the memory lock
    was acquired earlier and therefore is not responsible for releasing it.

  Input Parameters:
    amem - an AMS Memory structure

 @*/
int AMSP_Main_thread_unblock_AMS_MEM(AMS_MEM *amem)
{
#ifdef AMS_PUBLISHER
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    amem->c_stop = 0;
    amem->c_stop_wait = 0;
    AMSP_CondBroadcast(&amem->cond); /* The main thread can now proceed */

#endif
    return AMS_ERROR_NONE;
}

/*@
	AMSP_Lock_TBL_Write_AMS_MEM - Lock the memory table for write access

@*/
int AMSP_Lock_TBL_Write_AMS_MEM()
{
#ifdef AMS_PUBLISHER
    AMSP_LockMutexWrite(&pub_mem_tbl);
#endif
    return AMS_ERROR_NONE;
}

/*@
    AMSP_Unlock_TBL_Write_AMS_MEM - UnLock the memory table

@*/
int AMSP_UnLock_TBL_Write_AMS_MEM()
{
#ifdef AMS_PUBLISHER
    AMSP_RelMutexWrite(&pub_mem_tbl);
#endif
    return AMS_ERROR_NONE;
}

/*@
        AMSP_Find_name_AMS_MEM - Given a field's name, the function checks if the
        field is in the memory.

        Input Parameters:
+			amem - an AMS memory structure
+			name - Field's name to be searched
        Output Parameters:
            found - Flag indicating whether we found the field

@*/
Field *AMSP_Find_name_AMS_MEM(AMS_MEM *amem, const char *name, int *err)
{
    Field *fld;
    *err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr1(*err);
                
    fld = amem->fields;

    while (fld->next) {
        if (!strcmp(name, fld->name)) {
            break;
        }
        fld = fld->next;
    } /* Should terminate unless there is a bug */

    if (!fld->next)
        fld = NULL;

    *err = AMS_ERROR_NONE;
    return fld;

}

/*@
        AMSP_AddField_AMS_MEM - Adds a new field to AMS memory structure

        Input Parameters:
+			amem - an AMS memory structure
+			fld - Field to be added

@*/
int AMSP_AddField_AMS_MEM(AMS_MEM * amem, Field *fld)
{
    int err = Validate(AMSP_Valid_Field, fld);
    const char *buf;
    Field *_fld;

    /* Check the validity of the arguments */
    CheckErr(err);
                
    err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);
                
    /* Make sure the field is not already in the memory */
    buf = AMSP_Get_Name_Field(fld, &err);
    CheckErr(err);
                
    _fld = AMSP_Find_name_AMS_MEM(amem, buf, &err);
    CheckErr(err);
                
    if (_fld)
        return AMS_ERROR_FIELD_ALREADY_IN_MEMORY;

    /* Append new field at the end */
    if (amem->fields == amem->TailField) {
        /* Empty list, add the new field at the beginning */
        fld->next = amem->TailField;
        fld->prev = NULL;
        amem->fields = fld;
        amem->TailField->prev = fld;
    } else {
        /* Append at the end */
        fld->next = amem->TailField;
        fld->prev = amem->TailField->prev;
        fld->prev->next = fld;
        amem->TailField->prev = fld;
    }

    amem->CurField = fld; /* Current field may be useful in the future */
    amem->nb_fields += 1;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_DelField_AMS_MEM - Deletes field from an AMS memory structure

        Input Parameters:
+			amem - an AMS memory structure
+			fld - Field to delete

@*/
int AMSP_DelField_AMS_MEM(AMS_MEM *amem, Field *fld)
{
    Field *pfld,  *_fld;

    int err = Validate(AMSP_Valid_Field, fld);
    const char *buf;

    /* Check the validity of the arguments */
    CheckErr(err);
                
    err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    /* Make sure the field is already in the memory */
    buf = AMSP_Get_Name_Field(fld, &err);
    CheckErr(err);

    _fld = AMSP_Find_name_AMS_MEM(amem, buf, &err);
    CheckErr(err);

    if (_fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    pfld = amem->fields;
    while (pfld->next) {

        if (strcmp(pfld->name, fld->name))
            pfld = pfld->next;
        else
            break;
    }

    if (pfld->next == NULL)
        return AMS_ERROR_SYSTEM;

    if (amem->fields == pfld) {
        /* 1. delete from the begining */
        amem->fields = pfld->next;
        amem->fields->prev = NULL;
        if ((err = AMSP_Del_Field(pfld)) != AMS_ERROR_NONE)
            return err;
                        
    } else {
        /* 2. delete from the middle */
        pfld->prev->next = pfld->next;
        pfld->next->prev = pfld->prev;
        if ((err = AMSP_Del_Field(pfld)) != AMS_ERROR_NONE)
            return err;
    }

    amem->nb_fields -= 1;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_UpdField_AMS_MEM - Updates a field in an AMS memory structure

        Input Parameters:
+			amem - an AMS memory structure
+			fld - Field to update (the id-name can't be updated)

@*/
int AMSP_UpdField_AMS_MEM(AMS_MEM *amem, Field *fld)
{
    int err = AMSP_DelField_AMS_MEM(amem, fld);
    CheckErr(err);

    err = AMSP_AddField_AMS_MEM(amem, fld);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Activate_AMS_MEM - allocates memory for field buffers

        Input Parameters:
			mem - Memory structure

@*/

int AMSP_Activate_AMS_MEM(AMS_MEM *amem)
{
    Field *fld;
    int len, err;

	fld = amem->fields;
	while (fld->next) {
	    err = AMSP_Get_Length_Field(fld, &len);
		CheckErr(err);
		if(len < 0)
			len = len * -1;
		err = AMSP_Alloc_Memory_Field(fld, len);
		CheckErr(err);
		fld = fld->next;
	}

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Field_AMS_MEM - Returns the first field of an AMS memory structure

        Input Parameters:
			amem - an AMS memory structure

        Input/Output 
            fld - a pointer to the first field in the memory structure

        The caller has the responsibility of allocating enough memory for
        fld usind New_Field(fld)

@*/
int AMSP_Field_AMS_MEM(AMS_MEM * amem, Field * fld)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);
        
    err = Validate(AMSP_Valid_Field, fld);
    CheckErr(err);

    err = AMSP_Dup_Field(amem->fields, fld);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Nb_Fields_AMS_MEM - Current number of fields in the memory structure

        Input Parameters:
			amem - an AMS memory structure

        Output 
            nb - Number of fields
                
@*/
int AMSP_Nb_Fields_AMS_MEM(AMS_MEM * amem, int *nb)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    *nb = amem->nb_fields;

    return AMS_ERROR_NONE;
}

int AMSP_Get_field_list_AMS_MEM(AMS_MEM *amem, char ***fld_list)
{
    Field *fld;
    int nb_fields, i = 0, err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    /* Figure out the length to be allocated */
    err = AMSP_Nb_Fields_AMS_MEM(amem, &nb_fields);
    CheckErr(err);

    *fld_list = (char **) malloc((nb_fields+1) * sizeof(char *));
    if (*fld_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    fld = amem->fields;
    while(fld->next) {
        /* We do not copy the field's name, we just point to it */
        (*fld_list)[i] = fld->name;
        fld = fld->next;
        i++;
    }

    (*fld_list)[i] = NULL;

    /* Just checking for inconsistencies */
    if (i != nb_fields)
        return AMS_ERROR_SYSTEM;

    /* 007: Do not forget to clean the memory created in this routine later */

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Comm_AMS_MEM - Returns the memory structure computational's context

        Input Parameters:
			amem - an AMS memory structure

        Output 
			ams - an AMS computational context

        This is functions is not MT Safe.
                
@*/
int AMSP_Comm_AMS_MEM(AMS_MEM * amem, AMS_Comm * ams)
{
    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    *ams = amem->comm;
        
    return AMS_ERROR_NONE;
}


/* 
 * Member function for Mem nodes manipulation 
 */

/*@
        AMSP_New_Mem_Node - Creates a memory node
        
        Input Parameters:
			mem_node - A node within the communicator

@*/
MEM_NODE *AMSP_New_Mem_Node(int *err)
{
    MEM_NODE *mem_node = (MEM_NODE *) malloc(sizeof(MEM_NODE));
    if (mem_node == NULL) {
        *err = AMS_ERROR_INSUFFICIENT_MEMORY;
        return NULL;
    }

    mem_node->amem = NULL;
    mem_node->prev = NULL;
    mem_node->next = NULL;
    mem_node->memid = 0;

    *err = AMS_ERROR_NONE;
    return mem_node;
}

/*@
        AMSP_Del_Mem_Node - Releases resources associated with a memory node
        
        Input Parameters:
			mem_node - A node within the communicator

@*/
int AMSP_Del_Mem_Node(MEM_NODE * mem_node)
{
    int err = Validate(AMSP_Valid_Mem_Node, mem_node);
    CheckErr(err);

    /* Delete the memory and its fields */
    if (mem_node->next) {
        /* tailnodes do not have any fields  */
        err = AMSP_Del_AMS_Memory(mem_node->memid);
        CheckErr(err);

        /* Frees the table entry */
        err = AMSP_Lock_TBL_Write_AMS_MEM();
        CheckErr(err);

        MEM_TBL[getMemid(mem_node->memid)][pub_id] = NULL;

        /* Frees the memory associated to the structure */
        free(mem_node);

        err = AMSP_UnLock_TBL_Write_AMS_MEM();
        CheckErr(err);
    }
        
    return AMS_ERROR_NONE;
}


/*@
        AMSP_Set_Mem_Node - Sets the memory structure and id mem_node.
        
        Input Parameters:
             mem_node - Node to be initialized.
             amem - AMS memory structure. The caller has the responsibiliy of allocating
                       the memory.
             id - Memory Id

@*/
int AMSP_Set_Mem_Node(MEM_NODE *mem_node, AMS_MEM *amem, AMS_Memory id)
{

#ifdef AMS_ACCESSOR
    AMS_Comm ams;
    MEM_NODE *node;
    int old_id;
#endif

    int err = Validate(AMSP_Valid_AMS_MEM, amem);
    CheckErr(err);

    CheckMemory(id);


    if (MEM_TBL[getMemid(id)][pub_id] != NULL) {

#ifdef AMS_ACCESSOR
        /* The old memory does not exist on the publisher side */
        /* We received a new id so clean the old one */
        
        /* Get the Communicator */
        err = AMSP_Comm_AMS_Memory(id, &ams);
        CheckErr(err);
        
        /* Find the old memory node to be deleted */
        old_id = getMemId(getMemid(id), MEM_RECYCLE_NB[getMemid(id)][pub_id]);
        err = AMSP_Find_Mem_id_AMS_Comm(ams, old_id, &node);
        CheckErr(err);
        
        if (!node)
            AMSP_err_quit("Can't find a memory that is supposed to exist");
            
        /* Delete the memory node from the communicator */
        err = AMSP_Del_Mem_Node_AMS_Comm(ams, node);
        CheckErr(err);
#else
        /* Publisher should have deleted it */
        return AMS_ERROR_SYSTEM;
#endif

    }

    mem_node->amem = amem;
    mem_node->memid = id;
    MEM_TBL[getMemid(id)][pub_id] = amem;
    MEM_RECYCLE_NB[getMemid(id)][pub_id] = getRecyc(id);
        
    return AMS_ERROR_NONE;

}

/*@
        AMSP_Create_Mem_Node - Creates mem_node and sets its memory structure and mem id.
        
        Input Parameters:
+			mem_node - Node to be created and initialized.
.			amem - Memory structure
+			id - Memory Id for identification

@*/
MEM_NODE * AMSP_Create_Mem_Node(AMS_MEM *amem, AMS_Memory *id, int *err)
{
    static int firstcall = 1;
    int tmp_id;

    /* Create a Memory Node */
    MEM_NODE * mem_node = AMSP_New_Mem_Node(err);
    CheckErr1(*err);

    /* Initialize the memory table */
    if (firstcall) {
        firstcall = 0;

#ifdef AMS_PUBLISHER
        AMSP_NewMutex(&pub_mem_tbl);
        AMSP_LockMutexWrite(&pub_mem_tbl);
#endif

        for (tmp_id = 0; tmp_id < MAX_MEMID; tmp_id++) {
            PUBLISHED_MEM_TBL[tmp_id][pub_id] = 0;
            MEM_RECYCLE_NB[tmp_id][pub_id] = 0;
            MEM_TBL[tmp_id][pub_id] = NULL;
        }

#ifdef AMS_PUBLISHER
        AMSP_RelMutexWrite(&pub_mem_tbl);
#endif

    }

    /* 
     * Find an entry in the memory table. Note the accessor should have an entry id.
     */
#ifdef AMS_PUBLISHER
    AMSP_LockMutexRead(&pub_mem_tbl);

    for (tmp_id = 1; tmp_id < MAX_MEMID; tmp_id++)
        if (MEM_TBL[tmp_id][pub_id] == NULL) {
            *id = tmp_id;
            break;
        }

    if (tmp_id >= MAX_MEMID) {
        *err = AMS_ERROR_MEMORY_TBL_FULL;
        return NULL;
    }

    /* Create a recycle number */
    LAST_RECYCLE_NB = (LAST_RECYCLE_NB+1) % MAXREC;
    tmp_id = LAST_RECYCLE_NB;

    /* Create the id number */
    *id = MAXMEM * tmp_id + *id;
    
    /* Create the lock that indicates whether the memory is published */

    AMSP_NewMutex(&(MEM_PUB_LOCK[getMemid(*id)][pub_id]));

#endif

    *err = AMSP_Set_Mem_Node(mem_node, amem, *id);
    CheckErr1(*err);

#ifdef AMS_PUBLISHER
        AMSP_RelMutexRead(&pub_mem_tbl);
#endif

    *err = AMS_ERROR_NONE;
    return mem_node;

}


/*@
        AMSP_Compare_Mem_Node - This function checks whether two nodes are identical (same id
        and same name)

        Input Parameters:
+			com_str - A Communication structure
.			mem1 - Memory node
+			mem2 - Memory node

@*/
int AMSP_Compare_Mem_Node(MEM_NODE *mem1, MEM_NODE *mem2)
{
    int err = Validate(AMSP_Valid_Mem_Node, mem1);
    CheckErr(err);

    err = Validate(AMSP_Valid_Mem_Node, mem2);
    CheckErr(err);

    if ((mem1->memid == mem2->memid) && !strcmp(mem1->amem->name, mem2->amem->name))
        return AMS_IDENTICAL_MEMORY;

    if ((mem1->memid == mem2->memid) && strcmp(mem1->amem->name, mem2->amem->name))
        return AMS_IDENTICAL_MEM_ID;

    if ((mem1->memid != mem2->memid) && !strcmp(mem1->amem->name, mem2->amem->name))
        return AMS_IDENTICAL_MEM_NAME;

    if ((mem1->memid != mem2->memid) && strcmp(mem1->amem->name, mem2->amem->name))
        return AMS_DIFFERENT_MEMORY;

    /* Should not get here */
    return AMS_ERROR_NONE;
}


/* 
 * To following functions are used to hide the details of the memory
 * implementation to the user. We use a linked list of memory structures
 * while the user manipulates only memory ids
 */

/* Interface to AMS_Memory functions */

int AMSP_Del_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_Del_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

/* 
 * Given the fields' size this function allocates memory for them 
 */
 
int AMSP_Activate_AMS_Memory(AMS_Memory *memid, int nbmem)
{
	int i, err;

	for (i=0; i<nbmem; i++) {
		CheckMemory(memid[i]);
		err = AMSP_Activate_AMS_MEM(MEM_TBL[getMemid(memid[i])][pub_id]);
		CheckErr(err);
	}

    return AMS_ERROR_NONE;
}

/*
 * Increment the memory computation step
 */
int AMSP_IncrStep_AMS_Memory(AMS_Memory *memid, int nbmem)
{
	int i, err;
	for (i=0; i<nbmem; i++) {
		CheckMemory(memid[i]);
		err = AMSP_IncrStep_AMS_MEM(MEM_TBL[getMemid(memid[i])][pub_id]);
		CheckErr(err);
	}

    return AMS_ERROR_NONE;
}

int AMSP_IsNewerStep_AMS_Memory(AMS_Memory *memid, int nbmem, unsigned char *step, int *answer)
{
	int i, err;
	for (i=0; i<nbmem; i++) {
		CheckMemory(memid[i]);
		err = AMSP_IsNewerStep_AMS_MEM(MEM_TBL[getMemid(memid[i])][pub_id], step + STEP_BYTES * i, answer);
		CheckErr(err);

		if (*answer)
			break; /* At least one memory is ahead of step */
	}
	return AMS_ERROR_NONE;
}

int AMSP_StepGreaterThan(unsigned char *left, unsigned char *right)
{
    int done = 0, answer = 0, k = STEP_BYTES - 1;

    while(!done) {
        if((int) left[k] > (int) right[k]) {
            answer = 1;
            done = 1;
        }
        if((int) left[k] < (int) right[k])
            done = 1;

        --k;
        if(k < 0)
            done = 1;
    }

    return answer;
}

int AMSP_StepUndef(unsigned char *step)
{
    int k, answer = 1;

    for(k = 0; k < STEP_BYTES; ++k) {
        if(step[k] != (unsigned char) 255)
            answer = 0;
    }

    return answer;
}

int AMSP_TranslateStep(unsigned char *step, unsigned int *stepanswer)
{
        int k;
        unsigned int multiple;

        if(stepanswer == NULL)
                return AMS_ERROR_NONE;

        multiple = 1;
        *stepanswer = 0;
        for(k = 0; k < STEP_BYTES; ++k) {
                *stepanswer += ((unsigned int) step[k] * multiple);
                multiple *= 256;
        }

        return AMS_ERROR_NONE;
}

int AMSP_GetStep_AMS_Memory(AMS_Memory *memid, int nbmem, unsigned char *step)
{
	int i, err;

	for(i=0; i<nbmem; i++) {
		CheckMemory(memid[i]);
		err = AMSP_GetStep_AMS_MEM(MEM_TBL[getMemid(memid[i])][pub_id], step + i*STEP_BYTES);
		CheckErr(err);
	}

	return AMS_ERROR_NONE;
}

int AMSP_Lock_Read_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_Lock_Read_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

int AMSP_Lock_Write_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_Lock_Write_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

int AMSP_Main_thread_read_Lock_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_Main_thread_read_Lock_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

int AMSP_Main_thread_write_Lock_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_Main_thread_write_Lock_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

int AMSP_Main_thread_read_wait_AMS_Memory(AMS_Memory memid, int time)
{
    CheckMemory(memid);
    return AMSP_Main_thread_read_wait_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], time);
}

int AMSP_Main_thread_write_wait_AMS_Memory(AMS_Memory memid, int time)
{
    CheckMemory(memid);
    return AMSP_Main_thread_write_wait_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], time);
}

int AMSP_Main_thread_block_AMS_Memory(AMS_Memory memid, int timeout)
{
    CheckMemory(memid);
    return AMSP_Main_thread_block_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], timeout);
}

int AMSP_UnLock_Read_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_UnLock_Read_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

int AMSP_UnLock_Write_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_UnLock_Write_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}

int AMSP_Main_thread_unblock_AMS_Memory(AMS_Memory memid)
{
    CheckMemory(memid);
    return AMSP_Main_thread_unblock_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id]);
}
Field * AMSP_Find_name_AMS_Memory(AMS_Memory memid, const char *name, int *err)
{
    CheckMemory1(memid, *err);
    return AMSP_Find_name_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], name, err);
}

int AMSP_AddField_AMS_Memory(AMS_Memory memid, Field *fld)
{
    CheckMemory(memid);
    return AMSP_AddField_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], fld);
}

int AMSP_Add_Field_AMS_Memory(AMS_Memory memid, const char *name, void *addr, int len, AMS_Data_type dtype,
                         AMS_Memory_type mtype, AMS_Shared_type stype, AMS_Reduction_type rtype)
{

    Field *fld;
    int err;
#ifdef AMS_PUBLISHER
    int start;
    AMS_Comm_type ctype;
    AMS_Comm ams; 
    MPI_Comm mpi_comm;
#endif

    if (strlen(name) == 0)
        return AMS_ERROR_INVALID_USER_ARGUMENT;

    /* Check validity of arguments */
    CheckMemory(memid);

    /* Create a Field */
    fld = AMSP_New_Field(&err);
    CheckErr(err);

    /* Set Field Name */
    err = AMSP_Set_Name_Field(fld, name);
    CheckErr(err);

    /* Set Data Type */
    err = AMSP_Set_Data_type_Field(fld, dtype);
    CheckErr(err);

    /* Set Memory Type */
    err = AMSP_Set_Memory_type_Field(fld, mtype);
    CheckErr(err);

    /* Set Shared Type */
    err = AMSP_Set_Shared_type_Field(fld, stype);
    CheckErr(err);

    /* Set Reduction Type */
    err = AMSP_Set_Reduction_type_Field(fld, rtype);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    /* Set Field Data and length */
    err = AMSP_Set_Buffer_Field(fld, addr, len);
    CheckErr(err);

    /* If we're running under MPI, guess our starting index */
    err = AMSP_Get_Comm_type_AMS_Memory(memid, &ctype);
    CheckErr(err);
    if(ctype == MPI_TYPE && stype != AMS_COMMON) {
        err = AMSP_Comm_AMS_Memory(memid, &ams);
        CheckErr(err);

        err = AMSP_Get_MPI_Comm_AMS_Comm(ams, &mpi_comm);
        CheckErr(err);

        err = AMSP_Guess_start_index_AMS_MPI(len, &start, mpi_comm);
        CheckErr(err);

        err = AMSP_Set_Starting_Index_Field(fld, start);
        CheckErr(err);
    }
#else
    /* AMS_ACCESSOR */

    /* Copy Field Data and length */
    AMSP_Alloc_Memory_Field(fld, len);
#endif
        
    /* Add the field the Memory */
    err = AMSP_AddField_AMS_Memory(memid, fld);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

int AMSP_Set_Field_Start_Index_AMS_Memory(AMS_Memory memid, const char *name, int ind)
{
    Field *fld;
    int err;

    CheckMemory(memid);

    fld = AMSP_Find_name_AMS_Memory(memid, name, &err);
    CheckErr(err);

    if(fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    err = AMSP_Set_Starting_Index_Field(fld, ind);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

int AMSP_Set_Field_Permutation_AMS_Memory(AMS_Memory memid, const char *name, int *perm)
{
    Field *fld;
    int err;

    CheckMemory(memid);

    fld = AMSP_Find_name_AMS_Memory(memid, name, &err);
    CheckErr(err);

    if(fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    err = AMSP_Set_Permutation_Field(fld, perm);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

int AMSP_Set_Field_Block_AMS_Memory(AMS_Memory memid, const char *name,
                                     int dim, int *start, int *end)
{
    Field *fld;
    int err;

    CheckMemory(memid);

    fld = AMSP_Find_name_AMS_Memory(memid, name, &err);
    CheckErr(err);

    if(fld == NULL)
        return AMS_ERROR_FIELD_NOT_IN_MEMORY;

    err = AMSP_Set_Block_Field(fld, dim, start, end);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

int AMSP_DelField_AMS_Memory(AMS_Memory memid, Field *fld)
{
    CheckMemory(memid);
    return AMSP_DelField_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], fld);
}

int AMSP_UpdField_AMS_Memory(AMS_Memory memid, Field *fld)
{
    CheckMemory(memid);
    return AMSP_UpdField_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], fld);
}

int AMSP_Field_AMS_Memory(AMS_Memory memid, Field *fld)
{
    CheckMemory(memid);
    return AMSP_Field_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], fld);
}

int AMSP_Nb_Fields_AMS_Memory(AMS_Memory memid, int *nb)
{
    CheckMemory(memid);
    return AMSP_Nb_Fields_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], nb);
}

int AMSP_Get_field_list_AMS_Memory(AMS_Memory memid, char ***fld_list)
{
    CheckMemory(memid);
    return AMSP_Get_field_list_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], fld_list);
}

int AMSP_Get_Comm_type_AMS_Memory(AMS_Memory memid, AMS_Comm_type *ctype)
{
    int err;
    AMS_Comm ams;

    err = AMSP_Comm_AMS_Memory(memid, &ams);
    CheckErr(err);
    return AMSP_Get_Comm_type_AMS_Comm(ams, ctype);
}

int AMSP_Comm_AMS_Memory(AMS_Memory memid, AMS_Comm *ams)
{
    CheckMemory(memid);
    return AMSP_Comm_AMS_MEM(MEM_TBL[getMemid(memid)][pub_id], ams);
}

int AMSP_Publish_AMS_Memory(AMS_Memory memid)
{
    int err;

    CheckMemory(memid);
    err = Validate(AMSP_Valid_AMS_MEM, MEM_TBL[getMemid(memid)][pub_id]);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    AMSP_LockMutexWrite(&(MEM_PUB_LOCK[getMemid(memid)][pub_id]));
#endif

    PUBLISHED_MEM_TBL[getMemid(memid)][pub_id] = 1;
    MEM_RECYCLE_NB[getMemid(memid)][pub_id] = getRecyc(memid);

#ifdef AMS_PUBLISHER
    AMSP_RelMutexWrite(&(MEM_PUB_LOCK[getMemid(memid)][pub_id]));
#endif

    return AMS_ERROR_NONE;

}

int AMSP_UnPublish_AMS_Memory(AMS_Memory memid)
{
    int err, k;
    CheckMemory(memid);
    err = Validate(AMSP_Valid_AMS_MEM, MEM_TBL[getMemid(memid)][pub_id]);
    CheckErr(err);

#ifdef AMS_PUBLISHER
    AMSP_LockMutexWrite(&(MEM_PUB_LOCK[getMemid(memid)][pub_id]));
#endif

    PUBLISHED_MEM_TBL[getMemid(memid)][pub_id] = 0;
    MEM_RECYCLE_NB[getMemid(memid)][pub_id] = 0;

    /* Set current step to infinity, so accessors will stop waiting */
    for(k = 0; k < STEP_BYTES; ++k)
        (MEM_TBL[getMemid(memid)][pub_id])->current_step[k] = (unsigned char) 255;

#ifdef AMS_PUBLISHER
    AMSP_RelMutexWrite(&(MEM_PUB_LOCK[getMemid(memid)][pub_id]));
#endif

    return AMS_ERROR_NONE;
}
        
/*
 * Given a memory name, this function returns the id
 */
int AMSP_Get_AMS_Memory(const char *name, int nbmem, AMS_Memory *memid)
{
    int id = 0, published, i = 0;
    AMS_MEM *amem;
	char *buff = (char *)malloc(sizeof(char)*(strlen(name)+1)), *p;

	if (buff == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	strcpy(buff, name);
	p = strtok(buff, "|");

#ifdef AMS_PUBLISHER
    AMSP_LockMutexRead(&pub_mem_tbl);
#endif

	while(p) {
		published = 0;
		for (id = 0; id < MAX_MEMID; id++) {


		  if ((amem = MEM_TBL[id][pub_id]))
				if (!strcmp(amem->name, p)) {
					memid[i] = MAXMEM * MEM_RECYCLE_NB[id][pub_id] + id;
					published = PUBLISHED_MEM_TBL[id][pub_id];
					break;
				}

		}

		if (id == MAX_MEMID)
			break; /* A memory was not found */

		if (published == 0)
			break; /* A memory was found but not published */

		p = strtok(NULL, "|");
		i++;
	}


	/* Free the buffer */
	free(buff);

#ifdef AMS_PUBLISHER
     AMSP_RelMutexRead(&pub_mem_tbl);
#endif

	/*
	 * At this time, we are not going to say which memory was not found
	 * or is not published.
	 */
    if (id == MAX_MEMID)
        return AMS_ERROR_MEMORY_NAME_NOT_FOUND;
        
    if (published == 0)
        return AMS_ERROR_MEMORY_NOT_PUBLISHED;

    return AMS_ERROR_NONE;
}
/*@
 * Check if the memory is currently published and keep the lock if necessary 
 * if lock = AMS_DONOT_LOCK do not lock the resources
 * if lock = AMS_RELEASE_LOCK lock the resource and keep the lock (if memory published) after exiting
 * if lock = AMS_KEEP_LOCK lock the resource
 @*/

int AMSP_IsPublished_AMS_Memory(AMS_Memory *memid, int nbmem, int lock, int *flag)
{

#ifdef AMS_PUBLISHER
	int i;


	for (i=0; i<nbmem; i++) {

        if (lock != AMS_DONOT_LOCK)
            AMSP_LockMutexRead(&(MEM_PUB_LOCK[getMemid(memid[i])][pub_id]));

		if ( (PUBLISHED_MEM_TBL[getMemid(memid[i])][pub_id] == 1) && (getRecyc(memid[i]) == MEM_RECYCLE_NB[getMemid(memid[i])][pub_id]) )
		    *flag = TRUE;
		else {
		    *flag = FALSE;
		}

        if (lock == AMS_RELEASE_LOCK || *flag == FALSE)
            if (lock != AMS_DONOT_LOCK)
                AMSP_RelMutexRead(&(MEM_PUB_LOCK[getMemid(memid[i])][pub_id])); /*
										 * We keep the lock only if requested and if all the
										 * memories are published.
			    						 */
        if (*flag == FALSE)            		    
            break;
	}


#endif
    return AMS_ERROR_NONE;

}

#ifdef _DEBUG

/*@
        AMSP_Valid_AMS_MEM - Checks the validity of a memory structure

        Input Parameters:
			amem - an AMS memory structure

@*/
int AMSP_Valid_AMS_MEM(AMS_MEM *amem)
{
    if (amem == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (amem->nb_fields < 0)
        return AMS_ERROR_BAD_ARGUMENT;

    CheckComm(amem->comm);
        
    if (amem->fields == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (amem->TailField == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (amem->CurField == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (amem->name == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Valid_Mem_Node - Checks the validity of a Node

        Input Parameters:
			mem_node - Node to be validated

@*/
int AMSP_Valid_Mem_Node(MEM_NODE *mem_node)
{

    if (mem_node == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    /* 
     * Memory id is positive or null and smaller than MAX_MEMID 
     */
    CheckMemory(mem_node->memid);

    /* 
     * Check the memory structure 
     */
    AMSP_Valid_AMS_MEM(mem_node->amem);

    return AMS_ERROR_NONE;
}

#endif
