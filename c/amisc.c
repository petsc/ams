#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ams.h"
#include "acomm.h"
#include "amem.h"
#include "convert.h"
#include "amisc.h"
#include "amsdefs.h"
#include "netlib.h"
#include "amemfld.h"

#ifdef AMS_PUBLISHER
#include "amsmpi.h"
#endif

/*
 * Misc functions used to create and manipulate AMS objects
 * side
 */

/* Defined in sendrecv module */
extern void AMSP_extract(const char *, char *, int, int *);

int AMSP_Create_Comm_list(const char *ptr, int nbytes, char ***comm_list)
{
    int i = 0, index = 0;
    char *str = (char *) malloc((nbytes+1)*sizeof(char));

    if (str == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    AMSP_extract(ptr, str, nbytes, &index);

    i = 0;
    while(strlen(str) && index <= nbytes && i < MAX_COMM) {
        /* List not empty, at least one communicator was published */
        COMM_LIST[pub_id][i] = (char *) realloc(COMM_LIST[pub_id][i], (strlen(str)+1) * sizeof(char));
        if (COMM_LIST[pub_id][i] == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        strcpy(COMM_LIST[pub_id][i], str); i++;
        AMSP_extract(ptr, str, nbytes, &index);
    }

    /* Reset the last entry */
    if (i < MAX_COMM && COMM_LIST[pub_id][i]) {
        free(COMM_LIST[pub_id][i]);
        COMM_LIST[pub_id][i] = NULL;
    }

    *comm_list = COMM_LIST[pub_id];
    free(str);

    return AMS_ERROR_NONE;
}

/*
 * Deletes the communicator's list and frees associated resources
 */
int AMSP_Delete_Comm_list(void)
{
    int i;

    for (i = 0; i<MAX_COMM; i++)
        if (COMM_LIST[pub_id][i]) {
            free(COMM_LIST[pub_id][i]);
            COMM_LIST[pub_id][i] = NULL;
        }
    return AMS_ERROR_NONE;
}

int AMSP_Create_Memory_list(char *ptr, int nbytes, char ***mem_list)
{
    int i = 0, index = 0;
    char *str = (char *) malloc((nbytes+1)*sizeof(char));

    if (str == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    AMSP_extract(ptr, str, nbytes, &index);

    i = 0;
    while(strlen(str) && index <= nbytes) {
        /* List not empty, at least one memory structure was published */
        MEM_LIST[i] = (char *) realloc(MEM_LIST[i], (strlen(str)+1) * sizeof(char));
        if (MEM_LIST[i] == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        strcpy(MEM_LIST[i], str); i++;
        AMSP_extract(ptr, str, nbytes, &index);
    }

    if (MEM_LIST[i]) {
        /* Free the next when if already allocated */
        free(MEM_LIST[i]);
        MEM_LIST[i] = NULL;
    }

    *mem_list = MEM_LIST;
    free(str);

    return AMS_ERROR_NONE;
}

/*
 * Given a communicator id, this function deletes memory list attached to it
 */
int AMSP_Delete_Memory_list(AMS_Comm ams)
{
    int i, err;
    MEM_NODE *mem_node;
    AMS_Memory mem_id;

    for (i = 0; i<MAX_MEMID; i++) {

        if (MEM_LIST[i]) {
            err = AMSP_Find_Mem_name_AMS_Comm(ams, MEM_LIST[i], &mem_id, &mem_node);
            CheckErr(err);
            if (mem_node) {
                free(MEM_LIST[i]);
                MEM_LIST[i] = NULL;
            }
        }
    }

    return AMS_ERROR_NONE;
}

int AMSP_Extract_Field(char **name, void **data, int *len, int *dim,
                  int **array_start, int **array_end,
                  AMS_Data_type *dtype, AMS_Memory_type *mtype,
                  AMS_Shared_type *stype, AMS_Reduction_type *rtype,
                  char *ptr, int nbytes, int *index)
{
    char *AMS_char;
    int *AMS_int;
    double *AMS_dbl;
    float *AMS_flt;
    char **AMS_str, buff[255];
    int i;

    if (ptr == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    if (nbytes <= 0)
        return AMS_ERROR_BAD_ARGUMENT;

    if (*index < 0 || (*index > nbytes))
        return AMS_ERROR_BAD_ARGUMENT;

    if (*index >= nbytes)
        /* No field to extract */
        return AMS_NO_FIELD_TO_EXTRACT;

    /* Extract the Field name */
    AMSP_extract(ptr, buff, nbytes, index);

    *name = (char *) realloc(*name, strlen(buff) +1);
    if (*name == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;
    strcpy(*name, buff);

    /* Extract the Memory type */
    *mtype = (AMS_Memory_type) AMSP_ldlong(ptr + *index);
    *index += INT_SIZE;

    /* Extract the Data Type */
    *dtype = (AMS_Data_type) AMSP_ldlong(ptr + *index);
    *index += INT_SIZE;

    /* Extract the Shared Type */
    *stype = (AMS_Shared_type) AMSP_ldlong(ptr + *index);
    *index += INT_SIZE;

    /* Extract Reduction Type */
    *rtype = (AMS_Reduction_type) AMSP_ldlong(ptr + *index);
    *index += INT_SIZE;

    /* Extract the data length (number of elements) */
    *len = AMSP_ldlong(ptr + *index);
    *index += INT_SIZE;

    /* Extract the dimension */
    *dim = AMSP_ldlong(ptr + *index);
    *index += INT_SIZE;

    if(*dim > 0) {
        *array_start = (int *) realloc(*array_start, (*dim) * sizeof(int));
        *array_end = (int *) realloc(*array_end, (*dim) * sizeof(int));
        if(*array_start == NULL || *array_end == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        /* Extract the starting index array */
        for(i = 0; i < *dim; ++i) {
            (*array_start)[i] = AMSP_ldlong(ptr + *index);
            *index += INT_SIZE;
        }

        /* Extract the ending index array */
        for(i = 0; i < *dim; ++i) {
            (*array_end)[i] = AMSP_ldlong(ptr + *index);
            *index += INT_SIZE;
        }
    } else {
        *array_end = (int *) realloc(*array_end, (*len) * sizeof(int));
        if(*array_end == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        for(i = 0; i < *len; ++i) {
            (*array_end)[i] = AMSP_ldlong(ptr + *index);
            *index += INT_SIZE;
        }
    }

    /* Extract and convert the data from the network byte order */
    switch(*dtype) {
    case AMS_CHAR:
        /* Extract the data */
        *data = realloc(*data, (*len)*sizeof(char));
        if (*data == NULL)
          return AMS_ERROR_INSUFFICIENT_MEMORY;

        memcpy(*data, ptr + *index, (*len)*sizeof(char));
        *index += (*len)*sizeof(char);

        AMS_char = (char *)malloc((*len)*sizeof(char));
        if (AMS_char == NULL)
          return AMS_ERROR_INSUFFICIENT_MEMORY;

        for (i = 0; i < *len; i++)
          AMS_char[i] = *((char *)(*data) + sizeof(char)*i);

        memcpy(*data, (char *)AMS_char, *len*sizeof(char));
        free(AMS_char);
        break;
    case AMS_INT:
    case AMS_BOOLEAN:
        /* Extract the data */
        *data = realloc(*data, (*len)*sizeof(int));
        if (*data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        memcpy(*data, ptr + *index, (*len)*sizeof(int));
        *index += (*len)*sizeof(int);

        AMS_int = (int *)malloc((*len)*sizeof(int));
        if (AMS_int == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        for (i = 0; i < *len; i++)
            AMS_int[i] = AMSP_ldlong((char *)(*data) + sizeof(int)*i);

        memcpy(*data, (char *)AMS_int, *len*sizeof(int));
        free(AMS_int);
        break;

    case AMS_FLOAT:
        /* Extract the data */
        *data = realloc(*data, (*len)*sizeof(float));
        if (*data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        memcpy(*data, ptr + *index, (*len)*sizeof(float));
        *index += (*len)*sizeof(float);

        AMS_flt = (float *)malloc((*len)*sizeof(float));
        if (AMS_flt == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        for (i = 0; i < *len; i++)
            AMS_flt[i] = (float) AMSP_ldfloat((char *)(*data) + sizeof(float)*i);

        memcpy(*data, (char *)AMS_flt, *len*sizeof(float));
        free(AMS_flt);
        break;

    case AMS_DOUBLE:
        /* Extract the data */
        *data = realloc(*data, (*len)*sizeof(double));
        if (*data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        memcpy(*data, ptr + *index, (*len)*sizeof(double));
        *index += (*len)*sizeof(double);

        AMS_dbl = (double *)malloc((*len)*sizeof(double));
        if (AMS_dbl == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        for (i = 0; i < *len; i++)
            AMS_dbl[i] = AMSP_lddouble((char *)(*data) + sizeof(double)*i);

        memcpy(*data, (char *)AMS_dbl, *len*sizeof(double));
        free(AMS_dbl);
        break;

    case AMS_STRING:
        /* Extract the data */
        *data = realloc(*data, *len*sizeof(char *));
        if (*data == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        AMS_str = (char **)*data;
        for (i = 0; i < *len; i++) {
            AMSP_extract(ptr, buff, nbytes, index);
            if (strlen(buff)) {
                AMS_str[i] = (char *)malloc(strlen(buff) + 1);
                if (AMS_str[i] == NULL)
                    return AMS_ERROR_INSUFFICIENT_MEMORY;
                strcpy(AMS_str[i], buff);
            } else {
                AMS_str[i] = NULL;
            }

        }
        break;

    default:
        /* Should not get here */
        return AMS_ERROR_INVALID_DATA_TYPE;
    }

    if (*index > nbytes)
        /* We are in trouble */
        return AMS_ERROR_SYSTEM;

    return AMS_ERROR_NONE;
}


/*
 * This function extracts the different fields
 * received from the publisher and add them to the Memory Structure.
 */
int AMSP_Create_Memory_fields(char *ptr, int nbytes)
{

    char *name = NULL;
    void *data = NULL;

    AMS_Memory *memid;
    AMS_Data_type dtype;
    AMS_Shared_type stype;
    AMS_Reduction_type rtype;
    AMS_Memory_type mtype;
    int index = 0, len , dim, *start = NULL, *end = NULL;
	int err, list_len = 0, i, nbmem, extracted, oldindex;
    Field *fld;

    /* How many memories? */
	nbmem = AMSP_ldlong(ptr);
    index += INT_SIZE;

	/* Allocate space */
	memid = (AMS_Memory *) malloc (sizeof(AMS_Memory)*nbmem);
	if (memid == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	for (i=0; i<nbmem; i++) {
		memid[i] = (AMS_Memory) AMSP_ldlong(ptr + index);
		index += INT_SIZE;
	}

	for (i=0; i<nbmem; i++) {

		/* Extract this memory fields' length */
		list_len = AMSP_ldlong(ptr + index);
		index += INT_SIZE;

		oldindex = index;
		/* Extract the fields associated with the memory */
		err = AMSP_Extract_Field(&name, &data, &len, &dim, &start, &end, &dtype, &mtype,
                        &stype, &rtype, ptr, nbytes, &index);
		extracted = index - oldindex;

		while (err == AMS_ERROR_NONE) {

			/* Use negative lengths until we know the size for sure. */

			fld = AMSP_Find_name_AMS_Memory(memid[i], name, &err);
			if(fld) {
	            /* Already know about this field */
		        if(stype == AMS_DISTRIBUTED) {
			        /* See if it's bigger than we thought */
				    if(dim > 0)
					    err = AMSP_Expand_Field(fld, start, end);
					else
						err = AMSP_Expand_Field_Perm(fld, len, end);
					CheckErr(err);
				}
			}
			else {
				/* Add the Field to the memory structure */

				if(stype == AMS_REDUCED)
					len = 1;

				err = AMSP_Add_Field_AMS_Memory(memid[i], name, data, -1*len,
										   dtype, mtype, stype, rtype);
				CheckErr(err);

				if(dim > 0)
					err = AMSP_Set_Field_Block_AMS_Memory(memid[i], name, dim, start, end);
				else {
					fld = AMSP_Find_name_AMS_Memory(memid[i], name, &err);
					if(err != AMS_ERROR_NONE)
						return err;
					if(fld == NULL)
						return AMS_ERROR_SYSTEM;
					err = AMSP_Expand_Field_Perm(fld, len, end);
				}
				CheckErr(err);
			}


			if (extracted >= list_len)
				break; /* Go to the next memory */
			else {
				/* Extract the next field in the memory */
				oldindex = index;
				err = AMSP_Extract_Field(&name, &data, &len, &dim, &start, &end, &dtype, &mtype, &stype, &rtype, ptr, nbytes, &index);
				extracted += index - oldindex;
			}
		}

		if (err != AMS_NO_FIELD_TO_EXTRACT && err != AMS_ERROR_NONE)
			return err;

	} /* For each memory */

    /* Free the memory allocated in Extract_Field */
    if (name)
        free(name);
    if (data)
        free(data);
    if (start)
        free(start);
    if (end)
        free(end);

    if (err == AMS_NO_FIELD_TO_EXTRACT)
        return AMS_ERROR_NONE;
    else
        return err;

}


/*@
 *
 * AMSP_Update_Memory_fields - This function extracts the different fields received from
 * the publisher or accessor and use them to update the local
 * copy of the memory.
 *
 @*/

int AMSP_Update_Memory_fields(char *ptr, int nbytes, int proc_num,
                         int *changed, unsigned char *stepanswer,
                         RequestID req)
{
    char *name = NULL;
    void *data = NULL;

    AMS_Memory *memid;
    AMS_Data_type dtype;
    AMS_Shared_type stype;
    AMS_Reduction_type rtype;
    AMS_Memory_type mtype;
    int index = 0, len , dim, *start = NULL, *end = NULL, list_len = 0;
	int err, flag, nbmem, i, extracted, oldindex;

#ifdef AMS_ACCESSOR
    unsigned char *step;
#else
    Appointment **my_appt;
#endif
    Field *fld;
    flag = 0;

	/* How many memories */
	nbmem = AMSP_ldlong(ptr);
	index += INT_SIZE;

	/* Allocate Memory */
	memid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
	if (memid == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	for (i = 0; i<nbmem; i++) {
		/* Extract the memory's id */
		memid[i] = AMSP_ldlong(ptr + index);
		index += INT_SIZE;
	}

#ifdef AMS_PUBLISHER
    /* Check if Memory is published ? */
    err = AMSP_IsPublished_AMS_Memory(memid, nbmem, AMS_KEEP_LOCK, &flag);
    CheckErr(err);

    if (!flag)
        return AMS_ERROR_MEMORY_NO_LONGER_PUBLISHED;

#endif

#ifdef AMS_PUBLISHER
    /* Take access */
    err = AMSP_Take_synchronized_write_access(memid, nbmem, req, &my_appt);
    CheckErr(err);

#endif

#ifdef AMS_ACCESSOR

	/* Allocate Memory */
	step = (unsigned char *) malloc(STEP_BYTES * nbmem);
	if (step == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	/* Extract the step number */
	memcpy(step, ptr + index, STEP_BYTES * nbmem);
	index += STEP_BYTES * nbmem;

	/* no memory to update yet, *changed = 0 */
	*changed = 0;

    /* Record the step number */
    err = AMSP_SetStep_AMS_Memory(memid, nbmem, step);
    CheckErr(err);

    /* Return step number to the caller if interested */
    if(stepanswer != NULL) {
        err = AMSP_GetStep_AMS_Memory(memid, nbmem, stepanswer);
		CheckErr(err);
	}

#endif

	/* If index == nbytes, no update was received */
	for (i=0; (i < nbmem) && (index < nbytes); i++) {

		/* Extract the list length for the current memory */
		list_len = AMSP_ldlong(ptr + index);
		index += INT_SIZE;

		/* Extract the fields associated with the memory */
		oldindex = index;
		err = AMSP_Extract_Field(&name, &data, &len, &dim, &start, &end, &dtype, &mtype, &stype, &rtype, ptr, nbytes, &index);
		extracted = index - oldindex;

		while (err == AMS_ERROR_NONE) {

			*changed = 1;

			/* Find the Field in the memory structure */
			fld = AMSP_Find_name_AMS_Memory(memid[i], name, &err);
			CheckErr(err);

			/* If field not found create it */
			if (fld == NULL) {
				fld = AMSP_New_Field(&err);
				CheckErr(err);
			}

			/* Update ONLY the data and the length */
			switch(fld->stype) {
			case AMS_COMMON:
#ifdef AMS_ACCESSOR
	            if(proc_num == 0) {
#endif
#ifdef AMS_ACCESSOR
		            if(dim > 0)
			            err = AMSP_Upd_Buffer_MultDim_Field(fld, data, len, dim, start, end);
				    else
					    err = AMSP_Upd_Buffer_Perm_Field(fld, data, len, end);
#else
					if(fld->dim > 0)
						err = AMSP_Upd_Buffer_MultDim_Field(fld, data, len, dim, start, end);
					else
						err = AMSP_Upd_Buffer_Perm_Field(fld, data, len, fld->end);
#endif
					CheckErr(err);
#ifdef AMS_ACCESSOR
				}
#endif
				break;

			case AMS_SHARED_UNDEF:
			case AMS_DISTRIBUTED:
#ifdef AMS_ACCESSOR
                if(dim > 0)
                    err = AMSP_Upd_Buffer_MultDim_Field(fld, data, len, dim, start, end);
                else
                    err = AMSP_Upd_Buffer_Perm_Field(fld, data, len, end);
#else
                if(fld->dim > 0)
                    err = AMSP_Upd_Buffer_MultDim_Field(fld, data, len, dim, start, end);
                else
                    err = AMSP_Upd_Buffer_Perm_Field(fld, data, len, fld->end);
#endif
				CheckErr(err);
            break;

#ifdef AMS_ACCESSOR
			case AMS_REDUCED:
				/* set fld->data to NULL to indicate we don't have
				   any data yet */
				if(proc_num == 0) {
					if(fld->data != NULL)
						free(fld->data);
					fld->data = NULL;
				}

				if(len == 1) {
					if(fld->data == NULL) {
						AMSP_Alloc_Memory_Field(fld, 1);
						err = AMSP_Upd_Buffer_Field(fld, data, 1, start[0]);
					}
					else {
						if(len == 1)
							err = AMSP_Reduce_Field(fld, data);
					}
					CheckErr(err);
				}

				break;
#endif

	        default:
		        return AMS_ERROR_INVALID_SHARED_TYPE;
			}
#ifdef AMS_ACCESSOR
			/* got a fresh copy, equal to publisher's */
			err = AMSP_Set_Status_Field(fld, 0);
			CheckErr(err);
#endif

			if (extracted >= list_len)
				break;
			else {
				/* Extract the next field to update */
				oldindex = index;
				err = AMSP_Extract_Field(&name, &data, &len, &dim, &start, &end, &dtype, &mtype, &stype, &rtype, ptr, nbytes, &index);
				extracted += index - oldindex;
			}

		} /* Next field */

		if (err != AMS_NO_FIELD_TO_EXTRACT && err != AMS_ERROR_NONE)
			break;

	} /* For each memory */

    /* Before you leave, free the memory :-) */
    if (name)
        free(name);
    if (data)
        free(data);
    if (start)
        free(start);
    if (end)
        free(end);

#ifdef AMS_ACCESSOR
	if (step)
		free(step);
#endif

#ifdef AMS_PUBLISHER
	if (err != AMS_NO_FIELD_TO_EXTRACT && err != AMS_ERROR_NONE) {
		AMSP_Finish_synchronized_write_access(memid, nbmem, my_appt);
        for (i=0; i<nbmem; i++)
            AMSP_RelMutexRead(&(MEM_PUB_LOCK[getMemid(memid[i])][pub_id])); /* Release before leaving */
		return err;
	} else {
		err = AMSP_Finish_synchronized_write_access(memid, nbmem, my_appt);
		CheckErr(err);
	}

    for (i=0; i<nbmem; i++)
        AMSP_RelMutexRead(&(MEM_PUB_LOCK[getMemid(memid[i])][pub_id])); /* Release before leaving */

#else
    if (err == AMS_NO_FIELD_TO_EXTRACT)
        err = AMS_ERROR_NONE;
#endif

	/* Free the memory */
	if (memid)
		free(memid);

#ifdef AMS_PUBLISHER

#endif
	return err;

}



/*
 * Given a memory id, this function creates a list of fields. If upd = 0, all fields
 * are returned, otherwise only fields that have changed are returned
 */

int AMSP_Create_Fields_list(AMS_Memory *memid, int nbmem, int upd, char **upd_list, int *list_len)
{
    Field *fld;
    char tmpchar;
    int status, fld_len, err, *len, *send_len, tmpint;
    int i, k, send_fld_len, memlen;
    float tmpflt;
    double tmpdbl;
    char **tmpstr;
    void *data;
    const char *name;

    /* Initialize so the caller will know if the space was allocated */
    *upd_list = NULL;

	for (i=0; i<nbmem; i++){
		CheckMemory(memid[i]);
	    err = Validate(AMSP_Valid_AMS_MEM, MEM_TBL[getMemid(memid[i])][pub_id]);
		CheckErr(err);
	}

	/*
	 * Allocate space
	 */
	len = (int *) malloc(sizeof(int)*nbmem);
	if (len == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	send_len = (int *) malloc(sizeof(int)*nbmem);
	if (send_len == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

	/*
	 * Figure out the length to be created for all memories
	 */
	memlen = 0;
	for (k=0; k<nbmem; k++) {

		len[k] = INT_SIZE; /* To store the length of this memory fields' */
		fld = MEM_TBL[getMemid(memid[k])][pub_id]->fields;
		if (fld == NULL)
			/* We are in trouble */
			return AMS_ERROR_SYSTEM;

		while(fld->next) {
			err = AMSP_Get_Status_Field(fld, &status);
			CheckErr(err);
			if (status == 1 || upd == AMS_FORCE_UPDATE) {
				/* Field has changed */

				/* Name's length */
				name = AMSP_Get_Name_Field(fld, &err);
				CheckErr(err);
				len[k] += strlen(name) + 1;

				/* memory type length */
				len[k] += INT_SIZE;

				/* data type length */
				len[k] += INT_SIZE;

				/* shared type length */
				len[k] += INT_SIZE;

				/* reduction type length */
				len[k] += INT_SIZE;

				/* buffer length */
				data = AMSP_Get_Buffer_Field(fld, &fld_len, &err);
				CheckErr(err);

				if(fld->stype == AMS_REDUCED && fld_len > 0)
					send_fld_len = 1;
				else
					send_fld_len = fld_len;

				if(fld_len < 0)
					return AMS_ERROR_BAD_FIELD_LENGTH;

				/* Length size */
				len[k] += INT_SIZE;

				/* Dimension size */
				len[k] += INT_SIZE;

				if(fld->dim > 0) {
					/* Starting index array */
					len[k] += fld->dim * INT_SIZE;

					/* Ending index array */
					len[k] += fld->dim * INT_SIZE;
				} else
					len[k] += fld->len * INT_SIZE; /* permutation */

				/* buffer length */
				switch(fld->dtype) {
                                case AMS_CHAR:
                                        len[k] += send_fld_len * sizeof(char);
                                        break;
				case AMS_INT:
                                case AMS_BOOLEAN:
					len[k] += send_fld_len * sizeof(int);
					break;
				case AMS_FLOAT:
					len[k] += send_fld_len * sizeof(float);
					break;
				case AMS_DOUBLE:
					len[k] += send_fld_len * sizeof(double);
					break;
				case AMS_STRING:
					tmpstr = (char **)data;
					for (i = 0; i < fld_len; i++)
						if (tmpstr[i])
							len[k] += strlen(tmpstr[i]) + 1; /* +1 for zero terminator */
						else
							len[k] += 1;
					break;
				default:
					return AMS_ERROR_INVALID_DATA_TYPE;
				}
			}

			fld = fld->next;

		} /* For each field */
		memlen += len[k];
		len[k] -= INT_SIZE; /* The length does not include count itself */

	} /* For each memory */

    /* Allocate the space */
    *upd_list = (char *) malloc(memlen * sizeof(char));
    if (*upd_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /*
     * Construct the updated list.
     */
	*list_len = 0;

	for (k=0; k<nbmem; k++) {

		/* Store this memory field's length */
		AMSP_stlong(len[k], *upd_list + *list_len);
		*list_len += INT_SIZE;

		fld = MEM_TBL[getMemid(memid[k])][pub_id]->fields;
		while(fld->next) {

			err = AMSP_Get_Status_Field(fld, &status);
			CheckErr(err);
			if (status == 1 || upd == AMS_FORCE_UPDATE) {
				/* Field has changed */

				/* Store the name */
				name = AMSP_Get_Name_Field(fld, &err);
				CheckErr(err);

				strcpy(*upd_list + *list_len, name);
				*list_len += strlen(name) + 1;

				/* Store the memory type */
				AMSP_stlong(fld->mtype, *upd_list + *list_len);
				*list_len += INT_SIZE;

				/* Store the data type */
				AMSP_stlong(fld->dtype, *upd_list + *list_len);
				*list_len += INT_SIZE;

				/* Store the shared type */
				AMSP_stlong(fld->stype, *upd_list + *list_len);
				*list_len += INT_SIZE;

				/* Store the reduction type */
				AMSP_stlong(fld->rtype, *upd_list + *list_len);
				*list_len += INT_SIZE;

				/* Store the buffer and its length */
				data = AMSP_Get_Buffer_Field(fld, &fld_len, &err);
				CheckErr(err);

				if(fld->stype == AMS_REDUCED && fld_len > 0)
					send_fld_len = 1;
				else
					send_fld_len = fld_len;

				if(fld_len < 0)
					return AMS_ERROR_BAD_FIELD_LENGTH;

				AMSP_stlong(send_fld_len, *upd_list + *list_len);
				*list_len += INT_SIZE;                 /* length */

				AMSP_stlong(fld->dim, *upd_list + *list_len);
				*list_len += INT_SIZE;

				if(fld->dim > 0) {
					for(i = 0; i < fld->dim; ++i) {
						AMSP_stlong(fld->start[i], *upd_list + *list_len);
						*list_len += INT_SIZE;                  /* index */
					}

					for(i = 0; i < fld->dim; ++i) {
						AMSP_stlong(fld->end[i], *upd_list + *list_len);
						*list_len += INT_SIZE;                  /* index */
					}
				} else {
					for(i = 0; i < fld->len; ++i) {
						AMSP_stlong(fld->end[i], *upd_list + *list_len);
						*list_len += INT_SIZE;
					}
				}

				/*
				 * If data type AMS_INT, AMS_BOOLEAN, or AMS_FLOAT, or AMS_DOUBLE make
				 * sure the byte ordering is done properly
				 */

				switch(fld->dtype) {
                                case AMS_CHAR:
                                    if(fld->stype == AMS_REDUCED && fld->len > 0)
                                    {
                                      err = AMSP_Get_Reduced_Field(fld, (void *) &tmpchar);
                                      CheckErr(err);
                                      memcpy(*upd_list + *list_len, &tmpchar, sizeof(char));
                                      *list_len += sizeof(char);
                                    }
                                    else
                                    {
                                      for (i = 0 ;i < fld->len; i++)
                                      {
                                        tmpchar = *( (char *)(data) + i);
                                        memcpy(*upd_list + *list_len, &tmpchar, sizeof(char));
                                        *list_len += sizeof(char);
                                      }
                                    }
                                    break;
				case AMS_INT:
                                case AMS_BOOLEAN:
					if(fld->stype == AMS_REDUCED && fld->len > 0) {
						err = AMSP_Get_Reduced_Field(fld, (void *) &tmpint);
						CheckErr(err);
						AMSP_stlong(tmpint, *upd_list + *list_len);
                                                *list_len += sizeof(int);
					}
					else {
						for (i = 0 ;i < fld->len; i++) {
							tmpint = *( (int *)(data) + i);
							AMSP_stlong(tmpint, *upd_list + *list_len);
                                                        *list_len += sizeof(int);
						}
					}
					break;
				case AMS_FLOAT:
					if(fld->stype == AMS_REDUCED && fld->len > 0) {
						err = AMSP_Get_Reduced_Field(fld, (void *) &tmpflt);
						CheckErr(err);
						AMSP_stfloat((float)tmpflt, *upd_list + *list_len);
                                                *list_len += sizeof(float);
					}
					else {
						for (i = 0 ;i < fld->len; i++) {
							tmpflt = *((float *)(data) + i);
							AMSP_stfloat((float)tmpflt, *upd_list + *list_len);
                                                        *list_len += sizeof(float);
						}
					}
					break;

				case AMS_DOUBLE:
					if(fld->stype == AMS_REDUCED && fld->len > 0) {
						err = AMSP_Get_Reduced_Field(fld, (void *) &tmpdbl);
						CheckErr(err);
						AMSP_stdouble(tmpdbl, *upd_list + *list_len);
						*list_len += sizeof(double);
					}
					else {
						for (i = 0 ;i < fld->len; i++) {
							tmpdbl = *((double *)(data) + i);
							AMSP_stdouble(tmpdbl, *upd_list + *list_len);
							*list_len += sizeof(double);
						}
					}
					break;

				case AMS_STRING:
					tmpstr = (char **)data;
					for (i = 0 ;i < fld->len; i++) {
						if (tmpstr[i]) {
							memcpy(*upd_list + *list_len, tmpstr[i],
								   strlen(tmpstr[i]) + 1);
							*list_len += strlen(tmpstr[i]) + 1;
						} else {
							/* Zero terminator */
							memset(*upd_list + *list_len, 0, 1);
							*list_len += 1;
						}
					}
					break;

				default:
					return AMS_ERROR_INVALID_DATA_TYPE;

				}
			}
			fld = fld->next;
		} /* For each field */

	} /* For each memory */

    /* Just Checking */
    if (memlen != *list_len) {
        /* We are in trouble */
        AMSP_err_quit(" Create_Fields_list: Length calculated differs from length sent ");
    }

    return AMS_ERROR_NONE;
}

/*
 * Given a memory id, this function creates a list of fields that have changed
 * The list is sent to update the publisher or accessor copy.
 */
int AMSP_Create_Upd_Memory_list(AMS_Memory *memid, int nbmem, unsigned char *step,
                           char **upd_list, int *list_len, RequestID req)
{
    /* Return only those fields that changed */
    int err, err1;

#ifdef AMS_PUBLISHER
    Appointment **my_appt;
    int flag, err2, isnewer, i, j;

	/*
	 * Verify that the memory is published. Keep the lock if it is published
	 */
    err = AMSP_IsPublished_AMS_Memory(memid, nbmem, AMS_KEEP_LOCK, &flag);
    CheckErr(err);

	if (!flag)
		return AMS_ERROR_MEMORY_NO_LONGER_PUBLISHED;


	/*
	 * check whether the accessor has an updated version
	 */

	for (i=0; i<nbmem; i++) {

		err1 =  AMSP_Lock_Read_AMS_Memory(memid[i]);
		CheckErr(err1);

		err = AMSP_IsNewerStep_AMS_Memory(memid + i, 1, step + i*STEP_BYTES, &isnewer);
		err1 =  AMSP_UnLock_Read_AMS_Memory(memid[i]);
		CheckErr(err1);

		if (err != AMS_ERROR_NONE) {
            for (j=0; j<nbmem; j++)
                AMSP_RelMutexRead(&(MEM_PUB_LOCK[getMemid(memid[j])][pub_id])); /* Release before leaving */
	        return err;
        }

		if(isnewer) {
			break; /* Found at least one memory that needs to be updated */
		}
	}

	if (!isnewer) {

		*list_len = 0; /* No need to change */
        for (j=0; j<nbmem; j++)
            AMSP_RelMutexRead(&(MEM_PUB_LOCK[getMemid(memid[j])][pub_id])); /* Release before leaving */

		return AMS_ERROR_NONE;
	}
	/*
	 * Else, take a synchronized (among other processors) read access
	 */
    err = AMSP_Take_synchronized_read_access(memid, nbmem, req, &my_appt);
    CheckErr(err);

#endif

	/*
	 * Create the list of fields
	 */
#ifdef AMS_PUBLISHER
	err = AMSP_Create_Fields_list(memid, nbmem, AMS_FORCE_UPDATE, upd_list, list_len);
#else
	err = AMSP_Create_Fields_list(memid, nbmem, 0, upd_list, list_len);
#endif
	/*
	 * Get the memory step
	 */
	err1 = AMSP_GetStep_AMS_Memory(memid, nbmem, step);

#ifdef AMS_PUBLISHER
    err2 = AMSP_Finish_synchronized_read_access(memid, nbmem, my_appt);
    CheckErr(err2);

    for (j=0; j<nbmem; j++)
        AMSP_RelMutexRead(&(MEM_PUB_LOCK[getMemid(memid[j])][pub_id])); /* Release before leaving */
#endif
	/*
	 * Check previous errors
	 */
    CheckErr(err);
    CheckErr(err1);

    return AMS_ERROR_NONE;
}

/*
 * For each memory, list all the fields attached to them.
 */
int AMSP_Get_Memory_fields(AMS_Memory *memid, int nbmem, char **fld_list, int *list_len)
{
    int err;

#ifdef AMS_PUBLISHER
	int i, flag;

    /*
     * Make sure the memory is published and keep the lock on return
     * if the memory is published
     */
	err = AMSP_IsPublished_AMS_Memory(memid, nbmem, AMS_KEEP_LOCK, &flag);
    CheckErr(err);


    if (!flag)
		return AMS_ERROR_MEMORY_NO_LONGER_PUBLISHED;


    for (i=0; i<nbmem; i++) {
		flag = AMSP_Worker_take_read_access(memid[i]);
		CheckErr(flag);
	}

#endif

	err = AMSP_Create_Fields_list(memid, nbmem, AMS_UPDATE, fld_list, list_len);

#ifdef AMS_PUBLISHER

	for (i=0; i<nbmem; i++) {
		flag = AMSP_Worker_grant_read_access(memid[i]);
		CheckErr(flag);
	}

    for (i=0; i<nbmem; i++)
        AMSP_RelMutexRead(&(MEM_PUB_LOCK[getMemid(memid[i])][pub_id])); /* Release before leaving */

#endif


    return err;
}


/*
 * Given a Communicator name, this function returns its id
 */
int AMSP_Get_Comm_ID(char *name, AMS_Comm *ams)
{
    return AMSP_Get_AMS_Comm(name, ams);
}

/*
 * This function returns the list of published communicators
 * The caller has the responsibility of memory management
 */
int AMSP_Get_Comm_list(char **comm_list, int *list_len)
{
    int id, err, len = 0;
    COMM_STRUCT *com_str;
    HOST_NODE *cur_node;
    char *buff=NULL, tmp[20];

    *comm_list = NULL;

    err = AMSP_Lock_TBL_Read_Comm_Struct();
    CheckErr(err);

    /* Length */
    len = 0;
    for (id = 0; id < MAX_COMM; id++) {

        if (PUBCOMM_TBL[pub_id][id] == 1) {

            com_str = COMM_TBL[pub_id][id];
            if (com_str == NULL) {
                /* Should not happen */
                err = AMSP_UnLock_TBL_Read_Comm_Struct();
                CheckErr(err);
                return AMS_ERROR_SYSTEM;
            }

            cur_node = com_str->host_node;
            len += strlen(com_str->name) + 1; /* +1 the '|' */

            while(cur_node != com_str->TailHostNode) {
                len += strlen(cur_node->hostname) + 1; /* +1 the '|' */

#ifdef AMS_PUBLISHER
                /* This is no longer good enough... but it
                   should never be executed */
                if(cur_node->port == -1)
                    cur_node->port = com_str->pub_arg->port;
#endif
                sprintf(tmp, "%d", cur_node->port);
                len += strlen(tmp) + 1; /* +1 for '|' or '\0' */

                cur_node = cur_node->next;
            }
        }

    }

    /* Allocate Memory */
    *comm_list = (char *) malloc (len * sizeof(char));
    if (*comm_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

	buff = (char *) malloc ((len+1) * sizeof(char));
    if (buff == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Data */
    *list_len = 0;
    for (id = 0; id < MAX_COMM; id++) {

        if (PUBCOMM_TBL[pub_id][id] == 1) {

            com_str = COMM_TBL[pub_id][id];
            if (com_str == NULL) {
                /* Should not happen */
                err = AMSP_UnLock_TBL_Read_Comm_Struct();
                CheckErr(err);
                return AMS_ERROR_SYSTEM;
            }

            /* copy com_name + host_name + port# */
            strcpy(buff, com_str->name);

            cur_node = com_str->host_node;
            while(cur_node != com_str->TailHostNode) {
                strcat(buff, "|"); /* Hope that the caller is not using a | in the name */
                strcat(buff, cur_node->hostname);
                strcat(buff, "|");

#ifdef AMS_PUBLISHER
                /* No longer good enough */
                if(cur_node->port == -1)
                    cur_node->port = cur_node->port;
#endif
                sprintf(tmp, "%d", cur_node->port);
                strcat(buff, tmp);

                strcpy(*comm_list + *list_len, buff);
                cur_node = cur_node->next;
            }
            *list_len += strlen(buff) + 1; /* +1 for zero terminator */
            (*comm_list)[*list_len - 1] = '\0';

        }

    }

	/* Free the buffer */
	if (buff)
		free(buff);

    err = AMSP_UnLock_TBL_Read_Comm_Struct();
    CheckErr(err);

    if (len != *list_len) {
        AMSP_err_quit(" Get_Comm_list: Length calculated %d differs from length sent %d",len, *list_len);
    }

    return AMS_ERROR_NONE;
}

/*
 * Given a communicator id, this function returns the list
 * of memory name published within that communicator
 */
int AMSP_Get_Memory_list(AMS_Comm ams, char **mem_list, int *list_len)
{
    *mem_list = NULL;
    *list_len = 0;
    return AMSP_Get_Memory_list_AMS_Comm(ams, mem_list, list_len);
}

/*
 * Given a communicator id, this function returns the list
 * of hosts and port# that the communicator is using
 */
int AMSP_Get_Host_list(AMS_Comm ams, char **host_list, int *list_len)
{
    *host_list = NULL;
    *list_len = 0;
    return AMSP_Get_Host_list_AMS_Comm(ams, host_list, list_len);
}

/*
 * Given a memory name, this function returns that memory id
 */
int AMSP_Get_Memory_ID(char *name, int nbmem, AMS_Memory *memid)
{
    return AMSP_Get_AMS_Memory(name, nbmem, memid);
}





