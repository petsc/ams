/*
 * Send and receive packets.
 */


#include    <sys/stat.h>
#include    <ctype.h>
#include    <string.h>
#include    <stdlib.h>

#include    "amsdefs.h"
#include    "sendfun.h"
#include    "convert.h"
#include    "netapi.h"
#include    "netlib.h"
#include    "ams.h"
#include    "amisc.h"
#ifdef AMS_PUBLISHER
#include    "updatesync.h"
#endif
#include    "amem.h"


/*
 * Extracts a string from a buffer
 */

void AMSP_extract(const char *org, char *str, int nbytes, int *i)
{

    int j = 0;
    while(*i < nbytes && org[*i]) {
        str[j] = org[*i];
        (*i)++;
        j++;
    }

    str[j] = 0;
    (*i)++;
    return;
}

                
/*
 * Send Requests.
 */

int AMSP_send_rq(int opcode, int sfd, ...)
{
    va_list args;
    int sendlen = 0, list_len, reqsize, err, nbmem, timeout;
    char *sendbuff = NULL;   /* don't use static -- not thread safe! */
    char *p, *upd_list;
    unsigned char *step;
    AMS_Comm ams;
    AMS_Memory *pmemid, i;
    RequestID req;

    va_start(args, sfd);

    sendlen = INT_SIZE;

    switch (opcode) {
    case OP_ATTACH_COMM:
        /* op_code, name */
        p = (char *)va_arg(args, char *);
        sendlen += strlen(p) + 1;       /* +1 for null byte at end of fname */

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

        memcpy(sendbuff + sendlen, p, strlen(p)+1);
        sendlen += strlen(p)+1;
        break;

    case OP_MEM_LIST:
    case OP_DETACH_COMM:

        ams = (AMS_Comm)va_arg(args, AMS_Comm);
        sendlen += INT_SIZE;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(ams, sendbuff + sendlen);
        sendlen += INT_SIZE;

        break;

    case OP_COMM_LIST:
        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        /* Just send the request code OP_COMM_LIST */
        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;
        break;

    case OP_COMM_PORT:
        /* op_code, name */
        p = (char *)va_arg(args, char *);
        sendlen += strlen(p) + 1;       /* +1 for null byte at end of name */

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

        memcpy(sendbuff + sendlen, p, strlen(p)+1);
        sendlen += strlen(p)+1;
		break;

    case OP_ATTACH_MEM:
        /* op_code, name, nb memories */
        p = (char *)va_arg(args, char *);
        sendlen += strlen(p) + 1;       /* +1 for null byte at end of name */

		nbmem = (int) va_arg(args, int);
		sendlen += INT_SIZE;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

        memcpy(sendbuff + sendlen, p, strlen(p)+1);
        sendlen += strlen(p) + 1;

		AMSP_stlong(nbmem, sendbuff + sendlen);
        sendlen += INT_SIZE;
        break;

    case OP_MEM_FLDS:
        /* op_code, memid, nbmem */
        pmemid = (AMS_Memory *)va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);

        sendlen += INT_SIZE; /* Nb memories */
		sendlen += INT_SIZE * nbmem; /* Memories */

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = opcode;

		AMSP_stlong(nbmem, sendbuff + sendlen);
        sendlen += INT_SIZE;

		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}
        break;

    case OP_SEND_UPD_MEM:
        /* op_code, RequestID, nbmem, memid, upd_list, list_len */
        req = (RequestID) va_arg(args, RequestID);
        err = AMSP_Size_RequestID(req, &reqsize);
        CheckErr(err);
        sendlen += reqsize;

        pmemid = (AMS_Memory *)va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);

        sendlen += INT_SIZE; /* Nb memories */
		sendlen += INT_SIZE * nbmem; /* Memories */

        upd_list = (char *)va_arg(args, char *);
        list_len = (int) va_arg(args, int);

        sendlen += list_len; /* For upd_list */

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

        /* Request ID */
        err = AMSP_Copy_RequestID(sendbuff + sendlen, req);
        CheckErr(err);
        sendlen += reqsize;

		AMSP_stlong(nbmem, sendbuff + sendlen);
        sendlen += INT_SIZE;

		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}

        /* Fields */
        memcpy(sendbuff + sendlen, upd_list, list_len);
        sendlen += list_len;

        break;

    case OP_RECV_UPD_MEM:

        pmemid = (AMS_Memory *)va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);

        sendlen += INT_SIZE; /* Nb memories */
		sendlen += INT_SIZE * nbmem; /* Memories */

        step = (unsigned char *) va_arg(args, unsigned char *);
        sendlen += STEP_BYTES * nbmem;

        req = (RequestID) va_arg(args, RequestID);
        err = AMSP_Size_RequestID(req, &reqsize);
        CheckErr(err);
        sendlen += reqsize;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

		AMSP_stlong(nbmem, sendbuff + sendlen);
        sendlen += INT_SIZE;

		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}

        memcpy(sendbuff + sendlen, step, STEP_BYTES * nbmem);
        sendlen += STEP_BYTES * nbmem;

        err = AMSP_Copy_RequestID(sendbuff + sendlen, req);
        CheckErr(err);
        sendlen += reqsize;

        break;

#ifdef AMS_PUBLISHER
    case OP_NEGOTIATE_STEP:
    case OP_NOTIFY_STEP:

		nbmem = 1; /* At this time, only one memory is supported for negotiation */
        ams = (AMS_Comm) va_arg(args, AMS_Comm);
        sendlen += INT_SIZE;

        req = (RequestID) va_arg(args, RequestID);
        err = AMSP_Size_RequestID(req, &reqsize);
        CheckErr(err);
        sendlen += reqsize;

        pmemid = (AMS_Memory *) va_arg(args, AMS_Memory *);
        sendlen += INT_SIZE * nbmem;

        step = (unsigned char *) va_arg(args, unsigned char *);
        sendlen += STEP_BYTES * nbmem;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(ams, sendbuff + sendlen);
        sendlen += INT_SIZE;

        err = AMSP_Copy_RequestID(sendbuff + sendlen, req);
        CheckErr(err);
        sendlen += reqsize;

		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}

        memcpy(sendbuff + sendlen, step, STEP_BYTES * nbmem);
        sendlen += STEP_BYTES * nbmem;

        break;

#endif

        case OP_UNLOCK_MEM:

        pmemid = (AMS_Memory *)va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);

        sendlen += INT_SIZE; /* Nb memories */
		sendlen += INT_SIZE * nbmem; /* Memories */

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

		AMSP_stlong(nbmem, sendbuff + sendlen);
        sendlen += INT_SIZE;

		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}

        break;

        case OP_LOCK_MEM:

        pmemid = (AMS_Memory *)va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);
        timeout = (int) va_arg(args, int);

        sendlen += INT_SIZE; /* Nb memories */
		sendlen += INT_SIZE * nbmem; /* Memories */
        sendlen += INT_SIZE; /* Timeout */

        /* Request ID */
        req = (RequestID) va_arg(args, RequestID);
        err = AMSP_Size_RequestID(req, &reqsize);
        CheckErr(err);
        sendlen += reqsize;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;

		AMSP_stlong(nbmem, sendbuff + sendlen);
        sendlen += INT_SIZE;

		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}

		AMSP_stlong(timeout, sendbuff + sendlen);
        sendlen += INT_SIZE;

        err = AMSP_Copy_RequestID(sendbuff + sendlen, req);
        CheckErr(err);
        sendlen += reqsize;

        break;

    case OP_SHUTDOWN:
        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        /* Just send the request code OP_SHUTDOWN */
        AMSP_stlong(opcode, sendbuff);
        sendlen = INT_SIZE;
        break;

    default:
        AMSP_err_quit(" Unknown operation code : %d ", opcode);
        break;
    }

    err = AMSP_mem_net_send(sendbuff, sendlen, sfd);

    va_end(args);
    if(sendbuff)
        free(sendbuff);

    return err;
}

/*
 * Error packet received in response to a request.
 * We need to print the error message that's returned.
 */

int AMSP_recv_rqerr(char *ptr,int nbytes,int sfd,int ver_sent,int op_sent,int op_recv,va_list args)
  /* *ptr points just past received opcode */
  /* nbytes doesn't include received opcode */
{
    int *perr;
    int ecode = AMSP_ldlong(ptr);

    perr = (int *) va_arg(args, int *);
    *perr = ecode;

    ptr += INT_SIZE;
    nbytes -= INT_SIZE;

    return AMS_ERROR_COMM_PROTOCOL; 
}

/*
 * The publisher is being shutdown while the accessor was waiting
 * for a response to a previous request.
 */

int AMSP_recv_shutdown(char *ptr, int nbytes, int sfd, int ver_sent, int op_sent, int op_recv,va_list args)
/* *ptr points just past received opcode */
/* nbytes doesn't include received opcode */
{
    long *pStat = (long *) va_arg(args, long *);
    *pStat = AMSP_ldlong(ptr);

    return AMS_ERROR_NONE; /* terminate finite state loop */
}

/* Receive results */
int AMSP_recv_rslt(char *ptr, int nbytes, int sfd, int ver_sent, int op_sent, int op_recv, va_list args)
  /* *ptr   points just past received opcode */
  /*  nbytes doesn't include received opcode */
{
    int *pStat, err, *pport, *plist_len, proc_num, *flag;
	int nbmem, nbmemrecv, i;
    AMS_Comm *pams;
    AMS_Memory *pmemid;
    char ***mem_list, ***comm_list, *host_list;
        unsigned char *stepanswer;

    err = AMSP_Check_Net_Version(ver_sent, sfd);
    if(err == AMS_ERROR_NET_PROTOCOL)
        return (-1);

    pStat = (int *) va_arg(args, int *);
    *pStat = AMSP_ldlong(ptr);

    ptr += INT_SIZE; nbytes -= INT_SIZE;

    if (*pStat != AMS_ERROR_NONE)
        return AMS_ERROR_NONE; /* We're done */

    switch(op_sent) {
    case OP_ATTACH_COMM:
        /* Receives the Communicator ID */
        pams = (AMS_Comm *) va_arg(args, AMS_Comm *);
        *pams = AMSP_ldlong(ptr);
        ptr = ptr + INT_SIZE;

        /* Receive the list of hosts */
        host_list = (char *) va_arg(args, char *);
        plist_len = (int *) va_arg(args, int *);
                
        *plist_len = AMSP_ldlong(ptr);
        ptr = ptr + INT_SIZE;
                
        memcpy(host_list, ptr, *plist_len);

        break;

    case OP_MEM_LIST:
        /* Receives a memory list (only mem symbols are received) */
        mem_list = (char ***) va_arg(args, char ***);
        err = AMSP_Create_Memory_list(ptr, nbytes, mem_list);
        if (err != AMS_ERROR_NONE)
            AMSP_err_quit(" Error %d, can't create memory list ",err);
        break;

    case OP_COMM_LIST:
        /* Receive the list hosts/ports/comm_names */
        comm_list = (char ***) va_arg(args, char ***);
        err = AMSP_Create_Comm_list(ptr, nbytes, comm_list);
        if (err != AMS_ERROR_NONE)
            AMSP_err_quit(" Error %d, can't create communicator list ",err);
        break;

    case OP_COMM_PORT:
        /* Receives a port number */
        pport = (int *) va_arg(args, int *);
        *pport = AMSP_ldlong(ptr);
        break;
                
    case OP_ATTACH_MEM:
        pmemid = (AMS_Memory *) va_arg(args, AMS_Memory *);
		nbmem = (int ) va_arg(args, int); /* Number of memories expected */
	
		nbmemrecv =  AMSP_ldlong(ptr); /* Nb of memories received */
		ptr = ptr + INT_SIZE;
		
		if (nbmemrecv != nbmem)
			AMSP_err_quit(" Attached %d memory(ies) instead of %d \n", nbmemrecv, nbmem);

		for (i=0; i<nbmem; i++) {
			pmemid[i] = AMSP_ldlong(ptr);
			ptr = ptr + INT_SIZE;
		}
			
        break;

    case OP_MEM_FLDS:
        /* Receives a list of fields attached to a memory */
        err = AMSP_Create_Memory_fields(ptr, nbytes);
        if (err != AMS_ERROR_NONE)
            AMSP_err_quit( "Error %d, can't extract the received list of fields ",err);
        break;

    case OP_SEND_UPD_MEM:
        break;

    case OP_RECV_UPD_MEM:
        /* Receives a list of updated fields in a memory */
        proc_num = (int) va_arg(args, int);
        flag = (int *) va_arg(args, int *);
        stepanswer = (unsigned char *) va_arg(args, unsigned char *);

        err = AMSP_Update_Memory_fields(ptr, nbytes, proc_num, flag,
                                   stepanswer, NULL);
        if (err != AMS_ERROR_NONE)
            AMSP_err_quit( "Error %d Can't create a list of fields ",err);
        break;

    /* No action needed in the following cases */
    case OP_DETACH_COMM:
    case OP_NEGOTIATE_STEP:
    case OP_NOTIFY_STEP:
    case OP_UNLOCK_MEM:
    case OP_LOCK_MEM:
        break;

    default:
        AMSP_err_dump(" invalid operation code %d\n", op_sent);
        break;
    }

    return AMS_ERROR_NONE;
}


/*
 * The publisher received a request packet.
 */
int AMSP_recv_rq(char *ptr, int nbytes, int sfd, int ver_sent, int op_sent, int op_recv, va_list args)
{
    int list_len = 0, nbmem, i;
    int err, flag, index = 0;
    RequestID req;
    int reqsize;
#ifdef AMS_PUBLISHER
    int port, timeout;
#endif
    unsigned char *step;
    char *buff = NULL, tmpbuff[255];
    AMS_Comm ams;
    AMS_Memory *pmemid;

    err = AMSP_Check_Net_Version(ver_sent, sfd);
    if(err == AMS_ERROR_NET_PROTOCOL)
        return AMS_ERROR_NONE;

    switch(op_recv) {
    case OP_ATTACH_COMM:
        /* Extract the communicator's name */
        AMSP_extract(ptr, tmpbuff, nbytes, &index);

        /* Call an api to attach the communicator */
        err = AMSP_Get_Comm_ID(tmpbuff, &ams);

        if (err == AMS_ERROR_NONE) {
            /* Call another api to get the list of hosts attached to this comm */
            err = AMSP_Get_Host_list(ams, &buff, &list_len);
        }
        
		/* Send results */
        err = AMSP_send_rslt(op_recv, sfd, err, ams, buff, list_len);
        if (buff)
            free(buff);
        break;

    case OP_MEM_LIST:
        /* Extract the communicator's id */
        ams = AMSP_ldlong(ptr);

        /* Call an api to get a list of a published memory */
        err = AMSP_Get_Memory_list(ams, &buff, &list_len);
        /* Send results */
        err = AMSP_send_rslt(op_recv, sfd, err, buff, list_len); 
        if (buff)
            free(buff); 
        break;

    case OP_COMM_LIST:
        /* Call an api to get a list of a published communicator */
        err = AMSP_Get_Comm_list(&buff, &list_len);
        /* Send results */
        err = AMSP_send_rslt(op_recv, sfd, err, buff, list_len); 
        if (buff)
            free(buff);
		
        break;

    case OP_COMM_PORT:
#ifdef AMS_PUBLISHER
        /* Extract the communicator's name */
        AMSP_extract(ptr, tmpbuff, nbytes, &index);

        /* Call an api to attach the communicator */
        err = AMSP_Get_Comm_ID(tmpbuff, &ams);

        /* Call an api to get the port we're using for communicator */
        err = AMSP_Get_Comm_local_port(ams, &port);

        err = AMSP_send_rslt(op_recv, sfd, err, port);
#endif
        break;

    case OP_ATTACH_MEM:
        /* Extract the memory's name */
        AMSP_extract(ptr, tmpbuff, nbytes, &index);

		/* How many memories to attach? */
		nbmem = AMSP_ldlong(ptr+index);

		/* Allocate memory */
		pmemid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
		if (pmemid == NULL)
			AMSP_err_quit(" Not enough memory \n");

        /* Call an api to attach the memory */
        err = AMSP_Get_Memory_ID(tmpbuff, nbmem, pmemid);
        
		/* Send results */
        err = AMSP_send_rslt(op_recv, sfd, err, pmemid, nbmem);
		/* Free the memory */
		if (pmemid)
			free(pmemid);
        break;

    case OP_MEM_FLDS:

		/* How many memories */
		nbmem = AMSP_ldlong(ptr);
		ptr += INT_SIZE;

		/* Allocate Memory */
		pmemid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
		if (pmemid == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		for (i = 0; i<nbmem; i++) {
			/* Extract the memory's id */
			pmemid[i] = AMSP_ldlong(ptr);
			ptr += INT_SIZE;
		}
			
        /* Call an api to get a list of fields attached to a memory */
        err = AMSP_Get_Memory_fields(pmemid, nbmem, &buff, &list_len);

        /* Send results */
        err = AMSP_send_rslt(op_recv, sfd, err, pmemid, nbmem, buff, list_len);

        if (buff)
            free(buff); /* Was allocated in Get_Memory_fields. I know there is a better way to do this */
		if (pmemid)
			free(pmemid);

        break;

    case OP_SEND_UPD_MEM:
        req = (RequestID) ptr;
        err = AMSP_Size_RequestID(req, &reqsize);
        CheckErr(err);
        ptr += reqsize;
        nbytes -= reqsize;

        /* We just received a list memory fields to update */
        err = AMSP_Update_Memory_fields(ptr, nbytes, 0, &flag, NULL, req);
        /* Send results of update */
        AMSP_send_rslt(op_recv, sfd, err);
        break;

    case OP_RECV_UPD_MEM:
        /* Received a request to update the accessor's memory */

		/* How many memories */
		nbmem = AMSP_ldlong(ptr);
		ptr += INT_SIZE;

		/* Allocate space */
		pmemid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
		if (pmemid == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		step = (unsigned char *) malloc(STEP_BYTES * nbmem);
		if (step == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		for (i = 0; i<nbmem; i++) {
			/* Extract the memory's id */
			pmemid[i] = AMSP_ldlong(ptr);
			ptr += INT_SIZE;
		}
	
		/* Extract the step number */
		memcpy(step, ptr, STEP_BYTES * nbmem);
		ptr += STEP_BYTES * nbmem;

        req = (RequestID) ptr;

        /* Create a list of fields that need to be updated */
        err = AMSP_Create_Upd_Memory_list(pmemid, nbmem, step, &buff, &list_len, req);

        /* Send the new updated fields to the accessor */
        err = AMSP_send_rslt(op_recv, sfd, err, pmemid, nbmem, step, buff, list_len);

        if (buff)
            free(buff);
		if (step)
			free(step);
		if (pmemid)
			free(pmemid);

        break;

#ifdef AMS_PUBLISHER
    case OP_NEGOTIATE_STEP:
        ams = AMSP_ldlong(ptr);
        ptr += INT_SIZE;

        req = (RequestID) ptr;
        err = AMSP_Size_RequestID(req, &reqsize);
        CheckErr(err);
        ptr += reqsize;

		/* How many memories */
		nbmem = 1; /* At this time the step negotiation is valid for only one memory */

		/* Allocate space */
		pmemid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
		if (pmemid == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		step = (unsigned char *) malloc(STEP_BYTES * nbmem);
		if (step == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		for (i = 0; i<nbmem; i++) {
			/* Extract the memory's id */
			pmemid[i] = AMSP_ldlong(ptr);
			ptr += INT_SIZE;
		}

		/* Extract the step number */
		memcpy(step, ptr, STEP_BYTES * nbmem);
		ptr += STEP_BYTES * nbmem;

		/* Acknowledge request */
        err = AMSP_send_rslt(op_recv, sfd, AMS_ERROR_NONE);
		
		for (i = 0; i<nbmem; i++)
	        AMSP_Continue_Step_Negotiation(ams, req, pmemid[i], step + i*STEP_BYTES);
		
		if (pmemid)
			free(pmemid);
		if (step)
			free(step);

        break;

    case OP_NOTIFY_STEP:
        ams = AMSP_ldlong(ptr);
        ptr += INT_SIZE;

        req = (RequestID) ptr;
        err = AMSP_Size_RequestID(req, &reqsize);
        CheckErr(err);
        ptr += reqsize;

		/* How many memories */
		nbmem = 1; /* At this time the step notification is done only for one memory */

		/* Allocate space */
		pmemid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
		if (pmemid == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		step = (unsigned char *) malloc(STEP_BYTES * nbmem);
		if (step == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		for (i = 0; i<nbmem; i++) {
			/* Extract the memory's id */
			pmemid[i] = AMSP_ldlong(ptr);
			ptr += INT_SIZE;
		}

		/* Extract the step number */
		memcpy(step, ptr, STEP_BYTES * nbmem);
		ptr += STEP_BYTES * nbmem;

        err = AMSP_send_rslt(op_recv, sfd, AMS_ERROR_NONE);
	
		for (i=0; i<nbmem; i++)
			AMSP_Continue_Step_Notification(ams, req, pmemid[i], step +i*STEP_BYTES);

		if (pmemid)
			free(pmemid);
		if (step)
			free(step);

        break;

    case OP_UNLOCK_MEM:
        /* Received a request to unlock the memory */

		/* How many memories */
		nbmem = AMSP_ldlong(ptr);
		ptr += INT_SIZE;

		/* Allocate space */
		pmemid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
		if (pmemid == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		for (i = 0; i<nbmem; i++) {
			/* Extract the memory's id */
			pmemid[i] = AMSP_ldlong(ptr);
			ptr += INT_SIZE;
		}
	
        /* Unlock the main thread (application) memory */
         err = AMSP_Unlock_memory(pmemid, nbmem);

        /* Send operation's results to the accessor */
        err = AMSP_send_rslt(op_recv, sfd, err);

		if (pmemid)
			free(pmemid);

        break;

    case OP_LOCK_MEM:
        /* Received a request to lock the memory */

		/* How many memories */
		nbmem = AMSP_ldlong(ptr);
		ptr += INT_SIZE;

		/* Allocate space */
		pmemid = (AMS_Memory *) malloc(sizeof(AMS_Memory) * nbmem);
		if (pmemid == NULL)
			AMSP_err_quit(" recv_RQ: Not enough memory \n");

		for (i = 0; i<nbmem; i++) {
			/* Extract the memory's id */
			pmemid[i] = AMSP_ldlong(ptr);
			ptr += INT_SIZE;
		}
	
        /* Extract the timeout to block the memory for */
        timeout = AMSP_ldlong(ptr);
		ptr += INT_SIZE;

        /* Extract request id */
        req = (RequestID) ptr;

        /* Lock the main thread (application) memory */
        err = AMSP_Lock_memory(pmemid, nbmem, timeout, req);

        /* Send operation's results to the accessor */
        err = AMSP_send_rslt(op_recv, sfd, err);

		if (pmemid)
			free(pmemid);

        break;
#endif

    case OP_DETACH_COMM:
        /* Extract the communicator's id */
        ams = AMSP_ldlong(ptr);

        /* 
         * 007: This request would be useful to manage multiple
         * accesses to the communicator. For instance, we would keep
         * a list of accessors monitoring this communicator and only
         * one of them would be entitled to write accesses. When an
         * accessor detaches a communicator, a token is freed and other
         * accessor may have write access to this communicator
         */

        /* At this time, just acknowledge the request */
        err = AMSP_send_rslt(op_recv, sfd, AMS_ERROR_NONE);
        break;

    case OP_SHUTDOWN:
        /* 
         * The accessor actually did not send this request. The publisher is
         * being shutdown and we treat this as if the accessor requested a
         * shutdown.
         */
        err = AMSP_send_shutdown(sfd);
        break;
    default:
        AMSP_err_dump(" invalid operation code %d\n", op_recv);
        break;
    }

    return err;
}


/*
 * Send an error packet.
 * Note that an error packet isn't retransmitted or acknowledged by
 * the other end, so once we're done sending it, we can exit.
 */

int AMSP_send_error(int ecode, char *string, int sfd)
{
    char sendbuff[512];
    int sendlen = 0, err;

    AMSP_stlong(OP_ERROR, sendbuff);
    AMSP_stlong(ecode, sendbuff + INT_SIZE);

    strcpy(sendbuff + 2*INT_SIZE, string);

    sendlen = 2*INT_SIZE + strlen(string) + 1;         /* +1 for null at end */
    err = AMSP_mem_net_send(sendbuff, sendlen, sfd);

    return err;
}

/*
 * Send a Shutdowm message to accessor
 */

int AMSP_send_shutdown(int sfd)
{
    char sendbuff[255];
    int sendlen = 0, err;

    AMSP_stlong(OP_SHUTDOWN, sendbuff);
    sendlen = INT_SIZE;
    AMSP_stlong(AMS_ERROR_COMMUNICATOR_NO_LONGER_ALIVE, sendbuff + sendlen);
    sendlen += INT_SIZE;

    err = AMSP_mem_net_send(sendbuff, sendlen, sfd);

    return err;
}

/* 
 * Processing the request is done. Send results to the accessor side
 */
int AMSP_send_rslt( int opcode, int sfd, int err, ...)
{
    char *buff = NULL;
    char *sendbuff = NULL; /* don't use static - not thread safe! */
    int sendlen = 0, nbmem, i;
    int list_len;
    int port;
    unsigned char *step;
    AMS_Comm ams;
    AMS_Memory *pmemid;

    va_list args;
    va_start(args, err);
  
    /* First we need to figure out the length of the data being sent */

    sendlen = INT_SIZE; /* For the opcode: OP_RSLT */
    sendlen += INT_SIZE; /* For the error code */

    switch(opcode) {

    case OP_ATTACH_COMM:

        ams = (AMS_Comm) va_arg(args, AMS_Comm);
        sendlen += INT_SIZE;

        buff = (char *) va_arg(args, char *);
        list_len = (int) va_arg(args, int);
        sendlen += INT_SIZE;
        sendlen += list_len;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;

        AMSP_stlong(ams, sendbuff + sendlen);
        sendlen += INT_SIZE;
                
        AMSP_stlong(list_len, sendbuff + sendlen);
        sendlen += INT_SIZE;

        memcpy(sendbuff + sendlen, buff, list_len);
        sendlen += list_len;

        break;

    case OP_MEM_LIST:
    case OP_COMM_LIST:

        buff = (char *) va_arg(args, char *);
        list_len = (int) va_arg(args, int);
        sendlen += list_len;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;

        memcpy(sendbuff + sendlen, buff, list_len);
        sendlen += list_len;
        break;

    case OP_COMM_PORT:
        port = (int) va_arg(args, int);
        sendlen += INT_SIZE;

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;

        AMSP_stlong(port, sendbuff + sendlen);
        sendlen += INT_SIZE;
        break;


    case OP_ATTACH_MEM:
        pmemid = (AMS_Memory *) va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);

		sendlen += INT_SIZE; /* For the number of memories */
        sendlen += INT_SIZE * nbmem; /* For the array of memories */

        /* Allocate memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;

		AMSP_stlong(nbmem, sendbuff + sendlen);
        sendlen += INT_SIZE;

		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}
        break;

    case OP_MEM_FLDS:

		/* How many memories to send */
        pmemid = (AMS_Memory *) va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);

		sendlen += INT_SIZE; /* For the number of memories */
        sendlen += INT_SIZE * nbmem; /* For the array of memories */

        /* Send the field's buffer */
        buff = (char *) va_arg(args, char *);
        list_len = (int) va_arg(args, int); /* Total length, which is not sent */

        sendlen += list_len; /* For all the fields */

        /* Allocate the memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;

		/* Nb memories */
		AMSP_stlong(nbmem, sendbuff + sendlen);
		sendlen += INT_SIZE;

		/* Each memory id */
		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}

        memcpy(sendbuff + sendlen, buff, list_len);
        sendlen += list_len;

        break;

    case OP_SEND_UPD_MEM:
        /* Allocate the memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;

        break;

    case OP_RECV_UPD_MEM:
		
		/* How many memories to send */
        pmemid = (AMS_Memory *) va_arg(args, AMS_Memory *);
		nbmem = (int) va_arg(args, int);

		sendlen += INT_SIZE; /* For the number of memories */
        sendlen += INT_SIZE * nbmem; /* For the array of memories */

        /* Then send the step number */
        step = (unsigned char *) va_arg(args, unsigned char *);
        sendlen += STEP_BYTES * nbmem;

        /* Send the update list length and buffer */
        buff = (char *) va_arg(args, char *);
        list_len = (int) va_arg(args, int); /* Not sent. The field's length is in the buffer */

        sendlen += list_len;

        /* Allocate the memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;

		/* Nb memories */
		AMSP_stlong(nbmem, sendbuff + sendlen);
		sendlen += INT_SIZE;

		/* Each memory id */
		for (i=0; i<nbmem; i++) {
			AMSP_stlong(pmemid[i], sendbuff + sendlen);
			sendlen += INT_SIZE;
		}

        memcpy(sendbuff + sendlen, step, STEP_BYTES * nbmem);
        sendlen += STEP_BYTES * nbmem;

        memcpy(sendbuff + sendlen, buff, list_len);
        sendlen += list_len;

        break;

    case OP_NEGOTIATE_STEP:
    case OP_NOTIFY_STEP:
    case OP_LOCK_MEM:
    case OP_UNLOCK_MEM:
    case OP_DETACH_COMM:
        /* Send only the error code */

        /* Allocate the memory */
        sendbuff = (char *) realloc(sendbuff, sendlen);
        if (sendbuff == NULL)
            AMSP_err_ret(ams_msg, "Out of Memory");

        AMSP_stlong(OP_RSLT, sendbuff);
        sendlen = INT_SIZE;

        AMSP_stlong(err, sendbuff + sendlen);
        sendlen += INT_SIZE;
        break;

    default:
        AMSP_err_dump(" invalid operation code %d\n", opcode);
        break;
    }

    /* Ouf! now let send all this stuff to the accessor */
    err = AMSP_mem_net_send(sendbuff, sendlen, sfd);

    if(sendbuff)
        free(sendbuff);
    /* Clean our argument list */
    va_end(args);

    return err; /* Done */

}


/* Modify this function so you reject only the versions you can't speak to */
int AMSP_Check_Net_Version(int ver_sent, int sfd)
{
    char tmpbuff[255];
    if(ver_sent != AMS_MEMORY_NET_VERSION) {
#ifdef AMS_PUBLISHER
        sprintf(tmpbuff, "Publisher does not understand accessor's network packets.\n"
                "Publisher speaks version %d; accessor speaks version %d.\n",
                AMS_MEMORY_NET_VERSION, ver_sent);
        AMSP_send_error(AMS_ERROR_NET_PROTOCOL, tmpbuff, sfd);
#else
        sprintf(tmpbuff, "Accessor does not understand publisher's network packets.\n"
                "Publisher speaks version %d; accessor speaks version %d.\n",
                ver_sent, AMS_MEMORY_NET_VERSION);
        AMSP_err_quit(tmpbuff);
#endif
        return AMS_ERROR_NET_PROTOCOL;
    }

    return AMS_ERROR_NONE;
}



