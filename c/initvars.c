/*
 * Initialize the external variables.
 * Some link editors (on systems other than UNIX) require this.
 */

#include        "amsdefs.h"
#include    "ams.h"

int     connected               = 0;
char    AMS_PUB_HOST_NAME[MAXHOSTNAME+1]        = { 0 };
int     AMS_PUB_PORT_NUMBER     = -1;
int     inetdflag               = 1;

/* int     port    = DEFAULT_PORT; */

const char    *str_code[OP_MAX + 1] = {NULL}; 
