/* not-accessor */
/*
 * This module contains AMS Interface to LDAP. It is used to add, query
 * and delete AMS objects from an LDAP publisher
 */

#ifdef AMS_LDAP
#include "acomm.h"
#include "amsldap.h"
#include "netlib.h"

/*@
    AMSP_Ldap_add_publisher - Add a publisher to an LDAP publisher
    
    Input Parameters:
+   ld - Pointer to an LDAP publisher


@*/

int AMSP_Ldap_add_publisher(LDAP *ld)
{
    LDAPMessage	    *result;
    char	dn[255];
    int		i, NMODS;
    int		rc;
    int		msgid;
    int		finished;
    LDAPMod	**mods;
    struct timeval	zerotime;
    char buff[255], cn_buff[255], *p;
    char * cn_values[2], * port_values[2], * host_values[2], * user_values[2];
    char *objectclass_values[] = { "top", "ams", "amspublisher", NULL };

    /* Convert port number to a string */
    sprintf(buff,"%d",PUBLISHER.port);

    /* Full name, or cn values */
    strcpy(cn_buff, PUBLISHER.host);
    strcat(cn_buff, ":");
    strcat(cn_buff, buff);
    
    cn_values[0] = cn_buff;
    cn_values[1] = NULL;

    /* Host name */
    host_values[0] = PUBLISHER.host;
    host_values[1] = NULL;

    /* Port number */
    port_values[0] = buff;
    port_values[1] = NULL;

    /* User values */
#ifdef _WIN_ALICE
    if ((p = getenv("USERNAME")) == NULL) {
        p = getenv("AMS_USER_NAME");
        if (p == NULL)
            p = "ams_user"; /* Default user name */
    }
#else
    if ((p = getlogin()) == NULL) {
        p = getenv("AMS_USER_NAME");
        if (p == NULL)
            p = "ams_user"; /* Default user name */
    }
#endif

    user_values[0] = p;
    user_values[1] = NULL;

    /* Specify the DN for the Publisher we're adding */
    strcpy(dn, "cn=");
    strcat(dn, cn_values[0]);
    strcat(dn, ", cn=ALICE Memory Snooper, ");
    strcat(dn, AMS_BASE);

    /* Save the dn for later use */
    PUBLISHER.dn = (char *) malloc (sizeof(char)*(strlen(dn)+1));

    if (PUBLISHER.dn == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;
    strcpy(PUBLISHER.dn, dn);
    PUBLISHER.ld = ld;

    /* Construct the array of values to add */
    NMODS = 5;
    mods = ( LDAPMod ** ) malloc(( NMODS + 1 ) * sizeof( LDAPMod * ));
    if ( mods == NULL )
	    return AMS_ERROR_INSUFFICIENT_MEMORY;
    
    for ( i = 0; i < NMODS; i++ ) {
	    if (( mods[ i ] = ( LDAPMod * ) malloc( sizeof( LDAPMod ))) == NULL ) {
	        return AMS_ERROR_INSUFFICIENT_MEMORY;
	    }
    }

    mods[ 0 ]->mod_op = 0;
    mods[ 0 ]->mod_type = "objectclass";
    mods[ 0 ]->mod_values = objectclass_values;

    mods[ 1 ]->mod_op = 0;
    mods[ 1 ]->mod_type = "cn";
    mods[ 1 ]->mod_values = cn_values;

    mods[ 2 ]->mod_op = 0;
    mods[ 2 ]->mod_type = "host";
    mods[ 2 ]->mod_values = host_values;

    mods[ 3 ]->mod_op = 0;
    mods[ 3 ]->mod_type = "ipserviceport";
    mods[ 3 ]->mod_values = port_values;

    mods[ 4 ]->mod_op = 0;
    mods[ 4 ]->mod_type = "userid";
    mods[ 4 ]->mod_values = user_values;
    mods[ 5 ] = NULL;

    /* Initiate the add operation */
    if (( msgid = ldap_add( ld, dn, mods )) < 0 ) {
        free_mods( mods, NMODS);
	    AMSP_err_quit("ldap_add failed" );
    }

    /* Poll for the result */
    finished = 0;
    while ( !finished ) {
        rc = ldap_result( ld, msgid, 0, &zerotime, &result );
	    switch ( rc ) {
	    case -1:
	        /* some error occurred */
	        AMSP_err_quit("ldap_result" );
	    case 0:
    	    /* Timeout was exceeded.  No entries are ready for retrieval */
	        break;
	    default:
	        /* Should be finished here */
	        finished = 1;
	        if (( rc = ldap_result2error( ld, result, 0 )) != LDAP_SUCCESS ) {
		        AMSP_err_quit("Error while adding entry: %s\n", ldap_err2string( rc ));
	        }
	        ldap_msgfree( result );
	    }
    }

    free_mods( mods, NMODS);

    return AMS_ERROR_NONE;

}

/*@
    AMSP_Ldap_add_communicator - Add a communicator to an LDAP publisher
    
    Input Parameters:
+   ld - Pointer to an LDAP publisher
.   host - Publisher hostname
.   port - Publisher port number
+   ams - AMS Communicator

@*/

int AMSP_Ldap_add_communicator(LDAP *ld, char *host, int port, AMS_Comm ams)
{
    LDAPMessage	    *result;
    char dn[255], *p;
    int i, NMODS, err, nb_proc, am_first;
    int	rc;
    int	msgid;
    int finished;
    LDAPMod	**mods;
    struct timeval	zerotime;
    char buff[255], cn_buff[255];
    char * cn_values[2], ** port_values, ** host_values, * user_values[2];
    char *objectclass_values[] = { "top", "ams", "amspublisher", "amscommunicator", NULL };
    COMM_STRUCT *com_str;
    HOST_NODE *host_node;

    /* Get number of processors */
    err = AMSP_Get_Nb_Proc_AMS_Comm(ams, &nb_proc);
    CheckErr(err);

    if (nb_proc > 1) {

        /* Check if we are processor 0 */
        err = AMSP_Am_I_First_Host_AMS_Comm(ams, &am_first);
	    CheckErr(err);

        /* Do nothing if are not processor 0 */
	if(!am_first)
	    return AMS_ERROR_NONE;
    }
		
    /* Get the communicator structure */
    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    /* Allocate memory */
    port_values = (char **) malloc (sizeof(char *) * (nb_proc + 1));
    if (port_values == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    host_values = (char **) malloc (sizeof(char *) * (nb_proc + 1));
    if (host_values == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Store different hosts and port numbers */
    host_node = com_str->host_node;
    i = 0;
    while(host_node->next) {
        
        sprintf(buff, "%d", host_node->port);
        port_values[i] = (char *) malloc(strlen(buff)+1);
        if (port_values[i] == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        host_values[i] = (char *) malloc(strlen(host_node->hostname)+1);
        if (host_values[i] == NULL)
            return AMS_ERROR_INSUFFICIENT_MEMORY;

        strcpy(port_values[i], buff);
        strcpy(host_values[i], host_node->hostname);

        host_node = host_node->next;
        i++;
    }

    /* Terminate */
    host_values[i] = NULL;
    port_values[i] = NULL;

    /* Add user id */
#ifdef _WIN_ALICE
    if ((p = getenv("USERNAME")) == NULL) {
        p = getenv("AMS_USER_NAME");
        if (p == NULL)
            p = "ams_user"; /* Default user name */
    }
#else
    if ((p = getlogin()) == NULL) {
        p = getenv("AMS_USER_NAME");
        if (p == NULL)
            p = "ams_user"; /* Default user name */
    }
#endif

    user_values[0] = p;
    user_values[1] = NULL;   

    /* Full name, or cn values */
    strcpy(cn_buff, com_str->name);
    
    cn_values[0] = cn_buff;
    cn_values[1] = NULL;

    /* Specify the DN for the Communicator we're adding */

    /* Communicator's name */
    strcpy(dn, "cn=");
    strcat(dn, cn_values[0]); 

    /* Publisher's full name, cn=host:port */
    strcat(dn, ", cn=");
    strcat(dn, host);
    strcat(dn, ":");
    sprintf(buff,"%d",port);
    strcat(dn, buff);

    /* Memory Snooper entry */
    strcat(dn, ", cn=ALICE Memory Snooper, "); 
    strcat(dn, AMS_BASE);

    /* Store ld, dn for future use */
    com_str->pub_arg->dn = (char *) malloc (sizeof(char)*(strlen(dn)+1));
    if (com_str->pub_arg->dn == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    strcpy(com_str->pub_arg->dn, dn);
    com_str->pub_arg->ld = ld;

    /* Now construct the arrays of values to add */
    NMODS = 5;
    mods = ( LDAPMod ** ) malloc(( NMODS + 1 ) * sizeof( LDAPMod * ));
    if ( mods == NULL ) {
	fprintf( stderr, "Cannot allocate memory for mods array\n" );
    }
    for ( i = 0; i < NMODS; i++ ) {
	    if (( mods[ i ] = ( LDAPMod * ) malloc( sizeof( LDAPMod ))) == NULL ) {
	        fprintf( stderr, "Cannot allocate memory for mods element\n" );
	        exit( 1 );
	    }
    }

    mods[ 0 ]->mod_op = 0;
    mods[ 0 ]->mod_type = "objectclass";
    mods[ 0 ]->mod_values = objectclass_values;

    mods[ 1 ]->mod_op = 0;
    mods[ 1 ]->mod_type = "cn";
    mods[ 1 ]->mod_values = cn_values;

    mods[ 2 ]->mod_op = 0;
    mods[ 2 ]->mod_type = "host";
    mods[ 2 ]->mod_values = host_values;

    mods[ 3 ]->mod_op = 0;
    mods[ 3 ]->mod_type = "ipserviceport";
    mods[ 3 ]->mod_values = port_values;

    mods[ 4 ]->mod_op = 0;
    mods[ 4 ]->mod_type = "userid";
    mods[ 4 ]->mod_values = user_values;

    mods[ 5 ] = NULL;

    /* Initiate the add operation */
    if (( msgid = ldap_add( ld, dn, mods )) < 0 ) {
	    AMSP_err_quit("ldap_add failed" );
	    free_mods( mods, NMODS);
	    return( 1 );
    }

    /* Poll for the result */
    finished = 0;
    while ( !finished ) {
        rc = ldap_result( ld, msgid, 0, &zerotime, &result );
	    switch ( rc ) {
	    case -1:
	        /* some error occurred */
	        AMSP_err_quit("ldap_result" );
	    case 0:
    	    /* Timeout was exceeded.  No entries are ready for retrieval */
	        break;
	    default:
	        /* Should be finished here */
	        finished = 1;
	        if (( rc = ldap_result2error( ld, result, 0 )) != LDAP_SUCCESS ) {
		        AMSP_err_quit( "Error while adding entry: %s\n", ldap_err2string( rc ));
	        }
	        ldap_msgfree( result );
	    }
    }

    free_mods( mods, NMODS);

    return AMS_ERROR_NONE;

}

	
#endif

