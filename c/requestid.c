#include "requestid.h"
#include "amsdefs.h"
#include "convert.h"
#include "netlib.h"
#include "network.h"

#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN_ALICE

#include <process.h>
#define pid_t int
#include <winbase.h> 

#endif

/*
 * Returns the current IP address as a 32bit integer
 */
int AMSP_GetIpAddress(unsigned int *ip)
{
    char host[255];
    struct hostent *hent;
    int err;
    
    /* Get the host name */
    err = gethostname(host, sizeof(host));
    if (err < 0)
        AMSP_err_sys(" Get Host Name failed with error: %d ",err);

    /* Get the host name structure */
    hent = gethostbyname(host);
    if (hent == NULL)
        AMSP_err_sys(" Get Host By Name failed");

    /* Copy the address */
    *ip = inet_addr(hent->h_addr_list[0]);
    
    return AMS_ERROR_NONE;
}


int AMSP_Generate_RequestID(RequestID *output)
{
    /* Format is: Length of following stuff, IP address, PID, thread ID */
    unsigned int ip_addr;
    pid_t process;
    char *thread_id;
    int thread_len;
    int total_size;
    int err;
    char *out;

#ifdef _MT
    Thread thrid = AMSP_GetThreadId();
    thread_id = (char *) &thrid;
    thread_len = sizeof(thrid);
#else
    /* no threads */
    thread_id = NULL;
    thread_len = 0;
#endif

    process = getpid();

    err = AMSP_GetIpAddress(&ip_addr);
    CheckErr(err);

    /* Compute total length (not including length prefix) */
    total_size = sizeof(ip_addr) + sizeof(process) + thread_len;

    /* Pack it all in */
    out = (char *) malloc(INT_SIZE + total_size);
    if(out == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    AMSP_stlong(total_size, out);
    memcpy(out + INT_SIZE, &ip_addr, sizeof(ip_addr));
    memcpy(out + INT_SIZE + sizeof(ip_addr), &process, sizeof(process));
    if(thread_len > 0)
        memcpy(out + INT_SIZE + sizeof(ip_addr) + sizeof(process), thread_id,
               thread_len);

    *output = out;

    return AMS_ERROR_NONE;
}

int AMSP_Equals_RequestID(RequestID left, RequestID right)
{
    if(AMSP_ldlong(left) != AMSP_ldlong(right))
        return 0;

    return (!(memcmp((void *) (left + INT_SIZE), (void *) (right + INT_SIZE),
                     AMSP_ldlong(left))));
}

int AMSP_Size_RequestID(RequestID req, int *size)
{
    *size = AMSP_ldlong(req) + INT_SIZE;

    return AMS_ERROR_NONE;
}

extern int AMSP_Copy_RequestID(void *output, RequestID req)
{
    memcpy(output, (void *) req, AMSP_ldlong(req) + INT_SIZE);

    return AMS_ERROR_NONE;
}

extern int AMSP_Delete_RequestID(RequestID req)
{
    free(req);

    return AMS_ERROR_NONE;
}

