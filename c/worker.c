/* not-accessor */
#include <stdlib.h>

#include "thrapi.h"
#include "netapi.h"
#include "netlib.h"
#include "acomm.h"

/*
 * Create a new thread that will handle the accessor request.
 */
int AMSP_ProcessRequest(void *arg, int newsfd)
{
    extern Ret_Thr AMSP_worker_func(void *);
    Thread thr;
    Proc_arg *pro_arg = (Proc_arg *) malloc(sizeof(Proc_arg));
    Pub_arg  *pub_arg;
    int err;

    if (pro_arg == NULL)
        AMSP_err_quit("No enough memory for a new request");

    pub_arg = (Pub_arg *) arg;

    /* Increment the number of accessors, so the publisher would wait*/
    AMSP_LockMutexWrite(&(pub_arg->nbthr_mutex));
    pub_arg->nb_accessor += 1;
    AMSP_RelMutexWrite(&(pub_arg->nbthr_mutex));

    /* Is the publisher being shutdown ? */
    AMSP_LockMutexRead(&(pub_arg->pub_mutex));
    if (pub_arg->alive == 0)
        err = 1; /* True */
    else
        err = 0;
    AMSP_RelMutexRead(&(pub_arg->pub_mutex));

    pro_arg->arg = arg;
    pro_arg->sfd = newsfd;
	if (err == 1)
		pro_arg->last = 1; /* This thread is the last one */
	else
		pro_arg->last = 0;

    AMSP_CreatThread(AMSP_worker_func, (void *)pro_arg, &thr);
        
    return err;
}


/* Worker's Function */
Ret_Thr  AMSP_worker_func(void * func_arg)
{
    extern int AMSP_fsm_loop(int, int, ...);
    int sfd, op_sent, t_flag;

    Proc_arg *pro_arg = (Proc_arg *)func_arg;
    Pub_arg *pub_arg;

    pub_arg = pro_arg->arg;
    sfd = pro_arg->sfd;

    /* Is the publisher being shutdown ? */
    AMSP_LockMutexRead(&(pub_arg->pub_mutex));
    if (pub_arg->alive)
        op_sent = 0;    /* child processes accessor's request */
    else
        op_sent = -1;   /* send accessor a shutdown warning */
    AMSP_RelMutexRead(&(pub_arg->pub_mutex));

    /* Process accessor's request */
    AMSP_fsm_loop(op_sent, sfd);
    AMSP_mem_net_close(sfd);

    /* Decrement the number of accessors */
    AMSP_LockMutexWrite(&(pub_arg->nbthr_mutex));
    pub_arg->nb_accessor -= 1;
	
	if (pub_arg->nb_accessor < 0)
		AMSP_err_quit(" Invalid number of accessor threads %d \n", pub_arg->nb_accessor);

	if (pro_arg->last == 1) {
		/* I am the last accessor thread */
		while (pub_arg->nb_accessor) { 
			/* Wait for other threads */
			AMSP_CondWaitMutexWrite(&(pub_arg->cond), &(pub_arg->nbthr_mutex), 0, &t_flag);
		}
		/* Send a signal to the publisher-thread so it will proceed */
		AMSP_CondSignal(&(pub_arg->cond));
	    AMSP_RelMutexWrite(&(pub_arg->nbthr_mutex));
	} else {
		if (pub_arg->nb_accessor == 0)/* Signal the last thread in case it is waiting */
			AMSP_CondSignal(&(pub_arg->cond));

	    AMSP_RelMutexWrite(&(pub_arg->nbthr_mutex));
	}
        
    /* Should we be freeing the argument ? */
    free(func_arg);  /* I think so */

#ifndef _WIN_ALICE
    return func_arg;
#endif

}
