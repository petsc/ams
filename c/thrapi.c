/* not-accessor */
#include <errno.h>
#include <unistd.h>

#include "thrapi.h"
#include "netlib.h"

/*
 * Thread API. This module tries to hide to difference between
 * NT threads and UNIX (POSIX) threads by providing a common interface
 * to the most popular call to threads.
 * Many thanks to
 *	Douglas C. Schmidt and Irfan Pyarali
 *	Department of Computer Science
 *	Washington University, St. Louis, Missouri
 *	http://www.cs.wustl.edu/~schmidt/win32-cv-1.html
 * for the Condition variable port to Win32 of this code.
 */

/*
 * Creates a new thread and starting with the worker_func
 */
void AMSP_CreatThread(worker_func, pfunc_arg, thr)
     Ret_Thr  worker_func(void *);
     void * pfunc_arg;
     Thread * thr;
{

    Thread thrd;
    int err;

#if !defined(_WIN_ALICE) && !defined(SOLARIS)
    int nbtries = 0;
    pthread_attr_t thread_attr;
#endif

#ifdef _WIN_ALICE
    thrd = _beginthread( worker_func, 0, pfunc_arg); 
    if (thrd == -1) {
        err = GetLastError();
        AMSP_err_dump("Error %d Can't create a worker thread", err);
    }
#else

#ifdef SOLARIS
    err = thr_create(NULL, NULL, worker_func, pfunc_arg,
                     THR_BOUND | THR_DETACHED, &thrd);
    if (err)
        AMSP_err_dump("Error: %d Can't create a thread ",err);       
#else
    pthread_attr_init(&thread_attr);
    pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_DETACHED);
    err = pthread_create(&thrd, &thread_attr, worker_func, pfunc_arg);
    while (err == EAGAIN && nbtries++ < 100)
        err = pthread_create(&thrd, &thread_attr, worker_func, pfunc_arg);

    if (err == EAGAIN)
        AMSP_err_dump("Error: %d  Resource temporarily unavailble", err);

    if (err)
        AMSP_err_dump("Error: %d Can't create a thread ",err);
#endif

#endif

    *thr = thrd;
    return;

}

/*
 * Terminate the calling thread
 */

void AMSP_EndThread(void * status)
{

#ifdef _WIN_ALICE
    _endthread();
#else
#ifdef SOLARIS
    thr_exit(status);
#else
    pthread_exit(status);
#endif

#endif

}

/*
 * Cancels a thread
 */

void AMSP_CancelThread(Thread thr)
{


#ifdef _WIN_ALICE
    /* 
     * 007: The cleanest way to terminate a thread is to send a signal
     * to that thread that will trigger a call to terminate thread. We will
     * address thus issue later.
     */
#else
#ifndef SOLARIS
    int err = pthread_cancel(thr);
    if (err)
        AMSP_err_dump("Error: %d Can't cancel a thread ",err);
#endif
#endif

}

/* 
 * Hopefully, stops the execution of the current thread for
 * msec millisecond
 */
void AMSP_ThreadSleep(int msec)
{

#ifdef _WIN_ALICE
    Sleep(msec);
#elif SGI
    sginap(msec/10);
#else
    usleep(msec);
#endif

}

/*
 * Returns current thread ID
 */

Thread AMSP_GetThreadId()
{

#ifdef _WIN_ALICE
    return GetCurrentThreadId();    
#elif SOLARIS
    return thr_self();
#else
    return pthread_self();
#endif

}

/* 
 * Create a synchronization variables
 */
void AMSP_NewMutex(Mutex *pm)
{
    pm->readers_reading = 0;
    pm->writer_writing = 0;

#ifdef _WIN_ALICE
	InitializeCriticalSection(&(pm->mutex));
#else
#ifdef SOLARIS
    if (mutex_init(&(pm->mutex), NULL, NULL))
        AMSP_err_quit("Error Creating a new Mutex");
#else
    if (pthread_mutex_init(&(pm->mutex), NULL))
        AMSP_err_quit("Error Creating a new Mutex");
#endif

#endif
    /* Initialize the condition variable */
    AMSP_CondInit(&(pm->lock_free));

}

/* 
 * Create a mutex object
 */
void AMSP_NewRealMutex(RealMutex *pm)
{

#ifdef _WIN_ALICE
	InitializeCriticalSection(pm);
#else
#ifdef SOLARIS
    if (mutex_init(pm, NULL, NULL))
        AMSP_err_quit("Error Creating a new Mutex");
#else
    if (pthread_mutex_init(pm, NULL))
        AMSP_err_quit("Error Creating a new Mutex");
#endif

#endif

}

/* 
 * Destroys a synchronization variable and frees the associated resources
 */
void AMSP_DelMutex(Mutex *pm)
{

#ifdef _WIN_ALICE
	DeleteCriticalSection(&(pm->mutex));
#else
    int err = 0;
#ifdef SOLARIS
    if ((err = mutex_destroy(&(pm->mutex))))
        AMSP_err_quit(" Error: %d Can't destroy a mutex ",err);
#else
    if ((err = pthread_mutex_destroy(&(pm->mutex))))
        AMSP_err_quit(" Error: %d Can't destroy a mutex ",err);

#endif

#endif

	/* Destroy the Condition variable */
	AMSP_CondDestroy(&(pm->lock_free));

    return;
}

/* 
 * Destroys a mutex variable
 */
void AMSP_DelRealMutex(RealMutex *pm)
{

#ifdef _WIN_ALICE
	DeleteCriticalSection(pm);
#else
    int err;
#ifdef SOLARIS
    if ((err = mutex_destroy(pm)))
        AMSP_err_quit(" Error: %d Can't destroy a mutex ",err);
#else
    if ((err = pthread_mutex_destroy(pm)))
        AMSP_err_quit(" Error: %d Can't destroy a mutex ",err);

#endif

#endif

    return;
}

/* 
 * Sets a real lock on a synchronization variable
 */

void AMSP_LockRealMutex( RealMutex *pm)
{

#ifdef _WIN_ALICE
    EnterCriticalSection(pm);
#else
    int err;
#ifdef SOLARIS
    if ((err = mutex_lock(pm)))
        AMSP_err_quit("Error %d, Locking a new Mutex",err);
#else
    if ((err = pthread_mutex_lock(pm)))
        AMSP_err_quit("Error %d, Locking a new Mutex",err);

#endif

#endif

}

/*
 * Release a real locked variable
 */

void AMSP_RelRealMutex( RealMutex *pm)
{

#ifdef _WIN_ALICE
    LeaveCriticalSection(pm);
#else
#ifdef SOLARIS
    if (mutex_unlock(pm))
        AMSP_err_quit("Error Releasing a Mutex");
#else
    if (pthread_mutex_unlock(pm))
        AMSP_err_quit("Error Releasing a Mutex");
#endif

#endif

}

/*
 * Read Locking a Reader/Writer Lock
 */
void AMSP_LockMutexRead(Mutex *pm)
{
    int t_flag;
     AMSP_LockRealMutex(&(pm->mutex));
     while(pm->writer_writing) {
         AMSP_CondWait(&(pm->lock_free), &(pm->mutex), 0, &t_flag);
     }
     pm->readers_reading++;
     AMSP_RelRealMutex(&(pm->mutex));
}

/*
 * Write Locking a Reader/Writer Lock
 */
void AMSP_LockMutexWrite(Mutex *pm)
{
    int t_flag;
    AMSP_LockRealMutex(&(pm->mutex));
    while(pm->writer_writing || pm->readers_reading) {
        AMSP_CondWait(&(pm->lock_free), &(pm->mutex), 0, &t_flag);
    }
    pm->writer_writing++;
    AMSP_RelRealMutex(&(pm->mutex));
}

/*
 * Release a Read Lock.
 */
void AMSP_RelMutexRead(Mutex *pm)
{
    AMSP_LockRealMutex(&(pm->mutex));
    if(pm->readers_reading == 0){
        AMSP_RelRealMutex(&(pm->mutex));
        AMSP_err_quit("Error releasing a read-lock that does not exit");
    } else {
        pm->readers_reading--;
        if (pm->readers_reading == 0)
            AMSP_CondSignal(&(pm->lock_free));
        AMSP_RelRealMutex(&(pm->mutex));
    }
}

/*
 * Release a Write Lock.
 */
void AMSP_RelMutexWrite(Mutex *pm)
{
    AMSP_LockRealMutex(&(pm->mutex));
    if(pm->writer_writing == 0){
        AMSP_RelRealMutex(&(pm->mutex));
        AMSP_err_quit("Error releasing a write-lock that does not exit");
    } else {
        pm->writer_writing = 0;
        AMSP_CondBroadcast(&(pm->lock_free));
        AMSP_RelRealMutex(&(pm->mutex));
    }
}

/*
 * Condition Initialization
 */
void AMSP_CondInit(Cond * cond)
{
#ifdef _WIN_ALICE
	
	// Initialize the count to 0.  
	cond->waiters_count_ = 0;

	// Initialize Critical Section
	InitializeCriticalSection(&(cond->waiters_count_lock_));

	// Create an auto-reset event.
	cond->events_[SIGNAL] = CreateEvent (NULL,  // no security
                                     FALSE, // auto-reset event
                                     FALSE, // non-signaled initially
                                     NULL); // unnamed
	if (cond->events_[SIGNAL] == NULL)
		AMSP_err_quit("Error %d, while creating a signal event ", GetLastError());

	// Create a manual-reset event.
	cond->events_[BROADCAST] = CreateEvent (NULL,  // no security
                                        TRUE,  // manual-reset
                                        FALSE, // non-signaled initially
                                        NULL); // unnamed
	if (cond->events_[BROADCAST] == NULL)
		AMSP_err_quit("Error %d, while creating a broadcast event ", GetLastError());

#else
int err;
#ifdef SOLARIS
 if ((err = cond_init(cond, NULL, NULL)))
        AMSP_err_quit("Error %d, on Condition init call");
#else
 if ((err = pthread_cond_init(cond, NULL)))
        AMSP_err_quit("Error %d, on Condition init call");
#endif

#endif
}

/*
 * Destroy a Condition Variable
 */
void AMSP_CondDestroy(Cond *cond)
{
#ifdef _WIN_ALICE
	
	// Destroy the auto-reset event.
	if (CloseHandle(cond->events_[SIGNAL]) == 0)
		AMSP_err_quit("Error %d, can't destroy auto-reset envent", GetLastError());

	// Destroy the manual-reset event.
	if (CloseHandle(cond->events_[BROADCAST]) == 0)
		AMSP_err_quit("Error %d, can't destroy auto-reset envent", GetLastError());

	// Destroy the Critical Section
	DeleteCriticalSection(&(cond->waiters_count_lock_));

#else
int err;
#ifdef SOLARIS
 if ((err = cond_destroy(cond)))
        AMSP_err_quit(" Error: %d Can't destroy a condition variable",err);
#else
 if ((err = pthread_cond_destroy(cond)))
        AMSP_err_quit(" Error: %d Can't destroy a condition variable",err);
#endif

#endif
}

/*
 * Wait for a Condition to be signaled. t_flag indicates if a timeout occured
 */
void AMSP_CondWait(Cond * cond, RealMutex *mutex, int timeout, int *t_flag)
{
 
#ifdef _WIN_ALICE
	int result, last_waiter;

    // Initialize t_flag
    *t_flag = 0;
	// Avoid race conditions.
	EnterCriticalSection (&cond->waiters_count_lock_);
	cond->waiters_count_++;  
	LeaveCriticalSection (&cond->waiters_count_lock_);
  
	// It's ok to release the <mutex> here since Win32
	// manual-reset events maintain state when used with
	// <SetEvent>.  This avoids the "lost wakeup" bug...
	LeaveCriticalSection (mutex);
	
	// Wait for either event to become signaled due to <pthread_cond_signal>
	// being called or <pthread_cond_broadcast> being called.
    if (timeout == 0)
	    result = WaitForMultipleObjects (2, cond->events_, FALSE, INFINITE);
    else
        result = WaitForMultipleObjects (2, cond->events_, FALSE, timeout)
        ;
	EnterCriticalSection (&cond->waiters_count_lock_);  
	cond->waiters_count_--;
  
	last_waiter = cond->waiters_count_ == 0;
	LeaveCriticalSection (&cond->waiters_count_lock_);
	
	// Some thread called <pthread_cond_broadcast>.
	if (result == WAIT_OBJECT_0 + BROADCAST && last_waiter)
		// We're the last waiter to be notified, so reset the manual event. 
		ResetEvent (cond->events_[BROADCAST]);   
	
    // Time out
    if (result == WAIT_TIMEOUT )
        *t_flag = 1;

	// Reacquire the <mutex>.
  	EnterCriticalSection (mutex);

#else
    int err;

#ifdef SOLARIS
    timestruc_t to;
    /* Initialize t_flag */
    *t_flag = 0;

    if (timeout) {
        to.tv_sec = time(NULL) + timeout/1000;
        to.tv_nsec = (timeout%1000)*1000000; /* Convert to nanoseconds */
        err = cond_timedwait(cond, mutex, &to);
        if (err == ETIME || err == 0) {
            *t_flag = 1;
            return; /* Time out */
        }
        else
            AMSP_err_quit("Error %d, on Condition wait call", err);
    } else {
        if (err = cond_wait(cond, mutex))
            AMSP_err_quit("Error %d, on Condition wait call", err);
    }

#else
    struct timespec to;
    /* Initialize t_flag */
    *t_flag = 0;

    if (timeout) {
        to.tv_sec = time(NULL) + timeout/1000;
        to.tv_nsec = (timeout%1000)*1000000; /* Convert to nanoseconds */
        err = pthread_cond_timedwait(cond, mutex, &to);
        if (err == ETIMEDOUT || err == 0) {
            *t_flag = 1;
            return; /* Time out */
        }
        else
            AMSP_err_quit("Error %d, on Condition wait call", err);
    } else {
      if ((err = pthread_cond_wait(cond, mutex)))
            AMSP_err_quit("Error %d, on Condition wait call");
    }
#endif

#endif

}

/*
 * Similar to the CondWait function, but the calling thread
 * holds a MutexWrite lock, and upon return it will hold the
 * same type lock.
 */
void AMSP_CondWaitMutexWrite(Cond *cond, Mutex *pm, int timeout, int *t_flag)
{
	/* Start releasing the write-lock. */
	AMSP_LockRealMutex(&(pm->mutex));
    if(pm->writer_writing == 0){
        AMSP_RelRealMutex(&(pm->mutex));
        AMSP_err_quit("Error releasing a write-lock that does not exit");
    } else {
        pm->writer_writing = 0;
        AMSP_CondBroadcast(&(pm->lock_free));
	}
	/* This will release the real lock before waiting */
	AMSP_CondWait(cond, &(pm->mutex), timeout, t_flag); 

	/* The real lock is re-gained after the condition wait return. 
	 * Now complete our write lock regain
	 */
    while(pm->writer_writing || pm->readers_reading) {
        AMSP_CondWait(&(pm->lock_free), &(pm->mutex), timeout, t_flag);
    }
    pm->writer_writing++;
    AMSP_RelRealMutex(&(pm->mutex));
}

/*
 * Similar to the CondWait function, but the calling thread
 * holds a MutexRead lock, and upon return it will hold the
 * same type lock.
 */
void AMSP_CondWaitMutexRead(Cond *cond, Mutex *pm, int timeout, int *t_flag)
{
	/* Start releasing the read-lock. */
	AMSP_LockRealMutex(&(pm->mutex));
    if(pm->readers_reading == 0){
        AMSP_RelRealMutex(&(pm->mutex));
        AMSP_err_quit("Error releasing a read-lock that does not exit");
    } else {
        pm->readers_reading--;
        if (pm->readers_reading == 0)
            AMSP_CondSignal(&(pm->lock_free));
    }

	/* This will release completely the real lock before waiting */
	AMSP_CondWait(cond, &(pm->mutex), timeout, t_flag); 

	/* The real lock is re-gained after the condition wait return. 
	 * Now complete our read lock regain
	 */
     while(pm->writer_writing) {
         AMSP_CondWait(&(pm->lock_free), &(pm->mutex), timeout, t_flag);
     }
     pm->readers_reading++;
     AMSP_RelRealMutex(&(pm->mutex));
}

/*
 * Send a signal to awaiken threads that are waiting for the resources
 */
void AMSP_CondSignal(Cond *cond)
{

#ifdef _WIN_ALICE
	int have_waiters;
	// Avoid race conditions.  
	EnterCriticalSection (&cond->waiters_count_lock_);
  
	have_waiters = cond->waiters_count_ > 0;
	LeaveCriticalSection (&cond->waiters_count_lock_);  
	
	if (have_waiters)
		SetEvent (cond->events_[SIGNAL]);

#else

int err;
#ifdef SOLARIS
 if ((err = cond_signal(cond)))
        AMSP_err_quit("Error %d, on Condition signal call");
#else
 if ((err = pthread_cond_signal(cond)))
        AMSP_err_quit("Error %d, on Condition signal call");
#endif

#endif

}

/*
 * Send a broadcast to awaiken all threads that are waiting for thsi resource
 */
extern void AMSP_CondBroadcast(Cond *cond)
{
#ifdef _WIN_ALICE
	int have_waiters;
	// Avoid race conditions.  
	EnterCriticalSection (&cond->waiters_count_lock_);
  
	have_waiters = cond->waiters_count_ > 0;
	LeaveCriticalSection (&cond->waiters_count_lock_);  

	if (have_waiters)
		SetEvent (cond->events_[BROADCAST]);

#else
int err;
#ifdef SOLARIS
 if ((err = cond_broadcast(cond)))
        AMSP_err_quit("Error %d, on Condition broadcast call");
#else
 if ((err = pthread_cond_broadcast(cond)))
        AMSP_err_quit("Error %d, on Condition broadcast call");
#endif

#endif
}
















