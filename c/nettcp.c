/*
 *  network handling for TCP/IP connection.
 */

#include        <signal.h>
#include        <stdlib.h>

#ifndef _WIN_ALICE
#include    <unistd.h>
#else
#include        <io.h>
#endif

#include    <stdio.h>
#include    <stdlib.h>
#include    <errno.h>

#include "ams.h"
#include "netlib.h"
#include "amsdefs.h"

#include        "network.h"
#include        "netdefs.h"

extern int      errno;

char    openhost[MAXHOSTNAMELEN] = { 0 };       /* remember host's name */

extern int                      traceflag;      /* trace variable */


/*
 * Close the network connection.  Used by accessor and publisher.
 */

int AMSP_mem_net_close(int sfd)
{

#ifdef _WIN_ALICE
    closesocket(sfd);
#else
    close(sfd);
#endif

    return 0;
}

/*
 * Send a record to the other end.  Used by accessor and publisher.
 * With a stream socket we have to preface each record with its length,
 * We encode the length as a 2-byte integer in network byte order.
 */

int AMSP_mem_net_send(buff, len, sfd)
     char       *buff;
     int        len, sfd;
{
  
    int rc, err;
    int templen, tempver;
	char mesg[255];

    templen = htonl(len);
    rc = AMSP_writen(sfd, (char *) &templen, sizeof(int));
	if (rc == 0)
		return AMS_ERROR_CONNECT_TERMINATED;

    if (rc != sizeof(int))
        AMSP_err_quit("writen error of length prefix %s", AMSP_host_err_str(mesg, &err));

    tempver = htonl(AMS_MEMORY_NET_VERSION);
    rc = AMSP_writen(sfd, (char *) &tempver, sizeof(int));
	if (rc == 0)
		return AMS_ERROR_CONNECT_TERMINATED;

    if (rc != sizeof(int))
        AMSP_err_quit("writen error of version prefix %s", AMSP_host_err_str(mesg, &err));

    rc = AMSP_writen(sfd, buff, len);
	if (rc == 0)
		return AMS_ERROR_CONNECT_TERMINATED;

    if (rc != len)
        AMSP_err_quit("writen error %s", AMSP_host_err_str(mesg, &err));

    return 0;
}

/*
 * Receive a record from the other end.  Used by accessor and publisher.
 */

int AMSP_mem_net_recv(buff, nbytes, sfd)
     char **buff;
     int  *nbytes, sfd;
{
    int err, templen;
	char mesg[255];

#if !defined(_WIN_ALICE) && defined(AMS_PUBLISHER)
    extern void AMSP_Pipe(int);
    signal(SIGPIPE, AMSP_Pipe);
#endif

again1:
    if ( (*nbytes = AMSP_readn(sfd, (char *) &templen, sizeof(int))) < 0) {
        if (errno == EINTR) {
            errno = 0;          /* assume SIGCLD */
            goto again1;
        }
        AMSP_err_sys("AMSP_mem_net_recv: readn failed");
    }
	if (*nbytes == 0)
		return AMS_ERROR_CONNECT_TERMINATED;

    if (*nbytes != sizeof(int))
        AMSP_err_quit("AMSP_mem_net_recv: Expecting 4 bytes op code, got %d", *nbytes);

    templen = ntohl(templen) + INT_SIZE;   /* #bytes that follow; includes version prefix */
    if (templen < 0 || templen == 0)
        AMSP_err_quit("AMSP_mem_net_recv: Invalid message length %d", templen);

    *buff = (char *) malloc(sizeof(char) * templen);
    if (*buff == NULL)
        AMSP_err_quit("AMSP_mem_net_recv: Can't allocate memory for incoming message");

again2:
    if ( (*nbytes = AMSP_readn(sfd, *buff, templen)) < 0) {
        if (errno == EINTR) {
            errno = 0;          /* assume SIGCLD */
            goto again2;
        }
        AMSP_err_quit("AMSP_readn error %s", AMSP_host_err_str(mesg, &err));
    }
	if (*nbytes == 0)
		return AMS_ERROR_CONNECT_TERMINATED;

    if (*nbytes != templen)
        AMSP_err_quit("error in readn. Expecting %d, received %d ", nbytes, templen);

    return AMS_ERROR_NONE;
}

/*
 * Open the network connection. Used by both Accessor and Publisher
 */

int AMSP_mem_net_open(char *host, char *service, int port, int *sfd, char *errmsg)
{
    struct sockaddr_in  tcp_srv_addr;
    struct servent tcp_srv_info;
	int err;

    *sfd = AMSP_tcp_open(host, service, port, &tcp_srv_addr, &tcp_srv_info, &err, errmsg);
	if (*sfd < 0) {

#ifndef _WIN_ALICE
		if (err == ECONNREFUSED || err == ECONNRESET)
			return AMS_ERROR_CONNECT_REFUSED;
#else
		if (err == WSAECONNREFUSED || err == WSAECONNRESET)
			return AMS_ERROR_CONNECT_REFUSED;
#endif
		AMSP_err_quit(" Fatal error %d, in connecting to the other side \n", err);
	}

	return AMS_ERROR_NONE;
}

#ifndef _WIN_ALICE
/* Don't know how to take care of broken pipes in Windows */
void AMSP_Pipe(int sig)
{
 
#ifdef _MT
    signal(SIGPIPE, SIG_IGN);
#else
    signal(SIGPIPE, SIG_DFL);
#endif
             
}
#endif

