#include "ams.h"
#include "hostiter.h"
#include "netapi.h"
#include "netlib.h"
#include "amsdefs.h"
#include <stdlib.h>

/* Host iterator methods */

int AMSP_Comm_Hosts_begin(AMS_Comm ams, HOST_ITER *it)
{
    COMM_STRUCT *com_str;
    int err;
     
    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);
 
    it->node = com_str->host_node;
    it->is_open = 0;
 
    if(it->node != com_str->TailHostNode) {
        err = AMSP_mem_net_open(it->node->hostname, AMS_SERVICE,
                            it->node->port, &(it->sfd), ams_msg);
		if (err == AMS_ERROR_CONNECT_REFUSED)
            AMSP_err_ret(ams_msg, "Error connecting to %s\n", it->node->hostname);
	}
    it->is_open = 1;
 
    return AMS_ERROR_NONE;
}
 
int AMSP_Comm_Hosts_valid(AMS_Comm ams, HOST_ITER *it)
{
    COMM_STRUCT *com_str;
    int err;
 
    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);
 
    return (it->node != com_str->TailHostNode);
}
 
int AMSP_Comm_Hosts_advance(AMS_Comm ams, HOST_ITER *it)
{
    COMM_STRUCT *com_str;
    int err;
     
    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);
 
    if(it->node == com_str->TailHostNode)
        return AMS_ERROR_NONE;
 
    if(it->is_open) {
        AMSP_mem_net_close(it->sfd);
        it->node = it->node->next;
    }
    it->is_open = 0;
 
    if(it->node != com_str->TailHostNode) {
        err = AMSP_mem_net_open(it->node->hostname, AMS_SERVICE,
                            it->node->port, &(it->sfd), ams_msg);
		if (err == AMS_ERROR_CONNECT_REFUSED)
            AMSP_err_ret(ams_msg, "Error connecting to %s\n", it->node->hostname);
        it->is_open = 1;
    }
 
    return AMS_ERROR_NONE;
}

/* Related methods that don't actually use host iterators: */

int AMSP_Comm_Hosts_open_any(AMS_Comm ams, int *sfd)
{
    COMM_STRUCT *com_str;
    int err;

    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);
 
    if (AMSP_mem_net_open(com_str->host_node->hostname, AMS_SERVICE,
                        com_str->host_node->port, sfd, ams_msg) != AMS_ERROR_NONE)
        return AMS_ERROR_NET_OPEN;
 
    return AMS_ERROR_NONE;
}

/* This function opens connections to all the communicator's processors
   at once, returning an array of sfd's and its length. */
int AMSP_Comm_Hosts_open_all(AMS_Comm ams, int *num_hosts, int **sfd_list)
{
    COMM_STRUCT *com_str;
    HOST_NODE *node;
    int err, k;
     
    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);


    /* First, compute the number of hosts */
    *num_hosts = 0;

    node = com_str->host_node;
    while(node != com_str->TailHostNode) {
        ++(*num_hosts);
        node = node->next;
    }

    /* Allocate memory */
    *sfd_list = (int *) malloc((1+(*num_hosts)) * sizeof(int));

    /* Fill the array */
    node = com_str->host_node;
    for(k = 0; k < *num_hosts; ++k) {
      if((err = AMSP_mem_net_open(node->hostname, AMS_SERVICE,
				  node->port, &((*sfd_list)[k]), ams_msg)) != AMS_ERROR_NONE) {

             *num_hosts = 0;
             AMSP_err_ret(ams_msg, "Error connecting to %s\n", node->hostname);
             return err;
         }

         node = node->next;
    }

    return AMS_ERROR_NONE;
}

int AMSP_Comm_Hosts_close_all(int num_hosts, int *sfd_list)
{
    int k;

    for(k = 0; k < num_hosts; ++k)
        AMSP_mem_net_close(sfd_list[k]);

    free(sfd_list);

    return AMS_ERROR_NONE;
}
