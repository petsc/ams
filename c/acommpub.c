/* not-accessor */
#include <unistd.h>
#include <stdlib.h>
/* #include <malloc.h>*/
#include <memory.h>
#include <string.h>
#include <stdio.h>

#include "ams.h"
#include "acomm.h"
#include "netapi.h"
#include "netlib.h"
#include "sendfun.h"
#include "amsdefs.h"
#include "amisc.h"


/* Publisher */
AMS_Pub PUBLISHER;

int AMSP_Lock_Waiting_Room(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    AMSP_LockMutexWrite(&(com_str->waiting_room_lock));
    return AMS_ERROR_NONE;
}

int AMSP_UnLock_Waiting_Room(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    AMSP_RelMutexWrite(&(com_str->waiting_room_lock));
    return AMS_ERROR_NONE;
}


/*@ AMSP_Create_Appointment - Creates an appointment for a accessor thread

        Input Parameters:
                com_str - AMS communicator structure
                req - Request ID
                mem - AMS memory to which accessor desires access

        Output Parameters:
                appt - pointer to request's appointment
@*/

int AMSP_Create_Appointment(COMM_STRUCT *com_str, RequestID req, AMS_Memory mem, Appointment **appt)
{
	Appointment *tail;
	int reqsize, k;
	int err = Validate(AMSP_Valid_Comm_Struct, com_str);
	CheckErr(err);

	tail = com_str->waiting_room;

    *appt = (Appointment *) malloc(sizeof(Appointment));
	if (*appt == NULL)
		return AMS_ERROR_INSUFFICIENT_MEMORY;

    if(com_str->waiting_room == NULL)
        com_str->waiting_room = *appt;
    else {
        while(tail->next)
            tail = tail->next; /* Advance to the next appt */
        tail->next = *appt;
    }

    (*appt)->next = NULL; /* This is the last item on the list */

    err = AMSP_Size_RequestID(req, &reqsize);
    CheckErr(err);
    (*appt)->guest = (RequestID) malloc(reqsize);
    if((*appt)->guest == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    err = AMSP_Copy_RequestID((*appt)->guest, req);
    CheckErr(err);

    (*appt)->mem_wanted = mem;
	AMSP_CondInit(&((*appt)->cond));

    /* Denote indefinite appointment time */
    for(k = 0; k < STEP_BYTES; ++k)
        (*appt)->step[k] = (unsigned char) 255;

	return AMS_ERROR_NONE;

}

/*@ AMSP_Register_Guest - enter a new accessor request in the waiting room, finding its
                     appointment (creating it if necessary)

	Input Parameters:
+   ams - communicator
.   req - Request ID
+   mem -  memory to which accessor desires access

    Output Parameters:
	appt - pointer to request's appointment

@*/

int AMSP_Register_Guest(AMS_Comm ams, RequestID req, AMS_Memory mem, Appointment **appt, int not)
{
    COMM_STRUCT *com_str;
    Appointment *tail;
    int found = 0, err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    tail = com_str->waiting_room;

    /* First try to find appointment, in case it pre-registered */
    if(tail != NULL) {
        if(AMSP_Equals_RequestID(tail->guest, req))
            if (tail->mem_wanted == mem)
				found = 1;

        while(tail->next && !found) {
            tail = tail->next;
            if(AMSP_Equals_RequestID(tail->guest, req))
                if (tail->mem_wanted == mem)
					found = 1;
        }
    }

    if(found) {
        *appt = tail;
        return AMS_ERROR_NONE;
    }

    /* Hasn't registered yet, create a new appointment */
	err = AMSP_Create_Appointment(com_str, req, mem, appt);
	CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Assign_Appt - record appointment time with waiting room, and tell the memory if necessary

        Input Parameters:
+       appt - appointment to schedule
+       step - time of appointment

        Be sure to lock the memory being requested before assigning an appointment time!
@*/

int AMSP_Assign_Appt(AMS_Comm ams, Appointment *appt, unsigned char *step)
{
    int err;
	
    if(appt == NULL)
        return AMS_ERROR_SYSTEM;

    memcpy((unsigned char *) appt->step, (const unsigned char *) step, STEP_BYTES);

    /* Memory is already locked when we do this */
    err = AMSP_Announce_Next_Guest(ams, appt->mem_wanted);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Finish_Appt - delete appointment, tell memory the next time it's scheduled to stop

        Input Parameters:
+       ams - communicator
+       appt - appointment that just finished

@*/

int AMSP_Finish_Appt(AMS_Comm ams, Appointment *appt)
{
    COMM_STRUCT *com_str;
    Appointment *iter;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    /* Delete appointment */
        
    if(appt == com_str->waiting_room)
        com_str->waiting_room = appt->next;
    else {
        iter = com_str->waiting_room;
        while(iter->next) {
            if(iter->next == appt)
                iter->next = appt->next;
            if(iter->next)
                iter = iter->next;
        }
    }

    /* Who's next? */
    err = AMSP_Announce_Next_Guest(ams, appt->mem_wanted);
    CheckErr(err);

	AMSP_CondDestroy(&(appt->cond));
    free(appt->guest);
    free(appt);

    return AMS_ERROR_NONE;
}

int AMSP_Announce_Next_Guest(AMS_Comm ams, AMS_Memory mem)
{
    COMM_STRUCT *com_str;
    Appointment *iter;
    unsigned char next_time[STEP_BYTES];
    int k, err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    for(k = 0; k < STEP_BYTES; ++k)
        next_time[k] = (unsigned char) 255;

    for(iter = com_str->waiting_room; iter != NULL; iter = iter->next)
        if(iter->mem_wanted == mem && AMSP_StepGreaterThan(next_time, iter->step))
            memcpy(next_time, iter->step, STEP_BYTES);

    err = AMSP_Reserve_future_access(mem, (unsigned char *) next_time);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

int AMSP_Wait_For_Appt_Step(AMS_Comm ams, Appointment *appt)
{
	COMM_STRUCT *com_str;
	int err = AMSP_Get_Comm_Tbl(ams, &com_str), t_flag;
    CheckErr(err);

	
    AMSP_Lock_Waiting_Room(ams);

    while(AMSP_StepUndef(appt->step)) {
		AMSP_CondWaitMutexWrite(&(appt->cond) ,&(com_str->waiting_room_lock), 0, &t_flag);
    }

    AMSP_UnLock_Waiting_Room(ams);

    return AMS_ERROR_NONE;
}

/*@
        AMSP_New_Pub_Arguments - Creates Publisher Start function arguments

        Input Parameters:
			port - Communication port number

        Output Parameter:
			parg - Address of a pointer to the arguments

@*/
int AMSP_New_Pub_Arguments(int port, Pub_arg **parg)
{
    if (port <= 0 && port != -1)
        return AMS_ERROR_BAD_ARGUMENT;

    if (parg == NULL)
        return AMS_ERROR_BAD_ARGUMENT;

    *parg = (Pub_arg *) malloc(sizeof(Pub_arg));
    if (*parg == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Port number */
    (*parg)->port = port;

    (*parg)->care_about_port = 0;

    /* Status flag */
    (*parg)->alive = 0;

    /* Nb Thread connected */
    (*parg)->nb_accessor = 0;

    /* Status Mutex */
    AMSP_NewMutex(&( (*parg)->pub_mutex ));

    /* Nb of Connected Thread Mutex */
    AMSP_NewMutex(&((*parg)->nbthr_mutex));

	/* Condition variable to be signaled when the last thread is done */
	AMSP_CondInit(&((*parg)->cond));

    /* Condition variable to be signaled when the port number is set */
    AMSP_CondInit(&((*parg)->cond_port));

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Del_Pub_Arguments - Deletes the publisher start-funct arguments 

        Input Parameters:
			parg - Pointer the arguments
@*/
int AMSP_Del_Pub_Arguments(Pub_arg *parg)
{
    
    AMSP_DelMutex(&(parg->pub_mutex));
    AMSP_DelMutex(&(parg->nbthr_mutex));
	AMSP_CondDestroy(&(parg->cond));
    AMSP_CondDestroy(&(parg->cond_port));

    free(parg);
    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_Pub_Arguments_Comm_Struct - Sets the publisher startup function args

        Input Parameters:
			com_str - A Communication structure
            arg - Publisher-thread arguments
@*/
int AMSP_Set_Pub_Arguments_Comm_Struct(COMM_STRUCT *com_str, Pub_arg *arg)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    com_str->pub_arg = arg;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Set_MPI_Comm_Comm_Struct - Sets MPI_Comm member

        Input Parameters:
+			com_str - A Communication structure
+			mpi_comm - MPI Communicator

@*/
int AMSP_Set_MPI_Comm_Comm_Struct(COMM_STRUCT *com_str, MPI_Comm mpi_comm)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    com_str->mpi_comm = mpi_comm;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Get_MPI_Comm_Comm_Struct - Gets MPI_Comm member

        Input Parameter:
+			com_str - A Communication structure
		Output Parameter:
+			mpi_comm - Address of MPI Communicator

@*/

int AMSP_Get_MPI_Comm_Comm_Struct(COMM_STRUCT *com_str, MPI_Comm * mpi_comm)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    *mpi_comm = com_str->mpi_comm;

    return AMS_ERROR_NONE;
}


/*@
        AMSP_Get_Thread_ID_Comm_Struct - Gets the publisher-thread id

        Input Parameters:
+			com_str - A Communication structure
+			thr - Publisher-thread  id

@*/
int AMSP_Get_Thread_ID_Comm_Struct(COMM_STRUCT *com_str, Thread *thr)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    *thr = com_str->thrid;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_EndThread_Comm_Struct - Terminate the thread associated with a communicator

        Input Parameters:
			com_str - A Communication structure

@*/
int AMSP_EndThread_Comm_Struct(COMM_STRUCT *com_str)
{
    char host[MAXHOSTLEN+1], hosterr[255];
    int sfd, err1, err = Validate(AMSP_Valid_Comm_Struct, com_str), t_flag;
    CheckErr(err);

    /* publisher-thread should stop accepting new connections */
    AMSP_LockMutexWrite(&(com_str->pub_arg->pub_mutex));
    com_str->pub_arg->alive = 0;
    AMSP_RelMutexWrite(&(com_str->pub_arg->pub_mutex));

    /*
     * Any new pending request shoud be notified that the
     * the publisher-thread is not accepting new connections 
     */

    if ((err = gethostname(host, MAXHOSTLEN)))
        AMSP_err_quit("gethostname: %s", AMSP_host_err_str(hosterr, &err));

    /* Connect to myself if the communicator is still alive */
	err = AMSP_mem_net_open(host, AMS_SERVICE, com_str->pub_arg->port, &sfd, ams_msg);
	if (sfd < 0) {
		/* an error has occured */
		if (err != AMS_ERROR_CONNECT_REFUSED)
			return AMS_ERROR_NET_OPEN;
	}

	if (err != AMS_ERROR_CONNECT_REFUSED) {
		err1 = AMSP_send_rq(OP_SHUTDOWN, sfd);
		CheckErr(err1);

		err = AMSP_fsm_loop(OP_ATTACH_COMM, sfd, &err);
		AMSP_mem_net_close(sfd);
		if ((err != AMS_ERROR_COMMUNICATOR_NO_LONGER_ALIVE))
		  {CheckErr(err);}
		else
			err = AMS_ERROR_NONE;
	}

    /* 
	 * Wait until the last accessor thread is done. 
	 */

    AMSP_LockMutexRead(&(com_str->pub_arg->nbthr_mutex));
    while (com_str->pub_arg->nb_accessor) {
		AMSP_CondWaitMutexRead(&(com_str->pub_arg->cond), &(com_str->pub_arg->nbthr_mutex), 0, &t_flag);
    }
    AMSP_RelMutexRead(&(com_str->pub_arg->nbthr_mutex));

    /* 
     * Decrement the nubmer of communicators 
     */
    PUBLISHER.nb_com--;


    if (PUBLISHER.nb_com == 0) {

#ifdef AMS_LDAP
        /* If we are the last communicator, then delete the LDAP's publisher object */
        err = AMSP_Ldap_del_object(com_str->pub_arg->ld, PUBLISHER.dn);
        CheckErr(err);
        free(PUBLISHER.dn);
#endif
        PUBLISHER.alive = 0;
    }


    return AMS_ERROR_NONE;
}

/*@
    AMSP_Clean_up - Perform a clean up and delete AMS objects
@*/

void AMSP_Clean_up(void)
{
    int i;
    static int called = 1;

    if (called == 1) {
        AMS_Print(" Performing AMS Clean up \n");
        called = 0;
    } else {
        AMS_Print(" Clean up failed \n");
        return;
    }

    /* Delete all the communicators */
    for (i=0;i<MAX_COMM;i++) {
        if (COMM_TBL[pub_id][i] != NULL)
            AMSP_EndThread_Comm_Struct(COMM_TBL[pub_id][i]);
    }

#ifdef AMS_LDAP
    /* delete the publisher if necessary */
    if (PUBLISHER.alive == 1) {
        AMSP_Ldap_del_object(PUBLISHER.ld, PUBLISHER.dn);
    }
#endif

}

/*@
    AMSP_Start_up - Perform a start up initialization
@*/

void AMSP_Start_up(void)
{
    /* Register clean up function */
    if (atexit(AMSP_Clean_up))
        AMSP_err_quit("AMS Start up failed \n");
}

/*@
    AMSP_Am_I_First_Host_Comm_Struct - are we the first host in our host_node list?
    Input Parameters:
+   com_str - Communicator Structure
    
    Output Paramters:
+   answer - pointer to an interger. 1 for yes, 0 for no.

@*/
int AMSP_Am_I_First_Host_Comm_Struct(COMM_STRUCT *com_str, int *answer)
{
    char ourname[MAXHOSTLEN+1], nickname[MAXHOSTLEN+1], hosterr[255];
    int ourport, err;

    if ((err = gethostname(ourname, MAXHOSTLEN)))
        AMSP_err_quit("gethostname: %s", AMSP_host_err_str(hosterr, &err));
    strtok(ourname, ".");
 
    strcpy(nickname, com_str->host_node->hostname);
    strtok(nickname, ".");

    ourport = com_str->pub_arg->port;

    *answer = 0;
    if(!strcmp(nickname, ourname) && ourport == com_str->host_node->port)
        *answer = 1;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Who_Is_Next_Host_Comm_Struct - get next host in host node list, or first if we're last
@*/
int AMSP_Who_Is_Next_Host_Comm_Struct(COMM_STRUCT *com_str, HOST_NODE **answer)
{
    char ourname[MAXHOSTLEN+1], nickname[MAXHOSTLEN+1], hosterr[255];
    int ourport, err, found;
    HOST_NODE *host_node;

    if ((err = gethostname(ourname, MAXHOSTLEN)))
        AMSP_err_quit("gethostname: %s", AMSP_host_err_str(hosterr, &err));
    strtok(ourname, ".");
 
    ourport = com_str->pub_arg->port;

    host_node = com_str->host_node;
    found = 0;
    *answer = NULL;
    while(host_node != com_str->TailHostNode) {
        strcpy(nickname, host_node->hostname);
        strtok(nickname, ".");
        if(!strcmp(nickname, ourname) && ourport == host_node->port) {
            found = 1;
            *answer = host_node->next;
        }
        host_node = host_node->next;
    }
        
    if(found == 0)
        return AMS_ERROR_SYSTEM;

    /* After last host, return to first */
    if(*answer == com_str->TailHostNode)
        *answer = com_str->host_node;

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Get_Nb_Proc_Comm_Struct - get the number of processors
@*/
extern int AMSP_Get_Nb_Proc_Comm_Struct(COMM_STRUCT *com_str, int *nb)
{
    *nb = com_str->nb_proc;
    return AMS_ERROR_NONE;
}

int AMSP_Set_Pub_Arguments_AMS_Comm(AMS_Comm ams, Pub_arg *arg)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    return AMSP_Set_Pub_Arguments_Comm_Struct(com_str, arg);
}

/*@
        AMSP_Set_Thread_ID_Comm_Struct - Sets the publisher-thread id

        Input Parameters:
+			com_str - A Communication structure
+			thr - Publisher-thread  id

@*/
int AMSP_Set_Thread_ID_Comm_Struct(COMM_STRUCT *com_str, Thread thr)
{
    int err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    com_str->thrid = thr;

    return AMS_ERROR_NONE;
}

int AMSP_Set_Thread_ID_AMS_Comm(AMS_Comm ams, Thread thr)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    return AMSP_Set_Thread_ID_Comm_Struct(com_str, thr);
}

int AMSP_Get_Thread_ID_AMS_Comm(AMS_Comm ams, Thread *thr)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    return AMSP_Get_Thread_ID_Comm_Struct(com_str, thr);
}

int AMSP_Set_MPI_Comm_AMS_Comm(AMS_Comm ams, MPI_Comm mpi_comm)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    return AMSP_Set_MPI_Comm_Comm_Struct(com_str, mpi_comm);
}

int AMSP_Get_MPI_Comm_AMS_Comm(AMS_Comm ams, MPI_Comm *mpi_comm)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    return AMSP_Get_MPI_Comm_Comm_Struct(com_str, mpi_comm);
}

int AMSP_Lock_Read_AMS_Comm(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    AMSP_LockMutexRead(&(com_str->comm_mutex));

    return AMS_ERROR_NONE;
}

int AMSP_UnLock_Read_AMS_Comm(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    AMSP_RelMutexRead(&(com_str->comm_mutex));

    return AMS_ERROR_NONE;
}

int AMSP_Lock_Write_AMS_Comm(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    AMSP_LockMutexWrite(&(com_str->comm_mutex));

    return AMS_ERROR_NONE;
}

int AMSP_UnLock_Write_AMS_Comm(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    AMSP_RelMutexWrite(&(com_str->comm_mutex));

    return AMS_ERROR_NONE;
}

/*@
        AMSP_Construct_Port_list - Find out communicator ports from other hosts when we publish
                              a new communicator

        Input parameters:
+			ams - Communicator being constructed
+			host_list - list of hosts to contact for ports

@*/

int AMSP_Construct_Port_list_AMS_Comm(AMS_Comm ams, char **host_list,
                                 int *port_list, const char *name)
{
    int i, err, err1, cur_port, nbtry, ports_known = 0, t_flag;
    COMM_STRUCT *com_str;

    /* First, fill in our own port */
    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    AMSP_LockMutexRead(&(com_str->pub_arg->pub_mutex));
    /* (wait for another thread to fill it in): */
    while(com_str->pub_arg->care_about_port != 2) {
        AMSP_CondWaitMutexRead(&(com_str->pub_arg->cond_port), &(com_str->pub_arg->pub_mutex), 0, &t_flag);
    }
    AMSP_RelMutexRead(&(com_str->pub_arg->pub_mutex));

    /* (now copy it into the host node list): */
    err = AMSP_Set_Comm_local_port(ams);
    CheckErr(err);
    
    if(host_list != NULL) {
        nbtry = 100;
        while(ports_known == 0 && nbtry) {
            ports_known = 1;
            for(i = 0; host_list[i]; ++i) {
                err = AMSP_Get_Comm_port(ams, i, &cur_port);
                CheckErr(err);
                if(cur_port <= 0) {
                    int sfd, answer;

                    if (AMSP_mem_net_open(host_list[i], AMS_SERVICE,
                                        port_list[i], &sfd, ams_msg) == AMS_ERROR_CONNECT_REFUSED ) {
                        ports_known = 0;
                        nbtry--;
                        AMS_Print("Still trying to contact publisher %s\n",
                               host_list[i]);
                    } else {
                        err = AMSP_send_rq(OP_COMM_PORT, sfd, name);
						CheckErr(err);

                        err1 = AMSP_fsm_loop(OP_COMM_PORT, sfd, &err, &answer);
                        AMSP_mem_net_close(sfd);
                        CheckErr(err1);
						CheckErr(err);

                        if(answer <= 0)
                            ports_known = 0;
                        else {
                            err = AMSP_Set_Comm_port(ams, i, answer);
                            CheckErr(err);
                        }
                    }
                }
            }
        }
    }
    else {
        /* Wait for the local port to become active */
        cur_port = -1;
        while(cur_port <= 0) {
            err = AMSP_Get_Comm_local_port(ams, &cur_port);
            CheckErr(err);
        }
    }

    err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    /* We're alive for accessor requests */
    AMSP_LockMutexWrite(&(com_str->pub_arg->pub_mutex));
    com_str->pub_arg->alive = 1;
    AMSP_RelMutexWrite(&(com_str->pub_arg->pub_mutex));

    return AMS_ERROR_NONE;              
}

int AMSP_Am_I_First_Host_AMS_Comm(AMS_Comm ams, int *answer)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    return AMSP_Am_I_First_Host_Comm_Struct(com_str, answer);
}

int AMSP_Who_Is_Next_Host_AMS_Comm(AMS_Comm ams, HOST_NODE **answer)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    return AMSP_Who_Is_Next_Host_Comm_Struct(com_str, answer);
}

int AMSP_Get_Nb_Proc_AMS_Comm(AMS_Comm ams, int *nb_proc)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    return AMSP_Get_Nb_Proc_Comm_Struct(com_str, nb_proc);
}

int AMSP_EndThread_AMS_Comm(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);
    CheckErr(err);

    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
    return AMSP_EndThread_Comm_Struct(com_str);
}
