/*
 * Open a TCP connection.
 * ThreadSafe function.
 */



#include <netdb.h>
#include  <string.h>
#include  <unistd.h>
#ifdef _WIN_ALICE
#include  <io.h>
#include  <memory.h>
#else
#define far
#include <errno.h>
#endif

#include  "network.h"
#include  "netlib.h"
#include  "netdefs.h"


#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff  /* should be in <netinet/in.h> */
#endif


/**********************************************************************/
/* host:                name or dotted-decimal addr of other system   */
/* service:             name of service being requested               */
/* tcp_srv_addr: publisher's Internet socket addr                        */
/* tcp_serv_info:  from getservbyname(), can be NULL, if port > 0     */
/* port    if == 0, nothing special - use port# of service            */
/*                 if < 0, bind a local reserved port                 */
/*                 if > 0, it's the port# of publisher (host-byte-order) */
/*                                                                    */
/* err: Erreur code or zero if the call was successful                */
/* errmsg: detailed error message                                     */
/* return socket descriptor if OK, else -1 on error                   */
/**********************************************************************/

int AMSP_tcp_open(host, service, port, ptcp_srv_addr, ptcp_serv_info, err, errmsg)
     char  *host;
     char  *service, *errmsg;
     int port, *err;
     struct sockaddr_in  *ptcp_srv_addr;
     struct servent    *ptcp_serv_info;

{
    int   fd, resvport;
    unsigned int inaddr;
    char msgerr[255];
    struct servent *sp;
    struct hostent far *hp;
    struct hostent tcp_host_info;  /* from gethostbyname() */

	*err = -1; /* Undefined error */
    /*
     * Initialize the publisher's Internet address structure.
     * We'll store the actual 4-byte Internet address and the
     * 2-byte port# below.
     */

    memset((char *) ptcp_srv_addr, 0, sizeof(*ptcp_srv_addr));
    ptcp_srv_addr->sin_family = AF_INET;

    if (service != NULL) {
        if ( (sp = getservbyname(service, "tcp")) == NULL) {
 	  AMSP_err_ret(AMSP_host_err_str(msgerr, err),"tcp_open: can't get service" );
            return(-1);
        }

        *ptcp_serv_info = *sp;      /* structure copy */
        if (port > 0)
            ptcp_srv_addr->sin_port = htons((short)port);
        /* caller's value */
        else
            ptcp_srv_addr->sin_port = sp->s_port;
        /* service's value */
    } else {
        if (port <= 0) {
            AMSP_err_ret(errmsg, "tcp_open: must specify either service or port");
            return(-1);
        }
        ptcp_srv_addr->sin_port = htons((short)port);
    }

    /*
     * First try to convert the host name as a dotted-decimal number.
     * Only if that fails do we call gethostbyname().
     */

    if ( (inaddr = inet_addr(host)) != INADDR_NONE) {
    
		/* it's dotted-decimal */
        memcpy((char *) &inaddr, (char *) &(ptcp_srv_addr->sin_addr), sizeof(inaddr));
        tcp_host_info.h_name = NULL;

    } else {
        if ( (hp = gethostbyname(host)) == NULL) {
            AMSP_err_ret(errmsg, "tcp_open:  Unknown host: %s %s", host, AMSP_host_err_str(msgerr, err));

             /* Non-fatal error. Pretend connections was refused */
#ifdef _WIN_ALICE
            *err = WSAECONNREFUSED;
#else
            *err = ECONNREFUSED;
#endif
            return -1;
        }
        tcp_host_info = *hp;  /* found it by name, structure copy */
        memcpy(hp->h_addr, (char *) &(ptcp_srv_addr->sin_addr), hp->h_length);
    }

    if (port >= 0) {
        if ( (fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            AMSP_err_ret(errmsg, "tcp_open: can't create TCP socket");
            *err = errno;
            return(-1);
        }

    } else if (port < 0) {
        resvport = IPPORT_RESERVED - 1;
        if ( (fd = AMSP_rresvport(&resvport, errmsg)) < 0) {
            AMSP_err_ret(errmsg, "tcp_open: can't get a reserved TCP port");
            *err = errno;
            return(-1);
        }
    }

    /*
     * Connect to the publisher.
     */

    if (connect(fd, (struct sockaddr *) ptcp_srv_addr,
                sizeof(*ptcp_srv_addr)) < 0) {
#ifdef _WIN_ALICE
		AMSP_err_ret(errmsg, "tcp_open: can't connect to publisher \n %s \n", AMSP_host_err_str(errmsg, err));
#else
        AMSP_err_ret(errmsg, "tcp_open: can't connect to publisher \n %s \n", strerror(errno));
        *err = errno;
#endif
        close(fd);

        return(-1);
    }

    return(fd); /* all OK */
}
