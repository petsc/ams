/* not-accessor */
#include "ams.h"
#include "updatesync.h"
#include "netapi.h"
#include "netlib.h"
#include "amsdefs.h"
#include "acomm.h"
#include "sendfun.h"
#include "amsmpi.h"

/* 
 * If we're the first host, start negotiating a step at which to
 * perform an update 
 */
int AMSP_Start_Step_Negotiation(AMS_Memory memid, RequestID req)
{
    int err, err1, am_first, sfd, nbmem = 1, mpi_thread_safe = 0;
    AMS_Comm ams;
    HOST_NODE *next_host;
    unsigned char step[STEP_BYTES];
    Appointment *my_appt;
	MPI_Comm mpi_comm;
	AMS_Comm_type comm_type;

	err = AMSP_Comm_AMS_Memory(memid, &ams);
	CheckErr(err);

	err = AMSP_Get_Comm_type_AMS_Comm(ams, &comm_type);
	CheckErr(err);

#if MPI_THREAD_SAFE
	mpi_thread_safe = 1; /* This is an MPI Thread safe version */
#endif

	if (comm_type != MPI_TYPE || !mpi_thread_safe) {

		err = AMSP_Am_I_First_Host_AMS_Comm(ams, &am_first);
		CheckErr(err);

		/* Only need to initiate dialogue if we're first */
		if(!am_first)
			return AMS_ERROR_NONE;

		/* 
		   Even though we'll be writing the step later, we're not writing
		   to any memory fields, and we don't want to increment the current
		   step! 
		*/
		err = AMSP_Worker_take_read_access(memid);
		CheckErr(err);

		err = AMSP_GetStep_AMS_Memory(&memid, nbmem, step);
		CheckErr(err);

		err = AMSP_Lock_Waiting_Room(ams);
		CheckErr(err);

		err = AMSP_Register_Guest(ams, req, memid, &my_appt, 1);
		CheckErr(err);

		err = AMSP_UnLock_Waiting_Room(ams);
		CheckErr(err);

		err = AMSP_Who_Is_Next_Host_AMS_Comm(ams, &next_host);
		CheckErr(err);

		if (AMSP_mem_net_open(next_host->hostname, AMS_SERVICE,
							next_host->port, &sfd, ams_msg) != AMS_ERROR_NONE)
			AMSP_err_ret(ams_msg, "Error connecting to %s\n", next_host->hostname);

		err = AMSP_send_rq(OP_NEGOTIATE_STEP, sfd, ams, req, &memid, step);
		if (err == AMS_ERROR_CONNECT_TERMINATED) {
			/* None fatal error. Release resources */
			err = AMSP_Worker_grant_read_access(memid);
			CheckErr(err);
			return AMS_ERROR_CONNECT_TERMINATED;
		}

		err1 = AMSP_fsm_loop(OP_NEGOTIATE_STEP, sfd, &err);
		if (err1 == AMS_ERROR_CONNECT_TERMINATED) {
			/* None fatal error. Release resources */
			err = AMSP_Worker_grant_read_access(memid);
			CheckErr(err);
			return AMS_ERROR_CONNECT_TERMINATED;
		}

		err = AMSP_mem_net_close(sfd);
		CheckErr(err);

	} else { /* Use MPI to determine the maximum step */
		
		/* 
		 * Even though we'll be writing the step later, we're not writing
		 * to any memory fields, and we don't want to increment the current
		 * step! 
		 */
		err = AMSP_Worker_take_read_access(memid);
		CheckErr(err);

		err = AMSP_GetStep_AMS_Memory(&memid, nbmem, step);
		CheckErr(err);

		err = AMSP_Get_MPI_Comm_AMS_Comm(ams, &mpi_comm);
		CheckErr(err);

		err = AMSP_Get_Max_Step_AMS_MPI(step, mpi_comm);
		CheckErr(err);

		/* Set an appointment */
		err = AMSP_Lock_Waiting_Room(ams);
		CheckErr(err);

		err = AMSP_Register_Guest(ams, req, memid, &my_appt, 1);
		CheckErr(err);

		err = AMSP_Assign_Appt(ams, my_appt, step);
		CheckErr(err);

		/*
		 * Signal to other threads that the appointment is set
		 */
		AMSP_CondBroadcast(&(my_appt->cond));

		err = AMSP_UnLock_Waiting_Room(ams);
		CheckErr(err);

	    err = AMSP_Worker_grant_read_access(memid);
		CheckErr(err);
	}

    return AMS_ERROR_NONE;
}

int AMSP_Continue_Step_Negotiation(AMS_Comm ams, RequestID req, AMS_Memory memid,
                              unsigned char *step)
{
    int err, err1, sfd, am_first, newer_step, nbmem = 1;
    HOST_NODE *next_host;
    Appointment *my_appt;

    /* If we're first, we're done comparing and can start notifying */
    /* communicators of the step we determined */
    err = AMSP_Am_I_First_Host_AMS_Comm(ams, &am_first);
    CheckErr(err);
    if(am_first)
        return AMSP_Start_Step_Notification(ams, req, memid, step);

    err = AMSP_Worker_take_read_access(memid);
    CheckErr(err);

    err = AMSP_IsNewerStep_AMS_Memory(&memid, nbmem, step, &newer_step);
    CheckErr(err);

    if(newer_step) {
        err = AMSP_GetStep_AMS_Memory(&memid, nbmem, step);
        CheckErr(err);
    }

    err = AMSP_Lock_Waiting_Room(ams);
    CheckErr(err);

    err = AMSP_Register_Guest(ams, req, memid, &my_appt, 1);
    CheckErr(err);

    err = AMSP_UnLock_Waiting_Room(ams);
    CheckErr(err);

    err = AMSP_Who_Is_Next_Host_AMS_Comm(ams, &next_host);
    CheckErr(err);

    if (AMSP_mem_net_open(next_host->hostname, AMS_SERVICE,
                        next_host->port, &sfd, ams_msg) != AMS_ERROR_NONE)
        AMSP_err_ret(ams_msg, "Error connecting to %s\n", next_host->hostname);

    err = AMSP_send_rq(OP_NEGOTIATE_STEP, sfd, ams, req, &memid, step);
	if (err == AMS_ERROR_CONNECT_TERMINATED) {
		/* None fatal error. Release resources */
		err = AMSP_Worker_grant_read_access(memid);
		CheckErr(err);
		return AMS_ERROR_CONNECT_TERMINATED;
	}

    err1 = AMSP_fsm_loop(OP_NEGOTIATE_STEP, sfd, &err);
	if (err1 == AMS_ERROR_CONNECT_TERMINATED) {
		/* None fatal error. Release resources */
		err = AMSP_Worker_grant_read_access(memid);
		CheckErr(err);
		return AMS_ERROR_CONNECT_TERMINATED;
	}

    AMSP_mem_net_close(sfd);

    return AMS_ERROR_NONE;
}

int AMSP_Start_Step_Notification(AMS_Comm ams, RequestID req, AMS_Memory memid,
                            unsigned char *step)
{
    Appointment *my_appt;
    HOST_NODE *next_host;
    int sfd, err, err1;

    /* Pass on the message first */
    err = AMSP_Who_Is_Next_Host_AMS_Comm(ams, &next_host);
    CheckErr(err);

    if(AMSP_mem_net_open(next_host->hostname, AMS_SERVICE,
                        next_host->port, &sfd, ams_msg) != AMS_ERROR_NONE)
        AMSP_err_ret(ams_msg, "Error connecting to %s\n", next_host->hostname);

    err = AMSP_send_rq(OP_NOTIFY_STEP, sfd, ams, req, &memid, step);
	if (err == AMS_ERROR_CONNECT_TERMINATED) {
		/* None fatal error. Release resources */
		err = AMSP_Worker_grant_read_access(memid);
		CheckErr(err);
		return AMS_ERROR_CONNECT_TERMINATED;
	}

    err1 = AMSP_fsm_loop(OP_NOTIFY_STEP, sfd, &err);
	if (err1 == AMS_ERROR_CONNECT_TERMINATED) {
		/* None fatal error. Release resources */
		err = AMSP_Worker_grant_read_access(memid);
		CheckErr(err);
		return AMS_ERROR_CONNECT_TERMINATED;
	}

    AMSP_mem_net_close(sfd);

    /* Set an appointment */
    err = AMSP_Lock_Waiting_Room(ams);
    CheckErr(err);

    err = AMSP_Register_Guest(ams, req, memid, &my_appt, 1);
    CheckErr(err);

    /* 
	 * Memory is already locked for us; will be unlocked after
     * we set the appointment time here 
	 */
    err = AMSP_Assign_Appt(ams, my_appt, step);
    CheckErr(err);

	/*
	 * Signal to other threads that the appointment is set
	 */
	AMSP_CondBroadcast(&(my_appt->cond));

    err = AMSP_UnLock_Waiting_Room(ams);
    CheckErr(err);

    err = AMSP_Worker_grant_read_access(memid);
    CheckErr(err);

    return AMS_ERROR_NONE;
}

int AMSP_Continue_Step_Notification(AMS_Comm ams, RequestID req, AMS_Memory memid,
                               unsigned char *step)
{
    Appointment *my_appt;
    HOST_NODE *next_host;
    int err, err1, sfd, am_first;

    /* If we're first, we're done */
    err = AMSP_Am_I_First_Host_AMS_Comm(ams, &am_first);
    CheckErr(err);
    if(am_first)
        return AMS_ERROR_NONE;

    err = AMSP_Who_Is_Next_Host_AMS_Comm(ams, &next_host);
    CheckErr(err);

    /* Pass on the message first */
    if (AMSP_mem_net_open(next_host->hostname, AMS_SERVICE,
                        next_host->port, &sfd, ams_msg) != AMS_ERROR_NONE)
        AMSP_err_ret(ams_msg, "Error connecting to %s\n", next_host->hostname);

    err = AMSP_send_rq(OP_NOTIFY_STEP, sfd, ams, req, &memid, step);
	if (err == AMS_ERROR_CONNECT_TERMINATED) {
		/* None fatal error. Release resources */
		err = AMSP_Worker_grant_read_access(memid);
		CheckErr(err);
		return AMS_ERROR_CONNECT_TERMINATED;
	}

    err1 = AMSP_fsm_loop(OP_NOTIFY_STEP, sfd, &err);
	if (err1 == AMS_ERROR_CONNECT_TERMINATED) {
		/* None fatal error. Release resources */
		err = AMSP_Worker_grant_read_access(memid);
		CheckErr(err);
		return AMS_ERROR_CONNECT_TERMINATED;
	}

    AMSP_mem_net_close(sfd);

    /* Set an appointment */
    err = AMSP_Lock_Waiting_Room(ams);
    CheckErr(err);

    err = AMSP_Register_Guest(ams, req, memid, &my_appt, 1);
    CheckErr(err);

    /* 
	 * Memory is already locked for us; will be unlocked after
     * we set the appointment time here 
	 */
    err = AMSP_Assign_Appt(ams, my_appt, step);
    CheckErr(err);

	/*
	 * Signal to other threads that the appointment is set
	 */
	AMSP_CondBroadcast(&(my_appt->cond));

    err = AMSP_UnLock_Waiting_Room(ams);
    CheckErr(err);

    err = AMSP_Worker_grant_read_access(memid);
    CheckErr(err);

    return AMS_ERROR_NONE;
}


