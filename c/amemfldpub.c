/* not-accessor */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ams.h"
#include "amemfld.h"
#include "netlib.h"


/*@ 
	AMSP_Set_Setter_Func_C - define a setter to be used to change buffer value.

        The SetterFunc can change the address of the buffer if it frees
        the old buffer memory.

        Input Parameters:
+			fld - field whose setter function is to be defined
+			SetterFunc - pointer to function.

@*/

int AMSP_Set_Setter_Func_C(Field *fld, AMS_C_Setter *SetterFunc)
{
    fld->lang = AMS_C;
    fld->CSetValue = SetterFunc;
    fld->CPPSetValue = NULL;
    fld->JAVASetValue = NULL;
    fld->Obj = NULL;
    fld->Env = NULL;
    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Set_Setter_Func_CPP - define a setter to be used to change buffer value.

        The SetterFunc can change the address of the buffer if it frees
        the old buffer memory.

        Input Parameters:
+			fld - field whose setter function is to be defined
.			SetterFunc - pointer to function.
+			MyObj - Object to be given to setter function call

@*/

int AMSP_Set_Setter_Func_CPP(Field *fld, AMS_CPP_Setter *SetterFunc, void *MyObj)
{
    fld->lang = AMS_CPP;
    fld->CSetValue = NULL;
    fld->CPPSetValue = SetterFunc;
    fld->JAVASetValue = NULL;
    fld->Obj = MyObj;
    fld->Env = NULL;
    return AMS_ERROR_NONE;
}

/*@ 
	AMSP_Set_Setter_Func_JAVA - define a setter to be used to change buffer value.

        The SetterFunc can change the address of the buffer if it frees
        the old buffer memory.

        Input Parameters:
+			fld - field whose setter function is to be defined
.			SetterFunc - pointer to function.
.			MyEnv - Environment to be given to setter function call
+			MyObj - Object to be given to setter function call

@*/

int AMSP_Set_Setter_Func_JAVA(Field *fld, AMS_JAVA_Setter *SetterFunc,
                         void *MyEnv, void *MyObj)
{
    fld->lang = AMS_JAVA;
    fld->CSetValue = NULL;
    fld->CPPSetValue = NULL;
    fld->JAVASetValue = SetterFunc;
    fld->Obj = MyObj;
    fld->Env = MyEnv;
    return AMS_ERROR_NONE;
}

