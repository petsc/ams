/*
 * Error handling routines.
 *
 * The functions in this file are independent of any application
 * variables, and may be used with any C program. The calling program
 * must set the pointers to its abort, exit, and print functions, otherwise
 * these error routines call the default abort, exit, and printf functions.
 */

#include        <stdio.h>
#include        <stdlib.h>
#include        <stdarg.h>
#include        <errno.h>
#include        <string.h>

#include        "netlib.h"
#include        "systype.h"

#ifdef  BSD
/*
 * Under BSD, these publisher routines use the syslog(3) facility.
 * They don't append a newline, for example.
 */

#include        <syslog.h>

#else   /* not BSD */
/*
 * There really ought to be a better way to handle publisher logging
 * under System V.
 */

#define syslog(a,b)     fprintf(stderr, "%s\n", (b))
#define openlog(a,b,c)  fprintf(stderr, "%s\n", (a))

#endif  /* BSD */

/* Bu default, we assume the calling program is a accessor type */
/* This means errors are output to the stderr */

/* 
 * This global variable is thread-safe.
 * All threads within a process are accessors
 * or publishers. But not both.
 */
int mydaemon = 0;

/*
 * Define our default exit, abort, and print functions
 */

PTF_void_int func_exit = exit;
PTF_void func_abort = abort;
PTF_print func_print = printf;
int amsp_print_func = 0;


#ifndef _WIN_ALICE

#ifndef NULL
#define NULL    ((void *) 0)
#endif

#endif
char *pname = NULL;



/*
 * Fatal error.  Print a message and terminate.
 * Don't dump core and don't print the system's errno value.
 *
 *  err_quit(str, arg1, arg2, ...)
 *
 * The string "str" must specify the conversion specification for any args.
 */

/*VARARGS1*/
void    AMSP_err_quit(const char *str, ...) 
{
    char buff[255];

    va_list args;
    va_start(args, str);
  
    vsprintf(buff, str, args);

    if (!mydaemon) {
        if (pname != NULL)
            func_print("%s: ", pname);
        func_print("%s\n", buff); 
    } else {
        syslog(LOG_ERR, buff);
    } 
    va_end(args);

    func_abort(1);
}

/*
 * Fatal error related to a system call.  Print a message and terminate.
 * Don't dump core, but do print the system's errno value and its
 * associated message.
 *
 *  AMSP_err_sys(str, arg1, arg2, ...)
 *
 * The string "str" must specify the conversion specification for any args.
 */

/*VARARGS1*/
void AMSP_err_sys(const char *str, ...)
{
    char buff[255];
    va_list args;
    va_start(args, str);
  
    vsprintf(buff, str, args);

    if (!mydaemon) {
        if (pname != NULL)
            func_print("%s: ", pname);
        func_print("%s\n", buff); 
        AMSP_perror(buff);
    } else {
        AMSP_perror(buff);
        syslog(LOG_ERR, buff); 
    }
  
    va_end(args);

    func_exit(1);

}

/*
 * Recoverable error. Return the error message to the caller.
 * The string "str" must specify the conversion specification for any args.
 */

/*VARARGS1*/
void AMSP_err_ret(char *msg, const char *str, ...)
{
    char buff[255] = {0};
    va_list args;
    va_start(args, str);

    vsprintf(buff, str, args);
    va_end(args);

    /* Return low level explanation */
    strcpy(msg, buff);

    return;
}

/*
 * Fatal error.  Print a message, dump core (for debugging) and terminate.
 *
 *  err_dump(str, arg1, arg2, ...)
 *
 * The string "str" must specify the conversion specification for any args.
 */

/*VARARGS1*/
void   AMSP_err_dump(const char *str, ...)
{
    char buff[255];
    va_list args;
    va_start(args, str);
  
    vsprintf(buff, str, args);

    if (!mydaemon) {
        if (pname != NULL)
            func_print("%s: ", pname);
        func_print("%s\n", buff); 
        AMSP_perror(buff);
    } else {
        AMSP_perror(buff);
        syslog(LOG_ERR, buff);
    }
  
    va_end(args);

    func_abort();    /* dump core and terminate */
}

/*
 * Print more details on the error string.
 */

void AMSP_perror(char *buff)
{
    int len;
    char loc_buf[255];
    char  *AMSP_sys_err_str(char *);

    if (!mydaemon) {
        if (pname != NULL)
            func_print("%s: ", pname);
        func_print("%s\n", buff); 
    } else {
        len = strlen(buff);
        sprintf(buff + len, " %s", AMSP_sys_err_str(loc_buf));
    }
  
}

/*
 * LOG_PID is an option that says prepend each message with our pid.
 * LOG_CONS is an option that says write to console if unable to send
 * the message to syslogd.
 * LOG_DAEMON is our facility.
 */

/* This function must be Called by a publisher program to Initialize the
 * error routines functions.
 */

void AMSP_err_init(const char    *ident)
{
    mydaemon = 1;
    openlog(ident, (LOG_PID | LOG_CONS), LOG_DAEMON);
}


/*@ 
    err_init_exit - This function is called to initialize the exit function that
    the user want to be called. By default, the C function exit() is called 
    by the low-level error routines.

    Input Parameters:
    my_exit - Pointer to your exit function
 @*/

void AMSP_err_init_exit(PTF_void_int my_exit)
{
    if (my_exit != NULL)
        func_exit = my_exit;
}

/*@ 
    err_init_abort - This function is called to initialize the abort function that
    the user wants to be called. By default, the C function abort() is called 
    by the low-level error routines.

    Input Parameters:
    my_abort - Pointer to your abort function
 @*/

void AMSP_err_init_abort(PTF_void my_abort)
{
    if (my_abort != NULL)
        func_abort = my_abort;
}

/*@ 
    err_init_print - This function is called to initialize the print function that
    the user wants to be called. By default, the C function printf() is called 
    by the low-level error routines.

    Input Parameters:
    my_print - Pointer to your print function
 @*/

void AMSP_err_init_print(PTF_print my_print)
{
    if (my_print != NULL) {
        func_print = my_print;
        amsp_print_func = 1;
    }
}

#ifdef BSD
/* I beleive these global vars are ThreadSafe. */
extern int  errno;    /* Unix error number */
extern const int  sys_nerr; /* # of error message strings in sys table */
extern const char *const sys_errlist[]; /* the system error message table */
#else
#include <errno.h>
#endif

/*
 * Return a string containing some additional operating-system
 * dependent information.
 * Note that different versions of UNIX assign different meanings
 * to the same value of "errno" (compare errno's starting with 35
 * between System V and BSD, for example).  This means that if an error
 * condition is being sent to another UNIX system, we must interpret
 * the errno value on the system that generated the error, and not
 * just send the decimal value of errno to the other system.
 */

char * AMSP_sys_err_str(char *Msg)
{
  
    if (errno != 0) {
        #ifdef BSD
        if (errno > 0 && errno < sys_nerr)
            sprintf(Msg, "(%s)", sys_errlist[errno]);
        else
            sprintf(Msg, "(errno = %d)", errno);
        #else
        sprintf(Msg, "(%s)", strerror(errno));
        #endif
    } else {
        Msg[0] = '\0';
    }

    return(Msg);
}
