#include "network.h"
#include "amsdefs.h"

/*
 * Define functions to load and store 2-byte/4-byte integers.
 * These functions handle the conversion between host format
 * and network byte ordering.
 */

short AMSP_ldshort(char *addr)
{ 
    short s;
    memcpy((char *)&s, addr, sizeof(short));
    return ntohs (s);
}

void AMSP_stshort(short sval, char *addr)
{
    short s = ntohs(sval);
    memcpy(addr, (char *)&s, sizeof(short));
}

int AMSP_ldlong(char *addr) 
{
    int l;
    memcpy((char *)&l, addr, sizeof(int));
    return ntohl (l);
}

void AMSP_stlong(int lval,char *addr)
{
    int l = ntohl(lval);
    memcpy(addr, (char *)&l, sizeof(int));
}


/* 
 * Special case for loading and storing floats.
 * We make the assumption that if ntohl(l) == l, that
 * there should be no twiddling going on.
 */
float AMSP_twiddle_float(float d)
{
    int l = 123456; /* Is this scientific ?*/
    float tmp = d;
    char *p, q;

    if (l != (int) ntohl(l)) {
        /* twiddle the byte ordering */
        p = (char *)&tmp;

        q = p[0];
        p[0] = p[3];
        p[3] = q;

        q = p[1];
        p[1] = p[2];
        p[2] = q;
    }
    
    /* Were believe we are done */
    return tmp;
}

float AMSP_ldfloat(char *addr) 
{
    float d;
    memcpy((char *)&d, addr, sizeof(float));
    return AMSP_twiddle_float (d);
}

void AMSP_stfloat(float dval,char *addr)
{
    float d = AMSP_twiddle_float(dval);
    memcpy(addr, (char *)&d, sizeof(float));
}

/* 
 * Special case for loading and storing doubles.
 * We make the assumption that if ntohl(l) == l, that
 * there should be no twiddling going on.
 */
double AMSP_twiddle_double(double d)
{
    int l = 123456; /* Is this scientific ?*/
    double tmp = d;
    char *p, q;

    if (l != (int) ntohl(l)) {
        /* twiddle the byte ordering */
        p = (char *)&tmp;

        q = p[0];
        p[0] = p[7];
        p[7] = q;

        q = p[1];
        p[1] = p[6];
        p[6] = q;

        q = p[2];
        p[2] = p[5];
        p[5] = q;

        q = p[3];
        p[3] = p[4];
        p[4] = q;
    }

    /* Were believe we are done */
    return tmp;
}
double AMSP_lddouble(char *addr) 
{
    double d;
    memcpy((char *)&d, addr, sizeof(double));
    return AMSP_twiddle_double (d);
}

void AMSP_stdouble(double dval,char *addr)
{
    double d = AMSP_twiddle_double(dval);
    memcpy(addr, (char *)&d, sizeof(double));
}
