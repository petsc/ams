/* not-accessor */
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ams.h"
#include "acomm.h"
#include "amem.h"
#include "convert.h"
#include "amisc.h"
#include "amsdefs.h"
#include "netlib.h"
#include "amemfld.h"

#include "amsmpi.h"

/*
 * Given a communicator, this function returns the port used on localhost
 */
int AMSP_Get_Comm_local_port(AMS_Comm ams, int *port)
{
    COMM_STRUCT *com_str;
    int err = AMSP_Get_Comm_Tbl(ams, &com_str);

    CheckErr(err);
    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    AMSP_LockMutexRead(&(com_str->pub_arg->pub_mutex));
    *port = com_str->pub_arg->port;
    AMSP_RelMutexRead(&(com_str->pub_arg->pub_mutex));

    return AMS_ERROR_NONE;
}

/*
 * Set the communicator's local port from pub_arg
 */
int AMSP_Set_Comm_local_port(AMS_Comm ams)
{
    COMM_STRUCT *com_str;
    HOST_NODE *host_node;
    char myname[MAXHOSTLEN+1], hosterr[255];
    int hostnum;
    int k, err = AMSP_Get_Comm_Tbl(ams, &com_str);
 
    CheckErr(err);
    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    if(com_str->ctype == MPI_TYPE) {
        AMSP_Get_rank_AMS_MPI(&hostnum, com_str->mpi_comm);
        host_node = com_str->host_node;
        k = 0;
        while(host_node) {
            if(k == hostnum) {
                AMSP_LockMutexRead(&(com_str->pub_arg->pub_mutex));
                host_node->port = com_str->pub_arg->port;
                AMSP_RelMutexRead(&(com_str->pub_arg->pub_mutex));
            }
            ++k;
            host_node = host_node->next;
        }
    }
    else {
      if ((err = gethostname(myname, MAXHOSTLEN)))
            AMSP_err_quit("gethostname: %s", AMSP_host_err_str(hosterr, &err));

        err = AMSP_Find_Host_Comm_Struct(com_str, myname, &host_node);
        CheckErr(err);

        if(host_node == NULL)
            return AMS_ERROR_HOST_NODE_NOT_IN_COMMUNICATOR;

        AMSP_LockMutexRead(&(com_str->pub_arg->pub_mutex));
        host_node->port = com_str->pub_arg->port;
        AMSP_RelMutexRead(&(com_str->pub_arg->pub_mutex));
    }
 
    return AMS_ERROR_NONE;
}

/* Get a communicator's port for any host */
int AMSP_Get_Comm_port(AMS_Comm ams, int hostnum, int *port)
{
    COMM_STRUCT *com_str;
    HOST_NODE *host_node;
    int k, err = AMSP_Get_Comm_Tbl(ams, &com_str);
 
    CheckErr(err);
    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);

    host_node = com_str->host_node;
    *port = -1;
    k = 0;
    while(host_node) {
        if(k == hostnum)
            *port = host_node->port;
        ++k;
        host_node = host_node->next;
    }
 
    return AMS_ERROR_NONE;
}       
 
int AMSP_Set_Comm_port(AMS_Comm ams, int hostnum, int port)
{
    COMM_STRUCT *com_str;
    HOST_NODE *host_node;
    int k, err = AMSP_Get_Comm_Tbl(ams, &com_str);
 
    CheckErr(err);
    err = Validate(AMSP_Valid_Comm_Struct, com_str);
    CheckErr(err);
 
    host_node = com_str->host_node;
    k = 0;
    while(host_node) {
        if(k == hostnum)
            host_node->port = port;
        ++k;
        host_node = host_node->next;
    }
 
    return AMS_ERROR_NONE;
}       

/*
 * Lock the memory so that the main thread will block
 */

int AMSP_Lock_memory(AMS_Memory *memid, int nbmem, int timeout, RequestID req)
{
    int err, err1;

    Appointment **my_appt;
    int flag, i;

	/*
	 * take a synchronized (among other processors) read access
	 */
    err = AMSP_Take_synchronized_write_access(memid, nbmem, req, &my_appt);
    CheckErr(err);

	/* 
	 * Verify that the memory is published 
	 */
    err = AMSP_IsPublished_AMS_Memory(memid, nbmem, AMS_RELEASE_LOCK, &flag);

	if (err != AMS_ERROR_NONE) {
		err1 = AMSP_Finish_synchronized_write_access(memid, nbmem, my_appt);
		CheckErr(err1);
		return err;
	}
	if (!flag) {
		err1 = AMSP_Finish_synchronized_write_access(memid, nbmem, my_appt);
		CheckErr(err1);
		return AMS_ERROR_MEMORY_NO_LONGER_PUBLISHED;
	}

	/*
     * Note that at this moment all memories are synchronized and locked,
     * thus, the following function should not take a lock.
	 */
    for (i = 0; i<nbmem; i++) {
	    err = AMSP_Main_thread_block_AMS_Memory(memid[i], timeout);
        CheckErr(err);
    }

    err1 = AMSP_Finish_synchronized_write_access(memid, nbmem, my_appt);
    CheckErr(err1);

    return err;
}

/*
 * Unlock the memory so that the main thread will resume
 */
int AMSP_Unlock_memory(AMS_Memory *memid, int nbmem)
{

    int err, i;

	/*
	 * Unblock each of the memories from main thread only.
     * Note that we do not need to synchronize with other processors
	 */
    for (i = 0; i<nbmem; i++) {
	    err = AMSP_Main_thread_unblock_AMS_Memory(memid[i]);
        CheckErr(err);
    }

    return AMS_ERROR_NONE;
}





