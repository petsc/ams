/* not-accessor */
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "amsmpi.h"
#include "acomm.h"
#include "netlib.h"


/*
 * Construct the list of hostnames associated with this MPI communicator
 */
int AMSP_Construct_Host_list_AMS_MPI(char ***host_list, MPI_Comm mpi_comm)
{
    int nprocs, *namelengths, mylength, i, *displacements, totallength, err;
    char myname[MAXHOSTLEN+1], hosterr[255];

    MPI_Comm_size(mpi_comm, &nprocs);

    *host_list = (char **) malloc((nprocs+1) * sizeof(char *));
    if(*host_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;
    (*host_list)[nprocs] = NULL;

    namelengths = (int *) malloc(nprocs * sizeof(int));
    if(namelengths == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    displacements = (int *) malloc(nprocs * sizeof(int));
    if(displacements == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;


    /* First, get our own hostname and length */

    if(gethostname(myname, MAXHOSTLEN))
        AMSP_err_quit("gethostname: %s", AMSP_host_err_str(hosterr, &err));
    mylength = strlen(myname) + 1;


    /* Now compile the list of hostname lengths */

    err = MPI_Allgather(&mylength, 1, MPI_INT, namelengths, 1, MPI_INT,
                        mpi_comm);
    if(err)
        AMSP_err_quit("MPI_Allgather returned code %d", err);


    /* Allocate space, compute displacements for host strings */

    displacements[0] = 0;

    for(i = 1; i < nprocs; ++i)
        displacements[i] = displacements[i-1] + namelengths[i-1];

    totallength = displacements[nprocs-1] + namelengths[nprocs-1];
    **host_list = (char *) malloc(totallength * sizeof(char));
    if(**host_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    for(i = 1; i < nprocs; ++i)
        (*host_list)[i] = (*host_list)[i-1] + namelengths[i-1];

    /* Finally, fill in the host names! */
    
    err = MPI_Allgatherv(myname, mylength, MPI_CHAR,
                         **host_list, namelengths, displacements, MPI_CHAR,
                         mpi_comm);
    if(err)
        AMSP_err_quit("MPI_Allgatherv returned code %d", err);

    return AMS_ERROR_NONE;
}

/*
 * Given a memory id, this function returns the maximum step
 * among the different processors that comprise the MPI communicator
 */
int AMSP_Get_Max_Step_AMS_MPI(unsigned char *step, MPI_Comm mpi_comm)
{
    int nprocs, i, err;
    unsigned char *allsteps;

    err = MPI_Comm_size(mpi_comm, &nprocs);
	if (err)
		AMSP_err_quit(" MPI Error %d", err);

    allsteps = (unsigned char *) malloc(nprocs * STEP_BYTES * sizeof(unsigned char));
    if(allsteps == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    /* Finally, fill in the steps from all the other processors */
    err = MPI_Allgather((void *) step, STEP_BYTES, MPI_UNSIGNED_CHAR,
                         (void *) allsteps, STEP_BYTES, MPI_UNSIGNED_CHAR, mpi_comm);
    if(err)
        AMSP_err_quit("MPI_Allgather returned code %d", err);
	
	/*
	 * Figure out the maximum step
	 */
	for (i=0; i<nprocs; i++) {
		if (!AMSP_StepGreaterThan(step, allsteps + i*STEP_BYTES))
			memcpy(step, allsteps + i*STEP_BYTES, STEP_BYTES);
	}
	free(allsteps);

    return AMS_ERROR_NONE;
}

/*
 * Destroy the host list
 */
int AMSP_Destroy_Host_list_AMS_MPI(char **host_list)
{
    free(*host_list);
    free(host_list);

    return AMS_ERROR_NONE;
}

int AMSP_Guess_start_index_AMS_MPI(int len, int *start, MPI_Comm mpi_comm)
{
    int nprocs, rank, k, err, *locallengths;

    MPI_Comm_size(mpi_comm, &nprocs);
    MPI_Comm_rank(mpi_comm, &rank);

    locallengths = (int *) malloc(nprocs * sizeof(int));
    if(locallengths == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;
    
    err = MPI_Allgather(&len, 1, MPI_INT, locallengths, 1, MPI_INT,
                        mpi_comm);
    if(err)
        AMSP_err_quit("MPI_Allgather returned code %d", err);

    *start = 0;
    for(k = 0; k < rank; ++k)
        *start += locallengths[k];
 
    free(locallengths);

    return AMS_ERROR_NONE;
}

/*
 * Tell everyone else in the MPI_Comm what is my publisher's port number
 */
int AMSP_Construct_Pub_ports_AMS_MPI(int myport, int tell_ports,
                                char **host_list, int **port_list, MPI_Comm mpi_comm)
{
    int nprocs, rank, k, err, proc_to_suggest = -1;

    MPI_Comm_size(mpi_comm, &nprocs);
    MPI_Comm_rank(mpi_comm, &rank);

    *port_list = (int *) malloc(nprocs * sizeof(int));
    if(*port_list == NULL)
        return AMS_ERROR_INSUFFICIENT_MEMORY;

    err = MPI_Allgather(&myport, 1, MPI_INT, *port_list, 1, MPI_INT,
                        mpi_comm);
    if(err)
        AMSP_err_quit("MPI_Allgather returned code %d", err);

    if(!tell_ports || rank > 0)
        return AMS_ERROR_NONE;

    /* Processor 0 must tell us a publisher host and port to use */
    for(k = 0; k < nprocs; ++k) {
        if(strlen(host_list[k]) == strlen(host_list[0])
           && strcmp(host_list[k], host_list[0]) == 0
           && (*port_list)[k] == DEFAULT_PORT)

            proc_to_suggest = k;
    }
    if(proc_to_suggest < 0)
        proc_to_suggest = 0;

    AMS_Print("Publisher: Host %s, port %d\n",
              host_list[proc_to_suggest], (*port_list)[proc_to_suggest]);

    return AMS_ERROR_NONE;
}

int AMSP_Get_rank_AMS_MPI(int *rank, MPI_Comm mpi_comm)
{
    MPI_Comm_rank(mpi_comm, rank);
    return AMS_ERROR_NONE;
}
